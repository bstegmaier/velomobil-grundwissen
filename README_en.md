Velomobile Knowledge Base
=========================

Description
-----------

This is a FAQ about Velomobiles, derived from the discussions in the [Velomobile forum](https://velomobilforum.de/). Cooperation desired!

*Deutsche Version: [siehe hier](README.md)*

**Read velomobile knowledge base**:

* **HTML:** [English](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.html); [German](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.html)
* **PDF:** [English](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.pdf); [German](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.pdf)
* **PDF print version:** [English](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge_print.pdf); [German](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen_print.pdf)
* **EPUB:** [English](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.epub); [German](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.epub)
* **EPUB (raster images):** [English](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge_raster.epub); [German](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen_raster.epub)
* **MOBI:** [English](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.mobi); [German](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.mobi)
* **Plain text:** [English](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.txt); [German](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.txt)

**Model comparison:** [VM slider](https://cmoder.gitlab.io/velomobil-grundwissen/vm-slider/vm-slider.html)

Changelog
---------
* Version 2.1: many new diagrams, more data in VM slider
* Version 2.0: English language version, VM slider, newest Sphinx version
* Version 1.3: first graphics, EPUB with CSS, chapters sorted and divided
* Version 1.2: EPUB repaired and keyword index added
* Version 1.1: Glossary and index added
* Version 1.0: content largely complete
* Version 0.9: Versioning works, PDF generation works
* Version 0.5: first commits

Compile from the source code yourself
-------------------------------------
This project requires [Sphinx](http://www.sphinx-doc.org/), if this
is installed, it should be sufficient to use *make* to produce output formats:

```sh
 $ make html
 $ make latexpdf
 $ make epub
 $ make text
```

=> The result is then in the subdirectory *_build*.

Since version 1.3 other language versions exist; thus, the language must
be given in an environment variable:

```sh
 $ make -e LANG=en latexpdf
```

For full functionality, some additional software is required, e.g.
*LaTex* (w.g. *TeX Live*), *Python 3*, *Matplotlib*, *librsvg2* and the Sphinx
extensions *alabaster* und *svg2pdfconverter*. In addition, some post-processing
is done on *Gitlab*; see *.gitlab-ci.yml*.

Cooperation
-----------

* via pull request (requires Gitlab account)
* in the Velomobil forum [in this thread](https://www.velomobilforum.de/forum/index.php?threads/faq-velomobil-grundwissen.57837/) (requires an account there)
* by email to the authors

Contributors and License
------------------------

See text, section [*Contributors and Sources*](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.html#contributors-and-sources).

Unless otherwise noted, everything is under the license [*Creative Commons CC-BY*](https://creativecommons.org/licenses/by/4.0/deed.en) – in short: Anyone can use the text as desired, but must keep the names of the authors/translators.
