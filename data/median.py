#!/usr/bin/python3

import numpy as np
import re

# read data
script_dir = os.path.dirname(__file__)
data = np.genfromtxt(os.path.join(script_dir, 'rijderslist_velomobiel_intercitybike_2020-02-29.csv'), delimiter = "\t", dtype=None, encoding='utf8')

# extract those values from column #7 where column #5 (= total km) is greater than 7500
data = [x for x in data if x[5] > 7500]
median = {}
for r in [r'Quest', r'DF', r'Strada', r'QuattroVelo', r'Mango']:
    median[r] = np.median([x[7] for x in data if re.search(r, x[2])])
for k,v in sorted(median.items(), key=lambda item: item[1]):
    print("* {}: {:.0f} km".format(k, v))
