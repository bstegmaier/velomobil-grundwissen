#!/usr/bin/python3

import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np
import re
import os
import math
import json
import adjustText


# helper functions

# air resistance
def P_air(v, cWA = 0.05, ele = 200, T = 20):
    return (v / 3.6)**3 * (176.5 * math.exp(-ele * .0001253) * cWA / (273 + T))

# rolling resistance
def P_roll(v, m = 100, cR = 0.005, cRdyn = 0.1):
    return (v / 3.6) * ((9.81 * m * cR) + (v / 3.6 * cRdyn))

# uphill
def P_up(v, percent, m = 100):
    return (v / 3.6) * 9.81 * m * percent / 100

# kinetic energy
def E_kin(v, m = 100):
    return 1/2 * m * (v / 3.6)**2

# compute speed from kinetic energy
def v_from_E_kin(E, m = 100):
    return math.sqrt(2 * E / m) * 3.6

# compute equilibrium speed from given power; may not always converge
def get_vmax(P, cWA = 0.05, m = 100, cR = 0.005, cRdyn = 0.1, ele = 200, T = 20, percent = 0):
    v = 0
    P_comp = 0

    # only run until the difference is at most 1 W
    while abs(P - P_comp) > 1:
        P_comp = P_air(v, cWA, ele, T) + P_roll(v, m, cR, cRdyn) + P_up(v, percent, m)
        v += 0.05 if P_comp < P else -0.05

    return v



# settings
script_dir = os.path.dirname(__file__)
plt.axis('equal')
plt.autoscale(tight=True)



# SOURCE 1: rijderslist
data = np.genfromtxt(os.path.join(script_dir, 'rijderslist_velomobiel_intercitybike_2020-02-29.csv'), delimiter = "\t", dtype=None, encoding='utf8')

# DIAGRAM 1: countries
c = {}
for x in data:
    if x[12]:
        c[x[12]] = c.get(x[12], 0) + 1

# extract countries with less than 1.5%
threshold = 0.015 * data.size
vm = { k: v for k,v in c.items() if v >= threshold}
vm['andere'] = sum([ v for k,v in c.items() if v < threshold])
values = [ v for k,v in sorted(vm.items(), key=lambda item: item[1], reverse=True) ]
keys = [ k for k,v in sorted(vm.items(), key=lambda item: item[1], reverse=True) ]

# save German version
plt.clf()
plt.pie(values, labels=keys, autopct='%.1f%%', textprops={'size': 'smaller'})
plt.savefig('rijderslist_countries_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.clf()
plt.pie(values, labels=['others' if x == 'andere' else x for x in keys], autopct='%.1f%%', textprops={'size': 'smaller'})
plt.savefig('rijderslist_countries_en.svg', transparent=True, bbox_inches='tight')



# DIAGRAM 2: VM models

vm = {}
for x in data: m = re.search(r'^Quest|^DF|^Strada|^QuattroVelo|Mango', x[2]); m = m.group(0) if m else 'andere'; vm[m] = vm.get(m, 0) + 1
values = [ v for k,v in sorted(vm.items(), key=lambda item: item[1], reverse=True) ]
keys = [ k for k,v in sorted(vm.items(), key=lambda item: item[1], reverse=True) ]

# save German version
plt.clf()
plt.pie(values, labels=keys, autopct='%d%%')
plt.savefig('rijderslist_models_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.clf()
plt.pie(values, labels=['others' if x == 'andere' else x for x in keys], autopct='%.1f%%')
plt.savefig('rijderslist_models_en.svg', transparent=True, bbox_inches='tight')



# DIAGRAM 3: distance histogram

# extract those values from column #7 where column #5 (= total km) is greater than 7500
data = [x for x in data if x[5] > 7500]
vm_quest = [x[7] for x in data if re.search(r'^Quest.*', x[2])]
vm_df = [x[7] for x in data if re.search(r'^DF.*', x[2])]
vm_strada = [x[7] for x in data if re.search(r'^Strada.*', x[2])]
vm_qv = [x[7] for x in data if re.search(r'^QuattroVelo.*', x[2])]
vm_mango = [x[7] for x in data if re.search(r'Mango', x[2])]
vm_other = [x[7] for x in data if not re.search(r'Mango|^DF|^Quest|^QuattroVelo', x[2])]
median = np.median([x[7] for x in data])

plt.clf()
plt.hist([vm_quest, vm_df, vm_strada, vm_qv, vm_mango, vm_other], bins = 30, range = (0, 2000), stacked = True)
plt.xlim(0, 2000)

# save German version
plt.legend(['Quest', 'DF', 'Strada', 'QuattroVelo', 'Mango', 'andere'])
plt.xlabel('Strecke pro Monat [km]')
plt.ylabel('Anzahl')
plt.title('Monatliche Fahrleistung; Median: {:.0f} km'.format(median))
plt.savefig('rijderslist_dist-histogram_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.legend(['Quest', 'DF', 'Strada', 'QuattroVelo', 'Mango', 'others'])
plt.xlabel('Distance per month [km]')
plt.ylabel('Number')
plt.title('Monthly distance; median: {:.0f} km'.format(median))
plt.savefig('rijderslist_dist-histogram_en.svg', transparent=True, bbox_inches='tight')



# SOURCE 2: age survey in Velomobilforum
data = np.genfromtxt(os.path.join(script_dir, 'alter-hase-oder-junger-spund_2017.csv'), delimiter = "\t", dtype=None, skip_header=1, encoding=None)

# DIAGRAM 4: age histogram

median_age = np.median([x[0] for x in data])
avg_age = np.average([x[0] for x in data])
avg_vm = np.average([x[1] for x in data if not np.isnan(x[1])])

plt.clf()
plt.xlim(15, 80)
plt.subplots_adjust(top=0.85)
plt.hist([x[0] for x in data], bins = 30, range = (15, 80))

# save German version
plt.xlabel('Alter')
plt.ylabel('Anzahl')
plt.suptitle('Altersverteilung\nDurchschnitt: {:.1f}, Median: {:.1f}\nVM-Besitz seit durchschnittlich {:.1f} Jahren'.format(avg_age, median_age, avg_vm))
plt.savefig('age-histogram_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.xlabel('Age')
plt.ylabel('Number')
plt.suptitle('Age Distribution\naverage: {:.1f}, median: {:.1f}\nVM rider for {:.1f} years in average'.format(avg_age, median_age, avg_vm))
plt.savefig('age-histogram_en.svg', transparent=True, bbox_inches='tight')


# SOURCE 3: https://www.bicyclerollingresistance.com/road-bike-reviews
data = np.genfromtxt(os.path.join(script_dir, 'bicyclerollingresistance_2020-07-04.csv'), delimiter = "\t", dtype=None, skip_header=1, encoding=None)

# DIAGRAM 5: histogram of rolling resistance, with computed rolling resistance coefficient
# data[3]: type (TT, TL, TU)
# data[6]: measured width [mm]
# data[9-12]: power [W]
pressure = { 9: 8.3, 10: 6.9, 11: 5.5, 12: 4.1 }
tube = [x for x in data if x[3] == 'TT']
tubeless = [x for x in data if x[3] == 'TL']
tubular = [x for x in data if x[3] == 'TU']
m = 42.5
v = 0.77 * math.pi * 200/60
cRdyn = 0.1
# P = v * ((9.81 * m * cR) + (v * cRdyn))
# cR = (P/v - v * cRdyn) / (9.81 * m)

# print median rolling resistance for all pressures
for i in pressure.keys():
    print('{} bar: {} W'.format(pressure[i], np.median([x[i] for x in tube])))

plt.clf()
plt.hist([[x[i] for x in data] for i in reversed(list(pressure.keys()))], bins = 8, range = (9, 25), stacked = False)
xtick_locs, xtick_labels = plt.xticks()
plt.xticks(xtick_locs, ["{:.0f}\n({:.1f})".format(P, 1000 * (P/v - v * cRdyn) / (9.81 * m)) for P in xtick_locs])
plt.yticks([])

# save German version
plt.legend(['{} bar'.format(x) for x in reversed(list(pressure.values()))], loc='upper right', title='Reifendruck')
plt.xlabel('Rollwiderstand [W]\n(Rollwiderstandskoeffizient $c_R$ [‰])')
plt.ylabel('Anzahl')
plt.savefig('bicyclerollingresistance_hist_all_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.legend(['{} bar'.format(x) for x in reversed(list(pressure.values()))], loc='upper right', title='tire pressure')
plt.xlabel('rolling resistance [W]\n(rolling resistance coefficient $c_R$ [‰])')
plt.ylabel('number')
plt.savefig('bicyclerollingresistance_hist_all_en.svg', transparent=True, bbox_inches='tight')


# DIAGRAM 6: increase of rolling resistance with decreasing pressure (4.1 bar vs. 8.3 bar), in percent
plt.clf()
for t in ['TT', 'TL', 'TU']:
    P = [x[9] for x in data if x[3] == t]
    plt.scatter(P, [100 * x[12] / x[9] - 100 for x in data if x[3] == t])

# save German version
plt.legend(['mit Schlauch', 'Tubeless', 'Schlauchreifen'], loc='upper right')
plt.xlabel('Rollwiderstand bei $p = 8.3$ bar [W]')
plt.ylabel('Zunahme des Rollwiderstands bei $p = 4.1$ bar [%]')
plt.savefig('bicyclerollingresistance_p-inc-pc_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.legend(['tube', 'tubeless', 'tubular'], loc='upper right')
plt.xlabel('rolling resistance at $p = 8.3$ bar [W]')
plt.ylabel('increase of rolling resistance at $p = 4.1$ bar [%]')
plt.savefig('bicyclerollingresistance_p-inc-pc_en.svg', transparent=True, bbox_inches='tight')


# DIAGRAM 7: increase of rolling resistance with decreasing pressure (4.1 bar vs. 8.3 bar), absolute
plt.clf()
for t in ['TT', 'TL', 'TU']:
    P = [x[9] for x in data if x[3] == t]
    plt.scatter(P, [x[12] - x[9] for x in data if x[3] == t])

# save German version
plt.legend(['mit Schlauch', 'Tubeless', 'Schlauchreifen'], loc='upper left')
plt.xlabel('Rollwiderstand bei $p = 8.3$ bar [W]')
plt.ylabel('Zunahme des Rollwiderstands bei $p = 4.1$ bar [W]')
plt.savefig('bicyclerollingresistance_p-inc-abs_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.legend(['tube', 'tubeless', 'tubular'], loc='upper right')
plt.xlabel('rolling resistance at $p = 8.3$ bar [W]')
plt.ylabel('increase of rolling resistance at $p = 4.1$ bar [W]')
plt.savefig('bicyclerollingresistance_p-inc-abs_en.svg', transparent=True, bbox_inches='tight')



# SOURCE 4: JSON from vm-slider
filedata = open('../vm-slider/js/vm-data.js', 'r').read()

# it is not a JSON file, so remove everything before and after the first/last brace
data = json.loads(filedata[filedata.find('{'):filedata.rfind('}')+1])

# DIAGRAM 8: velomobile dimensions
plt.clf()

# take all velomobiles where length and width is specified, and write it as a dictionary with shortname as key
# => take only one from all models with the same shortname, and sort this dictionary according to length (= x[1][0], first of the values)
# => last one wins, i.e. if the versions are ordered by age, the newest (and probably best) version appears in the diagram
# => third value is weight; if it is not defined, set it to "None"
dimensions = dict(sorted({ data[k]['shortname']: [float(data[k]['length']), float(data[k]['width']), float(data[k]['weight']) if 'weight' in data[k] else None] for k in data if 'length' in data[k] and 'width' in data[k] }.items(), key=lambda x: x[1][0]))

# color map: from green to red
cdict = {'red'  :((0.0, 0.0, 0.0),   # no red at 0
                  (0.5, 0.9, 0.9),
                  (1.0, 0.9, 0.9)),  # set to 0.9 so its not too bright at 1
         'green':((0.0, 0.9, 0.9),   # set to 0.9 so its not too bright at 0
                  (0.5, 0.9, 0.9),
                  (1.0, 0.0, 0.0)),  # no green at 1
         'blue': ((0.0, 0.0, 0.0),   # no blue at 0
                  (1.0, 0.0, 0.0))   # no blue at 1
        }
colormap = colors.LinearSegmentedColormap('GnRd', cdict)

# make scatter plot; first with colors for entries with weight, and then in grey for entries without weight
plt.scatter([dimensions[k][0] for k in dimensions if dimensions[k][2]], [dimensions[k][1] for k in dimensions if dimensions[k][2]], c=[dimensions[k][2] for k in dimensions if dimensions[k][2]], cmap=colormap, vmin=20, vmax=40)
plt.scatter([dimensions[k][0] for k in dimensions if dimensions[k][2] == None], [dimensions[k][1] for k in dimensions if dimensions[k][2] == None], c='grey')

# annotate the plot: text = shortname, position = length and width;
# use plt.text() instead of plt.annotate(), so adjust_test() can remove overlaps
texts = []
for k in dimensions:
    texts.append(plt.text(dimensions[k][0], dimensions[k][1], k))

#plt.annotate(k, xy=(dimensions[k][0], dimensions[k][1]), ha='center', va='bottom', xytext=(dimensions[k][0], dimensions[k][1]+1))
adjustText.adjust_text(texts, expand_text=(1.4, 1.4), expand_points=(1.7, 1.7), arrowprops={'arrowstyle': '-'})

# save German version
plt.xlabel('Länge [cm]')
plt.ylabel('Breite [cm]')
plt.savefig('vm-dimensions_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.xlabel('length [cm]')
plt.ylabel('width [cm]')
plt.savefig('vm-dimensions_en.svg', transparent=True, bbox_inches='tight')



# SOURCE 5: GPS data from SPEZI velomobile tour 2019 (Ebersberg–Aalen)
data = np.genfromtxt(os.path.join(script_dir, '2019-04-26_Spezi_Muenchen-Aalen.csv'), delimiter = ',', dtype=None, skip_header=1, encoding='utf8')

# DIAGRAM 9: speed histogram, with ratio of drivetrain losses, rolling resistance and air drag

# make a histogram, but discard it and use only the values
hist_min = 0
hist_max = 70
bins = 35
histogramdata = plt.hist([x[3] * 3.6 for x in data], range = (hist_min, hist_max), bins = bins)

# compute power for all histogram bins, as a fraction of total power, multiplied by histogram value
# => compute rolling resistance and air drag as ratio of total (= including drivetrain), and add drivetrain to beginning of list as a ratio of new total (= drivetrain + old total)
drivetrain = 0.08
power = [ [(1 - 1 / (1 + drivetrain)) * histogramdata[0][i]] + list(map(lambda x: x * histogramdata[0][i] / ((1 + drivetrain) * (P_roll(v+1) + P_air(v+1))), [P_roll(v+1), P_air(v+1)])) for i, v in enumerate(histogramdata[1][:-1])]
power_sums = [sum([x[i] for x in power]) for i in [0,1,2]]
power_sums = [x / sum(power_sums) for x in power_sums]
print('Power in speed histogram: drivetrain {:.1f}%, rolling {:.1f}%, air {:.1f}%, median speed {:.1f} km/h'.format(100 * power_sums[0], 100 * power_sums[1], 100 * power_sums[2], np.median([x[3] * 3.6 for x in data])))

# plot stacked bar plot
plt.clf()
plt.bar([x + 1 for x in histogramdata[1][:-1]], [x[0] for x in power], bottom = 0, width = (hist_max - hist_min) / bins)
plt.bar([x + 1 for x in histogramdata[1][:-1]], [x[1] for x in power],  bottom = [x[0] for x in power], width = (hist_max - hist_min) / bins)
plt.bar([x + 1 for x in histogramdata[1][:-1]], [x[2] for x in power],  bottom = [x[0] + x[1] for x in power], width = (hist_max - hist_min) / bins)
plt.xlim(hist_min, hist_max)
plt.legend(['$P_\mathrm{chain}$', '$P_\mathrm{roll}$', '$P_\mathrm{air}$'], loc='upper left')

# save German version
plt.xlabel('Geschwindigkeit [km/h]')
plt.ylabel('Häufigkeit')
plt.yticks([])
plt.savefig('speed-histogram_spezi2019_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.xlabel('speed [km/h]')
plt.ylabel('frequency')
plt.yticks([])
plt.savefig('speed-histogram_spezi2019_en.svg', transparent=True, bbox_inches='tight')



# SOURCE 6: purely computed

# DIAGRAM 10: air drag coefficients for different bikes and car

# values: upright bike, racing bike, fast recumbent, velomobile, car
#cW = [1.2, 0.85, 0.75, 0.125, 0.3]
cWA = [0.72, 0.34, 0.225, 0.05, 0.6]
A   = [0.6, 0.4, 0.3, 0.4, 2]
cW  = [x1 / x2 for x1, x2 in zip(cWA, A)]
x = range(1, len(cWA) + 1)
barwidth = 0.25

plt.clf()
for rect in plt.bar([v - barwidth for v in x], cW, barwidth, label='$c_W$'):
    plt.annotate('{}'.format(rect.get_height()), xy=(rect.get_x() + rect.get_width() / 2, rect.get_height()), xytext=(0, 3), textcoords="offset points", ha='center', va='bottom', rotation=90)

for rect in plt.bar(x, A, barwidth, label='$A$'):
    plt.annotate('{} m$^2$'.format(rect.get_height()), xy=(rect.get_x() + rect.get_width() / 2, rect.get_height()), xytext=(0, 3), textcoords="offset points", ha='center', va='bottom', rotation=90)

for rect in plt.bar([v + barwidth for v in x], cWA, barwidth, label='$c_W \cdot A$'):
    plt.annotate('{} m$^2$'.format(rect.get_height()), xy=(rect.get_x() + rect.get_width() / 2, rect.get_height()), xytext=(0, 3), textcoords="offset points", ha='center', va='bottom', rotation=90)

plt.ylim(top=2.4)

# save German version
labels = ['Aufrechtrad', 'Rennrad', 'Liegerad', 'Velomobil', 'Auto']
plt.xticks(x, labels)
plt.legend()
plt.savefig('comparison_cW-A-cWA_de.svg', transparent=True, bbox_inches='tight')

# save English version
labels = ['upright', 'racing', 'recumbent', 'velomobile', 'car']
plt.xticks(x, labels)
plt.legend()
plt.savefig('comparison_cW-A-cWA_en.svg', transparent=True, bbox_inches='tight')



# DIAGRAM 11: speed vs. power

# resistances relative to speed, and compared to kinetic energy
x = range(0, 51, 1)
drivetrain = [0.08 * (P_roll(v) + P_air(v)) for v in x]
roll       = [P_roll(v) for v in x]
air        = [P_air(v) for v in x]

plt.clf()
plt.stackplot(x, drivetrain, roll, air)
plt.xlim(left=0, right=50)
plt.legend(['$P_\mathrm{chain}$', '$P_\mathrm{roll}$', '$P_\mathrm{air}$'], loc='upper left')

# save German version
plt.xlabel('Geschwindigkeit [km/h]')
plt.ylabel('Leistung [W]')
plt.savefig('power_comparison_stack_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.xlabel('speed [km/h]')
plt.ylabel('power [W]')
plt.savefig('power_comparison_stack_en.svg', transparent=True, bbox_inches='tight')



# DIAGRAM 12: compute time and distance until reaching 95% of end speed
cWA = cWA[:-1]  # use same cWA as before, but remove car
m_bike = [14, 7, 12, 25]    # weight [kg]: upright, racing, recumbent, VM
m_rider = 75
drivetrain = [2.5, 2.5, 4.3, 8]     # drivetrain losses in %
labels_de = ['Aufrechtrad', 'Rennrad', 'Liegerad', 'Velomobil']
labels_en = ['upright', 'racing', 'recumbent', 'velomobile']
P_muscle = 300   # available power [W]

plt.clf()

for i, cwa in reversed(list(enumerate(cWA))):
    x = []
    y = []
    v = 0
    t = 0
    d = 0
    m = m_rider + m_bike[i]
    P_avail = (1 - drivetrain[i] / 100) * P_muscle
    vmax = get_vmax(P_avail, cwa, m)

    while v < 0.95 * vmax:
        E = E_kin(v, m)
        P_resid = P_avail - P_roll(v, m) - P_air(v, cwa)
        E += P_resid
        v = v_from_E_kin(E, m)
        t += 1
        d += v / 3.6
        x.append(d)
        y.append(t)

    plt.plot(x, y)

    if i == len(cWA) - 1:
        plt.annotate('  {:.0f}\nkm/h'.format(0.95 * vmax), xy=(x[-1], y[-1]), ha='center', va='bottom', xytext=(x[-1], y[-1]-13))
    else:
        plt.annotate('{:.0f} km/h'.format(0.95 * vmax), xy=(x[-1], y[-1]), ha='left', va='center', xytext=(x[-1]+50, y[-1]))

plt.xlim(left=0)
plt.ylim(bottom=0)

# save German version
plt.legend(['{} ({})'.format(i, j) for i, j in zip(reversed(list(cWA)), reversed(list(labels_de)))], loc='upper left', title='$c_W \cdot A$')
plt.xlabel('Entfernung [m]')
plt.ylabel('Zeit [s]')
plt.savefig('time_distance_endspeed_300W_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.legend(['{} ({})'.format(i, j) for i, j in zip(reversed(list(cWA)), reversed(list(labels_en)))], loc='upper left', title='$c_W \cdot A$')
plt.xlabel('distance [m]')
plt.ylabel('time [s]')
plt.savefig('time_distance_endspeed_300W_en.svg', transparent=True, bbox_inches='tight')



# DIAGRAM 13: compute time and distance until reaching 99% of end speed
plt.clf()
fig, (plt_t, plt_d) = plt.subplots(nrows=2, sharey=True)
#plt_t.tick_params(axis='x')
#plt_d = plt_t.twiny()
vmax = []
P_muscle = 300   # available power [W]

for bike, cwa in enumerate(cWA[::-1]):
    y  = [0]
    xt = [0]
    xd = [0]
    v = 0
    t = 0
    d = 0
    m = m_rider + m_bike[::-1][bike]
    P_avail = (1 - drivetrain[::-1][bike] / 100) * P_muscle
    vmax.append(get_vmax(P_avail, cwa, m))

    while v < 0.99 * vmax[-1]:
        E = E_kin(v, m)
        P_resid = P_avail - P_roll(v, m) - P_air(v, cwa)
        E += P_resid
        v = v_from_E_kin(E, m)
        t += 1
        d += v / 3.6
        y.append(v)
        xt.append(t)
        xd.append(d)

    plt_t.plot(xt, y)   #, linestyle=(0,(10,1)))
    plt_d.plot(xd, y)   #, linestyle='solid')

    if bike == 0:
        plt_t.annotate('{:.0f} s'.format(t), xy=(xt[-1], y[-1]), ha='center', va='bottom', xytext=(xt[-1]-5, y[-1]-7))
        plt_d.annotate('{:.0f} m'.format(d), xy=(xd[-1], y[-1]), ha='center', va='bottom', xytext=(xd[-1]-100, y[-1]-7))
    else:
        plt_t.annotate('{:.0f} s'.format(t), xy=(xt[-1], y[-1]), ha='left', va='center', xytext=(xt[-1]+3, y[-1]))
        plt_d.annotate('{:.0f} m'.format(d), xy=(xd[-1], y[-1]), ha='left', va='center', xytext=(xd[-1]+50, y[-1]))

plt_t.set_ylim(bottom=0)
plt_t.set_xlim(left=0)
plt_d.set_xlim(left=0)
plt_t.set_yticks(range(0, math.ceil(vmax[0]), 10))
fig.tight_layout(h_pad=2)  # remove overlap between subplots; padding = 2 lines

# save German version
plt.legend(['{} ({:.1f} km/h)'.format(i, j) for i, j in zip(labels_de[::-1], vmax)], loc='lower right', title='Endgeschwindigkeit')
plt_t.set_ylabel('Geschwindigkeit [km/h]')
plt_d.set_ylabel('Geschwindigkeit [km/h]')
plt_t.set_xlabel('Zeit [s]')
plt_d.set_xlabel('Entfernung [m]')
plt.savefig('speed_vs_time_distance_300W_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.legend(['{} ({:.1f} km/h)'.format(i, j) for i, j in zip(labels_en[::-1], vmax)], loc='lower right', title='maximum speed')
plt_t.set_ylabel('speed [km/h]')
plt_d.set_ylabel('speed [km/h]')
plt_t.set_xlabel('time [s]')
plt_d.set_xlabel('distance [m]')
plt.savefig('speed_vs_time_distance_300W_en.svg', transparent=True, bbox_inches='tight')



# DIAGRAM 14: ratio of kinetic energy to driving resistance
plt.clf()

x = range(0, 51, 1)

for cwa in cWA:
    y = [E_kin(v) / (1.08 * (P_roll(v) + P_air(v, cwa))) if v > 0 else 0 for v in x]
    plt.plot(x, y) # plot the kinetic energy as a line

plt.xlim(0, 50)
plt.ylim(bottom=0)

# save German version
plt.legend(['{} ({})'.format(i, j) for i, j in zip(cWA, labels_de)], loc='upper left', title='$c_W \cdot A$')
plt.xlabel('Geschwindigkeit [km/h]')
plt.ylabel('Verhältnis kinetische Energie/Fahrwiderstand pro Sekunde')
plt.savefig('kinetic-energy_ratio_cWA_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.legend(['{} ({})'.format(i, j) for i, j in zip(cWA, labels_en)], loc='upper left', title='$c_W \cdot A$')
plt.xlabel('speed [km/h]')
plt.ylabel('ratio kinetic energy/power per second')
plt.savefig('kinetic-energy_ratio_cWA_en.svg', transparent=True, bbox_inches='tight')



# DIAGRAM 15: kinetic energy vs. rolling distance
plt.clf()

for i, cwa in enumerate(cWA):
    v = 50
    d = 0
    m = m_rider + m_bike[i]
    E = E_kin(v, m)
    x = [d]
    y = [v]

    while v > 0:
        E -= (P_roll(v, m) + P_air(v, cwa))
        E = max(E, 0)
        v_new = v_from_E_kin(E, m)
        d += (v + v_new) / 2 / 3.6
        v = v_new
        x.append(d)
        y.append(v)

    plt.plot(x, y)

plt.xlim(left=0)
plt.ylim(bottom=0)

# save German version
plt.xlabel('Rollweite [m]')
plt.ylabel('Geschwindigkeit [km/h]')
plt.legend(['{} ({})'.format(i, j) for i, j in zip(cWA, labels_de)], loc='upper right', title='$c_W \cdot A$')
plt.savefig('speed_vs_rolling-distance_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.xlabel('rolling distance [m]')
plt.ylabel('speed [km/h]')
plt.legend(['{} ({})'.format(i, j) for i, j in zip(cWA, labels_en)], loc='upper right', title='$c_W \cdot A$')
plt.savefig('speed_vs_rolling-distance_en.svg', transparent=True, bbox_inches='tight')



# DIAGRAM 16: speed vs. power for different slopes and bike types
slope = [0, 1, 2, 3, 4, 5]
linestyle = ['', 'dashed', '', 'solid']
legend_slope = [f(i) for i in slope for f in (lambda x: '{}%'.format(x), lambda x: '_')]

plt.clf()
for pc in slope:

    # select next color manually
    color = next(plt.gca()._get_lines.prop_cycler)['color']

    # racing bike and velomobile
    for bike in [3, 1]:
        x = []
        y = []

        # speed from 0 to 50 km/h
        for v in range(0, 51, 1):
            P = P_roll(v, m = m_rider + m_bike[bike]) + P_air(v, cWA = cWA[bike]) + P_up(v, pc, m = m_rider + m_bike[bike])
            x.append(v)
            y.append(P * (1 + drivetrain[bike] / 100))

        plt.plot(x, y, ls = linestyle[bike], c = color)

plt.xlim(left=0, right=50)
plt.ylim(bottom=0)
plt.legend(legend_slope, loc='upper left')

# save German version
plt.xlabel('Geschwindigkeit [km/h]')
plt.ylabel('Leistung [W]')
plt.savefig('speed_vs_power_uphill_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.xlabel('speed [km/h]')
plt.ylabel('power [W]')
plt.savefig('speed_vs_power_uphill_en.svg', transparent=True, bbox_inches='tight')



# DIAGRAM 17: speed vs. power exponent for different slopes
plt.clf()

for pc in slope:

    # select next color manually
    color = next(plt.gca()._get_lines.prop_cycler)['color']

    # racing bike and velomobile
    for bike in [3, 1]:

        x = []
        y = []
        P = 0

        # speed from 1 to 50 km/h
        for v in range(1, 51, 1):
            P_prev = P
            P = P_roll(v, m = m_rider + m_bike[bike]) + P_air(v, cWA = cWA[bike]) + P_up(v, pc, m = m_rider + m_bike[bike])
            P *= (1 + drivetrain[bike] / 100)
            exponent = math.log(P/P_prev, v/(v - 1)) if P_prev > 0 and v > 1 else 1
            x.append(v)
            y.append(exponent)

        plt.plot(x, y, ls = linestyle[bike], c = color)

plt.xlim(left=0, right=50)
plt.ylim(bottom=1)
plt.legend(legend_slope, loc='upper left')

# save German version
plt.xlabel('Geschwindigkeit [km/h]')
plt.ylabel('Exponent der Leistungszunahme')
plt.savefig('speed_vs_power-exponent_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.xlabel('speed [km/h]')
plt.ylabel('exponent of power increase')
plt.savefig('speed_vs_power-exponent_en.svg', transparent=True, bbox_inches='tight')



# DIAGRAM 18: speed vs. brake power for different slopes and bike types
slope = [1, 2, 4, 6, 8, 10]
legend_slope = [f(i) for i in slope for f in (lambda x: '{}%'.format(x), lambda x: '_')]
plt.clf()

for pc in slope:

    # select next color manually
    color = next(plt.gca()._get_lines.prop_cycler)['color']

    # racing bike and velomobile
    for bike in [3, 1]:
        x = []
        y = []

        # speed from 0 to 200 km/h
        for v in range(0, 201, 1):
            P = P_up(v, pc, m = m_rider + m_bike[bike]) - P_roll(v, m = m_rider + m_bike[bike]) - P_air(v, cWA = cWA[bike])
            x.append(v)
            y.append(P)

        plt.plot(x, y, ls = linestyle[bike], c = color)

plt.xlim(left=0, right=200)
plt.ylim(bottom=0, top=2000)
plt.legend(legend_slope, loc='upper left')

# save German version
plt.xlabel('Geschwindigkeit [km/h]')
plt.ylabel('Bremsleistung [W]')
plt.savefig('speed_vs_brakepower_downhill_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.xlabel('speed [km/h]')
plt.ylabel('brake power [W]')
plt.savefig('speed_vs_brakepower_downhill_en.svg', transparent=True, bbox_inches='tight')



# DIAGRAM 19: speed vs. power for different weights and bike types
luggage = [2.5, 5, 10, 20]
linestyle = ['', 'dashed', '', 'solid']
legend_lugg = [f(i) for i in luggage for f in (lambda x: '{} kg'.format(x), lambda x: '_')]

plt.clf()
for m_add in luggage:

    # select next color manually
    color = next(plt.gca()._get_lines.prop_cycler)['color']

    # racing bike and velomobile
    for bike in [3, 1]:
        x = []
        y = []

        # speed from 0 to 50 km/h
        for v in range(1, 51, 1):
            P0 = (1 + drivetrain[bike] / 100) * (P_roll(v, m = m_rider + m_bike[bike] + 0) + P_air(v, cWA = cWA[bike]) + P_up(v, 0, m = m_rider + m_bike[bike] + 0))
            P = (1 + drivetrain[bike] / 100) * (P_roll(v, m = m_rider + m_bike[bike] + m_add) + P_air(v, cWA = cWA[bike]) + P_up(v, 0, m = m_rider + m_bike[bike] + m_add))
            x.append(v)
            y.append((P / P0 - 1) * 100)

        plt.plot(x, y, ls = linestyle[bike], c = color)

plt.xlim(left=0, right=50)
plt.ylim(bottom=0)
plt.legend(legend_lugg, loc='upper right')

# save German version
plt.xlabel('Geschwindigkeit [km/h]')
plt.ylabel('Mehrleistung [%]')
plt.savefig('speed_vs_power_luggage_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.xlabel('speed [km/h]')
plt.ylabel('additional power [%]')
plt.savefig('speed_vs_power_luggage_en.svg', transparent=True, bbox_inches='tight')



# DIAGRAM 20: speed vs. uphill meters
m = 100
x = range(0, 51)
y = [E_kin(v) / (m * 9.81) for v in x]

plt.clf()
plt.plot(x, y)
plt.xlim(0, 50)
plt.ylim(bottom=0)

# save German version
plt.xlabel('Geschwindigkeit [km/h]')
plt.ylabel('Höhe [m]')
plt.savefig('speed_vs_uphill_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.xlabel('speed [km/h]')
plt.ylabel('height [m]')
plt.savefig('speed_vs_uphill_en.svg', transparent=True, bbox_inches='tight')



# DIAGRAM 21: damped harmonic oscillator
SAMPLE_TIME = 3
SAMPLE_RATE = 0.01
t = np.arange(0, SAMPLE_TIME, SAMPLE_RATE)
omega0 = 2 * np.pi

plt.clf()
plt.axhline(0, color = 'black', label = '_nolegend_')

for damping in [0.15, 1, 4]:
    damping_coefficent = damping * omega0
    omega1 = np.sqrt(np.complex(omega0**2 - damping_coefficent**2, 0))
    y = np.exp(-damping_coefficent * t) * np.cos(omega1 * t)
    plt.plot(t, y.real)

    if damping < 1:
        y = np.exp(-damping_coefficent * t)
        plt.plot(t, y.real, ls = 'dashed', color = 'grey', label = '_nolegend_')
        plt.plot(t, -y.real, ls = 'dashed', color = 'grey', label = '_nolegend_')

plt.xlim(0, SAMPLE_TIME)
plt.ylim(-1, +1)
plt.xticks([])
plt.yticks([-1, 0, +1])

# save German version
plt.xlabel('Zeit')
plt.ylabel('Amplitude')
plt.legend(['unterdämpft', 'aperiodischer Grenzfall', 'überdämpft'], loc='upper right')
plt.savefig('harmonic-oscillator_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.xlabel('time')
plt.ylabel('amplitude')
plt.legend(['underdamped', 'critically damped', 'overdamped'], loc='upper right')
plt.savefig('harmonic-oscillator_en.svg', transparent=True, bbox_inches='tight')



# stop here, skip the other diagrams
quit()


# DIAGRAM 22: speed of apparent wind
plt.clf()
x = range(0, 181)

for (v_bike, v_wind) in [[0.25, 1], [0.5, 1], [1, 1], [2, 1], [3, 1]]:

    # select next color manually
    color = next(plt.gca()._get_lines.prop_cycler)['color']

    y = [math.sqrt((v_bike + v_wind * math.cos(math.radians(angle)))**2 + (0 + v_wind * math.sin(math.radians(angle)))**2) for angle in x]
    plt.plot(x, y, ls = 'solid', c = color)

    #y = [v_wind * math.sin(math.radians(angle)) for angle in x]
    #plt.plot(x, y, ls = 'dashed', c = color)

plt.xlim(0, 180)
plt.ylim(bottom=0)

# save German version
plt.legend(['25%', '50%', '100%', '200%', '300%'], title='Geschwindigkeit vs.\nWindgeschwindigkeit')
plt.xlabel('Winkel [°]')
plt.ylabel('Windgeschwindigkeit/Fahrgeschwindigkeit')
plt.savefig('wind_speed_direction_de.svg', transparent=True, bbox_inches='tight')

# save English version
plt.legend(['25%', '50%', '100%', '200%', '300%'], title='speed vs.\nwind speed')
plt.xlabel('angle [°]')
plt.ylabel('wind speed/ride speed')
plt.savefig('wind_speed_direction_en.svg', transparent=True, bbox_inches='tight')



plt.clf()
y = [E_kin(50) / 5, P_roll(50), P_air(50)]
plt.pie(y, labels=['E_kin', 'P_roll', 'P_air'], autopct=lambda p: '{:.1f}%{}{:.0f} W'.format(p, ' = ' if p < 10 else '\n', p / 100 * sum(y)))
plt.savefig('power_comparison_50kmh.svg', transparent=True, bbox_inches='tight')

plt.clf()
plt.pie([E_kin(20), P_roll(20) * 5, P_air(20) * 5], labels=['E_kin', 'P_roll', 'P_air'], autopct='%.1f%%')
plt.savefig('power_comparison_20kmh.svg', transparent=True, bbox_inches='tight')

plt.clf()
plt.pie([P_up(20, 5), P_roll(20), P_air(20)], labels=['P_up', 'P_roll', 'P_air'], autopct='%.1f%%')
print("5% up: power = {}".format(P_up(20,5) + P_roll(20) + P_air(20)))
plt.savefig('power_comparison_20kmh_5pc-up.svg', transparent=True, bbox_inches='tight')


# IDEAS:
# * scenarios: braking often or constant driving, for same time, or time for same energy
# * higher influence of mass for a velomobile: impact of 50% more mass for different cWA values
