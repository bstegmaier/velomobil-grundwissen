$( function() {
	function createCheckbox(def) {
		$( '#'+def+'Area' ).append('<div id="'+def+'Boxes" class="property-boxes"></div>');
		$.each(dataDef[def]['values'], function(key, value) {
			var checkId = def+'-'+key;
			$( '#'+def+'Boxes' ).append('<span class="property-box"><label for="'+checkId+'">'+value+'</label><input type="checkbox" id="'+checkId+'" class="'+def+'Check"></span>');
			$( '#'+checkId ).change(function () {
				if (
					dataDef[def]['type'] == 'radio'
					&& this.checked
				) {
					$( '#'+def+'Boxes' ).find('input').prop('checked', false);
					this.checked = true;
				}
				if ($('.'+def+'Check:checked').length == 0) {
					dataDef[def]['changed'] = false;
				} else {
					dataDef[def]['changed'] = true;
				}
				evaluate()
			});
		});
	}

	function createSlider(def) {
		$( '#'+def+'Area' ).append('<div id="'+def+'InfoBox" class="property-info"></div>');
		$( '#'+def+'Area' ).append('<div id="'+def+'Slider" class="property-slider"></div>');
		$( '#'+def+'Slider' ).slider({
			range: true,
			min: dataDef[def]['min'],
			max: dataDef[def]['max'],
			values: [ dataDef[def]['min'], dataDef[def]['max'] ],
			step : (dataDef[def]['step'] !== null && dataDef[def]['step'] !== undefined) ? dataDef[def]['step'] : 1,
			slide: function( event, ui ) {
				$( '#'+def+'InfoBox' ).html( ui.values[ 0 ] + " - " + ui.values[ 1 ]  + ' ' + ((dataDef[def]['unit'] !== null && dataDef[def]['unit'] !== undefined) ? dataDef[def]['unit'] : ''));
				dataDef[def]['currMin'] = ui.values[ 0 ];
				dataDef[def]['currMax'] = ui.values[ 1 ];
				if (dataDef[def]['min'] == ui.values[ 0 ] && dataDef[def]['max'] == ui.values[ 1 ]) {
					dataDef[def]['changed'] = false;
				} else {
					dataDef[def]['changed'] = true;
				}
				evaluate();
			}
		});
		$( '#'+def+'InfoBox' ).html( dataDef[def]['min'] + " - " + dataDef[def]['max'] + ' ' + ((dataDef[def]['unit'] !== null && dataDef[def]['unit'] !== undefined) ? dataDef[def]['unit'] : ''));
	}

	function createVmList() {
		$('#result').html('');
		$.each(vmData, function(key, vm) {
			$('#result').append('<details id="'+key+'"><summary>'+vm.name+'</summary>'+vmDetails(key, vm)+'</details>');
		});
	}

	function vmDetails(vmKey, vm) {
		var html = '<ul>';
		$.each(dataDef, function(key, definition) {
			var value = '';
			if (vm.hasOwnProperty(key)) {
				switch (definition.type) {
					case 'checkbox':
						var komma = '';
						/* checkbox data does not need to be an object if it has just one value */
						if (typeof vm[key] !== 'object') {
							var makeObject = vm[key];
							vm[key] = null;
							vm[key] = [ makeObject ];
						}
						
						$.each(vm[key], function(index, val) {
							value += komma+definition['values'][val];
							komma = ', ';
						});
						break;
					case 'radio':
						value = definition['values'][vm[key]];
						break;
					case 'range':
						value = vm[key];
						if (definition.hasOwnProperty('unit')) {
							value += ' ' + definition.unit;
						}
						break;
					case 'info':
						value = vm[key];
						break;
					case 'link':
						value = '<a href="'+vm[key]['url']+'" target="_blank">'+vm[key]['label']+'</a>';
						break;
				}

				if (sources[vmKey].hasOwnProperty(key)) {
					value += ' <a href="'+sources[vmKey][key]+'" target="_blank">Quelle</a>';
				} 
				
				html += '<li>'+definition.title+': '+value+'</li>';
			}
		});
		html += '</ul>';
		return html;
	}

	function evaluate() {
		$.each(vmData, function(key, value) {
			$('#'+key).attr( 'class', 'vm-'+vmVisibility(value));
		});
		$.each(dataDef, function(key, value) {
			$('#'+key+'Title').removeClass('property-changed');
			if (value.changed) {
				$('#'+key+'Title').addClass('property-changed');
			}
		});
	}

	function vmVisibility(vm) {
		var visibility = true;
		var uncertain = false;
		$.each(dataDef, function(key, def) {
			if (
				def.changed
				&& !vm.hasOwnProperty(key)
			) {
				uncertain = true;
				return;
			}

			if (vm.hasOwnProperty(key)) {
				switch(def.type) {
					case 'range':
						if (def.currMin > vm[key] || def.currMax < vm[key]) {
							visibility = false;
						}
						break;

					case 'radio':
					case 'checkbox':
						var checkVisibility = false;
						var checkedNothing = true;
						$.each(def.values, function(vkey) {
							var checkId = key+'-'+vkey;
							if ($('#'+checkId).prop('checked')) {
								checkedNothing = false;
								if (
									$.inArray(vkey, vm[key]) > -1 // checkboxes
									|| vkey == vm[key] //radio
								) {
									checkVisibility = true;
								}
							}
						})
						visibility = visibility && (checkedNothing || checkVisibility);
						break;
				}
			}
		});
		return uncertain ? 'uncertain' : ( visibility ? 'valid' : 'invalid' );
	}


	/* if a value has a source, copy source in object and move value one level up */
	var sources = {};
	for (var vm in vmData) {
		sources[vm] = {};
		for (attr in vmData[vm]) {
			if (typeof vmData[vm][attr] === 'object' && vmData[vm][attr].hasOwnProperty('source')) {
				sources[vm][attr] = vmData[vm][attr]['source'];
				value = vmData[vm][attr]['value'];
				vmData[vm][attr] = null;
				vmData[vm][attr] = value;
			}
		}
	}
	
	for (var def in dataDef) {
		for (var vm in vmData) {
			if (dataDef[def].hasOwnProperty('enrich')) {
				dataDef[def].enrich(vmData[vm]);
			}

			switch (dataDef[def]['type']) {
				case 'checkbox':
				case 'radio':
					break;
				case 'range':
					dataDef[def]['min'] = Math.min((dataDef[def]['min'] !== null && dataDef[def]['min'] !== undefined) ? dataDef[def]['min'] : 9999, ((vmData[vm][def] !== null && vmData[vm][def] !== undefined) ? vmData[vm][def] : 9999));
					dataDef[def]['max'] = Math.max((dataDef[def]['max'] !== null && dataDef[def]['max'] !== undefined) ? dataDef[def]['max'] : 0, ((vmData[vm][def] !== null && vmData[vm][def] !== undefined) ? vmData[vm][def] : 0));
					dataDef[def]['currMin'] = dataDef[def]['min'];
					dataDef[def]['currMax'] = dataDef[def]['max'];
					break;
			}
		}
		dataDef[def]['changed'] = false;
	}

	for (var def in dataDef) {
		var sliderArea = '<div id="'+def+'Area" class="vm-property"><div id="'+def+'Title" ' + (typeof dataDef[def]['info'] != 'undefined' ? 'class="property-title property-help" title="' + dataDef[def]['info'] + '"' : 'class="property-title"') + '>'+dataDef[def]['title']+'</div></div>';
		$('#choice').append(sliderArea);
		switch (dataDef[def]['type']) {
			case 'checkbox':
			case 'radio':
				createCheckbox(def);
				break;
			case 'range':
				createSlider(def);
				break;
		}

	}

	createVmList();
});
