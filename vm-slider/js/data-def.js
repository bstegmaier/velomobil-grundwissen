var dataDef = {
// dimensions
	'weight'                 : { title : 'Gewicht'                   , type : 'range' , unit : 'kg' , step : .5 , info : 'fahrbereit ab' },
	'length'                 : { title : 'Länge'                     , type : 'range' , unit : 'cm' },
	'width'                  : { title : 'Breite'                    , type : 'range' , unit : 'cm' },
	'height'                 : { title : 'Höhe'                      , type : 'range' , unit : 'cm' },
	'groundclearance'        : { title : 'Bodenfreiheit'             , type : 'range' , unit : 'cm' , info : 'überfahren eines Hindernisses auf ebenem Gelände'},
	'trackwidth'             : { title : 'Spurbreite'                , type : 'range' , unit : 'cm' },
	'wheelbase'              : { title : 'Radstand'                  , type : 'range' , unit : 'cm' },
	'minlengthdriver'        : { title : 'min. Länge Fahrer'         , type : 'range' , unit : 'cm' },
	'maxlengthdriver'        : { title : 'max. Länge Fahrer'         , type : 'range' , unit : 'cm' },
	'minxseam'               : { title : 'min. X-Seam'               , type : 'range' , unit : 'cm' },
	'maxxseam'               : { title : 'max. X-Seam'               , type : 'range' , unit : 'cm' },
	'maxshoulderwidth'       : { title : 'max. Schulterbreite'       , type : 'range' , unit : 'cm' , step : .5 , info : 'Innenbreite des VMs im Schulterbereich' },
	'maxthighwidth'          : { title : 'Breite zwischen Radkästen' , type : 'range' , unit : 'cm' },
	'maxshoesize'            : { title : 'max. Schuhgröße'           , type : 'range' , info : 'größte Schuhgröße bei großem Fahrer, die je regelmäßig in diesem VM gefahren ist, ggf. mit einfachen Anpassungen (z.B. verlängerte Fußlöcher)' },
	'driversopeningwidth'    : { title : 'Breite Fahreröffnung'      , type : 'range' , unit : 'cm' , info : 'an der breitesten Stelle' },
	'driversopeninglength'   : { title : 'Länge Fahreröffnung'       , type : 'range' , unit : 'cm' , info : 'an der längsten Stelle' },
	'luggagecapacity'        : { title : 'Packvolumen'               , type : 'range' , unit : 'l' , info : 'Anzahl der 1l-Packungen, die man laden kann, ohne dass sie beim Fahrbetrieb stören' },
	'maxfrontwheelwidth'     : { title : 'max. Reifenbreite vorne'   , type : 'range' , unit : 'mm' },
	'maxrearwheeldiameter'   : { title : 'max. Reifendurchmesser hinten' , type : 'range' , unit : 'mm' , info : 'Felgenmaß + 2 x Reifen-Querdurchmesser, z.B. bei 40-622 -> 622 + (2 x 40) = 702' },
	'turningcircle28mm'      : { title : 'Wendekreis bei 28mm'       , type : 'range' , unit : 'm' , step : .5 , info : 'zwischen 2 Mauern' },
	'turningcircle40mm'      : { title : 'Wendekreis bei 40mm'       , type : 'range' , unit : 'm' , step : .5 , info : 'zwischen 2 Mauern' },
	'mindevelopment'         : { title : 'min. Entfaltung'           , type : 'range' , unit : 'cm' , info : 'rechnerisch aus größtem Kettenblatt, kleinstem Ritzel und Antriebsradmaß' },
	'maxdevelopment'         : { title : 'max. Entfaltung'           , type : 'range' , unit : 'cm' , info : 'rechnerisch aus größtem Kettenblatt, kleinstem Ritzel und Antriebsradmaß' },
	'comfortablespeed'       : { title : 'Wohlfühlgeschwindigkeit'   , type : 'radio' , values : { 'low' : '< 32 km/h' , 'medium' : '32-36 km/h' , 'high' : '37-43 km/h' , 'speed' : '> 43 km/h' } , info : 'dauerhafte Reisegeschwindigkeit nach der Beschleunigung auf gutem, ebenem Asphalt' },

// configuration
	'steering'               : { title : 'Lenkung'                   , type : 'checkbox' , values : { 'tiller' : 'Tiller' , 'sidestick' : 'Panzerlenkung' }},
	'frontwheels'            : { title : 'Vorderräder'               , type : 'radio' , values : { 'open' : 'offen' , 'covered' : 'geschlossen' }},
	'hood'                   : { title : 'Haube'                     , type : 'checkbox' , values : { 'removablehood' : 'abnehmbare Haube' , 'foldinghood' : 'klappbare Haube' , 'foamcover' : 'Schaumdeckel' , 'tarp' : 'Plane' }},
	'hoodtransport'          : { title : 'Haube transportierbar'     , type : 'checkbox' , values : { 'hoodyes' : 'Haube ja' , 'hoodno' : 'Haube nein' , 'foamcoveryes' : 'Schaumdeckel ja' , 'foamcoverno' : 'Schaumdeckel nein' }},
	'openchain'              : { title : 'Kette'                     , type : 'radio' , values : { 'open' : 'offen' , 'covered' : 'verkleidet' } , info : 'hinter der ersten Umlenkrolle' },
	'maintenance'            : { title : 'Wartbarkeit'               , type : 'checkbox' , values : { 'maintenanchole' : 'Wartungsklappe' , 'removablefront' : 'abnehmbare Front', 'removableend' : 'abnehmbares Heck' , 'openend' : 'offenes Heck' }},
	'motorizable'            : { title : 'mögliche Motorposition'    , type : 'checkbox' , values : { 'hub' : 'Nabe' , 'bottombracket' : 'Tretlager' , 'intermediate' : 'Zwischengetriebe' }},

// driving character
	'overallcharacter'       : { title : 'Fahrcharakter'             , type : 'checkbox' , values : { 'city' : 'Stadt' , 'travel' : 'Reise' , 'race' : 'Rennsport'} , 'info' : 'Stadt: wendig, gute Rundumsicht. Reise: läuft mit Gepäck auf langen Strecken. Rennsport: für sportliche Runden oder auf der Rennbahn. Kann für jeden Zweck anders konfiguriert sein.' },
	'sidewindsensitivity'    : { title : 'Seitenwind-Empfindlichkeit', type : 'radio' , values : { 'high' : 'hoch' , 'medium' : 'mittel' , 'low' : 'niedrig'}},
	'curvestability'         : { title : 'Kurvenstabilität'          , type : 'radio' , values : { 'low' : 'niedrig' , 'medium' : 'mittel' , 'high' : 'hoch'}},

// commercial information
	'pricefrom'              : { title : 'Preis ab'                  , type : 'range' , unit : '€' },
	'builtfrom'              : { title : 'gebaut ab'                 , type : 'range' , enrich : function(vmData) { if (typeof vmData.builtfrom !== 'undefined' && typeof vmData.builtuntil === 'undefined') { vmData.buyable = 'yes' } } },
	'builtuntil'             : { title : 'gebaut bis'                , type : 'range' },
	'buyable'                : { title : 'aktuell kaufbar'           , type : 'radio' , values : { 'yes' : 'ja' }},
	'website'                : { title : 'Hersteller-Webseite'       , type : 'link' },
};
