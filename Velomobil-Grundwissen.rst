.. only:: latex

    Velomobil-Grundwissen
    =====================

.. raw:: html

    <h1>Velomobil-Grundwissen</h1>

.. only:: epub

    .. toctree::
        :hidden:

        Velomobil-Grundwissen

Allgemeines
===========

Was ist ein Velomobil?
----------------------
Ein vollverkleidetes Liegefahrrad, typischerweise mit drei Rädern. Die Verkleidung sorgt für Wetterschutz und verbessert die Aerodynamik sehr deutlich, aber sorgt auch für Spitznamen wie „rollendes Ei“, „Torpedo“ oder „Zäpfchen“.


.. index:: Preis, Gebrauchtkauf

Was kostet das?
---------------
Die Neupreise bewegen sich meist zwischen 5000 und 10000 EUR. Gebraucht sind sie natürlich günstiger; aber da nicht nur die Produktion langsam anwächst, sondern auch die Nachfrage, sind Velomobile relativ wertstabil – zumindest bezogen auf die oft sehr hohen Kilometerleistungen.

.. _fig_secondhand_prices:

.. figure:: images/GebrauchtPreise2019_optim.*
    :alt: Gebrauchtpreise 2019
    :width: 100%

    Preise, für die gebrauchte Velomobile 2019 angeboten wurden (ohne Berücksichtigung von Neupreis, Ausstattung und Zustand). Wie man sieht, wurde das Quest, das eines der meistproduzierten Velomobile ist (siehe :numref:`fig_rijderslist_models`), entsprechend häufig und zu unterschiedlichen Preisen angeboten. Dagegen sind neuere Modelle wie DF oder Quattrovelo gebraucht noch entsprechend teuer.


.. index:: Gewicht, Motor

Wie viel wiegt das?
-------------------
Die meisten Velomobile wiegen zwischen 20 und 30 kg. Nur wenige ältere Modelle oder manche mit Motor liegen deutlich darüber; außerdem gibt es Spezialanfertigungen ab ca. 16 kg. Siehe :numref:`fig_vm-dimensions`, welche Modelle in welchem Gewichtsbereich sind.


.. index:: Hersteller, Dronten

Wer stellt so etwas her?
------------------------
Es gibt eine Handvoll Manufakturen, die Velomobile größtenteils in Handarbeit bauen – die meisten dieser Firmen sitzen in den Niederlanden (drei davon in Dronten), aber es gibt auch Hersteller in Deutschland, Dänemark, Frankreich und der Schweiz.


.. index:: Fahrleistung, Altersverteilung

Wer fährt so etwas?
-------------------
Tendenziell Vielfahrer, wie man auch im :ref:`Histogramm der monatlichen Fahrleistungen <fig_rijderslist_dist>` sehen kann. Oft wird das Velomobil zum täglichen Pendeln verwendet.

Das Hauptverbreitungsgebiet der Velomobile sind die Niederlande und Deutschland. Von den niederländischen Herstellern (*Velomobiel.nl*, *Intercitybike*) sind Verkaufsdaten öffentlich verfügbar; siehe :numref:`fig_rijderslist_countries`.

.. _fig_rijderslist_countries:

.. figure:: images/rijderslist_countries_de.*
    :alt: Länder, in die die niederländischen Velomobile verkauft wurden
    :width: 100%

    Länder, in die die niederländischen Velomobile (d.h. von Velomobiel.nl und Intercitybike) verkauft wurden

Altersverteilung, laut einer `Umfrage im Velomobil-Forum <https://www.velomobilforum.de/forum/index.php?threads/velomobil-alter-hase-oder-junger-spund.47682/>`_, siehe :numref:`fig_age`.

.. _fig_age:

.. figure:: images/age-histogram_de.*
    :alt: Altersverteilung der Velomobil-Fahrer aus einer Umfrage im Velomobil-Forum
    :width: 100%

    Altersverteilung der Velomobil-Fahrer aus einer Umfrage im Velomobil-Forum


.. index:: Händler, Hersteller

Wo kann man das kaufen?
-----------------------
Abgesehen von den Herstellern gibt es auch ein paar Händler, über ganz Mitteleuropa verteilt. Wer jeweils welche Modelle verkauft, sieht man am besten auf den Hersteller-Webseiten.


.. index:: Hersteller, Modelle

Wie heißen denn die gängigen Hersteller und Modelle?
----------------------------------------------------
Hersteller und Modelle (alphabetisch, nicht vollständig):

* `Alligt <http://www.alligt.nl/>`_: :index:`Alleweder` (A1 bis A6), :index:`Sunrider`
* `Beyss Leichtfahrzeuge <http://go-one.de/>`_: :index:`Go-One` Evo (K, Ks, R)
* `Cycles JV & Fenioux <https://cyclesjv.com/>`_: :index:`Le Mans`, :index:`Mulsanne`
* `Drymer (Sinner) <https://www.drymer.nl/>`_: :index:`Hilgo`, :index:`Mango` (Plus, Sport, Tour)
* `Flevobike <https://www.flevobike.nl/>`_: :index:`Orca`
* `InterCityBike <https://www.intercitybike.nl/>`_: :index:`DF`, DF XL
* `Katanga <http://www.katanga.eu/>`_: :index:`WAW`
* `Leiba <http://www.leiba.de/>`_: :index:`Leiba` (Classic, Hybrid, Record, X-Stream, XXL)
* `Leitra <http://leitra.dk/>`_: :index:`Leitra`
* `Räderwerk <https://velomobil.eu/>`_: :index:`Milan` (GT, SL)
* `Trisled <https://trisled.com.au/>`_: :index:`Aquila 2`, :index:`Kestrel`, :index:`Minihawk`, :index:`Rotovelo`, :index:`Tomahawk`
* `Velomobiel.nl <http://www.velomobiel.nl/>`_: :index:`Quattrovelo`, :index:`Quest`, Quest XS, :index:`Snoek`, :index:`Strada`
* `Velomobile World <https://www.velomobileworld.com/>`_: :index:`Alpha7`
* Sonstige: :index:`After7`, :index:`Velayo`

`Interaktiver Modellvergleich <https://cmoder.gitlab.io/velomobil-grundwissen/vm-slider/vm-slider.html>`_

Fotogalerien:

* https://cyclesjv.com/categorie-produit/velomobiles/
* http://velomobiles.de/html/modellubersicht.html

.. _fig_rijderslist_models:

.. figure:: images/rijderslist_models_de.*
    :alt: Relative Marktanteile der niederländischen Velomobile
    :width: 100%

    Relative Marktanteile der niederländischen Velomobile (d.h. von Velomobiel.nl und Intercitybike)


.. index:: Gesetz, StVO, StVZO, VwV-StVO, Deutschland, Radweg, Radwegbenutzungspflicht, S-Pedelec, Pedelec, Versicherungskennzeichen, Scheinwerfer, Rücklicht, LED, Blinker, Speichenreflektoren, Pedalreflektoren

Rechtssituation in Deutschland?
-------------------------------
Rechtlich gesehen ist ein Velomobil eindeutig ein Fahrrad – erfordert also weder eine Zulassung, noch KFZ-Steuer oder Versicherung, dafür gilt aber die Radwegbenutzungspflicht. Eine Ausnahme ist ein S-Pedelec; dieses muss vom Hersteller zugelassen sein, benötigt ein Versicherungskennzeichen und darf nicht auf Radwegen benutzt werden.

In der Praxis sind aber manche Regelungen für Velomobile nicht sinnvoll anwendbar. Das betrifft erstens die Radwegbenutzungspflicht (`StVO § 2 Abs. 4 <http://www.gesetze-im-internet.de/stvo_2013/__2.html>`_); Radwege sind aber selten für Velomobil-Geschwindigkeiten geeignet, und haben oft zu geringe Kurvenradien und Schikanen wie Drängelgitter. So gibt es einerseits eine Benutzungspflicht, aber gleichzeitig bauliche Maßnahmen, die die Benutzung unmöglich machen. Und dann gibt es noch Dinge wie unübersichtliche Stellen, parkende Autos, unaufmerksame Fußgänger und Radfahrer, schlechte Oberflächenqualität. Selbst wenn so ein benutzungspflichtiger Radweg von einem Velomobil benutzbar wäre, ist es oft doch deutlich sicherer, die Straße zu benutzen – gerade innerorts, wo man ähnlich schnell wie die Autos fährt, aber die Radwege von Ausfahrten und Einmündungen aus oft sehr schlecht einsehbar sind.

.. _fig_radweg_stvo:

.. figure:: images/StVO_Zeichen_237-240-241.*
    :alt: Verkehrszeichen 237 (Radweg), 240 (kombinierter Rad- und Fußweg), 241 (getrennte Rad- und Fußwege)
    :width: 100%

    Benutzungspflichtige Radwege: Zeichen 237, 240 und 241

Insbesondere die Aussagen der StVO zu Radwegen werden in der *Allgemeinen Verwaltungsvorschrift zur Straßenverkehrs-Ordnung* (VwV-StVO) präzisiert (z.B. durch Mindestmaße); für Velomobile ist insbesondere folgender Absatz interessant:

.. epigraph::

    *Die vorgegebenen Maße für die lichte Breite beziehen sich auf ein einspuriges Fahrrad. Andere Fahrräder (vgl. Definition des Übereinkommens über den Straßenverkehr vom 8. November 1968, BGBl. 1977 II S. 809) wie mehrspurige Lastenfahrräder und Fahrräder mit Anhänger werden davon nicht erfaßt. Die Führer anderer Fahrräder sollen in der Regel dann, wenn die Benutzung des Radweges nach den Umständen des Einzelfalles unzumutbar ist, nicht beanstandet werden, wenn sie den Radweg nicht benutzen.*

        -- `VwV-StVO 2017, Abs. 23 <http://www.verwaltungsvorschriften-im-internet.de/bsvwvbund_26012001_S3236420014.htm>`_

Die `genannte Definition eines Fahrrads <https://dejure.org/ext/a63942972b2d9b3a5f6937aa5e69f835>`_ findet sich auch in `StVZO § 63a Abs. 1 <http://www.gesetze-im-internet.de/stvzo_2012/__63a.html>`_ wieder, in Abs. 2 um Pedelecs ergänzt.

Der zweite Punkt ist die Fahrradbeleuchtung (`StVZO § 67 <http://www.gesetze-im-internet.de/stvzo_2012/__67.html>`_):

* Scheinwerfer: Die minimale Anbauhöhe von 400 mm wird von kaum einem Velomobil erfüllt.

* Rücklicht: Ausländische Hersteller verbauen selten Rücklichter mit deutschem Prüfzeichen. Oft werden einzelne LEDs oder LED-Streifen direkt in das Heck integriert, und käufliche Rücklichter mit Prüfzeichen sind für die Integration in eine Karosserie nicht geeignet.

* Rückstrahler: Ist auch so gut wie nie vorhanden. Manche Hersteller verbauen zwar eine retroreflektierende Folie am Heck, die an sich genauso gut ist, aber eben kein Prüfzeichen hat.

* Blinker: Diese sind erst seit 2017 überhaupt erlaubt; und entsprechend gibt es praktisch keine Fahrradblinker mit Bauartgenehmigung auf dem Markt. Da man bei Velomobilen die Richtung schlecht mit den Händen anzeigen kann, sind praktisch immer Blinker verbaut, die aber oft keine Prüfzeichen haben.

* Speichenreflektoren: Diese wären vorgeschrieben, aber praktisch nie vorhanden, und zumindest bei geschlossenen Radkästen auch vollkommen sinnlos. Lediglich reflektierende Streifen auf Reifen in offenen Radkästen sind problemlos möglich und auch sinnvoll.

* Pedalreflektoren: Sind ebenfalls komplett sinnlos und daher praktisch nie vorhanden.

* Die Fahrradbeleuchtung ist zwar in einem separaten Paragraph der StVZO geregelt, nicht jedoch die Bauartgenehmigung der Beleuchtung. Daher ist man bei einem Velomobil nicht auf Fahrradbeleuchtung beschränkt, sondern kann z.B. auch Komponenten für Motorräder verbauen. Das ist gerade bei Blinkern interessant, weil diese im Fahrradzubehör kaum existieren.

Es ist also sehr schwer, bei einem Velomobil Sicherheit und Gesetzeskonformität zu vereinen. Man befindet sich in einer rechtlichen Grauzone; vieles wird in der Praxis toleriert bzw. ist sogar in der VwV-StVO festgeschrieben, aber Rechtssicherheit gibt es nicht.


.. index:: Tandem, Leiba Cargo, Quattrovelo, Wendekreis

Gibt es Velomobile für mehrere Personen?
-----------------------------------------
Zumindest in der Leiba Cargo kann eine zweite Person mitgenommen werden, und im Quattrovelo zumindest ein kleines Kind. Diese können aber nicht mittreten. Echte Tandems gibt es nur als Prototypen, nicht als Serie.

Aber der Sinn ist auch etwas fraglich: Ein Velomobil-Tandem ist deutlich schwerer, und ist, wenn man es alleine fährt, deutlich langsamer. Zudem ist es nicht billiger als zwei einzelne Velomobile, und auch nicht effizienter. (Ein entsprechend hochoptimiertes Tandem-Velomobil ist denkbar, wurde aber bisher noch nicht gebaut, und hätte auch nur geringe Vorteile – bei gleichzeitig deutlich größeren Außenmaßen und einem deutlich größeren Wendekreis.)


.. index:: Modellvergleich, Sitzposition, Stauraum, Wendekreis, Körpergröße

Welches Modell ist das beste?
-----------------------------
Kommt darauf an, was für Anforderungen man hat:

* Zunächst einmal muss es passen und man sich darin wohlfühlen. Dabei hat man es als durchschnittlich großer Velomobilfahrer (ca. 1.70–1.90 m) deutlich leichter als besonders große oder kleine Leute.
* Die Sitzposition unterscheidet sich auch; bei manchen sitzt man aufrechter, bei anderen mehr liegend. Je nach Vorliebe.
* Manche Modelle sind besonders schnell, weil sie möglichst leicht und steif sind. Dafür sind sie tendenziell unkomfortabler (weniger Platz, strafferes Fahrwerk).
* Manche Modelle bieten besonders viel Stauraum für Gepäck bzw. für sperrige Gegenstände.
* Manche Modelle haben eine besonders große Einstiegsöffnung.
* Je nach Strecke können Modelle mit besonders großem Wendekreis unpraktisch sein.
* Je nach Abstellraum können besonders lange Modelle ungünstig sein.

Es gibt also viele Kriterien; man kann (und soll) sich die technischen Daten und Erfahrungsberichte anschauen, aber ob es wirklich passt, merkt man erst bei einer Probefahrt. Selbst wenn die Körpergröße passt, kann es z.B. an den Schultern zu eng sein.


.. index:: Probefahrt, Muskel, Stadtverkehr, Gebrauchtkauf

Wie ausführlich sollte eine Probefahrt sein?
-----------------------------------------------------
Schwer zu sagen; man sieht zwar, ob man hineinpasst, ob man eine gute Sicht hat und beschwerdefrei treten kann. Aber ob ein Velomobil wirklich das Richtige ist, merkt man nicht im Rahmen einer kurzen Probefahrt – die Muskulatur muss sich über viele 100 km umstellen, und auch eine effiziente und vorausschauende Fahrweise braucht Wochen bis Monate. Und schließlich kann ein Velomobil auf vielen Alltagsstrecken (z.B. im Stadtverkehr) seine Vorteile kaum ausspielen. Wenn man sich unsicher ist, sollte man einkalkulieren, dass man das Velomobil wieder verkaufen muss – oder gleich für den Anfang ein günstiges gebrauchtes Modell zu kaufen, um die ersten Erfahrungen zu sammeln.



Karosserie und Geometrie
=========================

.. index:: Windempfindlichkeit, Seitenwind, Mehrspurer, Einspurer

Warum haben Velomobile drei Räder?
----------------------------------
Ein Zweirad ohne Verkleidung ist deutlich kompakter als ein unverkleidetes Dreirad, und auch ein ganzes Stück leichter, weil ein Rad sowie die Lenk- und evtl. Federungsmechanik wegfällt. Mit Verkleidung ist das gar nicht mehr so eindeutig; ein verkleidetes Fahrzeug ist breit, egal ob es zwei oder drei Räder hat, und zusammen mit dem Gewicht der Verkleidung ist der relative Gewichtsunterschied deutlich kleiner. Ein weiterer Grund ist die Windempfindlichkeit. Eine Verkleidung bedeutet viel Angriffsfläche für Seitenwind; und ein Zweirad ist empfindlicher, weil es schmäler und höher ist, also mehr Seitenfläche bietet. Zudem kann man auf einem verkleideten Zweirad nicht einfach die Füße auf den Boden setzen, um einen Sturz abzufangen. Deshalb haben sich letztendlich :term:`Mehrspurer` durchgesetzt, weil sie kaum schwerer und kaum langsamer als :term:`Einspurer` sind, aber gerade bei hohen Geschwindigkeiten und auf langen Strecken sich entspannter fahren.


.. index:: Tadpole, Delta, Heck, Zentrifugalkraft

Warum sind üblicherweise zwei Räder vorne und eines hinten?
-----------------------------------------------------------
Das hat zwei Gründe:

* Erstens die Fahrstabilität. In einer Kurve zieht die Trägheitskraft das Fahrzeug tangential aus der Kurve. Mit zwei Vorderrädern (sogenannte :term:`Tadpole`-Anordnung) steht das kurvenäußere Vorderrad genau in der Richtung, in der die Trägheitskraft angreift – das Fahrzeug kann nicht so leicht kippen. Bei zwei Hinterrädern (sogenannte :term:`Delta`-Anordnung) befindet sich dort kein Rad, d.h. es kippt leichter.
* Zweitens die Aerodynamik. Es ist leichter, ein schnelles Fahrzeug zu bauen, bei dem die Vorderräder die breiteste Stelle bilden und im vorderen Bereich oder in der Mitte sitzen. Hätte man die beiden Räder hinten, müsste dahinter noch ein langes, ungenutztes Heck kommen – während im Heck der Tadpole-Konfiguration das Hinterrad problemlos Platz findet.


.. index:: Radkasten, Reifengröße, Vierrad, Zwischengetriebe, Übersetzung

Warum sind üblicherweise die Vorderräder klein und das Hinterrad groß?
----------------------------------------------------------------------
Die Vorderräder sind aus Platzgründen klein – wollte man große Räder in Radkästen verkleiden, müsste das Fahrzeug seitlich sehr groß sein; und da man mit den Vorderrädern lenken will, müssten die Radkästen entsprechend tief sein, was wiederum die Gesamtbreite erhöht. Daher macht man die Vorderräder nicht höher als den Innenraum, den der Liegeradfahrer für die Tretbewegung braucht.

Das Hinterrad hat dagegen genug Platz; hinter dem Kopf des Fahrers ist das Fahrzeug sowieso hoch (Ausnahme: `Vierrad <Warum ein Velomobil mit vier Rädern?>`_). Und ein großes Rad hat erstens einen etwas geringeren Rollwiderstand, und zweitens übersetzt es die Tretkraft des Fahrers stärker – bei einem kleinen Hinterrad müsste die Kette den Antrieb viel stärker übersetzen, d.h. das :term:`Kettenblatt` müsste um die 100 Zähne haben, oder man braucht ein Zwischengetriebe.


.. index:: Hinterradantrieb, Vorderradlenkung, Umlenkrolle, Sitz, Differentialgetriebe

Warum ist der Antrieb üblicherweise hinten?
-------------------------------------------
Weil die Lenkung vorne ist. Es wäre sehr umständlich, ein gelenktes Rad anzutreiben, weil man dazu die Kette irgendwie mitlenken müsste. Und beide Vorderräder anzutreiben würde z.B. eine Kardanwelle und ein Differentialgetriebe erfordern, und dafür ist kein Platz – zwischen den Rädern sitzt schließlich der Fahrer. Daher ist es das kleinere Übel, eine lange Kette nach hinten zu führen; wenn das mittels zwei Umlenkrollen unter dem Sitz erfolgt, kostet es dort nur wenige Zentimeter Höhe.


.. index:: Vierrad, Hutze, kippen, Seitenführung, Traktion, Reifengröße, Differentialgetriebe

Warum ein Velomobil mit vier Rädern?
------------------------------------
Ein Vierrad ist ein Velomobil, das zwei Hinterräder statt einem hat. Um die :term:`Hutze` nicht extrem breit machen zu müssen, werden kleine Räder als Hinterräder verwendet; diese kann man dafür näher an den Schwerpunkt bauen.

Vierräder haben ein paar Vorteile:

* Die Kippstabilität ist höher.
* Zwischen den Hinterrädern und unter der :term:`Hutze` ist ein großer Gepäckraum, in den auch sperrige Gegenstände passen.
* Das Heck bricht weniger leicht aus, weil erstens zwei Räder kaum gleichzeitig einen schlagartigen Luftverlust haben oder aus anderen Gründen gleichzeitig die Seitenführung verlieren (siehe `Warum ist es gefährlich, wenn das Hinterrad die Seitenführung verliert?`_) und zweitens die Hinterräder näher am Schwerpunkt sind, so dass sie mehr Auflast haben.
* Wenn beide Hinterräder angetrieben sind, ist die Traktion besser, ebenfalls weil sie mehr Auflast haben. Aus dem gleichen Grund ist ein einzelnes angetriebenes Hinterrad nur wenig schlechter als ein großes angetriebenes Hinterrad bei einem Dreirad.
* Man hat lauter gleich große Reifen, muss also nur einen Ersatzschlauch und -reifen mitnehmen.

Aber es gibt auch Nachteile:

* Ein zusätzliches Rad mit zusätzlicher bzw. größerer Schwinge bedeutet mehr Gewicht.
* Zwei angetriebene Hinterräder sind in Kurven problematisch; falls man nämlich eine starre Achse verwendet, dann radieren sie in der Kurve. Dies kann man zwar mit einem Differentialgetriebe lösen, aber das bedeutet wiederum mehr Gewicht und Komplexität und verursacht zusätzliche Antriebsverluste.
* Falls man nur ein Hinterrad antreibt, ist die Traktion geringer als bei einem Dreirad.
* Ein kleines Antriebsrad braucht eine höhere Übersetzung; evtl. ist ein Zwischengetriebe nötig, was wiederum ein höheres Gewicht verursacht.
* Die Kette verläuft in der Mitte, aber das Antriebsrad an der Seite; man braucht also eine schwere, da torsionssteife Achse.
* Kleine Räder haben einen höheren :term:`Rollwiderstand`.


.. index:: Radkasten, Reifenbreite, Karosserie, Strömung, Wartung, Reifenwechsel

Offene oder geschlossene Radkästen?
-----------------------------------
Beides hat Vor- und Nachteile:

* Geschlossene Radkästen sind aerodynamisch etwas besser.
* Bei geschlossenen Radkästen ist es aerodynamisch egal, wie weit innen das Rad sitzt; bei offenen Radkästen muss es möglichst bündig außen abschließen, d.h. die Einstellung ist flexibler.
* Geschlossene Radkästen sind oft etwas größer, entsprechend können breitere Reifen gefahren werden (die entsprechend höher sind). Bei offenen Radkästen versucht man dagegen, diese nicht so groß zu machen, da der Spalt zwischen Rad und Karosserie die Strömung stört.
* Velomobile mit geschlossenen Radkästen sind nicht unbedingt breiter, d.h. dann haben sie eine kleinere Spurbreite und sind deshalb etwas kippempfindlicher.
* Und natürlich ist bei offenen Radkästen der Aus- und Einbau der Räder sowie Einstellarbeiten an der Bremse wesentlich einfacher. Allerdings gibt es inzwischen auch Velomobile mit geschlossenen Radkästen, wo man über Wartungsklappen von außen die Achsen erreichen kann; dort ist zumindest der Aus- und Einbau der Räder ähnlich einfach.


.. index:: Schwinge, Hinterradnabe, Radnabenmotor, Tretlagermotor, Torsion, Gepäck, Wartung, Reifenwechsel

Einseitige oder zweiseitige Schwinge?
-------------------------------------
* Eine zweiseitige Schwinge ist die klassische Bauform; hier passen ganz normale Hinterradnaben. Bei einseitigen Schwingen braucht man spezielle Naben.
* Radnabenmotoren gibt es nur mit zweiseitiger Aufnahme. Bei einseitiger Schwinge kann man nur einen Tretlagermotor verwenden.
* Bei einer zweiseitigen Schwinge greifen die Kräfte mittig an; dadurch ist die Torsion deutlich geringer. Eine einseitige Schwinge muss wesentlich massiver gebaut werden, um ähnlich torsionssteif zu sein.
* Bei einer einseitigen Schwinge kann man den Reifen wechseln, ohne das Rad ausbauen zu müssen.
* Bei einer einseitigen Schwinge ist auf der linken Seite des Radkastens mehr Platz für Gepäck. Dafür ist auf der rechten Seite weniger Platz, weil die Schwinge deutlich voluminöser ist; allerdings wäre der Platz für Gepäck dort sowieso kaum nutzbar, weil dort die ölige Kette ist.
* Bei einer einseitigen Schwinge ist nur rechts ein Loch im Radkasten, durch das Dreck in den Innenraum eindringen kann. Die linke Seite bleibt komplett sauber.
* Bei einer einseitigen Schwinge besteht die Möglichkeit, das Rad auszubauen, ohne Ritzelpaket und Kette anzurühren. Bei einer zweiseitigen Schwinge wäre es schwierig, die Nabe zu zerlegen, weil seitlich kein Platz dazu ist.


.. index:: Carbon, Glasfaser, Steifigkeit

Warum baut man Velomobile aus Carbon oder Glasfaser?
----------------------------------------------------
Faserverstärkter Kunststoff hält ungefähr ähnlich viel aus wie Stahl – ist aber deutlich leichter. Allerdings müssen die Fasern immer in Zugrichtung belastet werden. Man braucht also immer Teile mit relativ großen Durchmessern und fließenden Übergängen, damit die auftretenden Kräfte immer irgendwo durch auf Zug belastete Fasern aufgenommen werden können. Bei Metall ist das nicht so; das hält in alle Richtungen gleich viel aus, und kann auch auf Druck belastet werden.

Einen dünnen Diamantrahmen mit hochbelasteten Teilen kann man darum sehr gut aus Stahl bauen; Carbon bietet da wenige Vorteile, außer man baut deutlich voluminöser. Bei einem Velomobil hat man es dagegen mit großen Flächen zu tun, die gering belastet sind. Das würde aus Stahlblech viel zu schwer – aber mit Carbon kann man eine dünne tragende Hülle bauen, die trotzdem steif genug ist, um Antriebskräfte zu übertragen. So kann man auf einen zusätzlichen tragenden Innenrahmen verzichten; die Bauteile kombinieren sowohl aerodynamische als auch tragende Funktion.


.. index:: Steifigkeit, Karosserie, Wellblech

Warum gekrümmte Flächen?
------------------------
Wegen der Steifigkeit. Diese hängt u.a. von der Dicke des Materials ab. Wenn man das Material biegt, dann wird es auf der Außenseite gedehnt und auf der Innenseite gestaucht. Und je weiter Innenseite und Außenseite voneinander entfernt sind, desto stärker müssen Dehnung/Stauchung sein, und desto steifer ist das Material.

Das ist der Grund, warum stark belastete Bauteile gerne recht voluminös gebaut werden – ein doppelt so großes Rohr hat einen doppelt so großen Umfang, braucht also doppelt so viel Material und ist doppelt so schwer. Da aber bei Biegung Innen- und Außenseite doppelt so weit voneinander entfernt sind, ist es viermal so steif, bzw. kann bei gleicher Steifigkeit entsprechend dünnwandiger gebaut werden und wird in der Summe leichter.

Das betrifft aber nicht nur geschlossene Formen wie Rohre, sondern auch offene Flächen. Wenn man ein Material in einer Richtung biegt, dann erhöht sich dadurch die Dicke für eine Biegung senkrecht dazu. Ein 1 mm dickes Material, das man 1 cm weit nach unten biegt, ist in die andere Richtung deshalb so steif wie ein Material, das 1 cm dick ist. Genau das macht Wellblech oder Wellpappe so steif gegenüber dem glatten Material. Oder: Wenn man ein Stück Pizza in Querrichtung zusammendrückt, dann hängt es in Längsrichtung nicht mehr herunter, sondern wird dort steifer.

Und das ist der Grund, warum gekrümmte Flächen in der Karosserie eines Velomobils einen deutlichen Beitrag zur Steifigkeit leisten können; beispielsweise ist der Kettenkanal eine in Querrichtung stark gekrümmte Form, und deshalb ist er in Längsrichtung unglaublich steif und kann den genau dort wirkenden Kettenzug gut aushalten.


.. index:: Fußlöcher, Torsion, Steifigkeit, Winter

Warum haben viele Velomobile keine Fußlöcher?
---------------------------------------------
* Weil diese aerodynamisch nachteilig sind.
* Weil sie auch die Torsionssteifigkeit beeinträchtigen; bei einem solchen Velomobil wird also entweder mehr Material gebraucht, um die selbe Steifigkeit zu erreichen, oder die Kraftübertragung ist weniger effizient.
* Weil von unten Dreck, Nässe und im Winter Schnee eindringen kann.


.. index:: Haube, Glaskuppel, Visier, Scheibenwischer, beschlagene Scheiben

Glaskuppel oder Motorradvisier?
-------------------------------
Zugegeben, so ein rollendes Ei mit Glaskuppel sieht schon sehr schick aus, und man hat von dort aus einen viel schöneren Panoramablick als aus einem dunklen Velomobil mit Fensteröffnungen in der :term:`Haube`. Aber bei Regen, Kälte und Dunkelheit hat es massive Nachteile, denn die Regentropfen verschlechtern die Sicht, gerade in Zusammenhang mit entgegenkommenden Scheinwerfern, und so eine Scheibe beschlägt auch. Das hat man bei einem Motorradvisier natürlich auch alles, aber dort ist die Fläche, die man sauber halten muss, viel kleiner. Zudem kann man ein Visier problemlos austauschen, wenn es durch zu viel Wischen verkratzt ist. Und dann steht ein Visier auch relativ steil, während man durch so eine Glaskuppel in einem viel flacheren Winkel durchschaut. Bei Autos sind diese Probleme lösbar, nämlich mit einem starken Gebläse und tausenden Watt Motor-Abwärme und einem starken Scheibenwischer. So ein Velomobil ist aber letztendlich eher ein Schönwetterfahrzeug. Zu sonnig darf es aber auch nicht sein, denn unter der Glaskuppel heizt es sich sonst wie in einem Treibhaus auf.


.. index:: Haube, Fenster, Visier, Rückspiegel

Sieht man denn mit einer Haube und ihren winzigen Fensterchen genug?
--------------------------------------------------------------------
Eigentlich schon.

Man hat natürlich ein deutlich kleineres Blickfeld, als wenn man im Cabrio-Modus fährt; vor allem nach oben ist die Sicht natürlich eingeschränkt. Aber nach vorne und zur Seite sieht man genug – ungefähr ähnlich viel wie im Auto. Man sitzt schließlich sehr viel näher an den Fenstern, so dass diese den gleichen Blickwinkel bieten, obwohl sie kleiner sind. Und 95% der Zeit muss man nur vorne durch das Visier schauen, um den gesamten Verkehr zu überblicken; nur bei Abzweigungen braucht man den Schulterblick, um zur Seite oder schräg nach hinten zu schauen. Und mit den Rückspiegeln sieht man den Verkehr hinter sich.



Lenkung
=======

.. index:: Panzerlenkung, Tiller, Lenkhebel, Armstützen, Bremshebel, Lenkrollradius, Doppelmanta, Schaltzug, Bremszug, Lenkbrücke, Spurstange

Panzerlenkung oder Tiller?
--------------------------
Das ist größtenteils Geschmackssache; aber beide Mechanismen haben Vor- und Nachteile:

* Beim :term:`Tiller` ist die Position der Hände flexibel; man kann steuern, egal wo man den Lenker hält.
* Bei der :term:`Panzerlenkung` ist die Position der Lenkhebel fest; manche bevorzugen das, weil sie damit präziser und schneller steuern können, was vor allem bei Rennen interessant ist.
* Da Panzerlenkungen oft sehr empfindlich ansprechen, reichen dort winzige Fingerbewegungen aus. Allerdings sollten dafür die Arme gut durch Armstützen fixiert sein, damit nicht jede Bodenwelle zu einer unfreiwilligen Lenkbewegung führt.
* Bei einem Tiller sind mechanisch gekoppelte Bremshebel einfach zu realisieren – die Bremshebel sind sowieso nebeneinander, und müssen nur noch verbunden werden. Bei einer Panzerlenkung kann man höchstens hydraulische Bremsen installieren und die Hydraulikschläuche koppeln, und bräuchte dann noch eine Rückfallebene, damit ein Hydraulik-Leck nicht zum Ausfall aller Bremsen führt.
* Aus diesem Grund ist bei einer Panzerlenkung eine gutmütige Lenkung nötig, d.h. ohne oder mit negativem :term:`Lenkrollradius`. Bei einem Tiller hat man entweder sowieso gekoppelte Bremsgriffe, oder kann zumindest die einzelnen Bremsgriffe gemeinsam betätigen, hat deshalb ein deutlich vorhersehbareres Verhalten.
* Bei einem Tiller sind die Arme weiter oben, d.h. man kann bei heißem Wetter auch mal die Arme im :term:`Doppelmanta`-Haltung aus dem Cockpit hängen und trotzdem noch lenken. Das ist bei einer Panzerlenkung vollkommen ausgeschlossen, weil zumindest eine Hand unten am Lenkhebel sein muss, und man dazu ziemlich schief sitzen müsste, um die andere Hand nach oben zu bringen.
* Bei einer Panzerlenkung müssen die Arme neben dem Körper sein; dieser Platz kann dann nicht für Gepäck genutzt werden. Bei einem Tiller sind die Ellenbogen viel höher.
* Bei der Panzerlenkung sind die Brems- und Schaltzüge kürzer und reibungsärmer.
* Bei der Panzerlenkung ist der Oberkörper besser belüftet, weil die Hände nicht davor sind.
* Ein Tiller ist mechanisch komplizierter, mit einem Kreuzgelenk auf der :term:`Lenkbrücke` und einer geteilten :term:`Spurstange`.
* Bei der Panzerlenkung dient die Spurstange nicht zur Lenkung, sondern die Lenkhebel sind direkt mit den Lenkplatten verbunden; d.h. die Spurstange koppelt nur die beiden Räder.
* Bei der Panzerlenkung sind dafür allerdings Löcher unten in der Lenkbrücke nötig, durch die Wasser eindringen kann, wenn man durch eine tiefe Pfütze fährt.


.. index:: Hinterradlenkung, Vorderradlenkung, Heck, Zentrifugalkraft, übersteuern, Meufl

Warum wird nicht hinten gelenkt?
--------------------------------
Eine Hinterradlenkung ist schwieriger beherrschbar. Bei der Kurvenfahrt zieht die Zentrifugalkraft das Fahrzeug auf die Kurvenaußenseite. Bei einer Vorderradlenkung müssen die Vorderräder in Richtung Kurveninnenseite steuern, d.h. beide Kräfte wirken entgegengesetzt, das Fahrzeug muss aktiv in die Kurve gelenkt werden. Bei einer Hinterradlenkung ist das anders herum; um in die Kurve zu fahren, muss das Heck nach außen schwenken, und wird mit der zunehmenden Zentrifugalkraft immer weiter nach außen gezogen – es neigt zum Übersteuern. Daher bräuchte es Gegenmaßnahmen, um bei hohen Geschwindigkeiten beherrschbar zu bleiben. Beispiel: Das *kleine Schwarze* von :term:`Harald Winkler <Meufl>` hat ein Gegengewicht, das in der Kurve von der Fliehkraft nach außen gezogen wird und dabei die Lenkung gerade stellt.


.. index:: Lenkrollradius, Einspurer, Mehrspurer, Lenkachse, Spurpunkt, Geradeauslauf

Was hat es mit dem Lenkrollradius auf sich?
-------------------------------------------
Bei einem :term:`Einspurer` befindet sich das Vorderrad in der Längsachse des Fahrrads und das Steuerlager über dem Vorderrad, d.h. die Lenkachse geht durch das Rad. Das ist bei einem Velomobil nicht unbedingt so: Da die Vorderräder einseitig aufgehängt sind, und es so etwas wie ein Steuerlager gar nicht gibt, geht die Lenkachse nicht unbedingt durch das Vorderrad, sondern trifft daneben auf den Boden. Wenn man also die Lenkung einschlägt, dann dreht sich das Vorderrad nicht auf der Stelle, sondern rollt auf einer Kreisbahn um den Schnittpunkt zwischen Lenkachse und Fahrbahn. Und der Radius dieser Kreisbahn ist der Lenkrollradius. Dieser kann übrigens positiv oder negativ sein:

* Wenn die Lenkachse neben dem Vorderrad verläuft, d.h. auf einen Punkt innen neben dem Vorderrad zeigt, dann spricht man von einem positiven Lenkrollradius.

* Wenn die Lenkachse schräg steht, so dass ihre Verlängerung durch das Vorderrad geht und auf der Außenseite auf den Boden trifft, spricht man von einem negativen Lenkrollradius.

Wichtig ist der Lenkrollradius beim Bremsen: Wenn ein Vorderrad bremst, zieht es das Federbein nach hinten – und zwar nicht direkt, sondern tangential über einen Hebel, dessen Länge eben der Lenkrollradius ist. Dieser Hebel erzeugt ein Drehmoment am Federbein. Bei positivem Lenkrollradius zieht das Rad das Federbein an der Außenseite nach hinten – damit lenkt das Rad nach außen, d.h. das Velomobil fährt in die die Richtung, auf der gebremst wird. Bei negativem Lenkrollradius ist es anders herum, das Vorderrad lenkt nach innen, das Velomobil fährt in die Richtung, auf der nicht gebremst wird.

Was ist nun besser? Idealerweise verhält sich das Velomobil neutral und lenkt überhaupt nicht beim Bremsen. Allerdings sorgt eine einseitige Bremsung auch ohne Lenkrollradius immer für eine Richtungsänderung – das gebremste Rad legt einen kürzeren Weg zurück, dort ist also die Kurveninnenseite. Daher tendiert man zu einem leicht negativen Lenkrollradius, der eine gegenteilige Wirkung hat, und das erwähnte Verhalten somit kompensiert.


.. index:: MacPherson, Federbein, Lenkachse, Längslenker, Querlenker, Lenkplatte, Lenkrollradius, Radkasten

Wo ist denn überhaupt die Lenkachse bei einem MacPherson-Federbein?
-------------------------------------------------------------------
Das ist gar nicht so leicht zu sagen. Auf den ersten Blick dreht sich beim Lenken das Federbein um sich selbst. Das trifft aber nur für das obere Ende zu: Dort ist das Federbein am Radkasten befestigt, und dreht sich um diese Befestigung. Unten gibt es dagegen gar keinen festen Drehpunkt.

Das Federbein wird unten von :term:`Längslenker` und :term:`Querlenker` gehalten (siehe: `Wie heißen denn die ganzen Teile am Fahrwerk?`_); deren Zusammenspiel ergibt einen virtuellen unteren Drehpunkt. Sowohl der Längs- als auch der Querlenker definiert zusammen mit dem oberen Drehpunkt am Radkasten jeweils eine Ebene, und die Schnittlinie dieser beiden Ebenen ist die Lenkachse. Da sich diese Schnittlinie in der Regel außerhalb der Längs- bzw. Querlenker befindet, ist es möglich, dass die Drehachse durch die Speichen oder sogar bis auf die Außenseite des Rades verläuft – obwohl das :term:`Federbein` und die :term:`Lenkplatte` auf der Innenseite sind.

Man kann entsprechend einen negativen Lenkrollradius erreichen, indem man den Winkel zwischen Längs- und Querlenker kleiner macht, so dass sich diese weiter außen schneiden. Das geht beispielsweise folgendermaßen:

* Der Längslenker wird nicht vorne am Radkasten befestigt, so dass er ungefähr parallel zur Fahrzeug-Längsachse stünde, sondern innen am Radkasten, so dass er schräg nach außen verläuft.

* Die Lenkplatte wird nach vorne verlängert und der Längslenker gekürzt, so dass dieser weniger parallel auf die Lenkplatte trifft.

Die virtuelle Drehachse ist nicht fix; da sich beim Einlenken die Längs- und Querlenker relativ zueinander bewegen, verändert sich mit dem Lenkeinschlag die Lenkachse und damit der Lenkrollradius. Entscheidend für die Fahrdynamik ist aber der Lenkrollradius bei Geradeausfahrt, also bei hoher Geschwindigkeit und kleinen Lenkbewegungen.


.. index:: Nachlauf, Lenkkopfwinkel, Spurpunkt, Radaufstandspunkt, Einspurer, Mehrspurer, Schwerpunkt, Geradeauslauf, Windempfindlichkeit

Was ist der Nachlauf, und was ist seine Funktion?
-------------------------------------------------
Bei einem konventionellen Fahrrad steht das Steuerlager üblicherweise nicht senkrecht. Dieser sogenannte Lenkkopfwinkel beträgt üblicherweise um die 70°, d.h. das Steuerrohr zeigt 20° nach vorne. Entsprechend liegt der :term:`Spurpunkt` vor dem Vorderrad. Das ergibt dann ein Rückstellmoment – das Vorderrad wird hinter der Lenkachse hergezogen, die Lenkung stellt sich dadurch von alleine gerade. Je größer der :term:`Nachlauf`, d.h. der Abstand zwischen Spurpunkt und Radaufstandspunkt, desto ausgeprägter ist dieses Verhalten.

Bei einem :term:`Einspurer` hat dieses Verhalten eine wichtige Funktion: Wenn das Fahrrad bei langsamer Fahrt zur Seite kippt (d.h. der :term:`Schwerpunkt` zur Seite wandert), lenkt es automatisch in diese Richtung, d.h. das Rad fährt wieder unter den Schwerpunkt und wirkt dem Kippen somit entgegen. Kurvenfahrt und Neigung sind somit untrennbar verbunden sind – mit einer Neigung kann man eine Kurvenfahrt auslösen (z.B. beim freihändigen Fahren oder Schieben), und durch die labile Lage des Schwerpunkts (geradeaus ist das Steuerlager am höchsten über dem Boden) führt ein Lenkeinschlag umgekehrt auch zur Neigung.

Bei einem :term:`Mehrspurer` ist das grundsätzlich anders; dort sind Lenkung und Neigung komplett entkoppelt – eine Kurvenfahrt schiebt nicht den Schwerpunkt zurück in die Mitte, sondern der bleibt immer gleich, löst also umgekehrt auch keine Kurvenfahrt aus. Bei langsamer Fahrt ist der Nachlauf also wirkungslos. Bei schneller Kurvenfahrt gibt es zumindest noch die Fliehkraft, auf die der Nachlauf reagiert; somit wirkt der Nachlauf einfach nur jeder schnellen Kurvenfahrt entgegen. Ein Velomobil kann sich zwar sehr wohl neigen – nämlich wenn es bei rasanter Kurvenfahrt kippt. Dann hebt sich das kurveninnere Rad an, und das Velomobil wird zu einem Einspurer – aber erst wenn es so weit gekippt ist, dass sich der Schwerpunkt jenseits des kurvenäußeren Rads befindet, trägt auch die Gewichtskraft zu einem Gegenlenken bei. Dann ist es aber schon viel zu spät für die zaghafte Reaktion einer kleinen rückstellenden Kraft, sondern hier muss man sofort beherzt gegenlenken und bremsen.

Somit könnte der Nachlauf im Velomobil zumindest den :term:`Geradeauslauf` bei schneller Fahrt verbessern. Aber erstens ist der Platz im Radkasten beschränkt, man kann das Federbein nur ganz wenig schräg stellen. Und zweitens kann sich die Lenkung bei einem Tadpole-Mehrspurer nicht so frei drehen wie bei einem Einspurer, denn das ganze Lenkgestänge (also Lenkhebel bzw. Tiller) muss noch mitbewegt werden, was mehr Kraft kostet bzw. mehr Nachlauf erfordern würde.

Der Nachlauf ist aber auch relevant für Kräfte von außen. Er ist beispielsweise der Grund, warum warum man ein Zweirad am Sattel schieben und durch seitliches Kippen lenken kann. Aber auch Wind stellt eine Seitenkraft dar, gerade beim Velomobil mit seiner großen seitlichen Angriffsfläche. Wenn dieser das Velomobil zur Seite drückt, sorgt der Nachlauf dafür, dass die Räder entsprechend einlenken – während der Nachlauf also den Geradeauslauf verbessert, erhöht er die Windempfindlichkeit.


.. index:: Ackermann-Bedingung, Spurstange

Was hat es mit der Ackermann-Bedingung auf sich?
------------------------------------------------
Bei Geradeausfahrt sind alle Räder eines mehrspurigen Fahrzeugs parallel. In der Kurve ist das anders; da nicht alle Räder die gleiche Entfernung zum Kurvenmittelpunkt haben, müssen sie auf verschieden engen Kreisbögen fahren, d.h. das kurveninnere Rad muss stärker eingeschlagen sein als das kurvenäußere Rad. Dies wird realisiert durch das sogenannte Lenktrapez – die :term:`Spurstange` ist kürzer als der Abstand der Drehpunkte der Vorderräder. Letztendlich müssen sich die Verlängerungen aller Achsen bei Kurvenfahrt in einem Punkt schneiden. Ist das nicht der Fall, radieren die Räder auf dem Boden.

Bei Velomobilen ist das aber kein großes Problem, da enge Kurven nur einen verschwindend kleinen Anteil der Gesamtstrecke ausmachen.

.. _fig_ackermann-bedingung:

.. figure:: images/Ackermann_radius_M_nebeneinander.*
    :alt: Ackermann-Bedingung
    :width: 100%

    Ackermann-Bedingung: In der Kurve lenken alle Räder um einen gemeinsamen Mittelpunkt.



Fahrwerk
========

.. index:: Sturz, Federbein, Spurstange, Längslenker, Querlenker

Warum stehen die Vorderräder schräg? Bremst das nicht?
------------------------------------------------------
Natürlich tut es das. Man stelle sich vor, ein Rad stünde extrem schräg, fast waagerecht – das würde fast überhaupt nicht mehr rollen, sondern tangential zur Felge über die Reifenflanken scheuern.

Allerdings ist Leichtlauf nicht das einzige Ziel. Ein anderes ist, eine möglichst große Spurbreite zu erreichen – dadurch kippt das Velomobil nicht so leicht, und man kann schneller durch die Kurven fahren. Ein breites Velomobil hätte allerdings eine größere Querschnittsfläche. Während in der Mitte der Platz gebraucht wird, um neben den Beinen des Fahrers auch Federbein, Trommelbremse und Nabe unterzubringen, braucht man unten und oben deutlich weniger Platz. Wenn man nun die Räder mit einem negativen :term:`Sturz` versieht, bekommt man bei gleichem Nabenabstand eine größere Spurbreite und oben eine geringere Breite, d.h. das Velomobil ist oben schmäler (= weniger Luftwiderstand) und unten breiter (= höhere Kurvengeschwindigkeit), und das erkauft man sich mit mehr Rollreibung. Aus diesem Grund stehen bei den meisten Velomobilen auch die Achsen der Vorderräder nicht senkrecht auf den Federbeinen, sondern im Winkel von 86°; das kippt die Räder oben näher an das :term:`Federbein` heran. Der negative Sturz muss deshalb aber nicht 4° betragen, sondern die Federbeine selbst müssen ja auch nicht senkrecht stehen. Deren Winkel hängt von der Einstellung von :term:`Spurstange`, :term:`Längslenker` und :term:`Querlenker` ab.


.. index:: Fahrwerk, Längslenker, Querlenker, Spurstange, Lenkplatte, Federbein

Wie heißen denn die ganzen Teile am Fahrwerk?
---------------------------------------------
Eine Skizze sagt mehr als tausend Worte:

.. _fig_lenkgestaenge:

.. figure:: images/Lenkplatte_Querlenker_Laengslenker_Spurstange.*
    :alt: linkes Vorderrad, Aufsicht
    :width: 100%

    Skizze des linken Vorderrads (Aufsicht)

Wie das in Realität aussieht sieht man in :numref:`fig_mango_steering`.

* Das Velomobil ist auf der Skizze grau, das Rad schwarz dargestellt. Fahrtrichtung: nach rechts.

* Der :term:`Längslenker` ist grün dargestellt; er ist meist vorne am :term:`Radkasten` festgeschraubt. Zusammen mit dem Querlenker führt er die Lenkplatte. Die gestrichelte Linie zeigt, wie er sich bewegen kann (wobei Lenkplatte und Querlenker der Bewegung folgen müssen).

* Der :term:`Querlenker` ist blau dargestellt; er ist typischerweise in der Fahrzeugmitte unter der :term:`Lenkbrücke` befestigt. Den Bewegungsradius zeigt ebenfalls eine gestrichelte Linie.

* Die :term:`Spurstange` ist rot dargestellt; sie verbindet die Lenkplatten beider Vorderräder, und evtl. ist daran noch der :term:`Tiller` befestigt. Die Spur wird eingestellt, indem man die Länge der Spurstange verändert; siehe `Wie findet man heraus, ob die Spur richtig eingestellt ist?`_

* Die :term:`Lenkplatte` ist orange dargestellt; sie befindet sich am unteren Ende des :term:`Federbeins <Federbein>`, und die drei obigen Lenkgestänge sind daran an Kugelköpfen befestigt. Da deren Abstand konstant ist, ist die Drehung der Lenkplatte dadurch festgelegt, dass sich die Befestigungspunkte von Längs- und Querlenker nur entlang der gestrichelten Linien bewegen können. Siehe: `Wo ist denn überhaupt die Lenkachse bei einem MacPherson-Federbein?`_

Bei einer :term:`Panzerlenkung` sind hinten an der Lenkplatte auch noch die Hebel, die zu den Panzerlenkhebeln führen, befestigt.

.. _fig_mango_steering:

.. figure:: images/Lenkgestaenge-Mango.jpg
    :alt: Lenkgestänge beim Mango
    :width: 100%

    Lenkgestänge bei einem Mango (Blick von vorne auf die Unterseite; Teile ausgebaut und außen an die entsprechenden Stellen gelegt). Von vorne nach hinten: Längslenker, Querlenker, Spurstange (geteilt und mit Anschluss an Tiller).


.. index:: Vorspur, Nachspur, Rollwiderstand, Reifenverschleiß, Lenkspiel, dynamische Radlastverteilung, Rolltest

Vorspur, Nachspur – was ist richtig?
------------------------------------
Gar keine. Die Räder sollen während der Fahrt vollkommen parallel stehen. Alles andere erhöht den Rollwiderstand und vor allem den Reifenverschleiß.

Bei Autos verwendet man manchmal eine leichte Vorspur oder Nachspur; das kann man sich erlauben, weil die Reifen unempfindlicher sind und der Motor genug Leistung hat. Die Vorteile:

* Lenkspiel: Eine verschlissene Lenkung mit Spiel kann flattern. Eine leichte Vorspur oder Nachspur zwingt die Lenkung in eine feste Lage.
* Einlenken: In der Kurve verlagert sich das Gewicht auf die Außenräder. Eine Vorspur am Vorderrad unterstützt entsprechend das Einlenken, weil das belastete Außenrad dann bereits in der Geradeausstellung etwas einlenkt, und der gegenteilige Einfluss des Innenrads reduziert wird, weil es entlastet wird.
* Am Hinterrad ist die Situation umgekehrt; hier unterstützt eine Nachspur das Einlenken. Allerdings führt das dann eher zum Übersteuern – durch die Kurvenfahrt wird Gewicht auf das äußere Rad verlagert, dieses lenkt durch die Nachspur weiter nach außen, was die Lenkbewegung verstärkt, und noch mehr Gewicht auf das äußere Rad bringt.
* Die Antriebskraft hat eine Auswirkung auf die Spur; so kann eine leichte Vorspur bei angetriebenen Hinterrädern bzw. eine leichte Nachspur bei angetriebenen Vorderrädern dafür sorgen, dass sich die Räder unter Last parallel stellen.

Bei Velomobilen ist dagegen eigentlich nur der Rollwiderstand interessant; und hier kommt es darauf an, wie sich die Spur zwischen Stillstand und Fahrt verändert. So kann es beispielsweise sein, dass man im Stillstand eine leichte Vorspur einstellen muss, damit die Räder optimal rollen. Daher Messungen immer mit Fahrer und Beladung machen, und erst nachdem man ein Stück gerollt ist.


.. index:: Vorspur, Nachspur, Reifenverschleiß, Spurstange, Rolltest

Wie findet man heraus, ob die Spur richtig eingestellt ist?
-----------------------------------------------------------
Wenn die Vorderreifen nach wenigen 100 km verschlissen sind, war die Spur falsch eingestellt.

Man kann die Spur aber relativ einfach vermessen. Da haben sich im Wesentlichen zwei Herangehensweisen bewährt:

* Stellung der Räder vermessen: Da gibt es verschiedene Verfahren; z.B. zwei Latten innen an die Räder legen, und vorne und hinten jeweils deren Abstand messen, oder z.B. Laserpointer an den Naben befestigen. Es empfiehlt sich, bei der Messung im Velomobil zu sitzen und von einer zweiten Person ablesen zu lassen, damit das Fahrwerk gleich wie in Fahrt belastet ist. Die Räder müssen möglichst parallel stehen.

* Rolltest: Man rollt bei verschiedenen Einstellungen der :term:`Spurstange` mit dem Velomobil bergab und misst, wie weit man kommt. Hier ist wichtig, dass der Startpunkt immer identisch ist, und der Rollweg auch möglichst glatt (z.B. ohne Schlaglöcher) und gerade (ohne Kurven) ist. Die Strecke muss nicht groß sein; z.B. kann man in einer Tiefgarage oder einem Keller von einer Rampe rollen, die nur einige Zentimeter hoch ist.

Bei einem Rolltest sind die Entfernungen der besten Einstellungen recht nahe beieinander; je schlechter die Spur eingestellt ist, desto stärker weichen die gerollten Entfernungen voneinander ab.


.. index:: Bump Steer, Federung, Querlenker, Spurstange

Was ist „Bump Steer“?
---------------------
Bei Bodenunebenheiten reagiert die Federung, d.h. das Vorderrad bewegt sich relativ zum Fahrzeug auf und ab, und mit ihm der :term:`Querlenker`. Dieser ist auf der anderen Seite am Fahrzeug befestigt, d.h. das am Vorderrad befestigte Ende des Querlenkers beschreibt eine Kreisbahn. Wenn er nicht annähernd waagerecht steht und/oder nicht sehr lang ist, dann hat diese Bewegung auch eine nennenswerte seitliche Komponente – d.h. das Vorderrad wird näher an das Fahrzeug herangezogen bzw. weggeschoben. Und da der Querlenker nicht unbedingt in Richtung des virtuellen Drehpunkts verläuft, führt das zu einer leichten Lenkbewegung.

Idealerweise ist aber der Querlenker möglichst lang und steht im belasteten Zustand möglichst waagerecht. Zudem ist idealerweise die :term:`Spurstange` geteilt, so dass die Teile genauso lang wie die Querlenker sind und ebenso möglichst waagerecht stehen. So kann eine Lenkbewegung durch das Ein- und Ausfedern verhindert werden.


.. index:: Fahrwerk, Radkasten, Lenkrollradius, Vorspur, Nachspur, Querlenker, Bump Steer, Ackermann-Bedingung

Wie sollte das Fahrwerk eingestellt sein?
-----------------------------------------

* Bei offenen :term:`Radkästen <Radkasten>` sollte das Federbein so geneigt sein, dass das Rad bündig zum Rand des Radkastens steht.
* Der :term:`Lenkrollradius` sollte leicht negativ sein (siehe: `Was hat es mit dem Lenkrollradius auf sich?`_).
* Die Spur sollte neutral sein, d.h. die Räder stehen parallel (abgesehen vom Sturz; siehe `Vorspur, Nachspur – was ist richtig?`_).
* Die :term:`Querlenker` sind möglichst lang und möglichst waagerecht, wenn das Fahrzeug belastet ist (siehe: `Was ist „Bump Steer“?`_).
* Die Ackermann-Bedingung ist erfüllt (siehe: `Was hat es mit der Ackermann-Bedingung auf sich?`_).
* Die Lenkung ist idealerweise so übersetzt, dass sie nahe der Neutralstellung unempfindlich ist, denn bei hohen Geschwindigkeiten haben bereits kleine Lenkbewegungen großen Auswirkungen. Bei starken Lenkeinschlägen kann die Lenkung dagegen empfindlicher reagieren, denn diese treten nur bei langsamer Fahrt auf.


.. index:: Geradeauslauf, Windempfindlichkeit

Warum ist ein guter Geradeauslauf bei Wind nicht ausreichend?
-------------------------------------------------------------
Manche Fahrer wundern sich, dass ihr Fahrzeug, das normalerweise sehr gutmütig ist und auch bei hohen Geschwindigkeiten wie auf Schienen läuft, von Windböen spürbar ausgelenkt wird. Der Grund dafür ist:

* Der :term:`Geradeauslauf` wird vor allem vom Fahrwerk bestimmt – also u.a. von Lenkgestänge und Radstand (siehe: `Was hat es mit dem Lenkrollradius auf sich?`_ und `Wie sollte das Fahrwerk eingestellt sein?`_).

* Die Windempfindlichkeit hängt dagegen auch von der Karosserieform ab (siehe: `Wie kann man die Windempfindlichkeit reduzieren?`_).

Es sind also grundsätzlich zwei unterschiedliche Dinge; ein spurstabiles Fahrzeug ist nicht unbedingt unempfindlich gegen Windböen, und ein windunemfindliches Fahrzeug hat nicht unbedingt einen guten Geradeauslauf.



Räder und Reifen
================

.. index:: Felge, ETRTO, Winterreifen, Spikes, Radkasten, Entfaltung, Kettenblatt

Welche Radgröße?
----------------
Bei den Vorderrädern ist es einfach: 406 mm (nach :term:`ETRTO`; entspricht 20 Zoll). Alle anderen Größen kleiner Räder sind deutlich ungebräuchlicher, und entsprechend gering ist die Auswahl an Reifen und Felgen. Lediglich 16 Zoll (349 mm bzw. 305 mm) sind noch halbwegs verbreitet, würde aber keinen echten Vorteil bringen. Ein paar Bastler haben für ihre 20-Zoll-Velomobile kleinere Laufräder aufgebaut, um darauf breitere Reifen (z.B. Winterreifen mit Spikes) unterzubringen; das ist aber nur nötig, wenn der Platz im Radkasten sehr knapp ist.

Beim Hinterrad hat man mehr Auswahl; bei vielen Velomobilen sind möglich:

* 559 mm (26 Zoll, Mountainbike)
* 571 mm (27 Zoll, Triathlon)
* 584 mm (27.5 Zoll)
* 622 mm (28 Zoll, Rennrad)

Der Einfluss auf die :term:`Entfaltung` ist eher gering und kann durch passende Kettenblätter ausgeglichen werden. Daher ist das wichtigste Kriterium, bei welcher Größe es die besten Reifen gibt. Die schnellsten Reifen gibt es grundsätzlich für Rennräder (also 622 mm), diese sind aber recht schmal (und breite passen auch oft nicht in den Radkasten). Für Mountainbikes gibt es zwar auch sehr viele Reifen, aber fast nur breit und mit Stollen. Schmale, schnelle Reifen in 559 mm gibt es immer weniger, so dass der beste Kompromiss (schnelle, nicht zu schmale Reifen) bei 584 mm liegen dürfte.


.. index:: Felge, Reifenbreite, Felgenhorn, Snakebite

Warum verwendet man im Velomobil so breite Felgen?
--------------------------------------------------
Die Felgenbreite hängt mit der Reifenbreite zusammen:

* zu schmal: Der Reifen hält nicht richtig auf der Felge, weil er vom Felgenhorn aus nicht nahezu senkrecht nach oben geht (also orthogonal zur Anpressrichtung), sondern stark nach außen geht. Zudem ist die Lauffläche weit von der Felge weg, d.h. Seitenkräfte ziehen über einen großen Hebel am Felgenhorn.
* zu breit: Die Lauffläche ist dicht über der Felge, d.h. der Reifen kann wenig einfedern und schlägt schneller durch. Wenn dabei der Schlauch eingequetscht wird, bekommt er die klassischen Snakebite-Löcher.

Im Velomobil sind :term:`Snakebites <Snakebite>` kein großes Problem, da sich das Gewicht auf ein zusätzliches Rad verteilt. Zudem fährt man typischerweise auf der glatten Straße, und nicht über Kanten und Steine. Dagegen sind Seitenkräfte in Kurven ein großes Problem – da man sich nicht in die Kurve legt, wirken die Kräfte nicht immer nur senkrecht zur Lauffläche, sondern oft auch von der Seite. Daher tendiert man zu breiten Felgen. Bei offenen Radkästen haben die auch den Vorteil, dass der Reifen seitlich weniger über die Felge hinaus ragt, d.h. das Rad ist glatter und damit aerodynamischer.


.. index:: Reifenbreite, Radkasten, Lenkeinschlag, Wendekreis, Snakebite

Welche Reifenbreite?
--------------------
Die Reifenbreite wird von der Größe der Radkästen begrenzt; und selbst wenn an den Vorderrädern ein breiter Reifen grundsätzlich hineinpassen würde, reduziert das oft den Lenkeinschlag deutlich, d.h. der Wendekreis wird deutlich größer.

Ansonsten verteilt sich ja das Gewicht auf mindestens drei Reifen, d.h. ein einzelnes Rad muss weniger Auflast aushalten als bei einem konventionellen Fahrrad. Es spräche also nichts gegen sehr schmale Reifen, die anfälliger für :term:`Snakebites <Snakebite>` sind. Aber da die Reifen innerhalb der Radkästen laufen, ist ein schmaler Reifen aus aerodynamischen Gründen nicht nötig.

Letztendlich will man möglichst schnelle Reifen; und da scheint es die besten bei einer Breite von ca. 28 mm zu geben. Aber das hängt auch vom Straßenzustand ab – auf schlechten Straßen können breite Reifen, die einen höheren Rollwiderstand als die schmalen Rennreifen haben, trotzdem schneller sein, weil es weniger Gerüttel gibt.


.. index:: Reifendruck, Reifenlatsch, Snakebite, Seitenkraft, Rollwiderstand, Latex, Tubeless

Welcher Reifendruck?
--------------------
Grundsätzlich sind der vom Hersteller angegebene Minimal- und Maximaldruck sinnvolle Anhaltspunkte. Aber es kommt auch auf folgende Dinge an:

* Federweg des Reifens: Ein breiter Reifen ist auch höher, d.h. hat mehr Federweg. Um einen :term:`Snakebite` zu vermeiden, muss ein schmaler Reifen härter aufgepumpt sein, damit er trotz geringem Federwegs nicht durchschlägt.

* Federhärte des Reifens: Ein breiter Reifen federt bei gleichem Druck und gleicher Auflast weniger stark ein als ein schmaler Reifen; d.h. um den gleichen Federweg wie ein schmaler Reifen zu erreichen, muss ein breiter Reifen mit weniger Druck gefahren werden. (Dazu passt, dass ein breiter Reifen auch weniger Druck aushält; siehe: `Was hat es mit der Kesselformel auf sich?`_)

* Auflast: Je höher das Systemgewicht ist, umso höher muss der Druck sein, damit der Reifen gleich weit wie bei einer geringeren Auflast einfedert.

* Zahl der Räder: Ein Velomobil ist zwar schwerer als andere Fahrräder, aber dafür hat es auch mehr Räder. Die Auflast pro Rad ist also geringer, und entsprechend verformt sich der Reifen weniger – der Druck kann also geringer sein.

* Seitenkräfte: Da ein :term:`Mehrspurer` nicht in die Kurve kippt, wirken die Kräfte nicht nur radial, sondern es gibt auch starke Seitenkräfte. Bei zu wenig Druck kann sich der Reifen stark seitlich verformen, was auf Dauer den Reifen schädigt oder im Extremfall den Reifen von der Felge zieht. Daher darf selbst bei wenig Auflast und breiten Reifen der Druck nicht zu gering sein, um den Reifen sicher auf der Felge zu halten.

Wie hoch soll der Druck dann sein?

* Je höher der Druck, desto weniger verformt sich der Reifen; diese Walkarbeit kostet Energie, d.h. ein hoher Druck reduziert den Rollwiderstand. Allerdings nimmt dieser Effekt mit zunehmendem Druck ab; wenn sich ein harter Reifen kaum noch verformt, dann kann man mit noch mehr Druck die Verformung nur noch sehr wenig weiter reduzieren (siehe :numref:`fig_bicyclerollingresistance_hist`).

* Wenn der Reifen überhaupt nicht federt, dann wird bei Unebenheiten nicht nur der Reifen ausgelenkt, sondern das gesamte Fahrzeug mit Fahrer. Das kostet natürlich wesentlich mehr Energie, denn diese Beschleunigung kann kaum wieder in Vortrieb umgesetzt werden und ist verloren (siehe: `Braucht man Federung?`_ und `Kostet Federung nicht wertvolle Energie?`_). Das spricht für einen niedrigeren Druck.

Für einen möglichst geringen Rollwiderstand empfiehlt sich daher grundsätzlich ein hoher Druck; aber so niedrig, dass der Federweg des Reifens die hochfrequenten Unebenheiten einer rauen Straße, bei denen die Federung noch nicht anspricht, schluckt, statt Fahrzeug und Fahrer durchzurütteln. Hier ist ein flexibler Reifen (d.h. dünnwandig, weiche Gummimischung, dünner Schlauch bzw. Latex-Schlauch bzw. tubeless) im Vorteil, weil er nicht nur weniger Verluste bei Verformung verursacht, sondern sich auch besser den Unebenheiten anpasst bzw. ein niedrigerer Luftdruck weniger Mehrleistung kostet (siehe: :numref:`fig_bicyclerollingresistance_scatter_p_inc`).

.. _fig_bicyclerollingresistance_hist:

.. figure:: images/bicyclerollingresistance_hist_all_de.*
    :alt: Rollwiderstand von Rennradreifen bei verschiedenen Drücken
    :width: 100%

    Histogramm des Rollwiderstands von Rennradreifen bei verschiedenen Drücken, `gemessen auf einem Rollenprüfstand <https://www.bicyclerollingresistance.com/the-test>`_. Der Medianwert sinkt von 19.6 W bei 4.1 bar auf 14.2 W bei 8.3 bar; wie man sieht, verbessert eine Erhöhung auf 5.5 bar den Median um ganze 3 W, die weitere Erhöhung aber immer weniger.


.. index:: Kesselformel, Karkasse, Reifenlatsch, Snakebite, Felge, Rollwiderstand

Was hat es mit der Kesselformel auf sich?
-----------------------------------------
Eigentlich ganz einfach: Druck ist Kraft pro Fläche; d.h. wenn die Innenfläche des Reifens größer ist, wirkt bei gleichem Druck eine höhere Kraft auf den Reifen.

Wenn also ein Reifen doppelt so breit ist, dann hat er ungefähr auch die doppelte Höhe, d.h. der Umfang des Reifenquerschnitts ist ebenfalls doppelt so groß. Da der Umfang des Rades nahezu gleich ist, hat also das Innere des Reifens die doppelte Fläche – also wirkt auf die :term:`Karkasse` des Reifens die doppelte Zugkraft.

Das ist der Grund, warum breite Reifen viel weniger Druck vertragen als schmale Reifen. Aber breite Reifen brauchen auch weniger Druck, da erstens der :term:`Reifenlatsch` breiter ist – bei gleicher Auflast und gleichem Druck ist der Latsch deshalb kürzer, d.h. man braucht weniger Druck, damit der Reifenlatsch gleich kurz wie bei einem schmalen Reifen ist. Zweitens ist ein breiter Reifen höher, d.h. er hat viel mehr Federweg, bis es zu einem Durchschlag („:term:`Snakebite`“) kommt.

Man könnte natürlich einen breiten Reifen entsprechend massiver bauen, damit er mehr Druck aushält. Aber dadurch wird er auch steifer, d.h. der Rollwiderstand erhöht sich – den man durch höheren Druck ja eigentlich verringern will. Aber durch hohen Druck wird nicht nur der Reifen, sondern auch die Felge stärker belastet. Gerade Leichbaufelgen halten nur einen bestimmten Druck aus, der angegeben ist; wenn man also einen breiteren Reifen montiert, muss man nicht nur dessen Maximaldruck beachten, sondern auch den der Felge (der immer auf eine bestimmte Reifenbreite bezogen ist) – und zwar mit Hilfe der Kesselformel korrigiert. Wenn der Reifen also doppelt so breit ist, darf man die Felge nur mit der Hälfte ihres Maximaldrucks belasten.



Federung
========

.. index:: Federung, Beschleunigung, Kraftspitzen, Seitenführung

Braucht man Federung?
---------------------
Normalerweise ja. Das Hauptmerkmal eines Velomobils ist dessen Geschwindigkeit. Und eine hohe Geschwindigkeit bedeutet, dass das Überfahren eines Hindernisses samt der dadurch verursachten Geschwindigkeitsänderungin einer kürzeren Zeit passiert, d.h. die Beschleunigung ist höher. Und Beschleunigung mal Masse ist Kraft; erstere ist konstant, d.h. bei höherer Beschleunigung müssen Fahrzeug und Fahrer stärkere Kräfte ertragen. Zudem ist bei einer hohen Geschwindigkeit weniger Zeit, eine Richtungsänderung durch ein Hindernis zu korrigieren. Daher ist es sinnvoll, mit Hilfe einer Federung erstens die Kraftspitzen, die auf Fahrzeug und Fahrer wirken, zu reduzieren, und auch das Fahrzeug kontrollierbarer zu machen.

Bei den Vorderrädern kommt noch ein weiterer Effekt hinzu: Da die Räder nebeneinander sind, aber selten die Unebenheiten, kippelt das Fahrzeug dabei um die Längsachse. Der Fahrer wird nicht nur nach oben und unten beschleunigt, sondern auch seitlich hin- und hergeworfen, was sehr unangenehm sein kann.

Beim Hinterrad ist eine Federung aus einem anderen Grund wichtig: Wenn ein ungedämpftes Hinterrad auf Unebenheiten mit passender Frequenz trifft, kann es so stark springen, dass sich das Fahrzeug seitlich überschlägt (siehe: `Warum ist es gefährlich, wenn das Hinterrad die Seitenführung verliert?`_).


.. index:: Federung, Dämpfung, Bewegungsenergie, Muskel, Trittfrequenz, Kettenzug, Ritzel, Kettenlinie, Schwerpunkt, Wiegetritt, Aufrechtrad

Kostet Federung nicht wertvolle Energie?
----------------------------------------
Ja, natürlich. Beziehungsweise: nicht die Federung, sondern die damit verbundene Dämpfung ist nichts anderes als eine Energievernichtung. Aber: Was würde sonst mit der Energie passieren?

* Der Fahrer hat mühsam :term:`Bewegungsenergie` aufgebaut; und Bodenunebenheiten sorgen dafür, dass die Bewegungsenergie zu einem kleinen Teil in eine andere Richtung gelenkt wird – beispielsweise von vorwärts nach aufwärts, das Fahrzeug wird abgebremst und angehoben. Aber nur selten kommt danach eine passende Gegen-Unebenheit, die wie eine kleine Rampe das wieder nach unten fallende Velomobil nach vorne beschleunigt. Diese Energie ist zum größten Teil also sowieso verloren; und so vernichtet man sie lieber gezielt im Dämpfungselement, statt andere Teile des Fahrzeugs zu belasten.

* Mit Federung wird bei Unebenheiten idealerweise nur der ungefederte Teil des Fahrzeugs beschleunigt, und nur dessen Bewegungsenergie ist verloren. Dagegen muss ohne Federung das gesamte Fahrzeug mit Fahrer auf die Unebenheit reagieren, es muss also eine viel größere Masse beschleunigt werden – es geht also mehr Energie verloren. Eine gut ansprechende Federung macht also schneller.

* Eine Federung kostet zwar Gewicht, aber ein ungefedertes, schnelles Fahrzeug muss ein ganzes Stück massiver gebaut werden, um die höheren Kraftspitzen aushalten zu können.

* Ein gefedertes Fahrzeug bietet auch eine bessere Kontrolle durch mehr Bodenhaftung (und somit wird auf schlechten Abschnitten eine höhere Geschwindigkeit möglich).

* Ohne Federung wird auch der Fahrer (der schließlich den größten Teil der Gesamtmasse ausmacht) mitbeschleunigt, und der gibt die Energie auch nicht 1:1 wieder über das Fahrzeug an die Straße zurück, sondern dämpft es über seine Muskeln und Sehnen weg. Und das ist nicht nur unangenehm, sondern belastet auch die Muskeln – d.h. kostet Kraft, die man lieber in den Vortrieb investieren sollte.

Und dann gibt es noch die Befürchtung, dass eine Federung mit der Trittfrequenz mitwippt und einen Teil der Tretkraft auffrisst. Das kann natürlich grundsätzlich passieren, ist aber bei einem gut abgestimmten Fahrzeug vernachlässigbar. Denn erstens kann der Antrieb so gestaltet werden, dass der Kettenzug fast senkrecht zur Federungsrichtung erfolgt, und die Kette fast durch den Drehpunkt des Schwingengelenks geht, so dass die an der Federung angreifende Kraftkomponente sehr klein ist. Ganz verhindern kann man das nicht, weil sich durch das benutzte :term:`Ritzel` oder auch die Beladung die :term:`Kettenlinie` leicht verändert, und auch die Hebelverhältnisse in jedem Gang anders sind. Zweitens ist der Fahrer in einem Liegerad recht stark fixiert; und verändert sich der :term:`Schwerpunkt` nicht derart stark wie beim :term:`Wiegetritt` auf einem Aufrechtrad. Entsprechend weniger Beschleunigungen treten auf, die dann von der Dämpfung aufgefressen werden.


.. index:: Federung, Federweg, Schlagloch, Negativfederweg, Ansprechverhalten, Dämpfung, Gleitreibung

Was muss eine gute Federung erfüllen?
-------------------------------------
Federweg: Mit einem Velomobil will man schnell fahren und hat wenig Bodenfreiheit. Entsprechend fährt man vorwiegend auf der Straße, wo es keine riesigen Hindernisse zu überwinden gibt, sondern schlimmstenfalls Schlaglöcher. Entsprechend braucht man nicht viel Federweg. Ein großer Federweg wäre sogar nachteilig, weil z.B. die Radkästen entsprechend voluminöser sein müssten, was letztendlich die Außenmaße und die Aerodynamik verschlechtert. Zudem müsste auch der Negativfederweg größer sein, d.h. das Fahrzeug müsste höher sein, was die Kippanfälligkeit in Kurven verstärkt.

Ansprechverhalten: Bei hoher Geschwindigkeit sind die Beschleunigungen hoch, darum muss eine gute Federung sehr schnell und bereits bei kleinen Kräften ansprechen. Das betrifft vor allem die Dämpfung. In vielen Dämpfungsmechanismen tritt Gleitreibung auf, was ungünstig ist, denn vor der Gleitreibung muss erst einmal die Haftreibung überwunden werden. Deshalb sind Reibungsdämpfer oder auch schlecht gebaute Kolbenmechanismen von Öldämpfern ungünstig, während Feder-Dämpfer-Systeme, die auf Biegung (z.B. Blattfedern), Torsion (z.B. Torsionselement im Gelenk) oder Kompression (z.B. Elastomere) beruhen, dieses Problem nicht haben. (Dafür haben diese andere Nachteile, wie z.B. Temperaturabhängigkeit, oder mangelnde Einstellbarkeit der Federungs-/Dämpfungscharakteristik.)

Stärke der Dämfpung: Diese darf weder zu stark noch zu schwach sein; wie man in :numref:`fig_harmonic-oscillator` sieht, klingt eine Schwingung am schnellsten ab, wenn die Dämpfung dem aperiodischen Grenzfall entspricht. Es muss also sowohl die Federhärte an die Masse angepasst werden, um die richtige Ruhelage zu erreichen, als auch die Dämpfung an die sich aus Federhärte und Masse ergebende Resonanzfrequenz. Dadurch kann das Velomobil Bodenunebenheiten möglichst gut ausgleichen und kommt danach möglichst schnell zur Ruhe.

.. _fig_harmonic-oscillator:

.. figure:: images/harmonic-oscillator_de.*
    :alt: gedämpfter harmonischer Oszillator
    :width: 100%

    Gedämpfter harmonischer Oszillator, mit unterschiedlich starker Dämpfung. In allen Fällen klingt die Schwingung exponentiell ab. Bei zu geringer Dämpfung (= Schwingfall) wird die Gleichgewichtslage am schnellsten erreicht, aber es tritt Überschwingen auf. Bei zu starker Dämpfung (= Kriechfall) gibt es kein Überschwingen, aber die Gleichgewichtslage wird nur langsam erreicht. Im aperiodischen Grenzfall (= kritische Dämpfung) klingt die Amplitude am schnellsten ab, weil es die geringste Dämpfung ohne Überschwingen ist.



.. index:: Hinterrad, Heck, Seitenführung, Luftverlust, Schwerpunkt, Lenkeinschlag, Radkasten, Schlagloch, dynamische Radlastverteilung, Tubeless, Vierrad

Warum ist es gefährlich, wenn das Hinterrad die Seitenführung verliert?
-----------------------------------------------------------------------
Es ist die Kombination mehrerer Dinge:

* Die Vorderräder verlieren nicht so leicht die Seitenführung, denn sie sind selten gleichzeitig in der Luft, und haben auch selten gleichzeitig z.B. einen plötzlichen Luftverlust. Beim Hinterrad ist das anders; wenn es ungedämpft über Unebenheiten springt oder wenn der Reifen platzt, dann ist die Seitenführung schlagartig stark reduziert.
* Da der :term:`Schwerpunkt` deutlich näher an den Vorderrädern als am Hinterrad ist, lastet wenig Gewicht auf dem Hinterrad. Entsprechend ist die Seitenführungskraft schon im Normalfall deutlich geringer als bei den Vorderrädern, und so wird es leicht kritisch, wenn sie nochmals ein ganzes Stück reduziert wird.
* Bei einer Drehung spielt nicht nur die Auflast, sondern auch die Position relativ zum Schwerpunkt eine Rolle: Wenn sich der Schwerpunkt nahe an den Vorderräder befindet, dann rollen diese tangential zu einer Drehung um die Hochachse und setzen dieser praktisch keinen Widerstand entgegen. Das Hinterrad rollt dagegen radial zur Drehung; der tangentialen Bewegung setzt es also einen hohen Widerstand entgegen. Ist der Schwerpunkt weiter hinten, ist die Richtung der Seitenführungskraft günstiger für die Vorderräder (sie rollen nicht mehr tangential dazu), und das Hinterrad hat mehr Auflast (siehe :numref:`fig_schwerpunkt_spurfuehrung`).
* Bei einem Velomobil ist der Lenkeinschlag recht klein, denn er wird von der Breite der Radkästen begrenzt. Wenn also das Hinterrad die Seitenführung verliert und seitlich ausbricht, dann kann man nur begrenzt gegensteuern.
* Mit einem Velomobil fährt man oft schnell. Entsprechend kurz ist die Reaktionszeit, um auf ein ausbrechendes Hinterrad reagieren zu können.

Wenn nun das Hinterrad aus irgendeinem Grund keine Seitenführungskraft mehr hat – egal ob es springt, oder blockiert, oder der geplatzte Reifen nicht mehr auf der Felge sitzt –, und dazu noch eine Querkraft kommt – weil man z.B. gerade durch eine Kurve fährt oder von einem Schlagloch einen seitlichen Schlag bekommt –, dann bricht das Heck aus. Und wenn sich das Velomobil stärker dreht, als man gegensteuern kann, kann es sich quer stellen und überschlagen. Dazu braucht es erstaunlich wenig; ein Beispiel ist ein Velomobil mit ungefedertem Hinterrad, das auf gerader Strecke mit dem Hinterrad auf einen Rüttelstreifen geraten und innerhalb nur weniger Sekunden in voller Fahrt umgekippt ist.

Was kann man dagegen tun?

* In Kurven nicht bremsen: Beim Bremsen wird das Hinterrad entlastet (dynamische Radlastverteilung) und kann entsprechend weniger Seitenführungskraft aufbringen. Daher vor der Kurve bremsen, so dass beim Einlenken das Hinterrad wieder volle Auflast hat.
* Gepäck nach hinten: Erhöht erstens die Auflast auf das Hinterrad, und rückt zweitens den Schwerpunkt weiter von den Vorderrädern weg.
* Reifen überprüfen: Reifen platzen selten unangekündigt; meist gibt es eine sichtbare Vorschädigung.
* Zuverlässige Reifen mit gut haftender Gummimischung verwenden.
* :term:`Tubeless`-Felge: Erschwert das Abspringen des Reifens bei einer Panne.
* Breite Felge: Macht den Reifen weniger hoch, d.h. solange er nicht abspringt, schlackert er weniger herum.
* ein Vierrad-Velomobil (= mit zwei Hinterrädern) fahren

.. _fig_schwerpunkt_spurfuehrung:

.. figure:: images/schwerpunkt_spurfuehrung.*
    :alt: Bremsleistung, um die Geschwindigkeit zu halten
    :width: 100%

    Richtung der Kräfte auf die Räder, wenn sich das Velomobil um die Hochachse dreht, bei verschiedenen Schwerpunkten. Da das Hinterrad radial hinter dem Schwerpunkt liegt, greifen bei einer Drehung die Kräfte immer senkrecht dazu an, die Spurführung ist entsprechend gut. Grün: Wenn der Schwerpunkt weit hinten ist (hier: 70% der Entfernung zwischen Vorderrädern und Hinterrad), dann ist viel Last auf dem Hinterrad (hier: 70%), und zudem sind die Kräfte auf die Vorderräder fast senkrecht zu diesen. Rot: Wenn der Schwerpunkt weit vorne ist (hier: 10% der Entfernung), dann verlaufen die Kräfte fast parallel zu den Vorderrädern – sie können also nicht viel Seitenführung bieten. Das kann das Hinterrad; auf dem ist aber nur 10% der Last. In Realität befindet sich der Schwerpunkt zwischen diesen Extremen, irgendwo beim orangen Punkt; also etwa auf einem Drittel der Strecke zwischen Vorderrädern und Hinterrad, und entsprechend ist die Radlastverteilung relativ gleichmäßig.



Schaltung
=========

.. index:: Schaltung, Übersetzungsbereich, Gangabstufung, Halfstep, Umwerfer

Was muss eine Schaltung erfüllen?
---------------------------------
Bei Velomobilen ist die Herausforderung, dass man einerseits eine sehr konstante Geschwindigkeit fährt – dort die Schaltung also sehr fein abgestuft sein muss –, andererseits aber einen viel größeren Geschwindigkeitsbereich als mit allen anderen Fahrrädern abdeckt, und ein schweres Gefährt hat, das bergauf auch ohne :term:`Wiegetritt` noch in einer passablen Trittfrequenz fahrbar sein muss.

Man könnte eine feine Abstufung zwar auch mit einer Halfstep-Abstufung der Kettenblätter erreichen. Es ist jedoch sehr unpraktisch, bei hohen Geschwindigkeiten für jeden Gangwechsel zwei Schaltvorgänge zu brauchen, statt einfach ein Ritzel weiterzuschalten.


.. index:: Schaltung, Übersetzungsbereich, Gangabstufung, Nabenschaltung, Rohloff, Pinion, Schlumpf, Zwischengetriebe

Ist eine Nabenschaltung möglich?
--------------------------------
Eher nicht – kaum eine hat einen ausreichend großen Übersetzungsbereich von mindestens 500%. Lediglich die Rohloff Speedhub und die Pinion-Getriebe kommen in den Bereich. Zudem haben Getriebeschaltungen prinzipbedingt konstante bzw. symmetrische Gangsprünge; eine flexible Abstufung ist nur mit Kettenschaltungen möglich.

Was aber möglich und auch populär ist: Kombinationen aus Ketten- und Getriebeschaltung. Da gibt es erstens kombinierte Ketten- und Nabenschaltungen; hier erweitert die Nabenschaltung den Übersetzungsbereich, und ermöglicht auch, z.B. an einer Ampel schnell zu schalten, und in der Kombination beider Schaltungen grobe Gangsprünge durch Zwischengänge abzumildern. Leider setzt so eine Nabe eine zweiseitige Hinterradschwinge voraus. Zweitens gibt es die Schlumpf-Tretlagerschaltungen; diese bieten zwar nur zwei Gänge und sind per Ferse weniger schnell zu schalten als per Schalthebel, aber dafür einen enormen Gangsprung, mit dem man den Übersetzungsbereich der Kettenschaltung deutlich erweitern und auf das zweite Kettenblatt verzichten kann. Drittens kann man bei Velomobilen mit Zwischengetriebe dort eine zusätzliche Schaltung einbauen, die auch eine Nabenschaltung sein kann. Nachteil aller Getriebeschaltungen ist aber das hohe Gewicht.


.. index:: Schaltung, Übersetzungsbereich, Gangabstufung, Ritzelpaket, Ritzel

Mountainbike- oder Rennradschaltung?
------------------------------------
Weder noch. Eine optimale Schaltung kann man nicht von der Stange kaufen. Mit zunehmender Ritzelanzahl bestehen Ritzelpakete nämlich nicht mehr aus einzelnen Ritzeln, sondern sind in Gruppen zusammengenietet. Und für Rennradfahrer gibt es fein abgestufte Pakete mit leider viel zu geringer Übersetzungsbandbreite, und für Mountainbiker gibt es ausreichend Übersetzungsbandbreite, aber falsch abgestuft.

Daher stellen sich manche Velomobilfahrer ihre Kassette selbst zusammen – entweder aus einer Mountainbike-Kassette, die sie durch einzelne Ritzel oder ganze Ritzelpakete aus anderen Kassetten ergänzen, oder aus speziellen Sets von Einzelritzeln zusammenstellen. Ganz ohne Gebastel geht das aber selten ab; z.B. müssen Spacer angepasst werden, oder überstehende Nasen abgeschliffen werden. Für eine optimale Abstufung ist es sogar oft nötig, die zusammengenieteten Ritzelpakete aufzubohren und mit neuen Ritzeln zusammenzunieten. Da bei einem Velomobil dank wenig Dreck der Ritzelverschleiß gering ist, hält so ein Paket dann aber auch viele Jahre.


.. index:: Schaltung, Kettenspanner, Kapazität, Schaltwerk, Ritzel

Warum kann man nicht alle Gänge schalten?
-----------------------------------------
Selbst eine Mountainbike-Schaltung mit langem Kettenspanner hat nur eine :term:`Kapazität` von gut 40 Zähnen; haben die Kettenblätter und Ritzel eine größere Zahndifferenz (d.h. Zähne des größten Kettenblatts + des größten Ritzels minus der Zähne des kleinsten Kettenblatts + des kleinsten Ritzels), dann kann die Kette nicht mehr gespannt werden und hängt schlapp durch. Und ein verlängerter Kettenspanner am :term:`Schaltwerk` ist selten möglich, da dafür der Platz innerhalb der Karosserie zu klein ist. In der Praxis bedeutet das dann, dass auf dem kleinen Kettenblatt nur die größten Ritzel überhaupt benutzt werden können. Aber damit verliert man nur „doppelte“ Gänge; und da diese bei den kleinen Ritzeln liegen, die idealerweise sowieso fein abgestuft sind, macht das nichts.


.. index:: Schaltung, Kettenblatt, Ritzel, Freilaufkörper, Lochkreis, Kompaktkurbel, Umwerfer

Welche Kettenblatt- bzw. Ritzelgrößen?
--------------------------------------
Die kleinsten Ritzel sind vom Freilaufkörper vorgegeben; kleiner als 11 Zähne passt nicht auf einen normalen Shimano-Freilaufkörper. Wenn man ein großes Hinterrad hat, braucht man dann ein :term:`Kettenblatt` mit ca. 60 Zähnen, um noch bis ca. 60 km/h bequem mittreten zu können. Und bergauf will man noch mit 5 km/h treten können; das spricht für ein Übersetzungsverhältnis von ca. 1:1. Weil bei gut abgestuften Ritzelpaketen die Ritzelgrößen nur bis ca. 40 Zähne gehen, und mit einem :term:`Lochkreis` von 130 mm das kleinste Kettenblatt 38 Zähne haben muss, spricht vieles für :term:`Kompaktkurbeln <Kompaktkurbel>`, d.h. mit 110 mm Lochkreis, wo man auch kleinere Kettenblätter montieren kann. Der dabei entstehende riesige Sprung zwischen kleinem und großem Kettenblatt ist von handelsüblichen Umwerfern tatsächlich noch gut schaltbar, wenn sie sorgfältig ausgerichtet sind (d.h. sowohl vom radialen Abstand als auch vom Winkel her).


.. index:: Schaltung, Ritzel, Gangabstufung, Ritzelpaket

Abstufung der Ritzel?
---------------------
Die meiste Zeit fährt man in der Ebene, mit Geschwindigkeiten zwischen 40 und 50 km/h. Entsprechend sollten dort die Ritzel sehr fein abgestuft sein – ein Zahn Unterschied ist bei kleinen Ritzelgrößen prozentual ein viel größerer Unterschied als bei großen Ritzeln, und bei hohen Geschwindigkeiten und damit Tretleistungen entspricht das dann auch einem großen Leistungssprung, den man bewältigen muss.
Höhere Geschwindigkeiten als ca. 65 km/h sollte man ignorieren. Die kommen zwar relativ häufig vor, aber nur bergab. Kein normaler Mensch kann so eine Geschwindigkeit über längere Zeit in der Ebene treten. Und solche Abfahrten sind dann auch üblicherweise nach einigen Sekunden vorbei; dafür braucht man keinen extra Gang, sondern lässt es eben dann etwas rollen.
Steile Berge sind zwar nicht häufiger als steile Abfahrten – aber für sie braucht man nicht Sekunden, sondern oft viele Minuten. Hier lohnt sich ein passender Gang also viel eher.
Für das Ritzelpaket lautet das Fazit also: bei den kleinen Ritzeln fein abgestuft, zu den großen hin immer gröber.


.. index:: Schaltung, Kettenblatt, Ritzel, Kettenzug, Zugtrum, Leertrum, Reibung, Antriebssteifigkeit

Große oder kleine Kettenblätter/Ritzel?
---------------------------------------
Bei gleicher Geschwindigkeit und gleicher Trittfrequenz:

* Bei kleinem :term:`Kettenblatt` und kleinem :term:`Ritzel` ist die Kettengeschwindigkeit gering, aber der Kettenzug hoch. Das bedeutet: hohe Verluste im :term:`Zugtrum` (bei der Kettenumlenkung), und eine starke Zugbelastung/Verformung des Antriebs.
* Bei großem Kettenblatt und großem Ritzel ist der Kettenzug gering und die Kettengeschwindigkeit hoch. Das bedeutet: höhere Verluste dort, wo wenig Zugkraft vorhanden ist, also z.B. im :term:`Leertrum`.

In der Summe empfiehlt es sich deshalb, auf großen Kettenblättern zu fahren – auch wenn es sich im Leerlauf so anfühlt, als seien die Verluste bei kleinem Kettenblatt geringer. Das stimmt ja auch, aber unter Last ist es eben anders herum.


.. index:: Schaltung, Kettenblatt, Ritzel, Umwerfer

Kleines kleinstes Kettenblatt oder großes größtes Ritzel?
---------------------------------------------------------
Das ist eigentlich Geschmackssache; letzteres hat aber den Reiz, dass man dann sehr viel mit dem großen Kettenblatt fahren kann, und nur selten den Umwerfer braucht. Zudem ist dort der Wirkungsgrad etwas höher. Allerdings ist es schwieriger, ein gut abgestuftes Ritzelpaket zu bekommen, wenn das größte Ritzel besonders groß sein soll.



Antriebsstrang
==============

.. index:: Umlenkrolle, Kettenzug, Reibung, Tretkurbel, Kettenblatt, Kugellager

Wie groß sind die Verluste durch Umlenkrollen?
----------------------------------------------
An einer Umlenkrolle passieren folgende Dinge:

* Die Kettenglieder werden abgewinkelt. Und zwar umso stärker, je kleiner die Umlenkrolle ist – da sich die Umlenkung auf weniger Kettenglieder verteilt.
* Die Rolle wird gegen das Lager gedrückt. Wenn die Kette z.B. um 90° umgelenkt wird, dann drückt sie im 45°-Winkel gegen die Umlenkrolle.

Die dabei auftretenden Kräfte sind nicht vernachlässigbar: Bei einer Umlenkung um z.B. 90° wirkt auf die Umlenkrolle eine Kraft von sin(45°) = ca. 70% des Kettenzugs, welcher wegen des Hebelverhältnisses zwischen Tretkurbel und Kettenblatt leicht das Doppelte der Tretkraft entsprechen kann. Das können dann mal eben 100 kg Gewicht entsprechen.

Beim Anpressdruck auf das Lager ist das alldings nicht so schlimm: Ein Kugellager hat sehr wenig Reibung, d.h. trotz hoher Kraft sind die Verluste gering. Bei der Kraft auf die Kettenglieder ist das anders. Die Umlenkung pro Kettenglied ist zwar viel geringer; die 90° Umlenkung verteilen sich bei einer großen Rolle z.B. auf 10 Glieder, und entsprechend gering ist die Kraft (hier z.B. sin(9°) = 16% des Kettenzugs). Dafür sind Kettenglieder keine Kugellager, sondern Gleitlager, die eine viel höhere Reibung haben – erst recht, wenn sie auch noch verdreckt sind.

Daher ist die wichtigste Maßnahme, um die Verluste durch Umlenkung klein zu halten: große Umlenkrollen!


.. index:: Umlenkrolle, Sitz, Schwerpunkt, Bodenfreiheit, Kettenblatt, Kettenrohr

Eine oder zwei Umlenkrollen?
----------------------------
Gute Frage. Grundsätzlich bekommt man die Kette auch mit einer Umlenkrolle unter dem Sitz durch. Aber wenn diese auch noch groß sein soll, damit die Verluste geringer sind, ragt sie sehr weit nach unten, und die Kette verläuft vorne und hinten trotzdem schräg. Mit zwei Umlenkrollen kann man den Sitz tiefer setzen (d.h. niedrigerer :term:`Schwerpunkt`), hat aber trotzdem mehr Bodenfreiheit, und vorne kann man die Kette vom Kettenblatt aus steil nach unten verlaufen lassen, so dass man sie nicht mit den Waden berührt. In der Summe sind zwei Umlenkrollen zwar geringfügig ineffizienter, bieten aber diese anderen Vorteile.


.. index:: Kette, Kettenrohr, Kettenkanal, Reibung, Zugtrum, Leertrum

Sind Kettenrohre ineffizient?
-----------------------------
Eine Kette, die an einem Rohr schleift, verursacht natürlich Reibung. Und wenn die Kette ölig ist, und lauter Dreck dranklebt, setzt sich dieser auch im Kettenrohr ab; der Innendurchmesser wird kleiner, und die Kette schleift noch öfter. Nasser Dreck kann auch nicht so leicht abtrocknen und abfallen, sondern in den Rohren hält sich der nasse Dreck ewig.

Aber: In einem Velomobil ist die Kette recht sauber, weil sie keinen direkten Kontakt mit Straßendreck hat. Nur der Dreck, den der Fahrer z.B. über die Schuhe hineinbringt, kann auf der Kette landen. Und da ist ein Kettenrohr eher positiv, weil es die Kette vor Verschmutzung schützt. In einem Velomobil ist es immer feucht, und Dreck sammelt sich am tiefsten Punkt; d.h. gerne unten im Kettenkanal. Da wäre es nicht gut, wenn die Kette permanent durch diesen feuchten Dreck gezogen würde, sondern zumindest dort helfen Rohre gegen die Verschmutzung.

Zudem ist die Verlegung der Rohre wichtig. Reibungskraft ist das Produkt aus Anpressdruck und Gleitreibungszahl; dort wo die Kette unter Zug ist, folgt sie nicht dem Verlauf des Rohrs, sondern wird gegen das Rohr gepresst, entsprechend hoch ist die Reibung. Daher ist es im :term:`Zugtrum` wichtig, dass nicht die Kette dem Rohr, sondern umgekehrt das Rohr exakt dem Kettenverlauf folgt. Wenn die Kette unter Zug nicht am Rohr anliegt, tritt auch keine Reibung auf.

Im :term:`Leertrum` ist das anders. Dort hängt die Kette durch – d.h. um reibungsfrei zu sein, müsste das Kettenrohr dem Verlauf der durchhängenden Kette folgen. Aber dort ist auch der Anpressdruck niedrig (nämlich nur ungefähr das Eigengewicht der Kette), d.h. die Reibung ist gering. Und im Velomobil ist sowieso kein Platz, dass die Kette frei durchhängen könnte – ob sie jetzt im Kettenrohr oder auf dem Boden schleift, macht keinen großen Unterschied. Im ersten Fall ist sie dafür vor Dreck geschützt.


.. index:: Antriebssteifigkeit, Muskel, Hysterese

Warum ein steifer Antrieb?
--------------------------
Immer dann, wenn man kraftvoll in die Pedale tritt, ist ein steifes Velomobil wichtig; denn dann landet die Tretkraft auf der Straße, statt in der Verformung des Fahrzeugs. Diese Verformung ist natürlich elastisch, d.h. wie als würde man eine Feder spannen. Allerdings hat so eine Verformung natürlich eine Hysterese, d.h. man bekommt nicht die gesamte Energie zurück, die man hineingesteckt hat.

Aber das ist noch gar nicht einmal das Hauptproblem: Wenn man bei einem steifen Fahrrad in die Pedale tritt, dann wird die Tretenergie unmittelbar in Geschwindigkeit umgesetzt – wenn man danach mit dem Treten aufhört, ist das Fahrrad schneller, und der Druck auf die Pedale ist weg. Wenn dagegen mit der Tretenergie der Rahmen verformt wird, steht er danach unter Spannung – d.h. er zieht an der Kette und beschleunigt das Fahrrad, aber er drückt auch auf die Pedale und somit auf die Muskeln. Aber das mögen diese überhaupt nicht. Wenn man beispielsweise ein Gewicht hochhebt, verrichten die Muskeln Hubarbeit; wenn man aber ein Gewicht auf der selben Höhe hält, wird physikalisch gesehen keine Arbeit verrichtet, aber es fühlt sich trotzdem anstrengend an, ermüdet, und die Muskeln verbrauchen dabei Nährstoffe. Muskeln verbrauchen also Energie, wenn sie nur eine statische Gegenkraft aufbringen müssen – eine Schraube in der Decke kann dagegen das Gewicht einer Lampe über Jahrhunderte tragen, ohne zu ermüden. Und darum kostet dieses Gegenhalten gegen einen elastisch verformten Antriebsstrang Kraft, die dann für die Beschleunigung fehlt.


.. index:: Antriebssteifigkeit, Lenkbrücke, Radkasten, Umlenkrolle, Kettentunnel, Kettenzug, Sitz, Schwinge, Karosserie

Was muss steif sein?
--------------------
Die auftretenden Kräfte sind vor allem der Kettenzug und der Gegendruck des Fahrers in die Sitzschale. Alles, was damit zusammenhängt, muss steif sein:

* Das Tretlager ist am Tretlagermast befestigt.
* Dieser ist über die Lenkbrücke mit den Vorderrad-Radkästen verbunden. Hier gibt es viele gekrümmte Flächen und relativ dickes Material, d.h. hier ist genug Steifigkeit vorhanden.
* Die Kettenumlenkung drückt die Umlenkrollen nach oben; die vordere ist deshalb meist mit der steifen :term:`Lenkbrücke` verbunden.
* Der Kettenzug will auch den Fahrzeugboden zwischen den Umlenkrollen wie eine Bogensehne zusammenziehen. Darum hat bei vielen Velomobilen der Kettentunnel auch eine statische Funktion, denn er dient als U-Profil, der den Fahrzeugboden in Längsrichtung versteift.
* Der Kettenzug geht auf das Hinterrad, dessen Schwinge entsprechend biegesteif sein muss und entsprechend gut an der Karosserie abgestützt sein muss.
* Auch der Sitz muss gut befestigt sein. Deshalb sind z.B. manche Sitze vorne an den Vorderrad-Radkästen festgeschraubt, und stützen sich hinten gegen den Hinterrad-Radkasten ab, der im vorderen Teil oft das Schwingenlager hält und entsprechend stabil ist.
* Auch die Karosserie zwischen Hinterrad-Schwingenlager und Vorderrad-Radkästen muss ausreichend steif sein. Das ist aber grundsätzlich gegeben, weil das eine Röhre mit großem Durchmesser ist. Zu große Öffnungen sind allerdings nachteilig; entsprechend sind die Velomobile, die riesige Einstiegsöffnungen haben, dort entsprechend massiv und schwer gebaut. (Es gab z.B. auch Cabrios, die im Vergleich zu ihren geschlossenen Coupé-Pendants extra verstärkt werden mussten, weil sie sonst zu wenig steif gewesen wären.)

Der Rest ist nicht so wichtig, und kann entsprechend dünnwandiger gebaut sein.



Bremsen
=======

.. index:: Bremse, Trommelbremse, Wärmekapazität, Fading, Speichenspannung, Materialermüdung

Was für Anforderungen muss eine Velomobil-Bremse erfüllen?
----------------------------------------------------------
Eine besonders hohe Bremskraft jedenfalls nicht. Denn erstens gibt es beim Velomobil zwei Vorderräder, die gebremst werden können; zweitens sind die Vorderräder klein, d.h. der Hebel zwischen Radius der Bremsen und Radius der Räder ist kleiner als bei großen Rädern, und drittens fährt man eher gleichmäßig, nicht wie beim Mountainbike sausteil bergab auf einem Untergrund, der Überraschungen bereit hält. In der Praxis sind der limitierende Faktor die Reifen, eine gut eingestellte Bremse lässt sich meist blockieren.

Dafür muss die Bremse eine hohe Wärmeleistung vertragen (siehe :numref:`fig_speed_vs_brakepower_downhill`). Denn erstens gibt es beim Velomobil wenig Luftwiderstand, der mitbremst; während ein Mountainbike auf kaum einer Straße schneller als 60 km/h wird, erreicht ein Velomobil locker über 100 km/h. Zweitens sind mit dem Velomobil die üblichen Geschwindigkeiten höher; bei gleichem Gefälle bedeutet eine höhere Geschwindigkeit, dass mehr Höhenenergie pro Zeit weggebremst werden muss. Und drittens ist ein Velomobil schwerer und die Bremsen sind in den Radkästen schlechter gekühlt.

Das Problem sind also nicht kurze Bremsmanöver, sondern lange Abfahrten. Hier helfen größere Trommelbremsen, ganz einfach weil dort die Wärmekapazität höher ist – es ist mehr Material vorhanden, das erhitzt werden kann, und die Oberfläche des Nabengehäuses ist auch etwas größer.

Und was passiert bei zu heißen Bremsen? Erstens kommt es zu Fading – d.h. die Bremswirkung nimmt spürbar ab. Zweitens kann sich auch das Nabengehäuse so weit ausdehnen, dass die Speichen ihre Spannung verlieren, beim Abrollen des Rades gestaucht werden, und bald durch Materialermüdung wegen der Wechselbelastung brechen.

.. _fig_speed_vs_brakepower_downhill:

.. figure:: images/speed_vs_brakepower_downhill_de.*
    :alt: Bremsleistung, um die Geschwindigkeit zu halten
    :width: 100%

    Nötige Bremsleistung, um bei verschiedenen Gefällen die Geschwindigkeit zu halten; die durchgezogenen Kurven zeigen ein Velomobil, die gestrichelten ein Rennrad. Während bei letzterem der Luftwiderstand so stark anwächst, dass bei höheren Geschwindigkeiten die nötige Bremsleistung wieder sinkt und man bei einem Gefälle unter 5% gar nicht bremsen muss, um nicht über 50 km/h zu kommen, ist beim Velomobil dieses Maximum viel höher und kommt bei viel höheren Geschwindigkeiten. Während eine Rennradbremse bei 10% Gefälle nicht mehr als gut 500 W vertragen muss, sind es beim Velomobil bei der gleichen Geschwindigkeit fast das Doppelte und im Maximum, bei der dreifachen Geschwindigkeit, fast das Vierfache.


.. index:: Bremse, Trommelbremse, Bremsenkühlung, Fahrtwind, Trans America Bike Race, Hosen, Wasserkühlung, Velomobilize, Hinterradbremse, Bremsfallschirm, Kühlrippen, Ginkgo Veloteile, Intervallbremsung

Wie kann man die Bremsen kühlen bzw. dafür sorgen, dass sie weniger heiß werden?
--------------------------------------------------------------------------------
Eine Möglichkeit ist, mehr Fahrtwind auf die Bremsen zu leiten. Das wurde beispielsweise bei dem Velomobil gemacht, mit dem Marcel Graber das Trans America Bike Race 2018 gefahren ist – er verwendete Radkastenverkleidungen („:term:`Hosen`“) und entsprechend Vorderräder ohne Radscheibe, und in die Radkastenverkleidungen wurden Luftkanäle zur Nabe hin geschnitten.

Eine andere Möglichkeit ist eine Wasserkühlung, die von Patrick Flé (*Velomobilize*) umgesetzt wurde: Mit einer Kunststoffflasche und einem Schlauch wird Wasser in die Bremstrommel gespritzt, welches sofort verdampft; mit der Wärmekapazität und der Verdampfungswärme kann eine recht hohe Energiemenge abgeführt werden. Mechanisch halten die Bremsen diesen Temperaturschock gut aus.

Dann gab es auch noch Experimente mit Dauerbremsen. Zum einen mit einer zusätzlichen Hinterradbremse; das hat aber den Nachteil, dass das Hinterrad leicht blockiert und dann zum Ausbrechen neigt. Eine andere Methode ist die Verwendung von Bremsfallschirmen; da hat sich bewährt, zwei Fallschirme mit jeweils ca. 40 cm Durchmesser zu verwenden. Das ist aber nur begrenzt praxistauglich, weil erstens die Fallschirme am Beginn der Abfahrt manuell ausgeworfen werden müssen, zweitens am Ende der Abfahrt angehalten werden muss, um sie wieder einzupacken, und drittens die herumtanzenden Fallschirme bei viel Verkehr wohl eher nicht empfehlenswert sind.

Die wohl praxistauglichste Methode sind aber Kühlrippen und Kühltürmchen, die von *Ginkgo Veloteile* in die Bremstrommeln gefräst bzw. auf diese aufgeklebt werden. Dadurch erhöht sich die Oberfläche der Bremstrommeln, und die Luft kann sie somit effektiver kühlen.

Und nicht zuletzt kann man auch mit seiner Fahrweise die Bremsen weniger heiß werden lassen: Am schlechtesten ist es, mit hoher Geschwindigkeit bergab zu fahren und dabei permanent zu bremsen (siehe :numref:`fig_speed_vs_brakepower_downhill`). Eine Intervallbremsung sorgt dafür, dass höhere Spitzengeschwindigkeiten erreicht werden, wo der bremsende Luftwiderstand höher ist, und beim anschließenden Bremsen auch höhere Temperaturen, wodurch die Wärmeleitung an die Luft und die Wärmestrahlung effektiver werden. Und in der darauf folgenden Bremspause können sich die Bremsen stärker abkühlen, als wenn permanent gebremst würde.


.. index:: Bremse, Trommelbremse, Scheibenbremse, Radkasten, Felgenbremse

Warum hat ein Velomobil Trommelbremsen?
---------------------------------------
Weil keine anderen Bremsen geeignet sind.

Es hat keine Scheibenbremsen, weil diese neben dem Rad befestigt werden. Genau dort, wo am wenigsten Platz ist, weil das Rad am weitesten Richtung Radkasten reicht, und auf der Innenseite der Fahrer seine Oberschenkel hat, müsste auch noch eine Bremse untergebracht werden. Das ginge nur, wenn man das Velomobil breiter machen würde. Eine Bremsscheibe an sich ist zwar nicht besonders breit, aber beim Einlenken wird zusätzlicher Platz gebraucht, der im Radkasten erst einmal vorhanden sein muss. Es gibt zwar Velomobile mit Scheibenbremsen; die haben aber keine normalen Speichenräder, sondern Scheibenräder – diese sind konkav geformt, so dass die Bremsscheibe weiter innen im Rad untergebracht werden kann. Dafür haben diese Räder andere Nachteile, z.B. höheres Gewicht, und sind nicht zentrierbar. Ein weiteres Argument gegen Scheibenbremsen ist die schlechtere Kühlung im Radkasten; Bremsscheibe und Bremsbelag sind relativ klein, und heizen sich darum schnell auf. Und: Da sich die Räder in Radkästen befinden, läuft das Regenwasser nicht einfach ab, sondern wird immer wieder vom Rad in den Radkasten geschleudert und tropft dort nach unten, zur Bremse – und zwar zusammen mit dem Straßendreck. Eine offene Bremse würde in einem Radkasten deshalb schnell verschleißen.

Es hat keine Felgenbremsen, weil das bei einseitig aufgehängten Vorderrädern schwierig wäre; beidseitige Cantilever-Sockel sind grundsätzlich nicht möglich, und auch eine Seitenzugbremse passt nicht problemlos in den Radkasten, wenn das Rad zudem noch einfedern können muss. Zudem ragt eine Felgenbremse nach außen, wäre also aerodynamisch sehr ungünstig. Und eine Felgenbremse erhitzt die Felge, was bei den hohen Fahrgeschwindigkeiten und kleinen Radgrößen die Felge bei einer Abfahrt schnell überhitzen würde.

Bleiben also nur noch Trommelbremsen.


.. index:: Bremse, Hinterradbremse, Schwerpunkt, dynamische Radlastverteilung, Seitenführung

Warum keine Bremse am Hinterrad?
--------------------------------
Aus zwei Gründen:

* Das Hinterrad ist relativ weit weg vom :term:`Schwerpunkt` – während der Fahrer fast zwischen den Vorderrädern sitzt, sitzt er ein Stück vom Hinterrad entfernt. Daher lastet wenig Gewicht auf dem Hinterrad – was man daran merkt, wie leicht es bei rutschiger Straße durchdreht. Zudem sorgt die Bremsverzögerung für eine dynamische Radlastverteilung nach vorne, was das Hinterrad weiter entlastet. Man könnte mit dem Hinterrad also nur geringe Bremskräfte übertragen, bevor es ins Rutschen kommt.

* Ein rutschendes Hinterrad ist gefährlich, weil es in alle Richtungen gleich gut rutscht – während ein rollendes Rad sich viel leichter in Rollrichtung als quer dazu bewegt. Das bedeutet, ein rutschendes Rad hat keine Seitenführung mehr und Querkräften entsprechend wenig entgegenzusetzen, was sehr gefährlich sein kann (siehe: `Warum ist es gefährlich, wenn das Hinterrad die Seitenführung verliert?`_).


.. index:: Bremse, Trommelbremse, Sturmey&Archer, Shim

Was hat es mit dem Unterlegen von Blechplättchen bei Trommelbremsen auf sich?
-----------------------------------------------------------------------------
Die Sturmey&Archer-Trommelbremsen bestehen aus zwei Bremsbelägen, die an der einen Seite auf einem gemeinsamen Lager drehbar befestigt sind, und auf der anderen Seite von einem drehbaren Nocken auseinandergedrückt werden. Bei neuen Bremsbelägen muss sich dieser Nocken nur wenig drehen, bis die Bremsbeläge auf der Bremstrommel aufliegen; gemäß der Kleinwinkelnäherung drückt der drehende Nocken die Beläge vor allem auseinander und bewegt sich kaum seitwärts.

Bei stärker verschlissenen Bremsbelägen ist das anders. Die Bremskraft bleibt ungefähr gleich, da der Bremszug entsprechend nachgestellt wurde. Allerdings muss sich der Nocken jetzt viel weiter drehen und bewegt sich dabei auch nennenswert seitwärts. Da beim Loslassen des Bremsgriffs die Rückstellung mittels einer Feder geschieht, die die Bremsbeläge zusammenzieht, schafft es diese Feder dann nicht mehr, den Nocken so stark zu drücken, dass er auch seitlich entlang der Bremsbelagshalter gleitet – die Bremse stellt sich nicht mehr zurück und blockiert.

Als Abhilfe kann man den Abstand zwischen Bremsbelägen und Nocken erhöhen, so dass die verschlissene Dicke des Bremsbelags ausgeglichen wird. Dies erreicht man, indem man unter die Gleitschuhe, mit denen die Bremsbelagshalter auf dem Nocken gleiten, mit einem dünnen Streifen Blech („Shim“) unterlegt werden (Dicke z.B. 0.8 mm). Dann muss sich der Nocken nicht mehr so weit drehen und bewegt sich entsprechend weniger seitlich. Für die Bremsbeläge ist das unkritisch; deren Dicke beträgt ein Vielfaches des Verschleißes, wenn die Rückstellung Probleme macht. Man kann also mehrfach unterfüttern; und selbst dann ist das Problem, dass der Gleitschuh nicht mehr hält, aber immer noch ausreichend Bremsbelag da wäre.

Bei dieser Gelegenheit sollte man gleich den Nocken und die Gleitschuhe schmieren; dazu nimmt man am besten Kupferpaste, denn diese ist hitzebeständig.


.. index:: Bremse, Feststellbremse, Trommelbremse

Warum soll man nach einer langen Bergabfahrt die Feststellbremse nicht anziehen?
--------------------------------------------------------------------------------
Auf längeren Abfahrten wird eine Bremse sehr heiß – inklusive dem Gehäuse, welches aus Aluminium besteht. Dieses wird durch die Hitze weich; und wenn sich das Rad nicht mehr dreht, aber die Bremse permanent angezogen ist, kann es sich geringfügig verformen. Das ist zwar nicht gleich gefährlich – aber die Bremse ist danach unrund und kann nicht mehr so gut eingestellt werden. Daher sollte man die Parkbremse erst anziehen, nachdem sich die Bremse abgekühlt hat.



Elektrik
========

.. index:: Scheinwerfer, Radweg, Blendung, Nässe

Scheinwerfer: Welche? Wo?
-------------------------
Licht gibt es im Velomobil nie genug. Erstens ist man oft mit hoher Geschwindigkeit unterwegs – möglicherweise auf Radwegen, die dafür nicht gemacht sind und keine Seitenstreifen oder Reflektoren haben –, zweitens sitzt man tief und hat darum einen schlechten Blick auf die Straße und wird auch häufiger von Auto-Scheinwerfern geblendet (erst recht, wenn der linksseitige Radweg tiefer als die Straße ist), und drittens die Scheinwerfer oft recht tief montiert sind – und Fahrradscheinwerfer sind meist für eine deutlich höhere Befestigungsposition ausgelegt.

Während eine helle Beleuchtung bei trockener Straße ausreichend ist, stößt man spätestens auf nasser Straße und bei blendendem Gegenverkehr mit normaler Fahrradbeleuchtung an Grenzen. Hier hilft es zumindest, den Scheinwerfer so hoch wie möglich zu montieren. Auf der Hutze ist aber oft kontraproduktiv, weil man sich dadurch selbst blendet. Daher empfiehlt sich oft ein Scheinwerfer vorne, aber so hoch wie möglich, ohne dabei die Aerodynamik zu ruinieren.


.. index:: Blinker, LED, Konstantstromquelle

Blinker: Welche? Wo?
--------------------
Da man praktisch keine Handzeichen geben kann, sind Blinker sehr empfehlenswert. Diese sollten so weit auseinander stehen, dass bei Dunkelheit klar erkennbar ist, welche Seite blinkt. Da Blinker nur immer sehr kurz in Benutzung sind, ist der Stromverbrauch relativ egal – möglichst helle LEDs (z.B. 3 W, 700 mA, möglichst großer Abstrahlwinkel), und vorne aus aerodynamischen Gründen in die Karosserie eingelassen. Passende Blinkrelais gibt es im Motorradbedarf, und zumindest für hinten auch passende (und in Deutschland zugelassene) Blinker.


.. index:: Elektronik, Feuchtigkeit, Korrosion

Elektronik ist im Inneren wettergeschützt, oder?
------------------------------------------------
Jein. Natürlich trifft Wind und Wetter nicht das Innere des Velomobils. Allerdings schwitzt man viel stärker, und bei kaltem Wetter kondensiert auch die Atemluft an den kalten Außenflächen. Im Velomobil herrscht also ein Klima wie in einer tropischen Tropfsteinhöhle, und das muss die Elektronik aushalten. Was nicht entsprechend abgedichtet oder versiegelt ist, korrodiert schnell.


.. index:: Dynamo, Nabendynamo, Seitenläuferdynamo, Trommelbremse, Scheibenbremse, Schwinge, Hinterradnabe, Sturmey&Archer, Akku

Dynamo ja oder nein?
--------------------
Ein einspuriges Fahrrad, das mehr als ein Schönwetter-Rad ist, sollte eine gute Lichtanlage mit Nabendynamo haben. Bei Velomobilen ist das etwas schwieriger: Einerseits sind es Alltagsräder mit hohen Fahrleistungen – andererseits gibt es bisher keine gute Dynamo-Lösung. Es gibt zwar hochwertige Nabendynamos für einseitig aufgehängte Vorderräder; aber diese sind z.B. für Trikes gedacht, die Scheibenbremsen haben. Velomobile haben dagegen meist Trommelbremsen, d.h. der Platz in der Nabe wird dafür gebraucht.

Am Hinterrad wird es auch schwierig; es gibt zwar Dynamos für eine Kassetten-Hinterradnabe; sie haben allerdings eine recht geringe Leistung. Und es gibt sie nur als zweiseitig aufgehängte Naben. Fahrer von Velomobilen mit einseitigen Schwingen bleiben da außen vor.

Dann gäbe es auch Seitenläuferdynamos. Diese kann man aber nur auf der Felge laufen lassen, da die dünnen Flanken schneller Reifen die Belastung einer Dynamorolle nicht aushalten würden. Zudem müsste man den Dynamo irgendwo im Radkasten befestigen, wo es bei Regen sehr viel Wasser und Dreck gibt – der Dynamo hat also kein leichtes Leben und würde wohl leicht durchrutschen.

Und nicht zuletzt gibt es dann noch eine Kombination aus Nabendynamo und Trommelbremse von Sturmey&Archer. So richtig überzeugend ist dieser aber auch nicht, weil erstens der Wirkungsgrad nicht spitzenmäßig ist, zweitens die Permanentmagnete die Hitze der Bremsen auf längere Zeit nicht gut vertragen dürften (Curie-Temperatur!), und drittens ist damit auch die Demontage des Rades bei weitem nicht mehr so einfach, weil nicht mehr nur eine Schraube gelöst werden muss, sondern auch die Kabel entfernt werden müssen.

Schließlich ist auch noch ein Problem, dass ein Velomobil relativ viel Strom braucht. Für einen Scheinwerfer sind die typischerweise 3 W eines Dynamos absolut unterste Grenze, es kann auch mal das Dreifache sein. Zumindest ein Puffer-Akku ist dafür Pflicht. Und darum bleiben die meisten Velomobilfahrer bei einer reinen Akku-Beleuchtung.


.. index:: Spannung, Akku, Konstantstromquelle

Welche Spannung im Bordnetz?
----------------------------
Früher war es einfach; da hat der Dynamo bereits bei mäßiger Fahrgeschwindigkeit 6 V geliefert, mit denen die Glühlampe passabel geleuchtet hat; und bei hohen Geschwindigkeiten hat der Dynamo dafür gesorgt, dass trotz einer möglicherweise hohen Spannung nicht zu viel Strom geflossen ist. Und bei einer Glühlampe muss man nur eine Spannung anlegen; mit der Temperatur des Glühfadens steigt dessen Widerstand, so dass der Strom nur so weit ansteigen kann, bis dieser hell leuchtet.

Bei heute üblichen Bordnetzen ist das anders. Ein Akku kann fast beliebig hohe Ströme liefern, und auch eine LED hat nicht mehr diese negative Rückkopplung, die den Strom begrenzt; eher im Gegenteil. Daher ist Elektronik nötig, die Strom und Spannung regelt. Viele Geräte haben so eine bereits eingebaut und vertragen einen weiten Spannungsbereich, z.B. von 6 bis 16 V. Und nackten LEDs ist die Spannung sowieso relativ egal; sie muss zwar einen gewissen Mindestwert erreichen, aber ansonsten muss vor allem der Strom begrenzt werden, was eine sogenannte Konstantstromquelle (KSQ) erledigt.

Entsprechend bevorzugen manche Leute Akku-Systeme mit ungefähr 12 V, weil sie damit problemlos Motorrad-Teile verbauen können. Andere nehmen ca. 6 V, um Fahrradkomponenten besser verbauen zu können. Solange alle Geräte mit der Spannung zurechtkommen, und alle LEDs an Konstantstromquellen angeschlossen sind und z.B. USB-Kabel an einem 5-V-Spannungswandler hängt, ist die Spannung im Bordnetz egal.


.. index:: LiIon-Akku, Batteriemanagementsystem, Balancing

Wie soll man einen Lithium-Ionen-Akku behandeln?
------------------------------------------------
* Nicht überladen: Wenn die Ladeschlussspannung erreicht ist, muss der Ladevorgang beendet werden.
* Nicht unterladen: Wenn die Mindestspannung erreicht wird, darf kein Strom mehr entnommen werden.
* Der Entnahmestrom darf nicht zu hoch sein (im Velomobil eher selten ein Problem).
* Der Ladestrom darf nicht zu hoch sein.
* Die Temperatur darf nicht zu hoch sein.

Diese Dinge werden bei Laden von jedem handelsüblichen Ladegerät überprüft; dabei wird der Ladestrom konstant gehalten und zusammen mit der steigenden Akkuspannung die Ladespannung erhöht, bis die Ladeschlussspannung erreicht wird. Für die Entnahme sind meist im Akku kleine BMS-Platinen (Batteriemanagementsystem) integriert, die bei einem Überschreiten des Stroms oder einem Über- oder Unterschreiten der Spannungslimits den Akku abschalten.

Zudem brauchen Akkus mit in Serie geschalteten Zellen auch ein sogenanntes Balancing, bei dem alle Zellen wieder gleichmäßig voll geladen werden, wenn sie sich zuvor ungleichmäßig entladen haben. Das ist aber v.a. bei hohen Strömen relevant, z.B. bei Antriebsmotoren; beim üblichen Einsatz als Licht-Akkus kann man auf das Balancing meist verzichten.

Damit der Akku möglichst lange hält, sollte erstens der Ladestrom gering gehalten werden, und zweitens der Akku weder ganz entleert noch ganz aufgeladen werden; idealerweise wird er nicht unter 30% entladen und nicht über 70% bis 90% (je nach Quelle) geladen.


.. index:: Tacho, GPS, Navi

Wo befestigt man Tacho/GPS/Navi?
--------------------------------
Dort, wo man es gut sehen kann. Bei großer Anzeige kann das auf dem Radkasten sein, oder auch direkt vor den Augen am vorderen Cockpit-Rand. Bei einem Tiller-Lenker kann man das Navi auch dort befestigen.


.. index:: GPS, Haube, Carbon

GPS-Empfang unter Carbon?
-------------------------
Carbonfasern sind bekanntlich elektrisch leitend – und müssten entsprechend elektromagnetische Wellen abschirmen. Erfahrungsgemäß funktioniert der GPS-Empfang im Inneren aber ganz passabel, jedenfalls mit einem modernen GPS-Empfänger. Aber die Signalstärke nimmt spürbar ab, wenn die Haube aufgesetzt wird; und wenn dann auch noch Regenwetter mit dicken Wolkenschichten herrscht, tun sich die Geräte erkennbar schwer, die genaue Position zu finden. Zur Navigation reicht es jedoch meist – zumindest wenn man auf das Display schaut und eine gute Kartenanzeige hat (siehe :numref:`fig_GPS_tracks_elevation`). Bei Navigation nach Sprachansagen ist die exakte Position kritischer; wenn durch schlechten Empfang die Ansage verspätet kommt, verpasst man leicht eine Abzweigung.

.. _fig_GPS_tracks_elevation:

.. figure:: images/GPS_100tracks_2maps_42elevation_optim.*
    :alt: GPS-Tracks und Höhenprofile übereinander gelegt
    :width: 100%

    GPS-Aufzeichnungen, die innen vom Radkasten aus gemacht wurden (Gerät: Wahoo Elemnt). Auf den beiden Karten sieht man jeweils 10 Fahrten, die direkt hintereinander gemacht wurden – links ohne Haube, rechts mit Haube. Auf dem rechten Diagramm sieht man 42 Höhenprofile; während die Formen der Höhenprofile sehr ähnlich sind, gibt es große Abweichungen bei den absoluten Höhen.


.. index:: GPS, Navi, Smartphone, BRouter, OsmAnd, Locus Map, Garmin, Wahoo

Welches Navi?
-------------
Im Velomobil ist ein Navigationsgerät bzw. GPS mit Kartenanzeige sehr sinnvoll, weil man damit in unbekannter Gegend viel vorausschauender und damit energiesparender fahren kann; sich zu verfahren kostet gerade im Velomobil viel Zeit und Kraft, um nach dem Abbremsen wieder auf Geschwindigkeit zu kommen.

Es gibt jedoch kaum eigenständige Navigationsgeräte, die für Fahrräder und erst recht für Velomobile geeignet sind. Auto-Navis sind vollkommen ungeeignet, weil sie Autobahnen und große Fernstraßen bevorzugen – wo Fahrräder entweder verboten sind oder man sich wegen großer Geschwindigkeitsdifferenzen sehr unwohl fühlt. Und Fahrrad-Navis bevorzugen oft Radwege und Feldwege, auf denen ein effizientes Vorwärtskommen unmöglich ist und deren Unübersichtlichkeit bei hohen Geschwindigkeiten gefährlich ist.

Echtzeit-Navigation funktioniert deshalb eigentlich nur sinnvoll auf dem Smartphone, wo die Routenberechnung von `BRouter <https://brouter.de/brouter-web/>`_ übernommen wird und zur Anzeige jede unterstützte App verwendet werden kann (z.B. *OsmAnd*, *Locus Map*).

Ansonsten kann man einen Track vor der Fahrt erstellen, und entweder der Linie auf der Karte nachfahren; oder, wenn der Track mit Navigationshinweisen versehen ist, diesen auch per Audio-Ansagen folgen.

Neben der Funktionalität gibt es bei der Hardware auch folgende Dinge zu bedenken:

* Stromverbrauch (höher bei großem Display)
* Schutz (gegen Spritzwasser): ein Velomobil ist wie eine Tropfsteinhöhle, Elektronik muss dem standhalten
* Bedienbarkeit, auch bei Feuchtigkeit; schlecht bei einem Touchscreen


.. index:: Motor, Durchschnittsgeschwindigkeit, Höchstgeschwindigkeit, Höhenmeter, Pedelec, S-Pedelec

Motor ja oder nein?
-------------------
Ein Velomobil fährt in der Ebene recht schnell, aber ist bergauf langsam, und beschleunigt auch langsam. Und die Durchschnittsgeschwindigkeit ist der zeitliche Mittelwert, d.h. langsame Abschnitte fallen stärker ins Gewicht – es bringt also mehr, auf langsamen Abschnitten schneller zu werden, als die Höchstgeschwindigkeit zu erhöhen. Daher bringt ein Motor für die Fahrzeit dann etwas, wenn es viele langsame Abschnitte gibt, die damit beschleunigt werden können. Weil man den Motor nur auf diesen wenigen Abschnitten braucht und sonst nicht, kann der Akku vergleichsweise klein sein.

Allerdings hilft ein Motor im Stop-and-Go-Verkehr auch nicht viel; es gibt zwar viele kraftraubende Beschleunigungsvorgänge (siehe: `Warum sind bei einem Velomobil die Beschleunigungsphasen so lang?`_), aber wegen der Bremsungen dazwischen wird man auch mit Motor bei weitem nicht die Geschwindigkeit der offenen Strecke erreichen. Hier entlastet ein Motor zwar den Fahrer, macht aber nicht viel schneller (siehe: `Wie erreicht man eine möglichst hohe Durchschnittsgeschwindigkeit?`_).

Und auch die Zahl der Höhenmeter ist nicht so aussagekräftig. Bei kleinen Anstiegen kann man nämlich vorher Schwung holen und hat oben noch genug Restgeschwindigkeit, um in der Summe schneller zu sein als mit einem deutlich leichteren Rad, aber das funktioniert nur bis etwa 10 Höhenmetern am Stück (siehe :numref:`fig_speed_vs_uphill`). Nur auf langen Anstiegen wird man langsam (siehe: `Kann man in den Bergen fahren?`_), und nur hier würde ein Motor die Fahrzeit spürbar reduzieren.

Und dann gibt es natürlich noch die gesetzlichen Bestimmungen. In Deutschland darf bei einem Pedelec der Motor bis maximal 25 km/h unterstützen; auf gerader Strecke wäre er damit im Velomobil nur wenige Sekunden im Einsatz, und lohnt sich deshalb einfach nicht. Bergauf ist das natürlich anders. Und dann gibt es noch das S-Pedelec, wo die Motor-Unterstützung bis 45 km/h reicht. Allerdings gibt es dort eine Versicherungspflicht, ein Zulassungsverfahren (entsprechend sind nur sehr wenige Velomobile dafür zugelassen), und man ist auch bei der Auswahl der Fahrradkomponenten stark eingeschränkt.


.. index:: Motor, Tretlagermotor, Nabenmotor, Rekuperation, Pedelec

Welcher Motor?
--------------
Hier gibt es als Bauformen grundsätzlich den Tretlagermotor und den Nabenmotor.

Ein Nabenmotor hat den Vorteil, dass er auch bremsen kann; man kann die Bremsleistung wieder in den Akku zurückspeisen (Rekuperation), und somit immerhin bis zu 2/3 der Bremsenergie zurückgewinnen. Das ist vor allem in stark hügeligem Terrain interessant, wo man viel wegbremsen müsste; so kann man die Bremsen schonen, und die Energie dann für kurze Beschleunigungen nutzen. Das Problem ist allerdings, dass Radnabenmotoren keine Schaltung haben; sie können also entweder schnell fahren oder kraftvoll sein, aber nicht beides. Und je steiler die Steigung ist, desto mehr Leistung müsste der Motor abgeben, aber dessen Leistung steigt mit der Drehzahl. Je langsamer es also bergauf geht, desto geringer wird der Wirkungsgrad – der Motor überhitzt schnell. Daher sind Radnabenmotoren ungeeignet für lange und/oder steile Steigungen.

Tretlagermotoren sind dagegen jenseits von Freilauf und Schaltung montiert. Daher können sie klein und leicht gebaut sein, denn sie können mit hoher Drehzahl arbeiten und die Fahrradschaltung mitnutzen, also unabhängig von der Steigung immer im optimalen Drehzahlbereich arbeiten, ohne zu überhitzen. Dafür ist damit keine Rekuperation möglich. Wenn man lange Steigungen hat, ist ein Tretlagermotor also besser.


.. index:: Motor, Tretlagermotor, Pedelec, Übersetzung

Wie sollte die Übersetzung bei einem Tretlagermotor ausgelegt sein?
-------------------------------------------------------------------
Elektromotoren haben bei einer bestimmten Drehzahl ihren höchsten Wirkungsgrad. Die Übersetzung sollte so gewählt werden, dass diese mit der Wohlfühl-Tretfrequenz im Unterstützungsbereich (also bei einem Pedelec unter 25 km/h) zusammenfällt. Wenn der Motor dann auch noch dafür sorgt, dass der Fahrer möglichst schnell über den Unterstützungsbereich hinaus beschleunigt, ist der Stromverbrauch am geringsten – entweder tritt der Fahrer (weitgehend) selber, oder der Motor arbeitet in seinem effizientesten Drehzahlbereich.



Aerodynamik
===========

.. index:: Luftwiderstand, Querschnittsfläche, Aufrechtrad, Rennrad, Liegerad, Auto

Warum sind Velomobile schnell?
------------------------------
Wegen ihres geringen Luftwiderstands. Wie man in :numref:`fig_comparison_cW-A-cWA` sieht, haben Fahrräder üblicherweise eine ziemlich schlechte Aerodynamik, d.h. ihr Strömungswiderstandskoeffizient (:math:`c_\text{W}`) ist sehr viel höher als bei einem Auto. Der Luftwiderstand ist nur deshalb nicht höher, weil die Querschnittsfläche (:math:`A`) deutlich geringer als bei einem Auto ist. Dagegen ist bei einem Velomobil die Querschnittsfläche nicht unbedingt kleiner als bei einem Rennrad oder Liegerad, aber dafür die Aerodynamik sehr viel besser als selbst bei einem durchschnittlichen Auto, so dass die effektive Querschnittsfläche (:math:`c_\text{W} \cdot A`) viel kleiner ist – bei einem sehr guten Velomobil nur etwa 0.03 m², das entspricht der Fläche eines DIN-A5-Blattes.

.. _fig_comparison_cW-A-cWA:

.. figure:: images/comparison_cW-A-cWA_de.*
    :alt: Vergleich von cW-Wert, Querschnittsfläche und effektiver Querschnittsfläche zwischen verschiedenen Fahrrädern, Velomobil und Auto
    :width: 100%

    Vergleich des Strömungswiderstandskoeffizienten (:math:`c_\text{W}`), der Querschnittsfläche (:math:`A`) und der effektiven Querschnittsfläche (:math:`c_\text{W} \cdot A`) zwischen verschiedenen Fahrrädern, Velomobil und Auto. Wie man sieht, sind Rennräder und Liegeräder vor allem deshalb schneller als Hollandräder, weil ihre Querschnittsfläche geringer ist; die Aerodynamik ist auch nicht sonderlich gut. Dagegen ist bei einem Velomobil die Querschnittsfläche ähnlich, aber die Aerodynamik sehr viel besser.


.. index:: Luftwiderstand, Strömung, Turbulenz

Wie erreicht man einen geringen Luftwiderstand?
-----------------------------------------------

Letztendlich, indem man so wenig :term:`Bewegungsenergie` wie möglich an die Luft abgibt. D.h. die Luft soll langsam beiseite geschoben werden und langsam in ihre Ursprungslage zurückkehren. Jegliche Beschleunigungsenergie, die an die Luft abgegeben wird, ist nämlich für den Antrieb verloren.

Ein einzelnes Luftpaket lässt sich linear nicht sehr stark beschleunigen, weil dabei die benachbarte Luft im Weg steht; diese müsste ebenfalls beschleunigt. Man kann diese zwar etwas zusammendrücken, aber das erzeugt einen Gegendruck. Die lineare Beschleunigung stößt deshalb schnell an Grenzen. Ganz anders ist das bei Rotationen: ein Luftpaket kann sich fast beliebig auf der Stelle drehen, ohne dabei von der benachbarten Luft behindert zu werden. Man kann also viel Energie verlieren, indem man Luft in Drehbewegung versetzt – darum sind Verwirbelungen unbedingt zu vermeiden.


.. index:: Tropfenform, Strömung, Strömungsabriss, Turbulenz, Heck, K-Heck

Warum ist eine Tropfenform üblicherweise recht günstig?
-------------------------------------------------------
Kurz gesagt: Weil sich Luft leichter wirbelfrei auseinander drücken lässt, als sie wirbelfrei zusammen strömen zu lassen. Ersteres kann daher schnell passieren, letzteres braucht mehr Zeit und damit Weg.

Aber es gibt natürlich Ausnahmen. Beispielsweise das :term:`K-Heck`, das einer scharf abgeschnittenen Tropfenform entspricht. Dadurch reißt die Strömung schlagartig ab, statt nach innen gesaugt zu werden; das ist zwar aerodynamisch nicht ganz so gut, aber man erspart sich ein langes Heck, mit seinem Gewicht und seiner Windangriffsfläche.


.. index:: Strömung, Strömungsabriss, Tragfläche

Warum kein Flügelprofil?
------------------------
Das Profil einer Tragfläche gilt als das Musterbeispiel für eine aerodynamisch gute Form. Aber warum hat es diese Form? Eine Tragfläche muss erstens Luft nach unten beschleunigen; und dazu muss sie relativ zur Strömungswinkel einen Anstellwinkel haben. Zweitens darf auf der anderen Seite, wo Unterdruck herrscht, die Strömung nicht abreißen; deshalb ist diese Seite nicht gerade, sondern gekrümmt, damit die Luft langsam um die Kurve geführt wird, statt schlagartig an der Vorderkante abzureißen. Ein Velomobil hat dagegen (bei resultierendem Wind von vorne) keinen Anstellwinkel; es soll die Luft eben möglichst nicht beschleunigen. Gleich ist aber, dass ein :term:`Strömungsabriss` verhindert werden soll – daher die gleichmäßige, fließende Form ohne scharfe Knicke.


.. index:: Luftwiderstand, Bug, Karosserie

Warum ist die Front stumpf statt spitz?
---------------------------------------
Normalerweise assoziiert man mit geringem Strömungswiderstand schlanke, spitze Formen. Aber die Front der meisten Velomobile ist rund. Es stimmt zwar, dass die anströmende Luft langsam zur Seite bewegt werden muss, statt schlagartig auf eine stumpfe Fläche zu treffen. Das ist aber nicht der Fall: Während der Fahrt staut sich die Luft vor dem Velomobil, und dieser „Luftberg“ wirkt wie eine verlängerte Schnauze, der die anströmende Luft schon vorher zur Seite ablenkt. Bei einer Spitze würde sich so ein Luftberg nicht aufbauen, d.h. da würde die Luft schlagartig auf die Karosserie treffen, es wäre nichts gewonnen. Da sich die Karosserie auch hinter der Schnauze weiter vergrößert, legt sich die durch die gestaute Luft abgelenkte Strömung danach wieder an die Karosserie an.


.. index:: Radkasten, Strömung, Karosserie, Turbulenz

Warum sind die (offenen) Radkästen vorne spitz und hinten stumpf?
-----------------------------------------------------------------
Vorne am Radkasten ist die Kante möglichst scharf, damit die Luftströmung dort nicht der Karosserie folgt, sondern möglichst abreißt und am Rad vorbei nach hinten fließt. Das gelingt natürlich nur zu einem gewissen Teil. Hinten soll dagegen der Teil der Strömung, der weiter in den Radkasten eingedrungen ist, nicht stumpf auf der Rückseite des Radkastens aufschlagen und verwirbeln, sondern sanft nach außen gedrückt werden, damit sich die Luftströmung dort wieder parallel an die Karosserie anlegt.


.. index:: Hosen, Radkasten, Wendekreis, Laufradsatz, DF

Was sind Hosen?
---------------
Das sind Verkleidungen, die bei Velomobilen mit offenen Radkästen außen über den Rädern befestigt werden und die Aerodynamik verbessern. Als Ergebnis hat man die gute Aerodynamik wie bei einem Velomobil mit geschlossenen Radkästen – aber eine etwas breitere Spur (also eine bessere Kurvenstabilität) und die Flexibilität, wieder auf offene Radkästen umrüsten zu können. Dafür müssen die Hosen entfernt werden, wenn man etwas an den Reifen oder Rädern gemacht werden muss, und der Wendekreis vergrößert sich auch deutlich. Letzteres lässt sich abmildern mit einem speziellen Laufradsatz, bei dem die Felge weiter nach innen eingespeicht ist.

Die Hosen wurden für das DF entworfen, passen aber auf die meisten Velomobile mit offenen Radkästen. Sie sind recht leicht und flexibel und werden einfach mit Klebeband aufgeklebt.


.. index:: Fußbeulen, Strömung

Warum ist die Unterseite neben den Fußbeulen nicht gerade, sondern weiter gekrümmt?
-----------------------------------------------------------------------------------
Fußbeulen ragen nach außen, und verdrängen die anströmende Luft. Diese kann aber nicht beliebig ausweichen, weil wenige Zentimeter darunter bereits die Fahrbahn ist. Bleibt also nur das Ausweichen zur Seite. Wäre der Fahrzeugboden neben den Fußbeulen gerade, dann wäre der Strömungsquerschnitt für die Luft auf Höhe der Fußbeulen verringert, die Luft würde abgebremst und zur Seite hinausgedrückt, wo sie die dortige Strömung stören würde. Wenn nun neben den Fußbeulen der Boden nach innen gewölbt ist, bleibt die Querschnittsfläche gleich, und die Strömung kann die Fußbeulen leichter umströmen.


.. index:: Windkanal, Strömung, CFD, Radkasten

Warum werden keine Windkanaltests gemacht?
------------------------------------------
Weil sie aufwändig und teuer sind.

Zudem ist es gar nicht so leicht, realistische Strömungsverhältnisse zu erzeugen. Während ein Flugzeug bis auf die Triebwerke außen glatt und nur von Luft umgeben ist, hat man bei einem Velomobil drehende Räder in Radkästen sowie die Straße, die sich in wenigen Zentimetern Abstand relativ zum Velomobil bewegt. Für eine korrekte Simulation reicht es darum nicht, das Velomobil in den Windkanal zu stellen oder im Computer zu modellieren (sogenannte :term:`CFD`-Simulationen), sondern die Räder müssen sich drehen und die Straße muss sich darunter fortbewegen.

Und nicht zuletzt: Um neue Erkenntnisse zu bekommen, reicht es nicht, einfach ein Fahrzeug in den Windkanal zu stellen und zu beobachten. Man muss gezielt Hypothesen aufstellen und testen, d.h. eine genaue Vorstellung davon haben, was man wie ändern könnte. Und gerade weil Velomobile aerodynamisch schon ziemlich gut sind, sind weitere Verbesserungen nur zu erreichen, wenn man ganz genau weiß, nach welchem Detail man sucht.


.. index:: Lufteinlass, Staupunkt, Staudruck, Strömung, Turbulenz

Warum ist der Lufteinlass vorne, weit weg vom Körper?
-----------------------------------------------------
Weil dort der Staupunkt ist.

Wenn der Lufteinlass weiter hinten am Rumpf wäre, dann müsste man die Luft irgendwie nach innen leiten, ohne dabei die äußere Strömung vom Rumpf abzulösen und zu verwirbeln – das würde passieren, wenn ein Lufteinlass im Wind steht. Vorne staut sich dagegen die Luft; man braucht keine Vorrichtung, die die Luft nach innen leitet, sondern ein simples Loch reicht. Zudem liegt die Strömung wegen des Staudrucks immer an, ein Lufteinlass macht da auch nichts kaputt.


.. index:: NACA-Duct, Lufteinlass, Strömung, Belüftung

Was ist ein NACA-Duct?
----------------------
Das ist ein dreieckiger Lufteinlass, der sich seitlich an einem umströmten Körper befindet. Erfinder und Namensgeber ist die NACA, eine US-Forschungseinrichtung für Luftfahrt und Vorläufer der NASA.

Wenn man Luft von der Seite in das Innere eines Fahrzeugs bringen will, reicht ein einfaches Loch nicht aus – die Luft strömt quer zum Loch, und somit kaum durch die Öffnung. Man müsste die Luft umlenken; eine entsprechende „Schaufel“ würde aber die Querschnittsfläche und damit den Luftwiderstand vergrößern, und dahinter womöglich noch bremsende Wirbel erzeugen. Der NACA-Duct geht einen anderen Weg: Die Öffnung befindet sich in Fahrtrichtung (also in Strömungsrichtung), aber nach innen versetzt – dadurch wird die Querschnittsfläche nicht vergrößert. Damit die Luft nach innen kommt, befinden sich in Fahrtrichtung davor zwei scharfe Kanten. An diesen reißt die Strömung ab, und es bilden sich Wirbel, die nach innen rotieren und auf die Einlassöffnung kommen. Der NACA-Duct bringt die Luft also nach innen, indem er nach innen gerichtete Wirbel erzeugt, und diese Wirbel anschließend absaugt – so dass die außen verbleibende Strömung schön glatt ist.


.. index:: Belüftung, Strömung, Grenzschicht

Macht die Belüftung langsamer?
------------------------------
Grundsätzlich ja. Die Luft trifft ja mit Fahrtgeschwindigkeit auf das Velomobil, und wird innen abgebremst – spätestens, indem sie auf den Fahrer trifft. Und die Energie für die Abbremsung der Luft (aus Sicht des Velomobils) bzw. deren Beschleunigung (aus Sicht der Umgebung) muss ja irgendwoher kommen, und zwar aus der :term:`Bewegungsenergie` des Fahrzeugs. Auf der Außenseite würde die Luft dagegen abgelenkt, aber kaum abgebremst, d.h. entsprechend weniger Energie geht dort verloren.

Einen positiven Effekt hat eine Belüftung dagegen, wenn es gelingt, damit die äußere Strömung zu verbessern. Beispielsweise sorgt die steile Front der Haube für einen Staupunkt, d.h. dort staut sich ein „Luftberg“, der einerseits die Front der Haube für die Strömung weniger steil macht (siehe: `Warum ist die Front stumpf statt spitz?`_), andererseits aber die effektive Größe der Haube vergrößert – die Luftströmung muss um die ganze gestaute Luft herumfließen und legt sich darum dahinter schlechter an die Haube/Hutze an. Wenn man nun einen Teil der Luft hier nach innen ableitet, landet sie genau dort, wo sie gebraucht wird (beim Fahrer bzw. auf der Innenseite der Scheibe, `wo sie das Beschlagen der Scheibe verhindert <Was macht man gegen beschlagene Scheiben?>`_), und dabei kommt es nicht nur zu keinen Verwirbelungen außen, sondern der dort gestaute „Luftberg“ mit seinen schädlichen Auswirkungen auf die Strömung wird kleiner. Ebenfalls positiv wäre, wenn es gelänge, eine bereits verwirbelte :term:`Grenzschicht` nach innen abzuleiten, so dass die Strömung außen wieder schön glatt anliegt. Allerdings passiert das vor allem im hinteren Teil, hinter dem Fahrer.


.. index:: Radkasten, Le Mans, Karosserie, Spur, Lenkeinschlag, Radscheiben Leistung, Luftwiderstand, Fahrtwind, laminare Strömung

Sind freistehende Räder viel schlechter als Radkästen?
------------------------------------------------------
Auf den ersten Blick wirkt ein Velomobil mit freistehenden Rädern (wie z.B. das *Le Mans* von *Cycles JV & Fenioux*) sehr attraktiv: Die Karosserie ist schmäler, die Spur ist trotzdem breit, das Lenkgestänge kann aerodynamisch günstig verkleidet werden, es sind größere Räder möglich, und der Lenkeinschlag wird nicht von Radkästen begrenzt. Und wenn die Räder mit :term:`Radscheiben` verkleidet werden, sollte man meinen, dass die Aerodynamik auch nicht so schlecht sein dürfte.

Das Problem ist allerdings, dass die Räder mit ihrer ganzen Oberseite im Wind stehen. Selbst ein glatter Reifen zieht nennenswert Luft mit sich (mit Profil ist das allerdings noch deutlich schlimmer); und bei freistehenden Rädern bewegt sich die Oberseite immerhin mit der doppelten Fahrtgeschwindigkeit gegen die Luft. Bei geschlossenen Radkästen greift wenigstens nicht der Fahrtwind an, die Luft in den Radkästen steht relativ zum Fahrzeug still, so dass die Räder dort nur mit der einfachen Fahrtgeschwindigkeit gegen die Luft arbeiten. Nun ist es aber so, dass die :term:`Luftwiderstandskraft <Luftwiderstand>` quadratisch mit der Geschwindigkeit anwächst, die Leistung sogar kubisch – d.h. ein freistehendes Rad braucht an der Oberseite wegen seiner doppelten Geschwindigkeit die achtfache Leistung. Schlimmer: Die dort von den Reifen nach vorne beförderte Luft bewegt sich ja nicht nur parallel zu den Reifen, sondern auch noch zur Seite, und trifft dort auf den Fahrtwind – d.h. die aerodynamisch effektive Breite eines solchen wunderbar schmalen Reifens ist sehr viel größer. Und noch schlimmer: Da die Räder nicht extrem weit von der Karosserie entfernt stehen, zerstören diese Luftverwirbelungen auch noch die Strömung entlang der Karosserie. Räder sind ja relativ weit vorne, d.h. im größten Teil des Velomobils ist die laminare Strömung zerstört. Das ist der Grund, warum freistehende Räder aerodynamisch extrem schlecht sind.

Anders sähe es aus, wenn man die Räder mit Radverkleidungen versehen würde – die könnte man sehr schmal gestalten (da sie in Kurven mitlenken), und wichtig ist auch nur, dass die obere Hälfte oder die oberen 2/3 des Rades bedeckt sind.


.. index:: Gegenwind, Rückenwind, Seitenwind

Wie macht sich Wind bemerkbar?
------------------------------
Von Gegenwind oder Rückenwind spürt man viel weniger als auf normalen Fahrrädern. Man sieht zwar auf dem Tacho, dass sich die Geschwindigkeit bei Gegen- oder Rückenwind etwas unterscheidet, aber man hat bei weitem nicht das Gefühl, bei Gegenwind „gegen eine Wand“ zu treten.

Seitenwind merkt man v.a. durch Einfluss auf die Lenkung; wenn man ein entsprechend empfindliches Velomobil hat, sollte man bei starkem Wind entsprechend vorsichtig fahren. Dabei ist nicht unbedingt die absolute Kraft das Problem, sondern deren plötzliche Änderung – wenn man beispielsweise bei schneller Bergabfahrt z.B. am Ende einer Hecke aus dem Windschatten kommt und eine Windbö schlagartig auf das Velomobil drückt, braucht man gerne einmal einen Meter seitlichen Platz, um das ausgleichen zu können. Da ist es natürlich extrem ungünstig, wenn es eine enge Straße mit Gegenverkehr ist. Der eigentliche Versatz durch den Wind ist zwar viel kleiner, aber ohne Vorwarnung braucht man sehr gute Reflexe, um die Lenkung halbwegs ruhig halten zu können.

Wenn der Seitenwind sehr stark ist, kann er ein Velomobil durchaus auch umwerfen. Das ist insofern kein Wunder, weil ein Velomobil von seiner Seitenfläche her nicht sehr viel kleiner als ein Auto ist (ca. ein Drittel der Fläche), aber sehr viel weniger wiegt (mit Fahrer ca. ein Zehntel des Gewichts); auch ist das Verhältnis von Höhe (bzw. Höhe des Windangriffspunkts) zu Spurbreite weniger günstig als bei den meisten PKWs. Und so kann es bei starkem Sturm (ca. Windstärke 9/10) durchaus passieren, dass man auf gerader Strecke vom Wind ins Kippen gebracht wird.


.. index:: Windempfindlichkeit, Seitenwind, Nachlauf, Federbein, Längslenker, Karosserie, scheinbarer Wind, Stormstrip

Wie kann man die Windempfindlichkeit reduzieren?
------------------------------------------------
Windempfindlichkeit kann verschiedene Ursachen haben:

* Nachlauf: Wenn die :term:`Federbeine <Federbein>` nicht senkrecht (zur Fahrzeug-Längsachse) stehen, ist der Radaufstandspunkt nicht am :term:`Spurpunkt`, sondern üblicherweise dahinter. Dieser :term:`Nachlauf` ist beim :term:`Einspurer` erwünscht, weil es die Fahrt stabilisiert – das Rad folgt der Richtung des Fahrrads und wirkt dem Kippen entgegen, die Lenkung stellt sich somit von selbst gerade. Bei einem :term:`Mehrspurer` ist das nicht nur weniger wichtig, sondern bei Wind sogar kontraproduktiv: Wenn der Wind das Fahrzeug zur Seite drückt, folgt die Lenkung und steuert das Velomobil vom Wind weg. Ohne Nachlauf gibt es keinen Hebel, über den die Windkraft ein Drehmoment auf die Lenkung ausüben könnte. Durch Einstellen des :term:`Längslenkers <Längslenker>` lässt sich der Nachlauf verringern.

* Umströmung: Ein umströmter Körper erfährt eine Kraft; wenn diese unerwünscht ist, kann es helfen, die Strömung gezielt abreißen zu lassen, beispielsweise mit :term:`Stormstrips <Stormstrip>` – das sind dünne, kantige Profile, die in Längsrichtung auf die Karosserie geklebt werden. Somit erzeugen sie in Fahrtrichtung keinen zusätzlichen Luftwiderstand, aber Windströmung quer zum Fahrzeug reißt dort ab und verwirbelt, statt seitlichen Auftrieb zu erzeugen.

* Form der Karosserie: Wenn die Windangriffsflächen der Karosserie asymmetrisch angeordnet sind, ist der Druckpunkt nicht in der Fahrzeugmitte, und somit entsteht ein Drehmoment, das abhängig von der Richtung des :term:`scheinbaren Windes <scheinbarer Wind>` ist. Auch hier könnten Stormstrips helfen, die Windkräfte besser auszubalancieren.

* Schwerpunkt und Höhe des Windangriffspunkts: Je höher der Windangriffspunkt ist, desto größer ist der Hebel, über den er das Fahrzeug zum Kippen bringt. Und je höher der Schwerpunkt ist, desto weniger muss dieser beim Kippen angehoben werden. Um ein Fahrzeug also weniger kippempfindlich zu machen, kann man den Schwerpunkt möglichst tief (und möglichst nah bei den breit auseinander stehenden Vorderrädern) setzen, und mit Tricks wie z.B. Stormstrips die Windkraft an der Oberseite verringern.

* Verwirbelung: Bei hohen Geschwindigkeiten kann es ein Problem sein, dass sich Wirbel ungleichmäßig ablösen und damit das Heck unruhig werden lassen. Während dieses Verhalten grundsätzlich durch die Karosserieform vorgegeben ist, kann man z.B. durch eine raue Oberfläche am Heck (Golfball-Effekt) für eine kleinräumige Verwirbelung sorgen, die das Ausbilden und Ablösen großer Wirbel verhindert.


.. index:: Segeleffekt, Seitenwind, scheinbarer Wind, Milan GT, Windkanal

Was ist der Segeleffekt?
------------------------
Seitenwind kann für Vortrieb sorgen, d.h. das Velomobil „segelt“ vorwärts. Das ist eigentlich auch kein Wunder, denn es hat eine relativ große Seitenfläche, an der der Wind gut entlangstreichen kann, es rollt leicht, und es ist sehr spurstabil. Also wie ein Segelboot – zwar mit sehr kleinem Segel, aber sehr wenig Fahrwiderstand und einem hervorragendem Kiel ganz ohne Abdrift. Das Problem ist nur, dass ein Velomobil normalerweise deutlich schneller als der Wind unterwegs ist. Das bedeutet: egal aus welcher Richtung der Wind weht, der :term:`scheinbare Wind <scheinbarer Wind>` kommt immer mehr oder weniger von vorne – und damit er bei voller Fahrt nennenswert von der Seite kommt, muss es schon recht ordentlich stürmen.

Ein starker Segeleffekt bedingt dabei nicht unbedingt eine starke Windempfindlichkeit (siehe: `Wie kann man die Windempfindlichkeit reduzieren?`_) – wenn die Karosserie so geformt ist, dass eine starke Windkraft entsteht und diese eine große Komponente in Fahrtrichtung hat, muss sich das weder auf die Lenkung auswirken, noch ein Drehmoment auf die Karosserie ausüben, noch zu ungleichmäßig ablösenden Wirbeln führen.

Der Segeleffekt ist je nach Velomobil-Modell unterschiedlich stark. Eine genauere Untersuchung wurde bisher nur für den *Milan GT* 2011 im Volkswagen-Windkanal gemacht; diese hat ergeben, dass der Segeleffekt existiert und bei einer Richtung des scheinbaren Winds von 75° am stärksten ist.


.. index:: Luftwiderstand, Luftdichte, Höhe, Temperatur, Luftfeuchtigkeit, Battle Mountain, HPV, Pedelec

Unter welchen Bedingungen ist der Luftwiderstand am geringsten?
---------------------------------------------------------------
Form und Querschnittsfläche eines Fahrzeugs sind unveränderlich, und die Geschwindigkeit möchte man nicht reduzieren, sondern erhöhen – somit bleibt nur noch die Luftdichte, mit der der Luftwiderstand verändert werden kann. Eine möglichst geringe Luftdichte gibt es:

* In großer Höhe. Allerdings nimmt mit der Höhe auch die körperliche Leistungsfähigkeit ab, da die Sauerstoffversorgung für den Körper schwieriger wird. Da Sauerstoff im Vergleich zu den anderen Luftbestandteilen relativ schwer ist, nimmt mit der Höhe die vorhandene Sauerstoffmenge überproportional ab. Daher sind Rekordversuche auf 1000 m Höhe eine gute Idee, auf 3000 m aber nicht.
* Bei hoher Temperatur. Dann ist auch der Gummi der Reifen nicht so starr, und der Rollwiderstand ist geringer. Allerdings darf der Körper nicht überhitzen, weil sonst die Leistung stark einbricht.
* Bei feuchter Luft. Allerdings erschwert das die Kühlung, weil Schweiß nicht mehr so gut verdunstet, und der Körper überhitzt leichter. Zudem stören Regentropfen auf der Oberfläche die Aerodynamik, und Wasser auf der Straße erhöht den Rollwiderstand.

Wie man sieht, ist Battle Mountain in der Wüste von Nevada auf einer Höhe von 1375 m und warmem Wüstenklima recht gut für Rekordversuche geeignet. Es gibt aber noch einen weiteren Grund, warum die Rekorde ausgerechnet dort gefahren werden: Es gibt kaum Strecken mit einem perfekten Asphalt, die über mehrere Kilometer absolut eben sind. Entsprechend erlaubt die :term:`IHPVA <HPV>` in ihren Regeln ein maximales Gefälle von zwei Drittel Prozent. Die Strecke von Battle Mountain ist haarscharf darunter, mit einem Gefälle von 0.64%. Das ist zwar sehr wenig und liegt ungefähr im Bereich des Losbrechwiderstands; d.h. ein Fahrrad rollt bei diesem Gefälle nicht von alleine los. Wenn aber ein Rekordfahrer mit 113 km/h unterwegs ist, sind das gut 31 Meter in der Sekunde; bei 0.64% Gefälle sind das 20 cm Höhenunterschied pro Sekunde, was bei einem Gewicht von 100 kg knapp 200 W Zusatzleistung bedeuten – fast so viel wie ein in Deutschland legaler Pedelec-Motor als Dauerleistung bringt.



Physikalische Grundlagen
========================

Anmerkung: Die Beispiel-Rechnungen beziehen sich hier immer auf einen 75 kg schweren Fahrer mit 25 kg Velomobil, bei halbwegs guten Bedingungen, aber ohne Rekord-Trimm.


.. index:: Rollwiderstand, Hysterese

Was ist die dynamische Rollreibung?
-----------------------------------
Grundsätzlich ist :term:`Rollreibung <Rollwiderstand>` kompliziert, da es ein Sammelbegriff ist, in den viele Dinge eingehen:

* Verformung des Reifens
* Rauigkeit der Straße
* Roll- und Gleitreibung in den Lagern

Speziell die Verformung des Reifens ist nicht nur geometrisch komplex, sondern die Hystereseverluste hängen auch von den Materialeigenschaften des Gummis ab. Und dieser ist selbst eine komplexe Zusammenstellung aus vielen Stoffen und ein Betriebsgeheimnis des Herstellers. Was da genau passiert, kann man nicht berechnen – jedenfalls nicht als Außenstehender.

Trotzdem zeigt sich, dass die Rollreibung vor allem von der Auflast abhängt. Entsprechend fasst man alle Einflüsse in der Rollreibungszahl zusammen, die man als Konstante betrachten kann, und damit den Rollwiderstand im relevanten Geschwindigkeitsbereich sehr gut beschreibt. Diese Konstante hat also wenig mit den physikalischen Vorgängen dahinter zu tun, funktioniert aber gut genug.

Nur Erbsenzählern wie Velomobilfahrern reicht das nicht; hier hat sich gezeigt, dass es noch einen kleinen geschwindigkeitsabhängigen Anteil in der Rollreibung gibt – und das ist die sogenannte dynamische Rollreibung. Über ihre Ursachen gibt es kaum Untersuchungen, nur Spekulationen. Es ist also auch ein empirischer Faktor, der gut genug funktioniert, und mit dessen Hilfe man die Rollreibung im Rahmen der Messgenauigkeit ausreichend beschreiben kann.


.. index:: Rollwiderstand, Reifendruck, Reifenflanken, Skinwall, Latex, Tubeless, Reifenlatsch

Wie reduziert man die Rollreibung?
----------------------------------
* Reifendruck: Hoher Luftdruck = niedrigere Reibung. Das funktioniert aber nur bis zu einem bestimmten Grad; wenn der Reifen schon hart ist, verformt er sich kaum noch, und kann deshalb auch durch noch so hohen Druck kaum dazu gebracht werden, sich noch weniger zu verformen. Dafür steigt das Risiko eines Reifenschadens. Und auf rauem Untergrund federt der Reifen nicht, sondern Fahrzeug und Fahrer werden durchgeschüttelt, was auch wieder Leistung kostet (siehe: `Welcher Reifendruck?`_ und :numref:`fig_bicyclerollingresistance_hist`). Deshalb ist eine gute Empfehlung, ungefähr den vom Hersteller angegebenen Maximaldruck zu verwenden, auf rauer Straße auch etwas weniger.

* Schnelle Reifen: Das hängt zum großen Teil von der Gummimischung ab, die man von außen nicht beurteilen kann. Hier muss man sich auf die Angaben der Hersteller verlassen, zusammen mit Erfahrungsberichten aus der Praxis.

* Dünnwandige Reifen: Alle schnellen Reifen haben gemeinsam, dass sie relativ dünnwandig sind – vor allem die Reifenflanken haben oft sehr wenig Gummi. Je weniger Gummi vorhanden ist, desto weniger muss verformt werden; entsprechend geringer sind die Verluste. Zudem passt sich der Reifen besser der Straße an – das erhöht nicht nur den Komfort, sondern reduziert auch die Verluste, die durch Beschleunigungen von Fahrzeug und Fahrer auf rauem Untergrund entstehen.

* Latex-Schläuche oder :term:`Tubeless`: Auch der Schlauch trägt zur Dicke und somit Steifigkeit des Reifens bei. Darum ist ein dünner Schlauch besser als ein dicker, ebenso Latex-Schläuche, oder gleich tubeless (wo dann allerdings die Reifen etwas dicker sind). Wie man in :numref:`fig_bicyclerollingresistance_scatter_p_inc` sieht, rollen diese Reifen nicht nur besser, sondern sind auch weniger empfindlich gegenüber einem reduzierten Luftdruck – der auf rauen Straßen die bremsenden Vibrationen besser schluckt.

* Breite Reifen: Geometrisch sind breite Reifen schneller, da der :term:`Reifenlatsch` kürzer ist und entsprechend in Fahrtrichtung nicht so stark verformt wird. Allerdings hält ein breiter Reifen auch weniger Druck aus, und ist trotzdem meist massiver gebaut. Deshalb kann man nicht einfach von der Reifenbreite auf den :term:`Rollwiderstand` schließen, sondern muss den gesamten Reifen betrachten.

.. _fig_bicyclerollingresistance_scatter_p_inc:

.. figure:: images/bicyclerollingresistance_p-inc-abs_de.*
    :alt: Zunahme des Rollwiderstands, wenn Druck von 8.3 bar auf 4.1 bar sinkt
    :width: 100%

    Rollwiderstand von Rennradreifen bei 8.3 bar sowie dessen Zunahme bei nur noch 4.1 bar Reifendruck. Wie man sieht, haben :term:`Tubeless`-Reifen nicht nur einen niedrigen Rollwiderstand, sondern dieser steigt bei niedrigem Luftdruck auch tendenziell weniger an.


.. index:: Reifen, Luftwiderstand, Rollwiderstand, Butyl, Latex, Temperatur

Warum sind gute Reifen im Velomobil so wichtig?
-----------------------------------------------
Auf gerader Strecke bei konstanter Geschwindigkeit geht die Antriebsenergie vor allem in den :term:`Luftwiderstand` und :term:`Rollwiderstand`. Während die Rollwiderstandskraft konstant ist (bis auf den kleinen Antei der `dynamischen Rollreibung <Was ist die dynamische Rollreibung?>`_), nimmt die Luftwiderstandskraft quadratisch mit der Geschwindigkeit zu; darum dominiert bei normalen Fahrrädern die Luftwiderstandskraft, sobald man mehr als nur gemütlich dahinrollt.

Bei Velomobilen ist das anders. Dort ist der Luftwiderstand viel geringer, so dass selbst bei ca. 50 km/h Luft- und Rollwiderstand ungefähr gleich groß sind (siehe :numref:`fig_power_comparison_stack`). Aber kaum ein Velomobilfahrer fährt ständig mehr als 50 km/h, wo der Luftwiderstand dominiert. Weil also der Luftwiderstand so klein ist, ist der Anteil des Rollwiderstands viel größer, so dass dieser während der meisten Fahrzeit dominiert (siehe :numref:`fig_speed_histogram_spezi2019`). (Auf schnellen Bergab-Passagen dominiert der Luftwiderstand deutlich, aber das macht nur einen kleinen Anteil der Fahrzeit aus.) Darum sind beim Velomobil gute Reifen wichtig, weil man damit den dominierenden Rollwiderstand reduzieren kann. Das geht so weit, dass sich sogar das Material der Schläuche (Butyl oder Latex) und die Umgebungstemperatur spürbar auf die erreichbare Geschwindigkeit auswirkt.

.. _fig_power_comparison_stack:

.. figure:: images/power_comparison_stack_de.*
    :alt: Vergleich von Antriebsverlusten, Rollwiderstand und Luftwiderstand
    :width: 100%

    Vergleich von Antriebsverlusten (:math:`P_\text{chain}`), Rollwiderstand (:math:`P_\text{roll}`) und Luftwiderstand (:math:`P_\text{air}`) bei verschiedenen Geschwindigkeiten. Wie man sieht, ist der Luftwiderstand unterhalb von ca. 15 km/h komplett vernachlässigbar, und selbst bei 50 km/h macht er noch nicht einmal die Hälfte aus. Das bedeutet, dass während der Fahrt meistens der Rollwiderstand dominiert – außer bei wenigen Hochgeschwindigkeitsabschnitten. Bei anderen Fahrrädern sind Rollwiderstand und Antriebsverlust ungefähr ähnlich, aber der Luftwiderstand deutlich höher, um den Faktor 5 bis 10.


.. _fig_speed_histogram_spezi2019:

.. figure:: images/speed-histogram_spezi2019_de.*
    :alt: Geschwindigkeitshistogramm einer Langstreckenfahrt
    :width: 100%

    Histogramm der Geschwindigkeiten einer Langstreckenfahrt (zur SPEZI 2019, erste ca. 200 km). Unter der Annahme der gleichen Widerstandskoeffizienten und des gleichen Systemgewichts wie in den anderen Diagrammen (und Vernachlässigung von Bremsungen) ergibt sich, dass trotz flotter Geschwindigkeit (Median: 36.7 km/h) etwa doppelt so viel Energie in den Rollwiderstand gegangen ist (62%) wie in den Luftwiderstand (31%).


.. index:: Gewicht, Luftwiderstand, Rollwiderstand, Bewegungsenergie, delphinieren

Warum sind Velomobil-Fahrer so besessen von niedrigem Gewicht?
--------------------------------------------------------------
Viele Leute sagen, dass ihnen ein niedriges Gewicht nicht so wichtig sei:

* Sie würden ja keine Rennen fahren, darum käme es nicht auf die letzte Sekunde an, sondern Zuverlässigkeit sei wichtiger.

* Die Gewichtsersparnis sei ja im Verhältnis zum Gesamtgewicht (Fahrer, Fahrzeug und Gepäck) zu vernachlässigen.

Ist zwar beides grundsätzlich richtig; aber abgesehen davon, dass die Zuverlässigkeit wenig mit dem Gewicht zu tun hat (ein leichtes Fahrzeug ist nicht unbedingt empfindlicher, sondern nur aufwändiger hergestellt), muss man sich die Energiebilanz anschauen. Und da kommt es auch wieder auf den :term:`Luftwiderstand` an – bzw. dessen Abwesenheit. Während bei einem normalen Fahrrad bei hoher Geschwindigkeit ein Großteil der Energie im Luftwiderstand verheizt wird, ist es beim Velomobil, trotz deutlich höherer Geschwindigkeit, meist weniger als die Hälfte. Und in den Luftwiderstand geht die Masse nicht ein, sondern in folgende Widerstände:

* :term:`Rollwiderstand`: Das ist meist der dominierende Widerstand, und er steigt linear mit dem Gewicht.

* :term:`Bewegungsenergie`: Da ein Velomobil deutlich höhere Geschwindigkeiten erreichen kann, sind die Beschleunigungsphasen auch deutlich länger – die :term:`kinetische Energie <Bewegungsenergie>` wächst quadratisch mit der Geschwindigkeit, d.h. für die doppelte Geschwindigkeit muss die vierfache Energie hineingesteckt werden. Entsprechend machen Beschleunigungsphasen einen viel größeren Anteil in der Energiebilanz aus (siehe :numref:`fig_kinetic-energy_ratio_cWA` und `Wie erreicht man eine möglichst hohe Durchschnittsgeschwindigkeit?`_).

* Bergauf: Hier merkt man das Gewicht auch deutlich; und weil bergauf das Velomobil keinen aerodynamischen Vorteil hat, sondern durch sein höheres Gewicht einen Nachteil (siehe :numref:`fig_speed_vs_power_uphill`), fühlt sich der Unterschied noch stärker an.

Bei einem Velomobil spielt das Gewicht also eine größere Rolle, gleichzeitig ist es aber deutlich schwerer – darum zahlt sich eine Gewichtsreduktion stärker aus (siehe :numref:`fig_speed_vs_power_with_luggage`). Und nicht nur bei sportlichen Fahrern: Je schwächer ein Fahrer ist (also je niedriger die Geschwindigkeit), desto geringer ist der Luftwiderstand – und der Rollwiderstand hat einen umso größeren Anteil. Zudem können starke Fahrer in welligem Gelände Hügel oft mit Schwung nehmen (:term:`delphinieren`), während schwache Fahrer ihr Gewicht ohne Schwung hinaufschleppen müssen.

Und ganz pragmatisch: Es ist schon sehr angenehm, wenn ein Velomobil so leicht ist, dass man es alleine z.B. über ein paar Stufen tragen oder in einen Zug heben kann, statt immer eine zweite Person dazu zu brauchen.

.. _fig_speed_vs_power_with_luggage:

.. figure:: images/speed_vs_power_luggage_de.*
    :alt: Mehrleistung mit Gepäck, bei Rennrad und Velomobil
    :width: 100%

    Zusätzlicher Leistungsbedarf durch Gepäck, bei einem Rennrad (gestrichelt) und bei einem Velomobil (durchgezogen). Bei geringen Geschwindigkeiten spielt das Gepäck beim Rennrad eine größere Rolle, weil das Fahrrad deutlich leichter ist – das Gepäck macht also einen größeren Anteil des Gesamtgewichts aus. Das ist aber ebenso der Fall bei leichten Fahrern. Bei höheren Geschwindigkeiten kommt der Luftwiderstand dazu, so dass sich Gewicht weniger bemerkbar macht; aber da ein Velomobil einen geringeren Luftwiderstand hat, bleibt der Einfluss des Gepäckgewichts deutlich größer. Entsprechend sollten vor allem leichte, leistungsschwache Fahrer auf ein möglichst geringes Gesamtgewicht achten.


.. index:: Gewicht, Bewegungsenergie, Rotationsenergie, Trägheitsmoment, Winkelgeschwindigkeit

Warum heißt es, dass beim Gewicht die Räder doppelt zählen?
-----------------------------------------------------------
Um ein Rad auf eine bestimmte Geschwindigkeit zu bringen, ist folgende Energie nötig:

* :term:`Bewegungsenergie`: :math:`E_\text{kin} = 1/2 \cdot m \cdot v^2`

* Rotationsenergie: :math:`E_\text{rot} = 1/2 \cdot J \cdot \omega^2`

Wie man sieht, sind beide Formeln recht ähnlich:

* :math:`J` ist das Trägheitsmoment; es hängt von der Masse und deren Entfernung vom Drehpunkt ab. Wenn man annimmt, dass die Masse ganz außen am Rad sitzt, gilt: :math:`J = m \cdot r^2`.

* :math:`\omega` ist die Winkelgeschwindigkeit, d.h. die Zahl der Umdrehungen pro Zeit (:math:`2 \pi / T`). Da sich die Lauffläche des Rades mit der Fahrgeschwindigkeit bewegen muss, also der Umfang pro Zeit gleich der Geschwindigkeit sein muss (:math:`2 \cdot r \cdot \pi / T = v`), ergibt sich: :math:`\omega = v / r`

Wenn man das einsetzt, ergibt sich für die Rotationsenergie: :math:`E_\text{rot} = 1/2 \cdot m \cdot r^2 \cdot (v / r)^2 = 1/2 \cdot m \cdot v^2`. Das bedeutet: Wenn beim Rad die gesamte Masse außen an der Lauffläche sitzt, ist die Rotationsenergie gleich der Bewegungsenergie – daher die Aussage, dass das Gewicht der Räder doppelt zählen würde, denn Rotationsenergie und Bewegungsenergie ist dann zusammengenommen doppelt so groß wie die Bewegungsenergie einer nicht rotierenden Komponente.

In Realität machen Felge und Reifen zwar den größten Teil des Rades aus, aber die Masse sitzt nicht ganz außen, daher ist der Beitrag der Rotationsenergie kleiner – es ist also nicht ganz doppelt so viel. Und wie man sieht, ist dabei die Größe des Rades irrelevant: ein großes Rad hat ein höheres Trägheitsmoment (in das der Radius quadratisch eingeht), aber dreht sich entsprechend langsamer (die Winkelgeschwindigkeit wird ebenfalls quadriert), so dass die Rotationsenergie gleich ist.


.. index:: Höchstgeschwindigkeit, Luftwiderstand, Bewegungsenergie, Beschleunigungsstrecke

Warum ist es so schwer, noch schneller zu fahren?
-------------------------------------------------
Die Höchstgeschwindigkeit wird vor allem vom :term:`Luftwiderstand` bestimmt – denn dieser steigt schneller als andere Fahrwiderstände. Die Luftwiderstandskraft steigt quadratisch mit der Geschwindigkeit; um eine Strecke doppelt so schnell zu fahren, erfordert also vier Mal so viel Energie für den Luftwiderstand. Da man die Strecke dann aber in der halben Zeit zurücklegt, muss man die erwähnte vierfache Energie in der halben Zeit erbringen – Arbeit pro Zeit ist Leistung, also muss man für die doppelte Geschwindigkeit die achtfache Leistung erbringen. (Das ist etwas vereinfacht und gilt nur näherungsweise für sehr hohe Geschwindigkeiten; da der Luftwiderstand nur einen Teil des gesamten Fahrwiderstands ausmacht, wächst dieser langsamer, siehe: `Warum beschleunigt man im Velomobil nicht „wie gegen eine Wand“?`_)

Ein zweiter limitierender Faktor ist die große Menge an :term:`Bewegungsenergie`, die aufgebaut werden muss (siehe :numref:`fig_kinetic-energy_ratio_cWA` und `Warum sind bei einem Velomobil die Beschleunigungsphasen so lang?`_). Das braucht nämlich viel Zeit und Strecke – gerade bei hohen Geschwindigkeiten wird ein Großteil der Tretleistung bereits von den Fahrwiderständen verbraucht, so dass nur noch wenig Leistung für die weitere Beschleunigung übrig bleibt. Somit beschleunigt man nur sehr langsam weiter, braucht aber wegen der bereits hohen Geschwindigkeit dafür viel freie Strecke.

Und ein drittes Problem ist die Zeit: Wenn man für eine bestimmte Strecke in einer bestimmten Geschwindigkeit 20 Minuten braucht, dann kann man mit der doppelten Geschwindigkeit 10 Minuten einsparen – muss aber (wenn man nur den Luftwiderstand betrachtet) die achtfache Leistung erbringen. Eine weitere Verdopplung der Geschwindigkeit führt wiederum zum achtfachen Leistungsbedarf (also insgesamt die 64-fache Leistung), aber erbringt nur noch weitere 5 Minuten Ersparnis (also spart nur ein weiteres Viertel der ursprünglichen Fahrzeit).


.. index:: Höchstgeschwindigkeit, Luftwiderstand, Bewegungsenergie, Beschleunigungsstrecke

Wie schnell kann ein Velomobil werden?
--------------------------------------
Während bei einem Velomobil der Luftwiderstand deutlich geringer und die theoretisch erreichbare Geschwindigkeit deutlich höher als bei anderen Fahrrädern ist, ist die dafür nötige Bewegungsenergie jedoch nicht geringer – wegen der höheren Masse von Velomobilen sogar etwas höher. In die Bewegungsenergie geht die Geschwindigkeit quadratisch ein; um doppelt so schnell zu fahren, muss man also nicht nur die achtfache Leistung bringen, um den Luftwiderstand zu überwinden, sondern während des Beschleunigens auch die vierfache Bewegungsenergie aufbringen.

Wenn man beispielsweise 300 Watt leisten kann, braucht man etwa zwei Minuten und 1.5 km, um 60 km/h zu erreichen (Systemgewicht: 100 kg). Wenn man bergab fährt, bekommt man zwar etliche 100 Watt vom Gefälle dazu; jedoch wird mit zunehmender Geschwindigkeit auch die nötige Gefällestrecke immer länger: wenn es beispielsweise mit 5% bergab geht, kann man mit 300 Watt Tretleistung in den zwei Minuten rund 125 km/h erreichen – das Gefälle muss dazu aber auch mehr als 3 km lang sein und 160 m Höhendifferenz haben – und für den Bremsweg noch einmal deutlich länger ohne Kurven oder Hindernisse sein. Daher erreicht selbst ein trainierter Fahrer nur sehr selten bei idealen Bedingungen überhaupt einmal 100 km/h, obwohl bei einem idealen Gefälle noch viel mehr möglich sein sollte.


.. index:: Höchstgeschwindigkeit, Durchschnittsgeschwindigkeit, Beschleunigungsstrecke

Sind Velomobile schnell wegen ihrer hohen Höchstgeschwindigkeit?
----------------------------------------------------------------
Eigentlich nicht. Da der Mensch recht leistungsschwach ist, hängt die erreichbare Höchstgeschwindigkeit mehr vom Gefälle als von der Tretleistung ab; und da hohe Endgeschwindigkeiten sehr lange Beschleunigungsstrecken brauchen (siehe :numref:`fig_time_distance_endspeed_300W`), sind in der Praxis die Höchstgeschwindigkeiten von Velomobil und schnellen anderen Fahrrädern gar nicht so unterschiedlich.

Was aber deutlich anders ist: Mit einem Velomobil hält man eine hohe Dauergeschwindigkeit sehr viel länger als mit einem anderen schnellen Fahrrad – wenn man nicht abbremsen muss.


.. index:: Luftwiderstand, Rollwiderstand

Warum beschleunigt man im Velomobil nicht „wie gegen eine Wand“?
----------------------------------------------------------------
Bei aerodynamisch schlechten Fahrrädern spürt man eine relativ deutliche Maximalgeschwindigkeit; selbst mit deutlich mehr Anstrengung wird man nicht mehr nennenswert schneller. Das fühlt sich im Velomobil anders an: man tritt nicht „wie gegen eine Wand“, sondern kann leichter weiter beschleunigen (was nur entsprechend lange dauert und entsprechend viel Strecke benötigt; siehe `Warum sind bei einem Velomobil die Beschleunigungsphasen so lang?`_). Der Grund liegt an dem hohen Anteil des Rollwiderstands (siehe :numref:`fig_power_comparison_stack`); da dieser langsamer anwächst als der Luftwiderstand, wächst auch der Gesamtwiderstand mit einer niedrigeren Potenz (siehe :numref:`fig_speed_vs_power-exponent`) – bei gleicher Geschwindigkeit ist bei einem Velomobil also nicht nur der Gesamtwiderstand niedriger als bei einem konventionellen Fahrrad, sondern der Gesamtwiderstand steigt dort auch langsamer, und somit ist es leichter, noch schneller zu werden.

.. _fig_speed_vs_power-exponent:

.. figure:: images/speed_vs_power-exponent_de.*
    :alt: Zunahme der Fahrwiderstände
    :width: 100%

    Effektiver Exponent, mit dem die Fahrwiderstände mit der Geschwindigkeit zunehmen, bei verschiedenen Steigungen; die durchgezogene Linie ist ein Velomobil, die gestrichelte Linie ist ein Rennrad. Die verschiedenen Fahrwiderstände nehmen unterschiedlich stark mit der Geschwindigkeit zu; während die (nicht geschwindigkeitsabhängige) Rollwiderstandsleistung linear mit der Geschwindigkeit steigt, wächst die Luftwiderstandsleistung kubisch. Aber da der Rollwiderstand bei einem Velomobil einen großen Anteil am Gesamtwiderstand hat, wächst dieser effektiv selbst bei hohen Geschwindigkeiten nur ungefähr quadratisch, während er bei einem Rennrad viel näher am kubischen Wachstum ist. Bergauf ist der effektive Exponent kleiner, da die nur linear mit der Geschwindigkeit wachsende Kletterarbeit dazu kommt – der Gesamtwiderstand ist bergauf natürlich deutlich höher als in der Ebene, er wächst nur langsamer an.


.. index:: Beschleunigung, Bewegungsenergie, Battle Mountain

Warum sind bei einem Velomobil die Beschleunigungsphasen so lang?
-----------------------------------------------------------------
Es liegt jedenfalls nicht an einer ineffizienten Kraftübertragung oder einem höheren Gesamtgewicht – wie man in :numref:`fig_time_distance_endspeed_300W` sehen kann, beschleunigt ein Velomobil praktisch genauso gut wie jedes andere Fahrrad – bei niedrigen Geschwindigkeiten macht sich zwar die höhere Masse bemerkbar, aber ab etwa 30 km/h wird das vom geringen Luftwiderstand überkompensiert.

Trotzdem sind die Beschleunigungsphasen sehr lang – so lang, dass man nicht nach jeder Bremsung wieder auf Reisegeschwindigkeit sprinten kann, sondern sich die Kräfte einteilen muss. Bei anderen Fahrrädern funktioniert das gut, und ist aus Effizienzbetrachtungen sogar empfehlenswert (siehe: `Wie erreicht man eine möglichst hohe Durchschnittsgeschwindigkeit?`_); beim Velomobil würde man sich jedoch total verausgaben, wenn man die langen Beschleunigungsphasen mit einer hohen Sprintleistung fahren wollte.

Der Grund dafür ist: Beim Velomobil ist zwar der Luftwiderstand klein, nicht jedoch die Bewegungsenergie. So kann man zwar hohe Geschwindigkeiten fahren, muss aber die dazu nötige Bewegungsenergie erst einmal aufbauen. Wie man in :numref:`fig_kinetic-energy_ratio_cWA` sieht, beträgt bei 50 km/h die Bewegungsenergie mehr als das 50-fache der Energie, die bei dieser Geschwindigkeit pro Sekunde für Luftwiderstand, Rollwiderstand und Antriebsverluste benötigt wird. Oder anders gesagt: Jemand, der genug Dauerleistung tritt, um 50 km/h zu fahren (das sind in unserem Beispiel etwa 179 W), muss erst einmal 40 Sekunden lang doppelt so viel leisten, um das Fahrzeug hochzubeschleunigen  – und braucht dafür 400 Meter freie Strecke. Und wenn dann eine rote Ampel kommt, beginnt das wieder von vorne.

Bei jedem anderen Fahrrad wäre es übrigens genauso mühsam und langwierig, die für eine hohe Geschwindigkeit nötige Bewegungsenergie aufzubauen – man wird lediglich viel früher vom Luftwiderstand gestoppt.

Das ist übrigens der Grund, warum in Battle Mountain selbst bei den 200-m-Sprints ein Anlauf von 8 km genommen wird – anders könnte man die Bewegungsenergie für 130 km/h gar nicht aufbauen; die Messstrecke selbst wird dann in wenigen Sekunden durchfahren. Und es dürfte auch einer der Gründe sein, warum in der Velomobil-Szene Langstrecken-Ausdauer-Rekorde populär sind – denn da muss man nur einmal beschleunigen, und dann die Geschwindigkeit „nur“ noch halten.

.. _fig_kinetic-energy_ratio_cWA:

.. figure:: images/kinetic-energy_ratio_cWA_de.*
    :alt: kinetische Energie und Fahrwiderstände
    :width: 100%

    Vergleich der kinetischen Energie mit den Fahrwiderständen; Bezugsgröße ist die Energie, die durch die Fahrwiderstände in einer Sekunde verbraucht wird. Da im Velomobil die Fahrwiderstände bei hoher Geschwindigkeit geringer sind, fällt der Aufbau der Bewegungsenergie viel mehr ins Gewicht.


.. index:: Beschleunigung, Bewegungsenergie, Auto

Warum beschleunigen Autos besser, obwohl sie schwerer sind?
-----------------------------------------------------------
Weil sie viel höhere Leistungsreserven haben.

Ein durchschnittlich großer sportlicher Radfahrer kann zwar viele hundert Watt leisten – aber nur über wenige Sekunden. Über längere Zeit sind es eher in der Größenordnung von 200 Watt. Wie man in :numref:`fig_power_comparison_stack` sieht, kann man im Velomobil damit gut 50 km/h fahren. Allerdings ist der Leistungsbedarf bergauf oder beim Beschleunigen ein Vielfaches davon (siehe: `Wie erreicht man eine möglichst hohe Durchschnittsgeschwindigkeit?`_ und `Warum sind bei einem Velomobil die Beschleunigungsphasen so lang?`_). Beispielsweise beträgt die :term:`Bewegungsenergie` bei 50 km/h bereits knapp 10 kJ; für ein Auto ist eine Beschleunigung von 0 auf 50 km/h in 10 Sekunden ziemlich lahm, aber ein Radfahrer müsste dafür durchschnittlich 1000 Watt zusätzlich zu den sonstigen Fahrwiderständen aufbringen. Das schafft nur ein Profi-Sportler.

Ein Auto braucht für 100 km/h ganz grob 13 kW (mit den Daten aus :numref:`fig_comparison_cW-A-cWA` und 1000 kg) – umgerechnet sind das knapp 18 PS. Aber kaum ein Auto hat weniger als 100 PS. Eine gemütliche Beschleunigung von 0 auf 50 km/h in 10 Sekunden kostet nur durchschnittlich 13 PS – zusammen mit den Fahrwiderständen braucht das Auto also weniger als ein Viertel seiner Maximalleistung (welche ein Auto nicht nur wenige Sekunden erbringen kann), statt sich wie ein Radfahrer dafür komplett verausgaben zu müssen.

Bergauf ist es übrigens ähnlich: Um eine 5%-Steigung mit 50 km/h zu fahren, erfordert mit dem Velomobil knapp 1000 Watt – selbst für einen Profi-Sportler kaum machbar. Ein 1000 kg schweres Auto braucht gut die zehnfache Leistung (über 11 kW), aber diese etwa 16 PS sind lächerlich für ein Auto. Das kommt erst langsam an die Grenzen, wenn auf der Autobahn die Kasseler Berge (bis zu 8%) mit deutlich über 100 km/h gefahren werden.


.. index:: Durchschnittsgeschwindigkeit, Rollwiderstand, Luftwiderstand, Kletterarbeit, Beschleunigung, Muskel, Normalized Power

Wie erreicht man eine möglichst hohe Durchschnittsgeschwindigkeit?
------------------------------------------------------------------
Eigentlich ist es ganz einfach: Nur die in Roll- und Luftwiderstand investierte Energie ist verloren (und natürlich die Bremsenergie); dagegen wird Beschleunigungs- und Kletterarbeit in eine andere Energieform umgewandelt, und kann wieder zurückgewonnen werden. Und von den Verlusten ist es nur der Luftwiderstand, der stark mit der Geschwindigkeit anwächst. Daher ist es energetisch am günstigsten, mit einer möglichst konstanten Geschwindigkeit zu fahren.

Dagegen will der Körper eine möglichst konstante Leistungsabgabe – hohe Leistungsspitzen erfordern einen anaeroben Stoffwechsel und ermüden die Muskeln.

Wie sieht das in Zahlen aus?

* Wie man in :numref:`fig_speed_vs_power_uphill` sieht, ist bei dem gerechneten Beispiel-Velomobil der Leistungsbedarf bei 1% Steigung ungefähr doppelt so hoch wie in der Ebene (bei hohen Geschwindigkeiten etwas weniger). Wegen der Kleinwinkelnäherung kann man entsprechend annehmen, dass man für 2% die dreifache Leistung, für 3% die vierfache Leistung usw. braucht. Bergauf kann man sich also extrem verausgaben, wenn man die Geschwindigkeit halten will. Oder umgekehrt: Wenn man annimmt, dass in Steigungen bei entsprechend niedrigen Geschwindigkeiten die Leistung ungefähr linear zunimmt (das stimmt recht gut, siehe :numref:`fig_speed_vs_power-exponent`), dann ist bei gleicher Tretleistung die Geschwindigkeit bei 1% Steigung nur noch halb so hoch, bei 2% Steigung nur noch ein Drittel usw.

* Die Beschleunigung ist etwas schwieriger zu beschreiben, denn man fährt ja nicht bewusst mit einer bestimmten Beschleunigungsleistung – sondern beschleunigt wird mit der verbleibenden Leistung, nach dem Abzug der ganzen anderen Fahrwiderstände. Aber letztendlich lässt sich die Beschleunigung genauso beschreiben wie eine Steigung – denn bei letzterer wirkt die Erdbeschleunigung, die 9.81 m/s² beträgt. Eine 1%ige Steigung, die den Fahrwiderstand verdoppelt, entspricht also einer Beschleunigung um 1% der Erdbeschleunigung; das ist etwa 1/3 km/h pro Sekunde. Das ist nicht besonders rasant – bei dieser Beschleunigung bräuchte man von 0 auf 100 knappe fünf Minuten. In der Praxis beschleunigt man aber deutlich stärker, weil es weniger lange dauert; wie man in :numref:`fig_speed_vs_uphill` sehen kann, entspricht 50 km/h einem Hügel von nur etwa 9 m Höhe, d.h. den kann man leichter wegsprinten als einen hohen Berg, und ist dafür früher schnell. (Wie man in :numref:`fig_time_distance_endspeed_300W` sieht, sind die Beschleunigungsstrecken selbst bei anstrengender Tretleistung noch lang genug.) Man investiert also oft ein Mehrfaches des Fahrwiderstands, aber nur für einige Sekunden.

* Ganz anders ist es beim Fahren mit konstanter Geschwindigkeit in der Ebene. Natürlich nimmt die Luftwiderstandsleistung kubisch mit der Geschwindigkeit zu – aber erstens stimmt das schon für den Gesamtwiderstand nicht mehr (siehe :numref:`fig_speed_vs_power-exponent`), und zweitens schwankt die Leistung hier auch nicht so stark – zwischen gemütlichem Herumgondeln und sportlichem Fahren liegt vielleicht 30% mehr Geschwindigkeit, was dann gerade einmal einer doppelt so hohen Leistung entspricht (siehe :numref:`fig_power_comparison_stack`). Dafür handelt es sich hier nicht um einen Sprint von einigen Sekunden, sondern die Leistung wird über Minuten bis Stunden erbracht.

* Der menschliche Körper lässt sich nicht so leicht berechnen (und ist zudem individuell verschieden). Aber zumindest das Konzept der :term:`Normalized Power` könnte einen Hinweis geben. Diese beschreibt, wie anstrengend eine bestimmte Durchschnittsleistung empfunden wird. Hierbei wird ein gleitender Durchschnitt der Tretleistung über 30 Sekunden berechnet und zur vierten Potenz genommen. Niedrige und hohe Leistungen heben sich also nicht gegenseitig auf, sondern starke Abweichungen von der Durchschnittsleistung in beide Richtungen machen sich deutlich bemerkbar.

Zusammengefasst heißt das: Die Verluste steigen zwar überproportional mit der Geschwindigkeit, aber sowohl bei Steigungen als auch bei Beschleunigungen kann sich die benötigte Leistung bei gleicher Geschwindigkeit vervielfachen. Der Körper reagiert aber sehr empfindlich auf Leistungsschwankungen – zumindest über Minuten, nicht über Sekunden. Daher muss man einen Kompromiss finden: Einerseits die Geschwindigkeit nicht zu sehr einbrechen lassen, andererseits die gemittelte Leistung konstant halten.

* Bergauf bedeutet das: Kurze Rampen wegsprinten, und sich in der Ebene erholen – aber lange Anstiege gemütlich nehmen. Darum ist das :term:`Delphinieren <delphinieren>` bei kleinen Hügeln so effizient: Man verausgabt sich nicht, weil die Durchschnittsleistung nicht übermäßig hoch ist, aber die Geschwindigkeit trotzdem nicht einbricht.

* Beim Beschleunigen: Schnell auf Reisegeschwindigkeit kommen, und dann gemütlich dahin fahren. Während man jedoch mit dem Rennrad problemlos in einigen Sekunden bis auf Endgeschwindigkeit sprinten kann, sind beim Velomobil wegen der höheren Endgeschwindigkeit und damit viel höheren :term:`kinetischen Energie <Bewegungsenergie>` die Beschleunigungsvorgänge deutlich länger. Daher sollte man zwar schon zügig losbeschleunigen, solange die Fahrwiderstände noch niedrig sind – aber nicht gleich bis auf Endgeschwindigkeit, sondern sich die Kräfte einteilen. Ansonsten werden Strecken mit häufigen Brems- und Beschleunigungsvorgängen sehr anstrengend, aber kaum schneller. Das heißt aber nicht, dass ein Velomobil bei häufigen Stopps im Stadtverkehr grundsätzlich langsam ist – wie man in :numref:`fig_time_distance_endspeed_300W` sieht, dauert die Beschleunigung auf die gleiche Geschwindigkeit ungefähr genauso lange wie bei einem Rennrad. Aber höhere Geschwindigkeiten sind mangels freier Strecke nicht erreichbar.

* Und in der Ebene bringt es nicht viel, maximal schnell zu sein, wenn einem die Körner an anderer Stelle fehlen. Hier sollte man sich lieber ausruhen. Oder langsam zusätzliches Tempo aufbauen, um den nächsten Anstieg mit mehr Schwung fahren zu können.

* Nicht zuletzt ist die Durchschnittsgeschwindigkeit der zeitliche Mittelwert, d.h. langsame Abschnitte fallen stärker ins Gewicht als schnelle Abschnitte.

Man fährt also nicht schnell, indem möglichst oft eine hohe Zahl auf dem Tacho steht – sondern möglichst selten eine ganz niedrige.


.. index:: Neigetechnik, Schwerpunkt, Spurbreite, Sitz, kippen

Ist Neigetechnik bei Velomobilen sinnvoll?
------------------------------------------
Mit Neigetechnik kann man schneller durch die Kurven fahren – heißt es. Das stimmt aber nur bedingt; ob das Fahrzeug nämlich aufrecht oder geneigt durch die Kurve fährt, ändert an der möglichen Geschwindigkeit normalerweise überhaupt nichts (wenn die Position des :term:`Schwerpunkts <Schwerpunkt>` gleich ist).

Der erste limitierende Faktor bei der Kurvengeschwindigkeit ist das Wegrutschen. Dies hängt neben der Oberflächenbeschaffenheit der Straße vor allem vom Reifenmaterial ab – ein guter Reifen soll leicht rollen, aber viel Haftung in den Kurven haben. Und hier gibt es tatsächlich Modelle, die in der Mitte der Lauffläche eine andere Gummimischung verwenden als am Rand, so dass man geradeaus auf dem leichtlaufendem Gummi fährt, aber wenn man sich in die Kurve legt, auf dem besser haftenden Gummi. Diesen Effekt kann man natürlich in einem mehrspurigen Fahrzeug ohne Neigetechnik nicht ausnutzen.

Der zweite limitierende Faktor ist das Umkippen. Und das hängt vor allem von der Höhe des Schwerpunkts (möglichst niedrig) und seinem Abstand zum kurvenäußeren Rad (möglichst groß) ab. Mit einer Neigetechnik kann man natürlich den Schwerpunkt Richtung Kurveninnenseite verschieben, und damit dessen Abstand zum kurvenäußeren Rad vergrößern. Das geht aber auch, indem man sich einfach Richtung Kurveninnenseite lehnt – vorausgesetzt, man hat eine Sitzposition mit ausreichend Seitenhalt, wo das möglich ist.

Eine Neigetechnik hätte also tatsächlich gewisse Vorteile; allerdings auch ein höheres Gewicht, eine größere technische Komplexität, und Platz braucht das auch noch. Man kann mit Neigetechnik zwar den Schwerpunkt seitlich verschieben, aber man wird ihn wohl nicht so niedrig bekommen wie ohne Neigetechnik. Selbst wenn man nicht das ganze Fahrzeug, sondern nur den schwersten Teil – den Fahrer – neigt. Und dann braucht man ja auch noch eine Energiequelle, die die Neigung in Gegenrichtung zur Fliehkraft durchführt – wenn man es per Hand macht, kann man gleich auf die Mechanik verzichten und einfach seinen Körper nach innen lehnen. Es gibt zwar so etwas wie eine passive Neigetechnik, aber dabei ist das Fahrzeug im Prinzip wie eine Schaukel auf dem Fahrgestell aufgehängt – ja, es neigt sich in der Kurve von alleine, aber das verhindert nur, dass man vom Sitz rutscht; schneller kann man damit nicht durch die Kurve fahren, sondern im Gegenteil nur langsamer (weil der Schwerpunkt nach außen statt innen schwingt).

Und nicht zuletzt gibt es in der Praxis sehr wenige enge Kurven, die man rasant fahren könnte; echte Kurven sind meist weit genug, und bei engen Einmündungen hat man selten die Übersicht, um ungebremst durchzufahren. Für diese wenigen Sonderfälle und wenigen gesparten Sekunden lohnt sich keine aufwändige Mechanik.



Messung und Optimierung
=======================

.. index:: Rollwiderstand, Rolltest

Wie kann man den Rollwiderstand messen?
---------------------------------------
* Um den Luftwiderstand zu minimieren, muss man den Versuch bei niedriger Geschwindigkeit durchführen.

* Um die ungleichmäßige Tretleistung auszuschließen, empfiehlt sich ein Rollversuch. (Siehe auch: `Wie findet man heraus, ob die Spur richtig eingestellt ist?`_)

* Um Umwelteinflüsse auszuschließen, sollte es eine windstille Strecke mit perfekt gleichmäßigem Asphalt und ohne Kurven sein.

* Die dabei verlorene Höhenenergie entspricht dann der Energie, die für Rollreibung aufgewendet wurde – je niedriger diese ist, desto größer ist die gerollte Strecke.


.. index:: Luftwiderstand, Rolltest, Querschnittsfläche

Wie kann man den Luftwiderstand messen?
---------------------------------------
* Damit der Luftwiderstand dominiert, muss man den Versuch bei möglichst hoher Geschwindigkeit durchführen; beispielsweise bei 70 km/h.

* Um die ungleichmäßige Tretleistung auszuschließen, empfiehlt sich ein Rollversuch.

* Da bei hoher Geschwindigkeit die Beschleunigungsphase viel Energie kostet, darum lange dauert (siehe: `Warum sind bei einem Velomobil die Beschleunigungsphasen so lang?`_) und somit während dieser Zeit der Rollwiderstand eine große Rolle spielt, sollte der Anfang der Rollstrecke möglichst steil sein, um möglichst schnell auf Messgeschwindigkeit zu kommen. Für 70 km/h wären dafür immerhin 20 Höhenmeter nötig.

* Der folgende Messbereich sollte dann ein konstantes Gefälle sein, bei dem das Velomobil mit konstanter Geschwindigkeit rollt; für 70 km/h wären das knapp 2% Gefälle (siehe: :numref:`fig_speed_vs_brakepower_downhill`).

* Am Ende folgt dann idealerweise ein steiler Gegenanstieg, bei dem man schnell zum Stehen kommt; der Höhenunterschied zwischen Start und Endposition entspricht dann größtenteils der Energie, die für den Luftwiderstand aufgewendet wurde.

* Die Querschnittsfläche eines Velomobils kann man durch Projektion mit einer möglichst parallelen Lichtquelle (= möglichst weit entfernt; z.B. Sonnenlicht) aufzeichnen, und dann z.B. das Zeichenpapier ausschneiden und wiegen, um die Fläche zu bestimmen. Somit kann man aus dem Luftwiderstand letztendlich den :math:`c_\text{W}`-Wert bestimmen.


.. index:: Antriebsverluste, Höhenmeter, Leistungsmesser

Wie kann man die Antriebsverluste messen?
-----------------------------------------
* Da es um die Differenz zwischen Tretleistung und Fahrleistung geht, ist ein Rollversuch ungeeignet, sondern hier muss die Tretleistung mit einem Leistungsmesser aufgezeichnet werden.

* Um den Einfluss von Luft- und Rollwiderstand zu minimieren, fährt man einen steilen Berg hinauf; dann ist die nötige Steigleistung viel höher als diese Verluste.

* Außerdem bestimmt man präzise den Höhenunterschied zwischen Start und Ziel sowie das Gesamtgewicht mit Fahrer.

* Die Tretleistung schwankt zwar sehr stark, sollte aber aufsummiert über die gesamte Fahrt recht gut der aufgewendeten Kletterarbeit plus den Fahrwiderständen entsprechen. Daher kann man einfach die Durchschnittsleistung mit der Fahrzeit multiplizieren.

* Während der Luftwiderstand bei diesen niedrigen Geschwindigkeiten praktisch komplett vernachlässigbar ist, ist es der Rollwiderstand nicht; er beträgt zwar nur einige Prozent des Gesamtwiderstands, aber da die Antriebsverluste in einer ähnlichen Größenordnung sind, muss man den Rollwiderstandskoeffizienten zumindest halbwegs genau kennen. Da die Rollwiderstandskraft weitgehend kostant ist (bis auf die `dynamische Rollreibung <Was ist die dynamische Rollreibung?>`_), muss die Geschwindigkeit nicht unbedingt konstant sein, um die Rollwiderstandsverluste einfach mit der Durchschnittsgeschwindigkeit berechnen zu können.

* Die Differenz zwischen der gemessenen Muskelarbeit und der Summe aus Rollwiderstand und der aus Gewicht und Höhenunterschied berechneten Kletterarbeit sind dann die Antriebsverluste.


.. index:: Luftwiderstand, Rollwiderstand, Robert Chung, GoldenCheetah

Kann man Luft- und Rollwiderstand auch gleichzeitig messen?
-----------------------------------------------------------
Es gibt Methoden, mit denen man durch Regressionsanalyse nach Parametern sucht, die die gemessenen Daten gut beschreiben. Ein besonders interessantes Verfahren ist dabei die `Methode von Robert Chung <https://wallace78tria.files.wordpress.com/2013/02/indirect-cda.pdf>`_, bei der ein hügeliger Rundkurs mehrfach mit verschiedenen Geschwindigkeiten gefahren wird. Dabei muss es möglichst windstill sein (allerdings kommt auf einem Rundkurs der Wind letztendlich von allen Seiten), und man darf nicht bremsen. Da es ein Rundkurs ist, ist auch eine genaue Höhenmessung nicht so wichtig – selbst wenn der Höhenmesser driftet, weiß man, dass die tiefsten und die höchsten Punkte der Strecke immer auf der gleichen Höhe sein müssen, und entsprechend kann man die Parameter für Rollreibung und Luftwiderstand so anpassen, dass der berechnete Netto-Höhengewinn null wird. Die Auswertung wird beispielsweise von der Software `GoldenCheetah <https://www.goldencheetah.org/>`_ unterstützt.


.. index:: Luftwiderstand, Rollwiderstand, Leistungsmesser

Gibt es einen Rechner für Luft- und Rollwiderstand?
---------------------------------------------------
Ja, sogar mehrere:

* `Kreuzotter-Rechner <http://kreuzotter.de/deutsch/speed.htm>`_: Berechnet die Endgeschwindigkeit bzw. Tretleistung aus zahlreichen Parametern, und enthält Voreinstellungen für verschiedene Aufrechträder, Liegeräder und Velomobile. `Diese Variante des Rechners <http://infotom.de/kreuzotter/>`_ erlaubt sogar die freie Eingabe von Roll- und Luftwiderstandskoeffizient.

* `HPV Speed Simulator <http://www.recumbents.com/wisil/simul/HPV_Simul.asp>`_: Dieser Rechner ist ähnlich wie Kreuzotter, aber zeigt auch graphisch die Zunahme der Geschwindigkeit an; so sieht man auch, wie lange es dauert, bis man näherungsweise die Endgeschwindigkeit erreicht hat.

* `Leistungsanalyse-Diagramm <https://christoph-moder.de/fahrrad/powermeter2diagram.php>`_: Hier kann man eine Datei mit aufgezeichneten Leistungsdaten hochladen, und erhält ein Diagramm mit den verschiedenen Fahrwiderständen zu jeder Zeit (siehe :numref:`fig_powermeter2diagram`).

Auf den jeweiligen Seiten stehen auch Beispielwerte für Luft- und Rollwiderstandskoeffizienten. Diese wurden meist aus Leistungsmessdaten gewonnen, entsprechend sind die Berechnungen daraus einigermaßen realistisch.

.. _fig_powermeter2diagram:

.. figure:: images/powermeter2diagram.*
    :alt: Leistungsanalyse-Diagramm
    :width: 100%

    Das Leistungsanalyse-Diagramm, das zu einer aufgezeichneten Fahrt mit Höhenprofil und Leistungsdaten den Rollwiderstand, Luftwiderstand sowie sonstige Leistungsdaten berechnet und anzeigt.



Ergonomie
=========

.. index:: Körpergröße, Beinlänge, Schulterbreite, Sitzposition, Karosserie, Haube, Tretkurbel

Was sind die begrenzenden Faktoren bei der Größe? Worauf muss man achten?
-------------------------------------------------------------------------
* Beinlänge: Da sich das Velomobil vorne verjüngt, schleifen die Füße an der Karosserie, wenn die Beine zu lang sind. Auch die Knie können ein Problem sein.

* Oberkörperlänge: Zu kleine Personen können oben nicht herausschauen, und zu große Personen passen nicht unter die Haube.

* Schulterbreite: In seltenen Fällen ist ein Velomobil an den Schultern zu schmal.

* Oberschenkelbreite: Mit großen Oberschenkeln passt man nicht mehr zwischen die Radkästen. Da man die Beine bewegen muss, darf hier absolut nichts schleifen.

* Fußlänge: Personen mit großen Füßen haben üblicherweise auch lange Beine und möchten evtl. auch längere Tretkurbeln benutzen; das sorgt in der Summe dafür, dass die Füße seitlich an der Karosserie und/oder unten und oben schleifen.

* Vorlieben: z.B. flache oder steile Sitzposition

* Technische Besonderheiten: Beispielsweise verläuft bei Fahrern mit kurzen Beinen, bei denen das Tretlager weit hinten ist, die Kette oft sehr steil nach unten zur Umlenkrolle. Das kann zu Problemen mit dem Umwerfer führen, der sich – je nach Größe der Kettenblätter – nicht weit genug einstellen lässt.


.. index:: Sitzposition, Körperöffnungswinkel, Tretlagerüberhöhung

Wie ist die ideale Sitzposition?
--------------------------------
Grundsätzlich muss es sich angenehm anfühlen – manche Leute bevorzugen eine steile Sitzposition, andere eine eher flache. Bergauf muss der :term:`Körperöffnungswinkel` stimmen; viele Leute können eine höhere Tretkraft ausüben, wenn sie aufrechter sitzen.

Im Vergleich zu einem unverkleideten Liegerad ist die :term:`Tretlagerüberhöhung` in einem Velomobil eher gering. Während beim Liegerad die Beine vor dem Körper sind, um die Querschnittsfläche zu reduzieren, würde das im Velomobil nicht funktionieren – man muss noch über die Karosserie schauen können, daher sind die Beine tiefer. Zudem resultiert der geringe Luftwiderstand eines Velomobils nicht primär aus einer kleinen Querschnittsfläche, sondern einem niedrigen :math:`c_\text{W}`-Wert.


.. index:: Kurbellänge, Trittfrequenz, Tretlager

Welche Auswirkungen hat eine kurze Kurbel?
------------------------------------------
Eine kurze Kurbel bedeutet, dass der Hebel, mit der das Kettenblatt gedreht wird, kürzer ist – es ist bei gleicher Trittfrequenz also mehr Kraft und weniger Weg (= weniger Umfang des Pedalkreises) erforderlich, die Pedalgeschwindigkeit sinkt. Umgekehrt: Um mit der gleichen Kraft und gleichen Pedalgeschwindigkeit zu treten, muss man in einem niedrigeren Gang als mit einer langen Kurbel fahren. Da der Pedalkreisumfang kleiner ist, bedeutet die selbe Pedalgeschwindigkeit, dass die Trittfrequenz höher ist.

Außerdem muss man das Tretlager weiter nach vorne schieben, damit das Bein gleich weit gestreckt wird, wenn die Kurbel vorne ist. Dafür wird das Bein dann in der entgegengesetzten Kurbelstellung deutlich weniger stark angewinkelt.


.. index:: Kurbellänge, Sitzposition, Sitz, Knieprobleme, Wiegetritt, Aufrechtrad, Tretlager

Warum fahren Liegeradler oft kurze Kurbeln?
-------------------------------------------
Die Kurbellänge hängt natürlich stark von der persönlichen Vorliebe und Gewöhnung ab, und natürlich auch von der Anatomie – längere Beine brauchen größere Kurbeln. Abgesehen davon haben sich kürzere Kurbeln bewährt, weil die Sitzposition stärker als auf dem Aufrechtrad fixiert ist (d.h. es gibt keinen :term:`Wiegetritt`) und gleichzeitig deutlich höhere Tretkräfte möglich sind (d.h. man kann nicht nur mit dem eigenen Gewicht auf das Pedal drücken, sondern sich gegen den Sitz stemmen). Das führt zu hohen Kräften bei stark angewinkelten Beinen, was für Knieprobleme sorgen kann. Beim Aufrechtrad ist es dagegen eher ungewöhnlich, bei kraftvollem Treten komplett im Sattel zu bleiben – wenn man im Stehen tritt, ist im oberen Totpunkt das Bein aber kaum angewinkelt. Mit kurzen Kurbeln wird auf dem Liegerad das Knie weniger stark angewinkelt, d.h. erfährt die höchste Belastung in einer Stellung, in der weniger Scherkräfte auftreten, d.h. erfährt die höchste Belastung in einer Stellung, in der weniger Scherkräfte auftreten.

Bei Velomobilen gibt es aber auch noch einen rein geometrischen Grund für kurze Kurbeln: Bei langen Beinen ist das Tretlager weit vorne – also dort, wo die Karosserie schmäler und niedriger als weiter hinten ist. Gleichzeitig haben groß gewachsene Fahrer meist auch große Füße. Um noch treten zu können, ohne mit den Füßen oben und unten anzustoßen, müssen die Kurbeln kürzer sein.

Bei einem offenen Liegerad gibt es dagegen aerodynamische Gründe: wenn die Beine dank kurzer Kurbeln weniger weit ausholen, dann wird weniger Luft verwirbelt.


.. index:: Kurbellänge, Tretkurbel, Pedalgewinde

Wie kürzt man Kurbeln?
----------------------
Die meisten Tretkurbeln haben eine Länge von ca. 170 mm; kürzer als 165 mm ist meist schwer zu bekommen. Darum muss man Kurbeln selber kürzen bzw. kürzen lassen.

An sich geht das recht einfach, wenn man das passende Werkzeug hat: Loch bohren und Gewinde schneiden. Da das Pedalgewinde einen Durchmesser von gut 14 mm hat, kann man ein zweites Loch nicht einfach in geringerem Abstand daneben setzen – damit das neue Loch von ausreichend Material umgeben ist, empfiehlt es sich, eine Kurbel zu nehmen, die 20 mm länger als die Wunschlänge ist.

Massive Aluminium-Kurbeln kann man natürlich problemlos kürzen. Aber auch hohlgeschmiedete Kurbeln sind normalerweise unproblematisch – man bohrt nur den Rand des Hohlraums an, wo noch genug Wandstärke für das Gewinde vorhanden ist. Eine Ausnahme sind z.B. Rotor-Kurbeln, da diese mehrere Hohlräume nebeneinander enthalten – unter anderem dort, wo seitlich das Gewinde wäre. Auch Carbon-Kurbeln kann man nicht sinnvoll kürzen, da das Gewinde nicht direkt in das Carbon geschnitten werden kann (was viel zu hohe punktuelle Belastungen bedeuten würde), sondern über Metall-Inlays realisiert ist.


.. index:: Q-Faktor, Tretkurbel, Tretlager, Tretlagermast

Was ist der Q-Faktor und warum wird versucht, ihn beim Velomobil klein zu machen?
---------------------------------------------------------------------------------
Der Q-Faktor ist die Breite der Tretkurbeln, also der seitliche Abstand zwischen den Pedalen. Er wird bestimmt durch die Breite des Tretlagers, die Breite der Kurbelarme, sowie deren Kröpfung.

Bei einem Aufrecht-Fahrrad mit breiten Reifen müssen die Kettenstreben entsprechend Platz bieten; daher darf der Q-Faktor nicht zu klein sein. Entsprechend sind die Kurbeln sind gekröpft, um hinten an den breiten Kettenstreben vorbei zu kommen. Aber auch die Kettenblätter können in Kontakt mit der Kettenstrebe kommen, wenn sie zu weit innen sitzen, daher darf dort auch das Tretlager nicht zu schmal sein.

Bei einem Velomobil gibt es dagegen keine Kettenstreben beim Tretlager; selbst ein breiter Tretlagermast ist noch schmäler als das Tretlager. Es gibt daher keinen Grund für einen großen Q-Faktor. Im Gegenteil; bei einem großen Fahrer (= lange Beine, große Füße) können die Füße leicht die Karosserie berühren, wenn sie zu weit auseinander stehen. Aus diesem Grund versucht man, den Q-Faktor klein zu halten – beispielsweise, indem die Tretlager-Achse etwas gekürzt wird, und wenig gekröpfte Kurbeln verwendet werden.

Und nicht zuletzt ist es für viele Fahrer auch angenehmer, die Füße enger und die Beine paralleler stellen zu können.


.. index:: Trittfrequenz, Knieprobleme, Muskel, Gelenkknorpel, Federung, Dämpfung, Totpunkt

Hohe oder niedrige Trittfrequenz?
---------------------------------
Je nach persönlicher Vorliebe und Anatomie, aber auch Gewöhnung. Beides hat Vor- und Nachteile, da eine niedrige Trittfrequenz eine hohe Kraft bedeutet und umgekehrt.

Oft wird gesagt, dass eine hohe Tretkraft das Kniegelenk kaputt machen würde. Das stimmt nur bedingt; denn ein trainiertes Knie kann sehr wohl problemlos große Kräfte aushalten. Man sollte aber wohl nicht untrainiert lange Touren mit hoher Kraft fahren, sondern sich langsam herantasten. Außerdem spielt die Bewegungsrichtung eine Rolle: ein Gelenk kann sehr hohe Kräfte aushalten, wenn es nur auf Druck belastet wird. Wenn es sich dabei noch bewegen muss, sind Komplikationen viel wahrscheinlicher – deshalb dürfte eine stampfende Bewegung mit kurzzeitig hoher Kraft viel weniger kritisch sein als eine mahlende Bewegung unter ständig hoher Kraft.

Aber auch eine hohe Trittfrequenz ist nicht unbedingt unproblematisch. Damit ein Knie beschwerdefrei ist, müssen u.a. die Muskeln gut ausgebildet sein, die das Gelenk in Position halten – und Muskeln bilden sich nur unter Belastung, und nicht, wenn man immer nur kraftfrei dahin kurbelt. Zudem braucht der Gelenkknorpel eine gewisse Wechselbelastung, weil diese Flüssigkeit und damit Nährstoffe in das Innere pumpt.

Auch auf die Technik hat die Trittfrequenz Auswirkungen: Ein Fahrrad mit einer sehr leicht ansprechenden Federung wippt stärker mit, wenn die Trittfrequenz niedrig und ungleichmäßig ist – da dort die Masse der Beine stärker beschleunigt und abgebremst wird. Entsprechend geht mehr Energie in der Dämpfung der Federung verloren. Dafür kann man die Tretkraft schneller verändern als die Trittfrequenz; wer mit hoher Frequenz tritt, braucht deshalb eine besser abgestimmte Schaltung, weil er beim Beschleunigungen nicht so leicht Gänge überspringen kann als ein kraftvoller Fahrer.

Bergauf wird bei den meisten Leuten die Trittfrequenz niedriger. Das ist auch sinnvoll, weil dort der Tretwiderstand höher ist, d.h. in den Totpunkten fällt die Geschwindigkeit stärker ab – und man braucht hinter den Totpunkten punktuell mehr Kraft. In der Ebene ist dagegen die Belastung gleichmäßiger; da wäre es kontraproduktiv, mit einer sehr langsamen und mahlenden Bewegung über die gesamte Pedaldrehung eine hohe Scherbelastung auf die Knie auszuüben.



Benutzung
=========

.. index:: Fahrgefühl, Liegerad, Aufrechtrad

Wie fährt sich ein Velomobil?
-----------------------------
Gute Frage – jedenfalls deutlich anders als andere Fahrräder:

* Die Geschwindigkeit ist höher, daher muss man viel vorausschauender fahren.

* Die Beschleunigungsphasen sind viel länger, daher muss man sich die Kräfte gut einteilen und vorausschauend fahren (siehe `Warum sind bei einem Velomobil die Beschleunigungsphasen so lang?`_ und :numref:`fig_time_distance_endspeed_300W`).

* Entfernungen erscheinen kürzer, weil man üblicherweise schneller fährt.

* Gepäckmitnahme ist unkritischer – man muss nicht extra Taschen oder Spannriemen mitnehmen und das Gepäck nicht besonders verpacken, sondern kann es einfach im Innenraum sauber und trocken transportieren (siehe: `Wie viel Gepäck bringt man unter?`_).

* Wind nimmt man kaum wahr, weil Gegenwind viel weniger bremst; man sieht auf dem Tacho zwar einen geringen Unterschied, aber hat nicht das Gefühl, gar nicht mehr vorwärts zu kommen. Und wenn man mit Haube fährt, spürt man den Wind auch überhaupt nicht, sondern merkt höchstens den Einfluss von Böen auf das Fahrverhalten (siehe: `Wie macht sich Wind bemerkbar?`_).

* Kälte ist kein Problem, weil man selbst im tiefsten Winter noch mit T-Shirt und dünner Hose fahren kann (siehe: `Welche Kleidung?`_).

* Niederschlag verliert auch seinen Schrecken; man bleibt zwar nicht komplett trocken (im Gegenteil), aber man wird auch nicht von einem Wolkenbruch bis auf die Haut durchnässt, und frieren tut man auch nicht. Eine Fahrt bei Regen ist zwar nicht besonders angenehm, aber auch nicht besonders abschreckend (siehe: `Kann man bei Regen fahren?`_).

* Belüftung ist sehr wichtig; bei tiefen Temperaturen, weil die Scheibe beschlägt, und bei höheren Temperaturen, weil die Kühlung und Trocknung vom Fahrtwind fast komplett fehlt. Auch bei guter Belüftung ist man eigentlich immer verschwitzt (siehe: `Wie sieht es mit der Belüftung aus?`_).

* Kleine Hügel erscheinen flacher, weil man sie zu einem Großteil mit Schwung fahren kann (siehe: `Kann man im Hügelland fahren?`_).

* Lange Steigungen erscheinen steiler, weil man erstens mehr Gewicht herumschleppt, zweitens der Leistungsbedarf hoch, aber die Kühlung schlecht ist, und man drittens von der sonstigen hohen Geschwindigkeit verwöhnt ist (siehe: `Kann man in den Bergen fahren?`_).

* Schnelle Reifen sind viel wichtiger, weil der Rollwiderstand einen viel größeren Anteil hat (siehe: `Warum sind gute Reifen im Velomobil so wichtig?`_).

* Eine gute Haftung ist weniger wichtig, weil Velomobile Mehrspurer sind – sie kippen nicht gleich um, wenn es rutschig ist. Man kann durchaus riskieren, etwas wegzurutschen, statt ständig wie auf rohen Eiern zu fahren (siehe: `Braucht man Winterreifen oder Spikes?`_).

* Gewicht macht sich stärker bemerkbar, weil der Luftwiderstand, bei dem das Gewicht keine Rolle spielt, geringer ist (siehe: `Warum sind Velomobil-Fahrer so besessen von niedrigem Gewicht?`_).

* Man kann seine Leistung viel gleichmäßiger abgeben, weil die Verluste geringer sind, d.h. man bekommt mehr von der hineingesteckten Leistung zurück; z.B. kann man bereits vor einer Steigung beschleunigen und diese dann mit mehr Schwung fahren, oder nach einer Abfahrt extrem lange ausrollen. Letztendlich hat man viel mehr Rollphasen als mit einem normalen Fahrrad.


.. index:: Gepäck, Einkauf

Wie viel Gepäck bringt man unter?
---------------------------------
Das hängt sehr vom jeweiligen Velomobil ab. Grundsätzlich sind sperrige Gegenstände oft problematisch. Aber ansonsten ist das Gepäckvolumen erstaunlich groß; selbst in den kleinsten Velomobilen bringt man mindestens so viel unter, wie bei einem Tourenrad in zwei große Satteltaschen plus auf den Gepäckträger passt. Radreisen sind also überhaupt kein Problem. Größere Velomobile haben sogar ähnlich viel Gepäckvolumen wie ein kleinerer Fahrradanhänger, und damit Platz für einen Großeinkauf.


.. index:: Fahrradanhänger

Kann man einen Anhänger verwenden?
----------------------------------
Grundsätzlich ja, wenn er eine Tiefdeichsel hat. Wenn man hinten die Karosserie verstärken lässt, kann man eine handelsübliche Anhängerkupplung anschrauben. Aber sinnvoll ist das eigentlich nur in Ausnahmefällen, denn im Velomobil bringt man schon relativ viel Gepäck unter, und ein Anhänger ist schwer, sperrig, und erhöht den Luftwiderstand deutlich. Man kann also nicht schnell fahren, und muss bergauf nicht nur das Gewicht des Anhängers und der Ladung ziehen, sondern eben auch das schwere Velomobil. Daher ist es meist sinnvoller, den Anhänger mit einem normalen Fahrrad zu ziehen.


.. index:: Kleidung, Winter

Welche Kleidung?
----------------
Ähnlich wie bei unverkleideten Fahrrädern, aber weniger. Selbst im tiefsten Winter kann man noch mit T-Shirt fahren; dann muss man allerdings warme Kleidung dabei haben, denn bei einer Panne wird es ansonsten schnell sehr kalt. Insgesamt reicht meist ein dünnes Hemd, eine kurze oder lange Hose, und im Winter evtl. ein Halstuch bzw. einen Halswärmer. Windbreaker, Ärmlinge und Gamaschen kann man auf jeden Fall zu Hause lassen.


.. index:: Klickschuhe, Aufrechtrad

Braucht man Klickschuhe?
------------------------
Eigentlich ja. Erstens, weil bei einem Liegerad der Fuß von hinten statt von oben auf das Pedal tritt – d.h. er wird nicht bereits von der Schwerkraft auf das Pedal gedrückt. Zweitens, weil man sonst vom Pedal abrutschen kann. Während das auf einem Aufrechtrad oft nur geringe Konsequenzen hat, ist gerade im Velomobil neben den Füßen fast kein Platz – wenn der Fuß nicht exakt richtig auf dem Pedal sitzt, stößt man irgendwo an, und wenn man während kraftvollen Tretens abrutscht, kann man bei dünnwandigen Velomobilen sogar die Hülle beschädigen. Und drittens ist die Effizienz auch etwas höher, wenn man sich einfach gar keine Gedanken um das Abrutschen machen muss, sondern sich immer auf den festen Halt auf dem Pedal verlassen kann.


.. index:: Wendekreis, Radkasten, Lenkeinschlag, Auto

Wie groß ist der Wendekreis?
----------------------------
Ungefähr 10 Meter; etwa ähnlich groß wie bei einem Auto.

Das klingt für ein Fahrrad nach viel (ist es auch), aber da man vorwiegend auf Straßen für Autos unterwegs ist, hat man in der Praxis fast nie ein Problem. Auf bekannten Strecken kann man sich oft auf Problemstellen einstellen und rechtzeitig ausreichend ausholen; lediglich auf unbekannten Strecken und dann typischerweise auf Radwegen kann es passieren, dass man gelegentlich einmal rangieren oder aussteigen muss. Das passiert aber zu selten, um ein echtes Problem zu sein.

Der genaue Wendekreis hängt von mehreren Faktoren ab:

* Velomobil-Typ: Je kleiner der Radkasten, desto größer der Wendekreis.

* Velomobile mit geschlossenen Radkästen haben meist einen etwas größeren Wendekreis.

* Reifenbreite: Je breiter der Reifen, desto geringer ist der mögliche Lenkeinschlag. Erstens stößt der Reifen seitlich schneller am Radkasten an, zweitens ist der Reifen dann auch höher und ragt weiter nach vorne/hinten.

* Einspeichung: Bei Velomobilen mit geschlossenen Radkästen ist der Wendekreis am kleinsten, wenn das Rad mittig im Radkasten sitzt. Mit sogenannten :term:`Hosen` kann man offene Radkästen auch verkleiden; dadurch vergrößert sich der Wendekreis deutlich, und man muss bei vielen Kurven vorher gegenlenken. Wenn man allerdings die Räder neu einspeicht, so dass sie nicht mehr bündig zu den offenen Radkästen sitzen, sondern mittig zu den Ausschnitten der Hosen, ist der Wendekreis ähnlich wie bei offenen Radkästen.


.. index:: Rückwärtsgang, Wendekreis, Fußlöcher

Wie fährt man rückwärts?
------------------------
Fahrräder haben üblicherweise keinen Rückwärtsgang. Braucht man normalerweise auch nicht, weil der Wendekreis klein ist und man ansonsten schnell abgestiegen ist. Beides ist beim Velomobil aber nicht der Fall; das Aussteigen ist umständlicher, erst recht wenn man noch :term:`Haube` oder :term:`Schaumdeckel` entfernen muss, und der Wendekreis ist ähnlich wie bei einem Auto.

Bei einem Auto braucht man den Rückwärtsgang, um auszuparken oder in mehreren Zügen zu wenden. Ersteres braucht man beim Velomobil nicht: man schiebt es einfach aus der Parklücke. Und wenden muss man nur selten, erst recht nicht auf bekannten Strecken. Nur auf Radwegen mit Haarnadelkurven kann es regelmäßig nötig sein. Aber selbst dann kann man oft auf einen Rückwärtsgang verzichten:

* Kaum eine Straße ist wirklich eben, und erst recht nicht angrenzende Einfahrten; oft kann man dort bergauf hineinfahren, sich zurückrollen lassen, und kommt anschließend um die Kurve. Dafür ist eine kaum wahrnehmbare Steigung ausreichend.

* Bei Velomobilen mit offenem Radkasten kann man zumindest auf das Aussteigen verzichten und mit den Händen die Vorderräder wie bei einem Rollstuhl drehen. Da macht man sich aber die Hände dreckig, und es funktioniert auch nur auf wirklich ebener Strecke, nicht bergauf.

* Und dann gibt es natürlich Velomobile mit Fußlöchern, wo man sich mit der Fred-Feuerstein-Technik nach hinten schieben kann.


.. index:: Belüftung, Winter, Haube, Visier, Tretlagermast, NACA-Duct, Sitz, schwitzen

Wie sieht es mit der Belüftung aus?
-----------------------------------
Im Velomobil ist man vom Fahrtwind abgeschirmt; d.h. wenn nicht gerade Winter ist, braucht der Körper Kühlung – vor allem an Rumpf und Kopf. Ein weiterer Punkt ist die Luftfeuchtigkeit; am Körper stört diese zwar nicht unbedingt, aber wenn es zu feucht ist, kann man sich z.B. im Schritt wundreiben. Mit Haube beschlagen bei kühler Temperatur allerdings die Scheiben. Mit einer Frischluftzufuhr kann man die feuchte Luft wegblasen.

Bei den meisten Velomobilen gibt es einen Lufteinlass vorne an der Spitze – das ist der :term:`Staupunkt`, wo erhöhter Druck herrscht, entsprechend erreicht man mit einer kleinen Öffnung einen großen Luftdurchsatz. Zudem stört ein Loch dort auch nicht die Strömung entlang der Karosserie.

Bei manchen Modellen sitzt dort auch der Scheinwerfer – das hat den Vorteil, dass für diesen kein weiteres Loch in die Karosserie gebohrt werden muss, und die LEDs durch den Fahrtwind gut gekühlt wird, was den Wirkungsgrad erhöht. Dafür sitzt der Scheinwerfer recht niedrig, und die Ausleuchtung der Straße ist entsprechend schlecht (nur ein geringer Anteil wird zurückgestreut).

Zudem ist bei manchen Velomobil-Modellen der :term:`Tretlagermast` vorne am Lufteinlass befestigt. Das hat den Vorteil, dass der Tretlagermast auch von vorne abgestützt werden kann, und die Luft durch den Mast nach hinten strömt und die Füße nicht von der kalten Frischluft ausgekühlt werden.

Wem diese Kühlung nicht ausreicht, der kann zusätzlich einen :term:`NACA-Duct` einbauen; meist vorne in die Oberseite der Karosserie.

Wer mit Haube fährt, kann außerdem das Visier anheben; dort reicht üblicherweise schon eine schmale Öffnung für einen ausreichenden Luftstrom.

Zudem haben manche Fahrer Löcher in den Sitz gebohrt, damit der Rücken besser belüftet wird.


.. index:: Regen, Visier, Haube, Scheibenwischer, beschlagene Scheiben, Scheinwerfer

Kann man bei Regen fahren?
--------------------------
Regen ist im Velomobil zwar nicht so lästig wie auf einem offenen Fahrrad, trocken bleibt man aber auch nicht. Man ist zwar dank Radkästen vor Straßendreck und Spritzwasser geschützt und wird von einem Wolkenbruch auch nicht bis auf die Knochen durchnässt, aber wegen der hohen Luftfeuchtigkeit wird kein Schweiß abtransportiert, und bei teils geöffnetem Visier oder ohne Haube wird der Kopf und Oberkörper nass.

Zudem behindern Regentropfen die Sicht – egal ob man mit Haube fährt oder im Cabrio-Modus mit Brille. Im ersteren Fall hilft es, eine Art Scheibenwischer zu haben, mit dem zumindest die großen Tropfen so verteilt werden können, dass die Sicht erträglich ist.

Während es bisher keine echten Scheibenwischer gibt, haben sich zumindest folgende Lösungen bewährt:

* Magnetwischer: Zwei langgestreckte Neodym-Magneten, mit saugfähigem Stoff umwickelt, sitzen auf beiden Seiten der Scheibe. Von innen aus kann man sich ein Sichtfenster frei wischen – das reicht, weil man dicht an der Scheibe sitzt und deshalb keine großen Flächen freihalten muss. Zudem sorgen die Magneten für Anpressdruck, so dass auch große Schneeflocken weggewischt werden können.

* Fadenwischer: Ein Gummifaden ist längs oder quer über die Scheibe gespannt, und kann senkrecht dazu mit einem zweiten Faden hin- und hergezogen werden. Damit können größere Flächen als mit einem Magnetwischer gewischt werden, allerdings ist der Anpressdruck geringer.

Bei Nacht und Regen macht das Fahren aber trotzdem keinen Spaß, weil sich erstens die entgegenkommenden Scheinwerfer in den Regentropfen auf der Scheibe oder Brille spiegeln, und zweitens die meist tief angebrachten Velomobil-Scheinwerfer eine nasse Straße nur sehr schwach ausleuchten.


.. index:: beschlagene Scheiben, Visier, Pinlock, Auto, Belüftung, schwitzen

Was macht man gegen beschlagene Scheiben?
-----------------------------------------
Was ist die Ursache von beschlagenen Scheiben? Der Fahrer schwitzt, und die warme, feuchte Luft kondensiert an kalten Oberflächen, wie zum Beispiel der Frontscheibe.

Entsprechend lautet die Abhilfe:

* Die Scheibe isolieren, damit sie auf der Innenseite nicht kalt ist. Das funktioniert beispielsweise mit der Pinlock-Scheibe, mit der man quasi eine Doppelverglasung erreicht.
* Die Feuchtigkeit von der Scheibe fernhalten. Das erreicht man, indem man das Velomobil gut belüftet; wenn laufend von vorne Frischluft einströmt, dann wird die feuchte Luft nach hinten hinausgedrückt.
* Außerdem kann man das Visier etwas öffnen, und vor dem unteren Rand des Visiers eine kleine Barriere montieren, die die einströmende Luft nach oben umlenkt, über die Innenseite des Visiers. Dieser Luftfilm verhindert, dass die feuchte Innenluft in Kontakt mit dem Visier kommt.
* Und wenn das alles nichts hilft – z.B. weil man steht, und darum keine Luftströmung hat –, muss man die Scheibe innen abwischen.

Warum ist das dann im Auto kein Problem?

* Im Auto sitzt man passiv, man schwitzt also nicht. Entsprechend ist die Luftfeuchtigkeit geringer.
* Im Auto sitzt man viel weiter von der Frontscheibe entfernt. Man atmet also nicht direkt gegen die Scheibe.
* Der Innenraum des Autos hat ein viel größeres Volumen. Selbst wenn man schwitzen würde, würde es viel länger dauern, bis die komplette Luft feucht wäre.
* Das Auto hat eine Heizung. Ein Verbrennungsmotor ist ineffizient, nur ca. 20% der Energie gehen in den Antrieb, der Rest ist Wärme – und mit diesen tausenden Watt lässt sich der Innenraum problemlos beheizen, und auch permanent frische Luft aufgeheizt auf die Scheibe leiten.


.. index:: Auto, Alltag

Eignet sich ein Velomobil als Auto-Ersatz?
------------------------------------------
Kommt darauf an. Es ist und bleibt ein Fahrrad, das lediglich eine etwas höhere Geschwindigkeit und Reichweite und einen etwas besseren Wetterschutz bietet. Ein Auto ist dagegen eine „eierlegende Wollmilchsau“; man kann mehrere Leute oder große Mengen Gepäck mitnehmen, hunderte Kilometer am Stück selbst durch sehr bergiges Gelände fahren, aber auch nur zum Einkaufen in die Stadt fahren. Ein Fahrrad ist natürlich deutlich eingeschränkter. Das heißt aber auch: Ein Auto ist für die meisten Alltagsaufgaben vollkommen überdimensioniert; diese kann man hervorragend mit dem Velomobil erledigen. Wenn das an den meisten Tagen der Fall ist, kann man auf ein Auto verzichten – und für die wenigen verbleibenden Transport-Aufgaben immer noch ein für den Bedarf passendes Auto mieten.


.. index:: schwitzen, Fahrtwind, Belüftung, Sitz, Motor

Kann man fahren, ohne verschwitzt anzukommen?
---------------------------------------------
Schwierig. Da im Inneren der Fahrtwind fehlt und der Rücken auf dem Sitz aufliegt, muss man sehr langsam fahren, um nicht zu schwitzen. Wenn es nicht gerade bergauf geht, hilft da auch ein Pedelec-Motor nicht, weil man praktisch immer schneller als die Maximalgeschwindigkeit des Motors fährt.


.. index:: Winter, Haube, Pinlock, Belüftung, Kleidung, Reifenpanne, Rollwiderstand

Wie fährt sich ein Velomobil im Winter?
---------------------------------------
Nicht viel anders als im Sommer; man braucht vielleicht eine lange statt einer kurzen Hose sowie ein Halstuch gegen den kalten Wind, und mit Haube noch eine :term:`Pinlock`-Scheibe gegen das Beschlagen – aber ansonsten ist alles wie im Sommer, man kann in der Regel im T-Shirt fahren (sollte aber eine warme Jacke dabei haben, falls man bei einer Panne bei Minusgraden aussteigen und länger basteln muss).

Zudem ist im Winter ein gutes Licht noch wichtiger als im Sommer – auch, weil Autofahrer im Winter weniger mit Radfahrern rechnen. Auch empfehlenswert sind halbwegs pannensichere Reifen; erstens macht es keinen Spaß, einen Platten bei Dunkelheit und Kälte zu flicken, und zweitens sorgt die Kälte auch dafür, dass der Gummi steifer wird, so dass gerade das Reifenwechseln auf Tubeless-Felgen sehr mühsam wird. Allerdings steigt auch der Rollwiderstand bei Kälte spürbar an – darum sind leicht laufende Reifen gerade im Winter nicht unwichtig.


.. index:: Winter, Winterreifen, Spikes, Schnee, Glatteis, Mehrspurer

Braucht man Winterreifen oder Spikes?
-------------------------------------
Eigentlich nicht; zumindest nicht auf flacher Strecke. Spikes an den Vorderrädern verhindern das Wegrutschen und verkürzen den Bremsweg, und Profil am Hinterrad sorgt dafür, dass das Hinterrad bei Schnee weniger leicht durchdreht. Letzteres ist aber eigentlich nur beim Anfahren und bei steilen Bergen ein Problem, also nur an wenigen Stellen. Und da ein :term:`Mehrspurer` bei Glatteis nicht umfällt, lohnt es sich nicht, wegen ein paar wenigen glatten Kurven Spikes zu montieren – die, genauso wie Profilreifen, den Rollwiderstand deutlich erhöhen, so dass man hohe Geschwindigkeiten auch auf gerader Strecke nicht mehr erreicht. Lieber fährt man in Kurven vorsichtig und macht langsame Lenkbewegungen, und kalkuliert ein, dass das Velomobil etwas rutschen kann – was im Normalfall vollkommen harmlos ist. Lediglich steile, kurvige Abfahrten sind bei Glatteis kritisch, weil man dann evtl. weder lenken noch bremsen kann. Aber solange man sich auf geräumten und gestreuten Straßen bewegt, gibt es im Allgemeinen wenig Probleme – und wenn es einmal sehr glatt ist, dann schleicht der Autoverkehr auch dahin.


.. index:: Rückspiegel, Vandalismus, Schaumdeckel, Haube, Karosserie

Wie verhalten sich die Leute?
-----------------------------
Grundsätzlich werden Velomobile sehr positiv aufgenommen, es gibt fast ausschließlich positives Interesse oder Kommentare – aber man zieht auch immer alle Blicke auf sich, mehr als mit dem teuersten Auto. Unterwegs ist das selten ein Problem; Kinder zeigen auf das Velomobil, und Autofahrer fahren oft langsam nebenher, bestaunen das Fahrzeug, kurbeln das Fenster herunter, geben einen Kommentar ab und/oder filmen oder fotografieren mit dem Smartphone.

Beim Parken kann das Interesse dagegen ein echtes Problem werden. Leute müssen alles nicht nur aus der Nähe betrachten oder vorsichtig anfassen, sondern sogar Schäden sind nicht selten. Kinder scheinen magisch von den Spiegeln und anderen exponierten Teilen angezogen zu werden, nicht selten ist der Spiegel danach verdreht, vielleicht sogar abgebrochen. Wenn das Velomobil offen war, findet man im Inneren Fußspuren, weil z.B. Eltern ihre Kinder ungefragt hineingesetzt haben.

Ärgerlich wird es aber dann, wenn etwas kaputt geht. Selbst wenn das Velomobil mit Schaumdeckel oder Haube verschlossen ist, wurde es teilweise gewaltsam aufgerissen, so dass beim :term:`Schaumdeckel` ein Klettstreifen abgerissen oder bei der Haube ein Riss im Carbon ist. Es ist einfach nur unbegreiflich, mit welcher Selbstverständlichkeit manche Menschen beim Vorbeigehen den Schaumdeckel abreißen, um einen Blick in das Innere werfen zu können. Aber auch die Karosserie ist gefährdet. Immer wieder findet man an der Front einen Riss im Carbon, weil sich ganz offensichtlich jemand draufgesetzt hat. Das alles beeinträchtigt zwar nicht die Funktionalität und Sicherheit, ist aber sehr ärgerlich und auch sehr teuer. Daher am besten das Velomobil nicht dort abstellen, wo es zu viele Leute mit zu viel Zeit und zu schlechten Manieren gibt.


.. index:: Schloss, Vandalismus, Abdeckplane

Wie abschließen und sichern?
----------------------------
Wie jedes Fahrrad: an einen festen Gegenstand anschließen. Mangels Rahmendreiecken geht das eigentlich nur am Hinterrad – oder am Tragegriff am Heck bzw. der Trageöse, falls vorhanden. Das ist zwar nicht so gut wie bei einem normalen Fahrrad, wo man das Schloss um den stabilen Hauptrahmen legen kann – allerdings ist ein Velomobil weder handlich noch unauffällig, d.h. nichts für Gelegenheitsdiebe. Zudem erfordert der Ausbau des Hinterrads bei einigen Modellen Spezialwissen, und teilweise auch Spezialwerkzeug.

Ein größeres Problem ist dagegen Vandalismus. Als beste Abhilfe hat sich hier eine Abdeckplane in einer möglichst unauffälligen Farbe herausgestellt – während ein offenes Velomobil alle Blicke auf sich zieht, wird ein abgedecktes Fahrzeug weitgehend ignoriert.


.. index:: Abmessungen

Wie groß muss ein Abstellraum sein?
-----------------------------------

Siehe :numref:`fig_vm-dimensions`; hier sind die Maße vieler Velomobile aufgezeichnet. Die Höhe beträgt meist einen knappen Meter; und von der Breite her passen die meisten Velomobile durch normale Türen.

.. _fig_vm-dimensions:

.. figure:: images/vm-dimensions_de.*
    :alt: Streudiagramm VM-Dimensionen
    :width: 100%

    Länge und Breite vieler Velomobile; die Farbe gibt das Gewicht an – grün ist 20 kg, rot ist 40 kg. Zu beachten ist, dass sich das Gewicht je nach Ausstattung stark unterscheiden kann; und z.B. beim WAW sind verschiedene Front- und Heckteile erhältlich, so dass die Länge 269–301 cm betragen kann.


.. index:: Eisenbahn

Kann man ein Velomobil im Zug mitnehmen?
----------------------------------------
Grundsätzlich geht es, aber problemlos ist es nicht – Velomobile sind groß und sperrig, ungeeignet für normale Fahrradständer, und können auch nicht an Haken aufgehängt werden. So kann es sein, dass sie das halbe Fahrradabteil oder den Einstiegsbereich blockieren, und die Bahn-Angestellten verweigern dann zu Recht die Mitnahme. Einen Anspruch darauf hat man grundsätzlich nicht, oft sind sogar Spezialräder (Tandems, Lastenräder) in den Beförderungsbedingungen nicht erlaubt – letztendlich liegt es immer im Ermessensspielraum des Personals.

Aber man kann mit ein paar Maßnahmen die Wahrscheinlichkeit der Mitnahme deutlich erhöhen:

* Eine Strecke wählen, wo man möglichst selten umsteigen muss; und wenn, die Umsteigezeiten sehr großzügig wählen.

* Sich informieren, welcher Wagen-Typ auf der Strecke verkehrt. Ein Fahrradabteil muss vorhanden sein; gut geeignet sind beispielsweise Doppelstock-Waggons mit Tiefeinstieg oder Steuerwagen mit Fahrradabteil, da diese auch breite Türen haben. Alte Waggons mit engen Türen und steilen Treppen sind dagegen ungeeignet.

* Nicht unterwegs ein- oder aussteigen, sondern am Anfangs- oder Endbahnhof. Dann bleibt mehr Zeit zum Ein- oder Aussteigen, und der Zug ist noch nicht so voll oder bereits etwas leerer.

* Frühzeitig am Bahnhof und dort auf dem richtigen Bahnsteig an der richtigen Stelle sein.

* Wenn nötig, sich rechtzeitig Hilfe organisieren, so dass man beim Ein- oder Aussteigen nicht länger braucht als andere Reisende.

* Fahren, wenn wenig los ist. Also nicht im Berufsverkehr, sondern z.B. an Wochenenden oder Feiertagen sehr früh oder spät, oder bei Strecken mit viel Freizeit-Radverkehr möglichst in der Nebensaison oder unter der Woche.

* Eventuell gleich zwei Fahrradkarten kaufen. Wird zwar nicht explizit gefordert, aber vermeidet Diskussionen, wenn man auch den Platz von zwei Fahrrädern braucht.

* Einkalkulieren, dass der Zug überraschend voll ist, und man den nächsten nehmen muss.

Letztendlich gibt es keine Garantie, dass man im Zug Platz findet oder vom Schaffner mitgenommen wird. Aber wenn man keine Probleme verursacht (keinen Weg versperrt, keine Verzögerungen verursacht, keine Mitreisenden stört, dem Personal keine zusätzliche Arbeit macht und gegen keine Sicherheitsvorschriften verstößt) und freundlich ist, klappt es normalerweise recht problemlos – dann haben die Bahn-Angestellten keinen Grund zur Beschwerde. Und wenn man so schnell ist, dass man vollendete Tatsachen geschafft hat, bevor das Personal reagieren kann, haben sie normalerweise keine Lust, danach noch eine unnötige Diskussion anzufangen.



Streckenwahl
============

.. index:: Höhenmeter, Aufrechtrad, Wiegetritt, Bremsenkühlung, Fahrtwind, schwitzen

Kann man in den Bergen fahren?
------------------------------
Wenn man eine passende Übersetzung hat, geht das problemlos. Man braucht aber eine andere Technik als auf dem Aufrechtrad: Nicht kraftvoll im :term:`Wiegetritt` treten, sondern in einem niedrigen Gang bergauf kurbeln – langsam, aber gleichmäßig.

Trotzdem spielt ein Velomobil seine Stärken vor allem im Flachland aus:

* Bergauf bringt die Aerodynamik keine Vorteile.
* Bergab bremst der Luftwiderstand viel weniger, daher überhitzen die Bremsen viel schneller (siehe :numref:`fig_speed_vs_brakepower_downhill`).
* Das höhere Gewicht kostet bergauf mehr Kraft (siehe :numref:`fig_speed_vs_power_uphill`) und belastet bergab die Bremsen stärker.
* Bergauf fehlt ohne Fahrtwind die Kühlung; das macht Bergfahrten in der Sommerhitze sehr mühsam.

.. _fig_speed_vs_power_uphill:

.. figure:: images/speed_vs_power_uphill_de.*
    :alt: Leistung bei verschiedenen Steigungen
    :width: 100%

    Leistungsbedarf bei verschiedenen Steigungen; die durchgezogene Linie ist ein Velomobil, die gestrichelte Linie ein Rennrad. Wie man sieht, sind Velomobile bei zunehmender Steigung bei niedrigen Geschwindigkeiten immer mehr im Nachteil – beziehungsweise müssen bei zunehmender Steigung immer schneller fahren, um gegenüber einem Rennrad im Vorteil zu sein. Während bei 1% Steigung ein Velomobil bereits ab ungefähr 17 km/h und 100 W Tretleistung im Vorteil ist, verschiebt sich dieser Punkt bei 5% Steigung auf über 30 km/h und über 500 W Tretleistung. Bei 200 W Tretleistung ist dagegen ein Velomobil bei 4% Steigung nur so schnell, wie es ein Rennrad bei 5% Steigung wäre.


.. index:: Höhenmeter, Sprint, delphinieren

Kann man im Hügelland fahren?
-----------------------------
Im Unterschied zu hohen Bergen funktionieren kurze Hügel viel problemloser, weil man erstens kurzfristig auch einmal mit viel Kraft und/oder einer hohen Leistung treten kann – da ist es nicht so wichtig, wenn die Schaltung keinen passenden Gang hat.

Zweitens bedeutet die hohe Geschwindigkeit eines Velomobils, dass man eine Menge kinetischer Energie hat (siehe :numref:`fig_speed_vs_uphill`) – so kann man kleine Hügel mit Schwung nehmen, ohne sich zu verausgaben. Dies nennt man :term:`delphinieren`.

Allerdings gilt dies alles nur für Hauptstraßen, die für eine zügige Fahrweise ausgelegt sind. So sind beispielsweise Passstraßen im Hochgebirge mit dem Fahrrad erstaunlich gut zu befahren, weil die Steigung sehr konstant und die Straßen sehr gut an das Landschaftsprofil angepasst sind. Dagegen gibt es im Hügelland gerade auf Nebenstraßen oft auch sehr steile (wenn auch kurze) Abschnitte, die schlecht an die Landschaft angepasst sind und die man deshalb nicht mit konstanter Leistung fahren kann. Eine Serie solcher Steigungen ist oft erstaunlich anstrengend – mit jedem Fahrrad, aber gerade beim Velomobil macht es sich negativ bemerkbar, wenn man keinen Schwung ausnutzen kann.

.. _fig_speed_vs_uphill:

.. figure:: images/speed_vs_uphill_de.*
    :alt: kinetische Energie und Höhenmeter
    :width: 100%

    Umrechnung von kinetischer Energie in Höhenmeter. Bei Tempo 50 hat man also ausreichend Energie, um einen ca. 9 m hohen Hügel hinaufzurollen. Davon müssen einerseits noch die Fahrwiderstände abgezogen werden, andererseits kann man mittreten. Somit hängt die genaue Bilanz dann von der Steigung ab; für steile Hügel stimmt das Diagramm recht gut, während man bei einem flachen Hügel länger mittreten kann und somit höher kommt.


.. index:: Radweg, Auto, Mehrspurer, Geschwindigkeit, Sicht, Wendekreis

Kann man Radwege fahren?
------------------------
Gute Frage – die Qualität der Radwege ist viel zu unterschiedlich, um dazu eine allgemeine Aussage zu machen.Aber grundsätzlich gilt:

* Radwege sind nicht für die Geschwindigkeiten von Velomobilen ausgelegt; selbst mit einem Rennrad stößt man an Grenzen.
* Radwege haben oft enge Kurven und sind sehr unübersichtlich; selbst wenn man teilweise schnell fahren könnte, müsste man oft abbremsen.
* Einmündungen sind oft unübersichtlich und von parkenden Autos verdeckt, und Radfahrern wird häufig die Vorfahrt genommen.
* Radwege sind oft schmal, so dass andere Radfahrer kaum überholt werden können.
* Radwege haben oft Hindernisse wie Drängelgitter oder Haarnadelkurven, die mit einem Velomobil nicht passiert werden können.
* Wurzelaufbrüche sind gerade mit einem :term:`Mehrspurer` mit straffem Fahrwerk sehr unangenehm.

Darum ist es meist nicht nur bequemer, sondern vor allem deutlich sicherer, auf der Straße zu fahren. Aber es gibt auch gute, breite und übersichtliche Überland-Radwege, die sich hervorragend mit dem Velomobil befahren lassen. Man kann sich allerdings auf keinen Mindeststandard verlassen.


.. index:: Rollwiderstand, Asphalt, rumpeln

Kann man ungeteerte Wege fahren?
--------------------------------
Gehen tut es, aber es macht keinen Spaß. Wie erwähnt spielt der :term:`Rollwiderstand` beim Velomobil eine große Rolle; dieser ist auf unbefestigten Strecken sehr viel höher, und entsprechend ist man deutlich langsamer. Zudem hat ein Velomobil typischerweise schmale, hart aufgepumpte Reifen und ein straffes Fahrwerk, was die Fahrt wenig komfortabel macht. Ein :term:`Mehrspurer` hoppelt dabei nicht nur auf und ab, sondern kippelt auch um die Längsachse. Und da ein Velomobil ein wunderbarer Resonanzkörper ist, rappelt und dröhnt es ganz ordentlich.


.. index:: Stadtverkehr, Radweg, Auto, Beschleunigungsstrecke, Sicht

Wie fährt es sich im Stadtverkehr?
----------------------------------
Grundsätzlich kann man auch gut in der Stadt fahren; allerdings hat ein Velomobil dort kaum Vorteile.

Der Hauptvorteil ist die Geschwindigkeit; aber da die Beschleunigungsstrecken groß sind (siehe :numref:`fig_time_distance_endspeed_300W`) und man in der Stadt häufig anhalten muss, erreicht man kaum eine hohe Geschwindigkeit. Das bedeutet aber nicht, dass Velomobile dort langsamer als andere Fahrräder sind – die Beschleunigung ist sehr ähnlich, aber die hohe Endgeschwindigkeit ist nur dann erreichbar, wenn man es kilometerweit laufen lassen kann. In der Stadt ist ein Velomobil deshalb permanent in der Beschleunigungs- oder Ausrollphase.

Da Velomobile nicht so wendig sind, sind viele Radwege ungeeignet; entweder wegen Hindernissen nicht befahrbar, oder zu schmal, um andere Radfahrer überholen zu können, oder zu unübersichtlich für eine höhere Geschwindigkeit (siehe: `Kann man Radwege fahren?`_). Die Liegeposition spielt dabei praktisch keine Rolle: Weil der Kopf weiter hinten ist, kann man Kreuzungen später einsehen – dabei geht es aber nur um Sekundenbruchteile; bei höheren Geschwindigkeiten wird diese Zeit immer kürzer, aber der Bremsweg immer länger. Unübersichtliche Radwege bremsen deshalb alle Fahrräder auf eine ähnliche Geschwindigkeit; beim Velomobil ist lediglich die Differenz zur normalen Reisegeschwindigkeit größer. Auf der Straße ist dagegen der dichte Verkehr oft ein Problem; dort steht man mit den Autos im Stau. Auf größeren Durchgangsstraßen kann man jedoch durchaus von einer „grünen Welle“ profitieren, für die man mit einem normalen Fahrrad zu langsam wäre.

.. _fig_time_distance_endspeed_300W:

.. figure:: images/speed_vs_time_distance_300W_de.*
    :alt: Zeit und Strecke, um die Endgeschwindigkeit zu erreichen
    :width: 100%

    Zeit und Strecke, um mit 300 W Tretleistung 99% der Endgeschwindigkeit zu erreichen. Beide steigen stark mit der Geschwindigkeit an; darum sind im Velomobil die Beschleunigungsphasen so lang. Im unteren Geschwindigkeitsbereich beschleunigen alle Fahrräder ähnlich gut; ein Velomobil wegen seiner höheren Masse und Antriebsverluste minimal langsamer (oberes Diagramm), hat diesen Rückstand aber bei gut 30 km/h aufgeholt. In der Beschleunigungsstrecke macht sich das kaum bemerkbar (unteres Diagramm). Nach etwa einem Drittel der Zeit/Strecke ist man bereits nahe der Endgeschwindigkeit; die weitere Beschleunigung ist sehr langsam.


.. index:: Kurzstrecke

Wie fährt es sich auf Kurzstrecken?
-----------------------------------
Für Kurzstrecken ist ein Velomobil eher ungeeignet. Die höhere Geschwindigkeit bringt dort keine nennenswerte Zeitersparnis, der Wetterschutz ist weniger wichtig (in wenigen Minuten kühlt man kaum aus; man kann besser eine Lücke zwischen Regenfronten abpassen), das Ein- und Aussteigen dauert länger, und man kann keinen Rucksack tragen, sondern muss Gepäck verstauen und wieder herausholen.


.. index:: Langstrecke

Wie fährt es sich auf Langstrecken?
-----------------------------------
Hervorragend. Hier kommen die Stärken eines Velomobils voll zum Tragen: große Zeitersparnis durch hohe Durchschnittsgeschwindigkeit, guter Wetterschutz, große Gepäckkapazität, keine Sitzbeschwerden, keine tauben Hände, keine Nackenschmerzen.


.. index:: Bewegunsenergie, Streckenplanung

Wie fährt man möglichst energiesparend?
---------------------------------------
Indem man nicht bremst.

Gegen Antriebsverluste, Roll- und Luftwiderstand kann man während der Fahrt nichts machen, denn die sind immer da. Beziehungsweise, durch eine möglichst konstante Geschwindigkeit kann man den Luftwiderstand reduzieren, welcher überproportional mit der Geschwindigkeit ansteigt (siehe: `Wie erreicht man eine möglichst hohe Durchschnittsgeschwindigkeit?`_). Die Energie aus Kletterarbeit bekommt man bei einer Abfahrt zurück, die Beschleunigungsleistung beim Ausrollen (oder `indem man mit dem Schwung bergauf rollt <Kann man im Hügelland fahren?>`_), aber alles, was weggebremst wird, ist verloren und muss mühsam wieder aufgebaut werden.

Dagegen hilft erstens `eine gute Streckenplanung <Wie plant man eine Strecke für Velomobile?>`_, so dass man möglichst nicht bremsen muss, und wenn, dann möglichst frühzeitig rollen lassen – man kommt erstaunlich weit, siehe :numref:`fig_speed_vs_rolling-distance`.

.. _fig_speed_vs_rolling-distance:

.. figure:: images/speed_vs_rolling-distance_de.*
    :alt: kinetische Energie und Ausrollstrecke
    :width: 100%

    Umrechnung von kinetischer Energie in Rollweite für verschiedene Fahrradtypen


.. index:: Streckenplanung, Routenplaner, Höchstgeschwindigkeit, Openstreetmap, BRouter, Auto

Wie plant man eine Strecke für Velomobile?
------------------------------------------
Wenn man möglichst zügig und entspannt vorwärts kommen will, muss man eine möglichst konstante Geschwindigkeit fahren können. Das bedeutet:

* übersichtliche Durchgangsstraßen, statt verwinkelter Gassen oder Wohnstraßen
* Vorfahrtsstraßen oder Kreisverkehre statt Stoppschilder
* möglichst wenige Steigungen
* Abfahrten möglichst flach, damit man nicht bremsen muss, sondern immer konstant mittreten kann

Tendenziell kann man sich eher an Routenplanern für Autos statt für Fahrräder orientieren; letztere führen zwar meist über verkehrsärmere Strecken, aber oft auch sehr verwinkelt. Im Unterschied zum Auto braucht man aber keine hohe Höchstgeschwindigkeit, sondern möglichst wenige Bremsvorgänge (siehe: `Wie fährt man möglichst energiesparend?`_) – jedes Beschleunigen kostet nicht nur viel Kraft, sondern auch mehrere 100 Meter Strecke, so dass man bei häufigen Ampeln nie auf eine hohe Geschwindigkeit kommt, aber sich trotzdem verausgabt (siehe: `Warum sind bei einem Velomobil die Beschleunigungsphasen so lang?`_). Lieber eine Strecke wählen, bei der man nicht ganz so schnell, aber dafür gleichmäßig fahren kann – statt eines einzigen Bremsvorgangs, der die Geschwindigkeit über etliche 100 Meter ruiniert.

Als Routenplaner empfiehlt sich `BRouter <https://brouter.de/brouter-web>`_; dieser hat u.a. ein Routing-Profil für Velomobile und ist komplett konfigurierbar. Die Daten stammen von Openstreetmap; ein falsches Routing hat daher meist falsch eingetragene oder unvollständige Openstreetmap-Daten als Ursache. Zum Glück kann diese jeder selber korrigieren; bis die geänderten Daten dann im BRouter angekommen sind, dauert es jedoch einige Zeit. Die von BRouter getroffenen Annahmen haben sich grundsätzlich bewährt; in bestimmten Gegenden oder anderen Ländern können sie jedoch unpassend sein.

Wenn man die Strecke per Hand planen möchte, kann man sich trotzdem am BRouter-Velomobil-Profil orientieren. Dieses rechnet nämlich sämtliche Hindernisse in Umwege um; so bedeutet beispielsweise die Angabe *uphillcost=80* aus der Profil-Datei, dass ein Höhenmeter bergauf einem Umweg von 80 Metern entspricht, und *turncost=150*, dass eine rechtwinklige Abzweigung 150 m Umweg entspricht.



Arbeitsweg
==========

.. index:: Arbeitsweg, Entfernung, Winter, Regen, Kleidung

Eignet sich ein Velomobil für den Arbeitsweg?
---------------------------------------------
Kommt darauf an; wenn man beispielsweise den Arbeitsweg in unter einer Stunde zurücklegen möchte, kann man im Flachland eine Strecke bis etwa 30 km fahren – mit Bergen entsprechend weniger. Mit dem Velomobil ist man dann je nach Strecke einige Minuten schneller als mit einem Aufrechtrad, aber vor allem deutlich erholter (da es sich weniger lohnt, überall Vollgas zu fahren), kann mehr Gepäck mitnehmen, und ist auch weniger witterungsabhängig – die Kleidung ist Sommer wie Winter fast gleich, und auch von einem Wolkenbruch wird man nicht komplett durchnässt, sondern kann trotzdem weiterfahren.

Wechselkleidung sollte man eigentlich immer dabei haben – außer die Strecke ist sehr kurz. Idealerweise kann man dann am Arbeitsplatz duschen, ansonsten kommt man auch mit einer reduzierten „Katzenwäsche“ aus.


Welche Entfernung kann man täglich zurücklegen?
-----------------------------------------------
Das hängt sehr von der jeweiligen Strecke ab; aber einen groben Eindruck geben die Fahrerlisten `bei Velomobiel.nl <http://www.velomobiel.nl/rijderslijst/index.php?amount=0>`_ und `bei Intercitybike <https://www.intercitybike.nl/rijderslijst/index.php?amount=0>`_. Die Eintragung der Kilometer ist zwar freiwillig und nicht immer aktuell, aber zumindest bei größeren Kilometerständen kann man davon ausgehen, dass es sich nicht nur um eine kurze Urlaubstour handelt, sondern die Strecken über mehrere Monate zusammengekommen sind. Daraus ergibt sich :numref:`fig_rijderslist_dist`; hier sieht man auch, dass mit den schnelleren Velomobilen tendenziell etwas größere Entfernungen zurückgelegt werden.

.. _fig_rijderslist_dist:

.. figure:: images/rijderslist_dist-histogram_de.*
    :alt: Monatliche Fahrleistungen der Velomobile von Velomobiel.nl und Intercitybike
    :width: 100%

    Monatliche Fahrleistungen der Velomobile von den Herstellern Velomobiel.nl und Intercitybike. Die Eintragung ist freiwillig und daher nicht repräsentativ. Ausgewertet wurden nur Velomobile, deren Gesamtfahrleistung mindestens 7500 km beträgt.


.. index:: Arbeitsweg, Motor, Pedelec, S-Pedelec

Braucht man für eine lange Pendelstrecke einen Motor?
-----------------------------------------------------
Um die Fahrzeit auf einer langen Strecke erträglich zu machen, wird oft gewünscht, einen Motor einzubauen. Ob das sinnvoll ist, hängt jedoch stark von der Strecke und vom Motor ab (siehe `Motor ja oder nein?`_). Ein 45-km/h-Motor reduziert zwar in fast allen Fällen die Fahrzeit, ist aber zumindest in Deutschland rechtlich nicht so einfach – nur wenige Modelle sind überhaupt zugelassen und Radwege dürfen nicht benutzt werden (siehe: `Rechtssituation in Deutschland?`_). Ein 25-km/h-Motor ist dagegen rechtlich ziemlich unproblematisch. Allerdings fährt man mit einem Velomobil meist über 25 km/h, wo der Motor nichts bringt. Daher bringt so ein Motor nur etwas bei langen Steigungen, auf denen man deutlich Zeit sparen kann. Bei häufigen Beschleunigungen hilft ein Motor auch, aber spart dabei nur Kraft – nennenswert schneller beschleunigt man nicht, und entsprechend spart man auch fast keine Zeit.


.. index:: Arbeitsweg, Steifigkeit, Gewicht, Luftwiderstand

Braucht man für eine lange Pendelstrecke ein schnelles Velomobil?
-----------------------------------------------------------------
Anfängern, die ein allwettertaugliches Fahrrad für eine längere Pendelstrecke suchen, wird oft zu einem schnellen Velomobil geraten – die aber neu sehr teuer und gebraucht schlecht zu bekommen sind. Anfänger wollen dagegen selten so viel Geld ausgeben und haben auch nicht vor, Geschwindigkeitsrekorde aufzustellen. Wer hat Recht?

Die Höchstgeschwindigkeit ist im Alltag tatsächlich gar nicht so relevant; und verglichen mit normalen Fahrrädern ist die Aerodynamik bei „langsamen“ Velomobilen immer noch hervorragend. Dagegen haben auf maximale Geschwindigkeit getrimmte Velomobile oft andere Nachteile, wie z.B. einen sehr großen Wendekreis, wenig Federweg und wenig Bodenfreiheit, was sie für manche Strecken unpraktisch bis untauglich macht. Zudem macht der Luftwiderstand selbst bei hohen Geschwindigkeiten nur gut die Hälfte des Widerstands aus, bei der Median-Geschwindigkeit im Alltag noch weniger (siehe :numref:`fig_power_comparison_stack`) – daher lohnt es sich kaum, hier viel Geld und Aufwand in die letzte Optimierung zu stecken.

Aber schnelle Velomobile haben nicht nur eine sehr gute Aerodynamik, sondern sind auch besonders effizient – d.h. sie sind relativ leicht und haben einen steifen Antriebsstrang. Von beidem profitiert man auch bei niedriger Geschwindigkeit: Ein geringes Gewicht macht sich nicht nur bergauf positiv bemerkbar (dort jedoch stärker als bei normalen Fahrrädern, siehe: `Warum sind Velomobil-Fahrer so besessen von niedrigem Gewicht?`_), sondern reduziert auch den Rollwiderstand (siehe: `Warum sind gute Reifen im Velomobil so wichtig?`_) und die Bewegungsenergie (siehe: `Warum sind bei einem Velomobil die Beschleunigungsphasen so lang?`_). Und ein steifer Antriebsstrang macht einen kraftvollen Antritt effizienter, wie er beim Beschleunigen oder bergauf gebraucht wird (siehe: `Warum ein steifer Antrieb?`_).

Das bedeutet, dass sich auf langen und bergigen Strecken ein Velomobil lohnt, das leicht ist und einen steifen Antriebsstrang hat.



Reparatur und Wartung
=====================

.. index:: Fahrradwerkstatt, Federbein, Fehlersuche, Hersteller, Fehlersuche

Braucht man eine Spezialwerkstatt?
----------------------------------
Eigentlich nicht. Velomobil-Technik ist kein Hexenwerk; grundsätzlich sind die allermeisten Dinge mit ein paar Grundkenntnissen von Fahrradtechnik gut beherrschbar. Und da die allermeisten Komponenten aus dem normalen Fahrradbereich stammen, sollte auch eine Fahrradwerkstatt mit den meisten Dingen kein Problem haben. Man muss also kein Hobby-Schrauber sein, um mit einem Velomobil fahren zu können. Dass es viele Velomobil-Fahrer trotzdem sind, dürfte erstens mit ihrem Interesse an Fahrrädern und Fahrradtechnik zu tun haben, und zweitens damit, dass sie oft hohe Fahrleistungen haben und es darum viel Zeit spart, wenn man Routine-Arbeiten selbst erledigen kann.

Nur bei vergleichsweise wenigen Defekten braucht man Hersteller-Support; aber selbst Spezialteile (z.B. :term:`Federbeine <Federbein>`) werden üblicherweise zügig zugeschickt und sind einfach einzubauen. Und wenn man ein etwas älteres Velomobil-Modell gekauft hat, dessen Kinderkrankheiten inzwischen beseitigt wurden, sollte man viele zehntausend Kilometer ohne Velomobil-spezifische Ersatzteile auskommen.

Definitiv schwieriger ist beim Velomobil allerdings die Fehlersuche, weil viele Teile schlecht einsehbar sind und man nicht z.B. mit einer Hand kurbeln und dabei das Schaltwerk beobachten kann. Darum ist es eine undankbare Aufgabe, bei diffusen Symptomen deren Ursache finden zu müssen. Ein Velomobil-Fahrer sollte daher Augen und Ohren offen halten, ob sich irgend etwas anders anfühlt oder anhört, und den Dingen auf den Grund gehen.


.. index:: Verschleißteile, Trommelbremse, Reifen, Kette

Verschleißteile, im Unterschied zu normalen Rädern?
---------------------------------------------------
Grundsätzlich tritt beim Velomobil eher weniger Verschleiß auf; vor allem der Antriebsstrang ist sehr langlebig, da er vor Dreck geschützt ist. Vor allem die Kette hält sehr lange, da sich dort zudem der Verschleiß auf eine viel größere Länge verteilt. Auch die Trommelbremsbeläge sind sehr langlebig – nicht, weil sie wenig beansprucht würden, sondern weil sie ziemlich überdimensioniert sind. Die kurzlebigsten Teile sind üblicherweise die Reifen.


.. index:: Wartung, Reifen, Bremse, Shim, Bremszug, Schaltzug, Kettenrohr

Was sind übliche Wartungsarbeiten?
----------------------------------
Am häufigsten wohl: Luftdruck kontrollieren, Reifen aufpumpen, und die Reifen (vor allem am Hinterrad) auf Schäden untersuchen (Steinchen, Schnitte, Schäden an der :term:`Karkasse`). Und natürlich den Licht-Akku aufladen.

Gelegentlich müssen die Kette abgewischt und geölt und die Bremsen nachgestellt werden. Wenn sich letztere nicht mehr richtig zurückstellt, kann man Shims bei der Bremse unterlegen (siehe `Was hat es mit dem Unterlegen von Blechplättchen bei Trommelbremsen auf sich?`_), oder die Bremsbeläge gleich komplett austauschen. Und bei Bedarf müssen die Brems- und Schaltzüge gewechselt werden. Und wenn die Kettenrohre verdreckt sind, steckt man einen dünnen Stofffetzen durch die Kette und zieht diese gefühlvoll durch die Rohre.


.. index:: Haltbarkeit, Carbon, Epoxidharz, UV-Strahlung, Lackierung, Gelcoat, Folierung

Wie lange hält Carbon? Wie kann man es schützen?
------------------------------------------------
Carbonfasern ermüden nicht; lediglich das Epoxidharz altert durch UV-Einfluss. Aber das ist nur ein kosmetisches Problem; wenn sich die oberste Harz-Schicht verfärbt oder zersetzt, kann man diese abschleifen und eine neue dünne Schicht Harz draufstreichen. Mit einem UV-beständigen Lack oder Gelcoat oder Folierung kann man das Epoxidharz schützen.


.. index:: Carbon, Aluminium, Korrosion, elektrochemische Spannungsreihe

Warum sollte man Alu-Teile nicht direkt in Carbon kleben?
---------------------------------------------------------
Wegen der elektrochemischen Spannungsreihe. Sowohl Kohlenstoff als auch Aluminium leiten Strom, und Feuchtigkeit ist im Velomobil auch ausreichend vorhanden; Kohlenstoff hat ein elektrochemisches Potenzial von +0.75 V (gegenüber der Normal-Wasserstoffelektrode), Aluminium von –1.66 V – somit ist Aluminium das unedlere Material. Wenn ein Strom zwischen beiden Materialien fließen kann, kommt es zur Kontaktkorrosion; Carbon dient dabei als Kathode und Aluminium als Anode, dabei wird es zu Aluminiumoxid oxidiert – es löst sich langsam auf.

Epoxidharz wirkt zwar isolierend; aber z.B. an Bohrungen besteht eine direkte Verbindung zwischen Carbon und Aluminium. Daher empfiehlt es sich, eine dünne Lage Glasfaser-Gewebe um das Metall zu legen, das dieses vom Carbon isoliert.


.. index:: Karosserie, Gelcoat, Lackierung, Folierung

Wie kann die Velomobil-Karosserie eingefärbt werden?
----------------------------------------------------
Hier haben sich drei Methoden etabliert:

* :term:`Gelcoat`: Dies ist ein gefärbtes Kunstharz, das gleich beim Bau mit in die Form eingebracht wird. Hier sind nur wenige Farben möglich, und nur einfarbige Bauteile.
* Lackierung: Hier wird das fertige Velomobil lackiert; dadurch sind mehr Farbtöne möglich, und durch Abkleben auch die Kombination mehrerer Farben. Zudem ist die Schicht tendenziell dünner (ca. 0.08 mm) und damit leichter (ca. 200 g/m²).
* Folierung: Folie bietet die meisten Gestaltungsmöglichkeiten und benötigt am wenigsten Ausrüstung, um ein gutes Ergebnis zu erzielen. Von der Dicke her (ca. 0.09 mm) ist sie vergleichbar mit Lack, aber tendenziell leichter. Im Vergleich zum Auto ist eine Velomobil-Folierung aber relativ teuer, da es viele gekrümmte Flächen gibt.



Sicherheit
==========

.. index:: Sichtbarkeit, Auto, Seitenabstand

Wie wird ein Velomobil von Autofahrern wahrgenommen?
----------------------------------------------------
Grundsätzlich wird man eher wie ein Auto statt wie ein Radfahrer behandelt. Dazu gehört auch, dass man üblicherweise mit mehr Seitenabstand überholt wird, und nur selten auf den Radweg verwiesen wird.

Die Geschwindigkeit wird jedoch oft unterschätzt; so passiert es häufig, dass man von einmündenden Autofahrern ausgebremst wird, die noch schnell vor dem Fahrrad rausfahren wollten, oder dass Überholer viel mehr Strecke brauchen und dem Gegenverkehr oder einer anderen Engstelle nahe kommen.

Zudem wird auch häufig im Überholverbot oder bei Gegenverkehr überholt; hier muss man entsprechend selbstbewusst reagieren und an gefährlichen Stellen einfach die Fahrspur dicht machen.


.. index:: Sichtbarkeit, Auto, Tagfahrlicht, Silhouette

Sichtbarkeit?
-------------
Ein Velomobil ist an sich definitiv nicht unauffällig oder unsichtbar; und wer es trotzdem schlecht sieht, sollte sich überlegen, ob er für den öffentlichen Straßenverkehr tauglich ist.

Trotzdem gibt es Schwachpunkte; z.B. ist die schmale und niedrige Silhouette gerade bei schwierigen Lichtverhältnissen schlecht zu sehen. Hier hilft eine gute Beleuchtung mit Tagfahrlicht. Das zweite Hauptproblem dürfte die selektive Wahrnehmung sein – mit Radfahrern wird oft nicht gerechnet, erst recht nicht mit schnellen, und auch nicht bei schlechtem Wetter. Um das zu ändern, müssten mehr Leute radfahren.


.. index:: Auto, Sicht, Rückspiegel

Wie soll man sich im Verkehr verhalten?
---------------------------------------
Wie üblich: vorausschauend, selbstbewusst, eindeutig.

Eine besondere Gefahr besteht jedoch darin, hinter Autos zu stehen, ohne in deren Spiegeln sichtbar zu sein. Viele SUV-artige Autos haben nur noch sehr hohe und kleine Heckfenster, so dass man ein gutes Stück Abstand halten sollte, um noch vom Innenspiegel sichtbar zu sein. Zudem ist ein Velomobil zu schmal, um von den Seitenspiegeln gesehen zu werden, wenn man nicht gerade seitlich versetzt zum Auto steht. Viele Autos haben zwar Rückfahrsensoren, aber diese sind auch oft recht hoch montiert, so dass ein Velomobil damit nicht zuverlässig erkannt werden dürfte.


.. index:: Unfall, Alleinunfall, Mehrspurer, Auto, Karosserie, Knautschzone, Hutze, Haube, Überrollbügel, Helm, Aufrechtrad

Wie ist die Verletzungsgefahr bei einem Unfall?
-----------------------------------------------
Alleinunfälle sind bei :term:`Mehrspurern <Mehrspurer>` grundsätzlich seltener, weil man nicht so leicht umfällt; wenn es glatt ist, rutscht man etwas, aber mehr passiert üblicherweise nicht.

Bei einem Zusammenstoß mit einem Auto hat ein Velomobil weder die Stabilität noch die Masse, um dem nennenswert etwas entgegenzusetzen. Trotzdem sind viele Unfälle bisher erstaunlich glimpflich ausgegangen, aus folgenden Gründen:

* Schutz durch die Karosserie: Das Velomobil rutscht auf seiner Karosserie über den Asphalt, statt der Fahrer mit seiner nackten Haut – selbst wenn die Karosserie durch den Zusammenstoß gebrochen ist.

* Liegeposition: Bei einem Aufprall von vorne sind als erstens die Beine betroffen; diese können aber recht gut hohe Kräfte aushalten, im Gegensatz zu Kopf oder Armen, die bei einem Aufrechtrad oft als erstes kollidieren.

* Form: Die konvexe Form der Karosserie verhindert, dass sich das Fahrrad unter das Auto schiebt und überrollt wird.

Darum entstehen Verletzungen oft vor allem durch scharfkantige Gegenstände im Innenraum, z.B. Schrauben, Lenker, Brems- und Schalthebel, oder auch durch Kollision von Kopf und Oberkörper mit dem Süllrand oder dem Inneren der Haube.

Es gab jedoch auch einige schwere Unfälle; bei diesen ist der Fahrer entweder aus dem Velomobil herausgeschleudert worden oder mit dem Kopf mit einem Gegenstand (z.B. Stein, Pfosten) kollidiert. Aus diesem Grund haben manche Velomobile eine möglichst kleine Cockpit-Luke, die nicht nur aerodynamisch günstig ist, sondern (speziell wenn sie hinten spitz zuläuft) den Körper an den Schultern fixiert. Da die Heck-:term:`Hutze` üblicherweise höher ist als der Kopf, wirkt diese wie ein Überrollbügel – falls der Fahrer ausreichend weit hinten sitzt. Hier würde ein Schultergurt zusätzlich helfen.

Eine :term:`Haube` ist dagegen üblicherweise nicht so steif und fest, dass sie hohe Kräfte aushält. Aber vor allem ist sie nur mit Gummispannern befestigt, so dass sie bei einer größeren Krafteinwirkung davonfliegen dürfte. Zumindest sollte sie Gegenstände am Kopf vorbei lenken und Hautkontakt zum Asphalt verhindern.

Relativ verbreitet ist die Angst, bei einem Crash von den dabei entstehenden scharfkantigen Carbon-Splittern durchsiebt zu werden. Das ist aber weitgehend unbegründet; die Carbon-Hülle ist nämlich so dünn, dass sie schnell reißt und kaum Splitter produziert. Zudem haben moderne Velomobile eine Nylon-Zwischenlage, die das Reißen der Hülle weitgehend verhindert; so brechen zwar einige Carbonfasern, bleiben aber an Ort und Stelle. Eine Gefahr entstünde nur, wenn sehr massive und steife Carbon-Teile durch die Kollision pulverisiert würden; aber bei einem derart heftigen Unfall hat man ganz andere Probleme als ein paar Carbonsplitter in der Haut.

Wegen der Überrollbügelfunktion der Hutze und dem geringeren Alleinunfall-Risiko gibt es deutlich weniger Gründe, einen Helm zu tragen; zudem passen viele nicht unter die Haube oder sind hinten zu lang. Außerdem wäre das Gesicht dann immer noch ungeschützt.



.. raw:: latex

    \appendix

Mitwirkende und Quellen
=======================

`Version <https://gitlab.com/cmoder/velomobil-grundwissen>`_ |version| (Git-Commit: |commit|, Datum: |commit_date|)

Mitwirkende:

* Text und Diagramme: `Christoph Moder <https://christoph-moder.de/>`_
* Englische Übersetzung: `Tony Grant <https://www.velomobilforum.de/forum/index.php?members/anotherkiwi.21983/>`_
* VM-Slider: `Jockel Hofmann <https://www.velomobilforum.de/forum/index.php?members/24kmh_sammler.17233/>`_

.. raw:: latex

    \begin{samepage}

Quellen:

* :numref:`fig_secondhand_prices`: `LazyBiker <https://www.velomobilforum.de/forum/index.php?members/lazybiker.21824/>`_
* :numref:`fig_rijderslist_countries`, :numref:`%s <fig_rijderslist_models>`, :numref:`%s <fig_rijderslist_dist>`: Daten `Intercitybike <https://www.intercitybike.nl/rijderslijst/index.php?amount=0>`_, `Velomobiel <http://www.velomobiel.nl/rijderslijst/index.php?amount=0>`_ (Stand: 2020-02-29)
* :numref:`fig_age`: `Daten <https://www.velomobilforum.de/forum/index.php?threads/velomobil-alter-hase-oder-junger-spund.47682/>`_ (Stand: 2020-03-04)
* :numref:`fig_ackermann-bedingung`: Ist eine Bearbeitung `einer Grafik aus der Wikipedia <https://commons.wikimedia.org/wiki/File:Ackermann_radius_M.svg>`_, die ursprünglich vom User *Bromskloss* stammt und unter der `GNU-Lizenz für freie Dokumentation 1.2 <https://commons.wikimedia.org/wiki/Commons:GNU_Free_Documentation_License,_version_1.2>`_ sowie der `Creative Commons CC-BY-SA <https://creativecommons.org/licenses/by-sa/3.0/deed.de>`_ steht.
* :numref:`fig_mango_steering`: `WeLi-Arno <https://www.velomobilforum.de/forum/index.php?members/weli-arno.8903/>`_
* :numref:`fig_bicyclerollingresistance_hist`, :numref:`fig_bicyclerollingresistance_scatter_p_inc`: Daten von `BicycleRollingResistance.com <https://www.bicyclerollingresistance.com/road-bike-reviews>`_ (Autor: Jarno Bierman; Stand: 2020-07-04)

.. raw:: latex

    \end{samepage}


Glossar
=======
.. glossary::

    Aerobelly
        Eine größere Menge viszeralen Fettgewebes (:term:`Biopren`), dem scherzhaft nachgesagt wird, die Aerodynamik zu verbessern – jedoch irrelevant im Velomobil. Auch *Gössermuskel* oder *Knödelfriedhof* genannt.

    Bewegungsenergie
        Auch *kinetische Energie*; diese muss aufgewendet werden, um eine Masse in Bewegung zu versetzen. Die Formel lautet :math:`E_\text{kin} = 1/2 \cdot m \cdot v^2`, d.h. die Masse :math:`m` geht linear und die Geschwindigkeit :math:`v` geht quadratisch ein.

    Bigfoot
        Bauteil beim :index:`DF`-Velomobil, das den Tretlagermast am hinteren Ende abstützt.

    Biopren
        Scherzhafte Bezeichnung des Körperfetts, das analog zu z.B. Neopren der Wärmeisolierung und Polsterung dient, aber zusätzlich noch als Energiespeicher dient und dabei kein Zusatzgewicht am Fahrrad verursacht.

    boruttisieren
        Diebstahlschutz, indem man das Rad optisch so verschandelt und ungepflegt aussehen lässt, dass es für Diebe unattraktiv wird.

    Bovidenmatte
        Scherzhafte Bezeichnung einer Sitzunterlage aus Lammfell.

    Bump Steer
        Federungsbedingter Lenkeinfluss; siehe `Was ist „Bump Steer“?`_.

    CFD
        *Computational Fluid Dynamics*, also Simulation der Strömung von Fluiden (Flüssigkeiten, aber auch Gase wie Luft) per Computer.

    Cleat
        Metallplatte an der Sohle von :term:`Klickschuhen <Klickschuhe>`, mit der diese in :term:`Klickpedalen <Klickpedal>` einrasten.

    Delta
        Dreirad, bei dem ein Rad vorne und zwei hinten sind. Benannt nach dem griechischen Buchstaben Delta, der wie ein Dreieck aussieht.

    delphinieren
        In hügeligem Gelände kann man schnell und energiesparend unterwegs sein, wenn die Hügel nicht zu hoch sind – dann kann man nämlich in der Ebene in Ruhe Schwung nehmen, damit die Steigung überwinden, und bergab wieder Geschwindigkeit für den nächsten Hügel aufnehmen. Wie Delphine, die beim Schwimmen aus dem Wasser springen und dabei auch nicht langsamer werden.

    Dichtmilch
        Eine Latex-haltige Flüssigkeit, mit der ein :term:`Tubeless`-Reifen abgedichtet wird. Diese wird vor der Montage in den Reifen gegeben, oder kann beim *MilkIt*-System durch spezielle Ventile mittels einer Spritze nachträglich eingespritzt oder auch abgesaugt werden. Da die Dichtmilch austrocknet, muss sie alle paar Monate nachgefüllt werden.

    Doppelmanta
        Fahrweise, bei der man beide Arme aus dem Cockpit hängt. Dies funktioniert nur bei einem :term:`Tiller`-Lenker gut, da sich :term:`Panzerlenkhebel <Panzerlenkung>` zu tief befinden, um sie mit heraushängenden Armen zu bedienen. Der Name kommt vom *Opel Manta*, dessen Fahrer dafür bekannt waren, gerne den linken Ellenbogen aus dem Fenster zu halten. Da das Velomobil schmäler ist, kann man es mit beiden Armen machen – das ist vor allem im Sommer interessant, um die Arme im Fahrtwind besser zu kühlen.

    Einspurer
        Ein Fahrrad, bei dem die Räder hintereinander stehen, und das sich deshalb in die Kurve neigen kann. Dies kann ein Liegerad, aber ebenso ein :index:`Aufrechtrad` sein. Velomobile sind aber praktisch immer Mehrspurer.

    Entfaltung
        Entfernung, die man mit einer Kurbelumdrehung zurücklegt; und damit ein Maß für die Übersetzung des Antriebs. Die Entfaltung hängt von der Schaltung, dem gewählten Gang und der Größe des Antriebsrads ab; im niedrigsten Gang haben Velomobile eine Entfaltung von etwa 2 m, im höchsten Gang von etwa 11 m.

    ETRTO
        Standard zur Bezeichnung von Radgrößen. Der Durchmesser wird bis zur Höhe des :index:`Felgenhorns <Felgenhorn>` gemessen; d.h. das ist weder der Durchmesser des Felgenbetts, noch der Außendurchmesser der Felge. Die Zoll-Angabe bezeichnet dagegen nicht die Größe der Felge, sondern die Gesamtgröße mit Reifen – daher kann ein Mountainbike-Rad mit breiten Reifen größer sein als ein der Bezeichnung nach größeres Rennrad-Rad.

    Federbein
        Bestandteil einer Einzelradaufhängung, bei der der drehbare Radträger mit einer Federung und Dämpfung ausgestattet ist. Auch *MacPherson-Federbein* genannt.

    Gelcoat
        Gefärbtes Kunstharz, das als äußere Schicht den Faserverbundwerkstoff vor u.a. :index:`UV-Strahlung` schützt. Wird vor allem im Bootsbau verwendet, aber auch bei manchen Velomobilen. Der Gelcoat wird beim Laminieren zuerst in die Negativform gestrichen; darüber kommt dann das Fasergewebe und Epoxidharz. Wird kein Gelcoat verwendet, muss das fertige Velomobil lackiert oder foliert werden.

    Geradeauslauf
        Der Geradeauslauf wird positiv beeinflusst von einem großen Radstand, einer unempfindlichen Lenkung, einer geringen Windempfindlichkeit, und (v.a. bei einem :term:`Einspurer`) von einem großen Nachlauf.

    Grenzschicht
        Bereich eines strömenden Fluids nahe einer Oberfläche, bei dem dessen Geschwindigkeit relativ zur Oberfläche von null bis auf die Strömungsgeschwindigkeit des restlichen Fluids anwächst. Diese kann laminar sein (d.h. es gibt innerhalb der Strömung einen Geschwindigkeitsgradienten) oder turbulent (d.h. es gibt kleine Wirbel entlang der Oberfläche, die die laminare Strömung von dieser trennen).

    Haube
        Die Abdeckung der Einstiegsluke, meist mit seitlichen Fenstern und vorne einem aufklappbaren Motorradvisier, heißt Haube.

    Hosen
        Eine aerodynamische Abdeckung offener :term:`Radkästen <Radkasten>`. Siehe `Was sind Hosen?`_.

    HPV
        *Human Powered Vehicle*, also sinngemäß *muskelbetriebenes Fahrzeug*. Auch der Name des deutschen Vereins, *HPV Deutschland e.V.*; der internationale Dachverband heißt *IHPV*.

    Hump
        Erhöhung im Felgenbett mancher :term:`Tubeless`-Felgen, die gewährleistet, dass der Reifen außen am :index:`Felgenhorn` bleibt und nicht nach innen rutscht. Dazu muss man meist beim Aufpumpen den Reifen etwas überpumpen, bis er durch den Druck über den Hump rutscht. Man kann einen Hump selber nachrüsten, indem man einen Faden in das Felgenbett parallel zum Felgenhorn legt und mit Tubeless-Felgenband abdeckt.

    Hutze
        Der hintere obere Teil eines Velomobils, der hinter dem Kopf beginnt und in einem Bogen zum :index:`Heck` verläuft. Sie dient als aerodynamische Fortsetzung des Kopfs und verhindert dort Verwirbelungen, ähnlich wie ein überdimensionierter Aero-Helm. Zugleich ist es der höchste Punkt des Velomobils und wegen der starken Krümmung sehr steif, sie dient daher auch als Überrollbügel bei einem Unfall.

    IGUS
        Eine Firma, die Kunststoffgleitlager herstellt. Da im Lenkgestänge eines Velomobils etliche Kugelköpfe verbaut sind, kann man durch Kunststoffausführungen einiges an Gewicht sparen.

    ingmarisieren
        Kettenpflege, indem man die Kette nicht pflegt. Da Verschleiß vor allem von Dreck verursacht wird und dieser durch Öl besonders gut haftet, hat eine ölfreie Kette eine gewisse Berechtigung, wenn die Kette permanent Wasser und Dreck ausgesetzt ist, man also permanent nachölen müsste und die Kette effektiv wassergeschmiert ist – und das Fahrrad auch häufig benutzt wird, so dass die Kette nicht festrostet. Im Velomobil ist das keine gute Idee, weil dort die Kette fast keinem Dreck und Wasser ausgesetzt ist, und eine saubere Kette ist am reibungsärmsten, wenn sie ausreichend geölt ist.

    K-Heck
        Ein Fahrzeugheck, das sich nicht komplett verjüngt, sondern scharf abgeschnitten ist. Die scharfe Kante sorgt dafür, dass die Strömung hinten nicht nach innen umgelenkt wird und dort verwirbelt, sondern schlagartig abreißt. Es ist zwar geringfügig ineffizienter als ein langes Heck, aber die Verluste durch die stärkere Verwirbelung werden zum Teil kompensiert durch die kleinere umströmte Oberfläche und das geringere Gewicht. Benannt nach Wunibald Kamm, daher auch *Kamm-Heck*.

    Kapazität
        Länge um, die ein Kettenspanner die Kette verkürzen kann. Kettenspanner mit langen Käfigen, wie sie im Mountainbike-Bereich üblich sind, haben eine Kapazität um die 40 Kettenglieder. Um alle Gänge schalten zu können, muss die Kapazität gleich der Differenz zwischen der Summe der Zähnezahlen des größten Kettenblatts und größten Ritzels und der Summe der Zähnezahlen des kleinsten Kettenblatts und kleinsten Ritzels sein. Wenn die Kapazität nicht ausreicht, sollte man groß–groß noch schalten können, da sonst durch eine zu straff gespannte Kette Schäden auftreten können. Bei klein–klein hängt die Kette dann durch; das ist aber weniger problematisch.

    Karkasse
        Ein Gewebe, das im Reifen eingebettet ist und die Zugkräfte durch den Reifendruck und die Verformung aufnimmt. Der Gummi sorgt nur für die Luftdichtheit und die Haftung des Reifens auf der Straße; daher führt eine Beschädigung der Karkasse (z.B. durch einen tiefen Schnitt) oft zu einer Beule im Reifen oder lässt den Reifen gleich platzen. Um so eine Stelle vorübergehend zu reparieren, reicht nicht nur z.B. Klebeband, sondern man braucht ein Stück festen Stoff, das den Druck des Schlauches auf den umliegenden Mantel verteilt.

    Kettenblatt
        Als Kettenblätter bezeichnet man die von der Tretkurbel angetriebenen Zahnräder. Damit sie zu einer bestimmten Tretkurbel passen, muss der :term:`Lochkreis` übereinstimmen.

    Kettenlinie
        Eine Fahrradkette hat eine gewisse seitliche Beweglichkeit, sollte aber möglichst nicht permanent schräg laufen, da dies den Tretwiderstand und Verschleiß erhöht. Allerdings sind bei Velomobilen die Umlenkrollen weit genug von :term:`Kettenblatt` bzw. :term:`Ritzel` entfernt, dass sich der Schräglauf in allen Gängen in Grenzen hält. Zudem sind die Umlenkrollen bei manchen Velomobilen seitlich verschiebbar.

    Kleiderbügel
        Ein Aluminium-Bauteil in Velomobilen von Velomobiel.nl, z.B. dem :index:`Quest` oder :index:`Strada`, das sich quer hinter dem Sitz befindet. Wie der Name schon sagt, ähnelt es einem Kleiderbügel; die beiden langen Schenkel sind links und rechts an der Karosserie befestigt, und der kurze obere Teil oben in der Hutze hinter dem Kopf des Fahrers.

    Klickpedal
        Ein Pedal, bei dem spezielle Schuhe (:term:`Klickschuhe`) mit einer Metallplatte (:term:`Cleat`) einrastet und somit den Fuß gut fixiert. Dies erlaubt eine effizientere Kraftübertragung, weil man nicht vom Pedal abrutschen kann. Es gibt mehrere verschiedene Systeme; die im Rennradbereich haben meist große dreieckige Cleats, die mit drei Schrauben befestigt werden. Im Mountainbike-Bereich dominiert dagegen das SPD-System, dessen kleinere Cleats mit zwei Schrauben befestigt werden und im Profil der Sohle versenkt ist, so dass man damit recht gut laufen kann. Daneben ist z.B. noch Speedplay erwähnenswert, das einen besonders großen Drehbereich bietet.

    Klickschuhe
        Schuhe für :term:`Klickpedale <Klickpedal>`. Siehe auch: `Braucht man Klickschuhe?`_

    Kompaktkurbel
        Eine Kurbelgarnitur mit zwei Kettenblättern. Da der :term:`Lochkreis` nur 110 mm beträgt, kann ein recht kleines Kettenblatt montiert werden – man erreicht damit also mit nur zwei Kettenblättern einen ähnlich großen Übersetzungsbereich wie sonst mit drei Kettenblättern.

    konschen
        Kopfüber im Velomobil hängen (so dass nur noch die Beine herausschauen), um vorne z.B. an der Schaltung etwas einzustellen. Da sich der werkseitige Einbau einer Wartungsöffnung im Tretlager-/Bugbereich noch nicht bei allen Velomobil-Modellen durchgesetzt hat, ist die Einnahme einer akrobatischen Montageposition immer wieder unterhaltsames Thema unter Velomobil-Besitzern.

    Körperöffnungswinkel
        Der Winkel zwischen Rumpf und Beinen. Viele Leute können besser bergauf fahren, wenn dieser Winkel kleiner ist, d.h. sie aufrechter sitzen oder die :term:`Tretlagerüberhöhung` größer ist.

    Längslenker
        Eine Stange, die das untere Ende des :term:`Federbeins <Federbein>` hält und typischerweise vom vorderen Ende des :term:`Radkastens <Radkasten>` zur :term:`Lenkplatte` verläuft. Je weiter innen im Radkasten und je weiter vorne an der Lenkplatte der Längslenker befestigt ist, desto weiter außen kreuzen sich die Richtungen von Längs- und Querlenker, d.h. umso negativer wird der :term:`Lenkrollradius`. Siehe: `Wie heißen denn die ganzen Teile am Fahrwerk?`_

    Leertrum
        Der Teil der Kette, der vom :term:`Kettenblatt` zurück zum :term:`Ritzel` läuft und keine Zugkraft überträgt (also nicht gespannt ist) – im Unterschied zum :term:`Zugtrum`.

    Lenkbrücke
        Abdeckung zwischen den vorderen Radkästen, unter der sich das Lenkgestänge befindet (d.h. :term:`Spurstange` und :term:`Querlenker`). Da die Brücke ziemlich voluminös und damit steif ist, wird dort oft der Tretlagermast oder die vordere Umlenkrolle befestigt.

    Lenkplatte
        Eine Befestigung unteren Ende des :term:`Federbeins <Federbein>`, an der :term:`Längslenker`, :term:`Querlenker`, :term:`Spurstange` und, bei der :term:`Panzerlenkung`, auch die Lenkhebel angeschraubt sind. Siehe: `Wie heißen denn die ganzen Teile am Fahrwerk?`_

    Lenkrollradius
        Der Radius, um den sich das Rad beim Lenken um den Drehpunkt der Lenkung dreht. Siehe `Was hat es mit dem Lenkrollradius auf sich?`_ und `Wo ist denn überhaupt die Lenkachse bei einem MacPherson-Federbein?`_.

    Lochkreis
        Durchmesser, in dem sich die Befestigungsschrauben der Kettenblätter befinden. Gängige Standards sind 130 mm und 110 mm (:term:`Kompaktkurbel`).

    Luftwiderstand
        Kraft, die aufgewendet werden muss, um die Luft zu verdrängen. Die Formel lautet :math:`F_\text{air} = 1/2 \cdot c_W \cdot A \cdot \rho \cdot v^2`, d.h. die Querschnittsfläche :math:`A` geht linear und die Geschwindigkeit :math:`v` geht quadratisch ein. Der Faktor :math:`1/2 \cdot \rho \cdot v^2` entspricht dem Staudruck, der von Geschwindigkeit :math:`v` (quadratisch) und Luftdichte :math:`\rho` (linear) abhängt.

    Mehrspurer
        Ein Fahrzeug mit mehreren Rädern, die nicht hintereinander laufen. So ein Fahrzeug kann sich nicht in die Kurve neigen (außer spezielle Neigefahrzeuge), fällt dafür auch nicht um, wenn es still steht oder ein Rad wegrutscht.

    Meufl
        Eigentlich eine Abkürzung für *Mitteleuropäisches Fahrradlabor*; so bezeichnet Harald Winkler seine Fahrrad-Projekte. Bekannt geworden ist er aber in den 1990er-Jahren durch seine Fahrradverkleidungen aus geschlossenporigem Schaumstoff, die aerodynamisch und sehr leicht waren. Entsprechend bedeutet *gemeufelt* meist, dass ein Bauteil aus Schaumstoff hergestellt ist.

    NACA-Duct
        Siehe `Was ist ein NACA-Duct?`_

    Nachlauf
        Der Abstand zwischen :term:`Spurpunkt` und Aufstandspunkt eines gelenkten Rades. Wenn dieser positiv ist, d.h. der Radaufstandspunkt hinter dem Spurpunkt ist, dann folgt das Rad der Richtung des Rahmens – bzw. aus Sicht des Rahmens kehrt das gelenkte Rad von alleine in die Geradeausstellung zurück. Einen positiven Nachlauf kann man erreichen, indem man das Lenklager vor dem Rad anordnet, oder wenn das nicht der Fall ist, die Lenkachse so schräg stellt, dass deren Verlängerung vor das Rad zeigt. Siehe: `Was ist der Nachlauf, und was ist seine Funktion?`_

    Normalized Power
        Fahrten mit der gleichen Durchschnittsleistung können für den Körper sehr unterschiedlich belastend sein – je nachdem, wie stark die Leistung schwankt. Dies wird mit der *Normalized Power* ausgedrückt, einer empirischen Formel, die Leistungsschwankungen auf die Durchschnittsleistung anrechnet. Dazu wird die Tretleistung als gleitender Durchschnitt über 30 Sekunden berechnet, davon die vierte Potenz gebildet, gemittelt, und daraus die vierte Wurzel gezogen. Das Ergebnis ist eine Durchschnittsleistung, die bei konstanter Leistungsabgabe genauso anstrengend wäre wie die schwankende Leistung.

    Panzerlenkung
        Lenkung bei Dreirädern, bei der zwei Lenkhebel links und rechts die Räder lenken. Dabei sind die Lenkhebel mit den :term:`Lenkplatten <Lenkplatte>` verbunden, die :term:`Spurstange` dient nur dazu, die beiden Räder und damit auch die Lenkhebel zu koppeln.

    Pinlock
        Scheibe, die innen an einem Motorradvisier befestigt wird und für eine luftdichte Doppelverglasung sorgt. Diese dient der Wärmeisolation und verhindert das Beschlagen. Siehe: `Was macht man gegen beschlagene Scheiben?`_ und `Wie fährt sich ein Velomobil im Winter?`_

    Querlenker
        Eine Stange, die das untere Ende des :term:`Federbeins <Federbein>` hält und typischerweise in der Mitte des Velomobils, unterhalb der :term:`Lenkbrücke`, befestigt ist. Je länger der Querlenker ist, desto weniger ändert sich sein Winkel beim Einfedern des Federbeins, und entsprechend geringer ist der Effekt des :term:`Bump Steer`. Siehe: `Wie heißen denn die ganzen Teile am Fahrwerk?`_

    Radkasten
        Gehäuse, in denen sich die Räder befinden. Diese dienen der aerodynamischen Verkleidung, aber auch dem Schutz vor Dreck (also wie Schutzbleche); und da die Radkästen oft integraler Bestandteil der Karosserie sind, haben sie auch eine tragende Funktion; z.B. sind an den Vorderrädern die :term:`Federbeine <Federbein>` oben an den Radkästen festgeschraubt.

    Radscheiben
        Scheiben, die an Speichenräden befestigt werden, um für eine glatte Oberfläche und damit reduzierten Luftwiderstand zu sorgen. Die Scheiben werden meist auf den Felgen angeklebt; wenn sie aus Stoff sind, auch an den Speichen angeclipst.

    Reifenlatsch
        Die Fläche des Reifens, die durch die Auflast auf der Straße platt gedrückt wird. Die Größe des Latsches hängt von der Auflast und dem Luftdruck ab; da Druck mal Fläche gleich Druckkraft ist, ist die Fläche so groß, dass die Druckkraft gleich der Auflast ist – mit höherem Druck lässt sich entsprechend der Reifenlatsch verkleinern. Die Form hängt von der Reifenbreite ab; bei einem breiten Reifen ist der Reifenlatsch kurz und breit, bei einem schmalen Reifen schmal und lang.

    Risse-Dämpfer
        Ein Hinterraddämpfer für bestimmte Velomobile. Bietet ein besseres Dämpfungsverhalten, aber hat den Ruf, empfindlicher zu sein.

    Ritzel
        Als Ritzel bezeichnet man die von der Kette angetriebenen Zahnräder, die auf dem Freilauf des Antriebsrads sitzen. Meist handelt es sich um einen Shimano-Freilaufkörper mit Nuten, auf dem die Ritzel entweder einzeln oder auf einem gemeinsamen Ritzelträger („Spider“) zusammengenietet sitzen. Die Gesamtheit der Ritzel nennt man *Kassette*.

    Rollwiderstand
        Kraft, die aufgewendet werden muss, um ein Rad über eine raue Oberfläche zu rollen. Die Formel lautet :math:`F_\text{roll} = c_R \cdot F_N`, d.h. die Normalkraft :math:`F_N` geht linear ein. Wenn man noch einen geschwindigkeitsabhängige Komponente berücksichtigt (siehe: `Was ist die dynamische Rollreibung?`_), lautet die Formel :math:`F_\text{roll} = c_R \cdot F_N + c_\text{dyn} \cdot v`.

    Sahnehäubchen
        So wird eine :term:`Haube` für das :index:`Quest` genannt.

    Schaltwerk
        Das Schaltwerk ist die Kettenschaltung bei den Ritzeln, also typischerweise am Hinterrad. Meist wird geschaltet, indem der Schaltwerkskäfig mit den Schaltröllchen bewegt wird; dieser dient gleichzeitig als Kettenspanner – je länger der Käfig ist, desto größer ist die :term:`Kapazität` des Schaltwerks. Bei manchen Velomobilen sind diese Aufgaben allerdings getrennt; dort drückt das Schaltwerk die Kette mittels zwei Platten zur Seite, und der Kettenspanner befindet sich getrennt davon an einer anderen Stelle.

    Schaumdeckel
        Bei einigen Velomobilen (v.a. der `niederländischen Hersteller <Wie heißen denn die gängigen Hersteller und Modelle?>`_) kann man die Einstiegsöffnung nicht nur mit einer Haube verschließen, sondern alternativ mit einem flexiblen Deckel aus einem Schaumstoff. Dieser wird von innen angeklettet und bietet eine Öffnung, so dass der Kopf nach draußen ragen kann, aber der restliche Körper von Fahrtwind und Regen weitgehend geschützt ist. Zum Parken kann man auch noch die verbliebene Öffnung verschließen.

    scheinbarer Wind
        Vektorsumme des Windes („wahrer Wind“) und dem Fahrtwind – wenn beispielsweise wahrer Wind und Fahrtwind gleich groß sind, und der wahre Wind rechtwinklig von der Seite kommt, dann kommt der scheinbare Wind im Winkel von 45° von vorne und ist gemäß Satz des Pythagoras um 41% stärker als jeweils wahrer Wind und Fahrtwind.

    Schwerpunkt
        Durchschnittlicher Ort der Masse eines Körpers. Für viele Berechnungen kann man annehmen, dass die gesamte Masse eines Körpers im Schwerpunkt versammelt wäre, und sämtliche Kräfte dort angreifen.

    Snakebite
        Luftverlust im Reifen durch zwei Löcher im Schlauch, die wie der Biss einer Schlange aussieht. Ursache ist ein Durchschlag; der Reifen wird so stark eingedrückt, dass der Schlauch auf beiden Seiten zwischen Reifen und :index:`Felgenhorn` eingequetscht wird, was zu den beiden Löchern führt. Mit einem höheren Luftdruck und/oder breiteren Reifen kann die Anfälligkeit für Snakebites reduziert werden.

    Spurpunkt
        Schnittpunkt der verlängerten Lenkungsdrehachse mit der Straße

    Spurstange
        Stange in Querrichtung des Fahrzeugs, die die beiden Vorderräder verbindet und die Lenkungbewegungen überträgt. Bei einem :term:`Tiller` wird die Lenkbewegung über die Spurstange an die Vorderräder übertragen, während bei einer :term:`Panzerlenkung` die Lenkhebel direkt auf die :term:`Lenkplatten <Lenkplatte>` einwirken und die Spurstange lediglich die Lenkbewegungen beider Räder koppelt. Eine Spurstange kann durchgehend oder geteilt sein; in letzterem Fall bewegt sie sich beim Einfedern ähnlich wie der Querlenker, was den :term:`Bump Steer` reduziert. Siehe: `Wie heißen denn die ganzen Teile am Fahrwerk?`_

    Staupunkt
        Der Punkt in der Front eines Fahrzeugs, an dem die Luft nicht schräg auftritt und seitlich abgelenkt wird, sondern senkrecht auf die Oberfläche trifft und sich staut. Die Bewegungsenergie der Luft resultiert in einer Erhöhung des Drucks. Hier bietet sich eine Belüftungsöffnung an, weil wegen des erhöhten Drucks eine kleine Öffnung für einen großen Luftdurchsatz ausreicht. Zudem gibt es hier keine laminare Strömung, die durch die Öffnung gestört werden könnte.

    Stormstrip
        Ein Band in Längsrichtung des Fahrzeugs, das die Überströmung in Querrichtung behindert, und somit die Seitenwindanfälligkeit reduziert. Siehe: `Wie kann man die Windempfindlichkeit reduzieren?`_.

    Strömungsabriss
        Luftströmung liegt entweder laminar an einer Oberfläche an und folgt deren Verlauf, oder löst sich ab und verwirbelt. Der Übergang erfolgt relativ abrupt, daher der Name.

    Sturz
        Neigung des Rades, so dass dieses nicht senkrecht auf der Fahrbahn steht, sondern nach außen („positiver Sturz“) oder nach innen („negativer Sturz“) gekippt. Siehe: `Warum stehen die Vorderräder schräg? Bremst das nicht?`_

    Tadpole
        Dreirad, bei dem ein Rad hinten und zwei vorne sind. Benannt nach dem englischen Wort für *Kaulquappe*, da das Dreirad ebenso vorne dick und hinten dünn ist.

    Tretlagermast
        Mast, der von der :term:`Lenkbrücke` nach vorne verläuft und oft auch vorne an der Spitze des Velomobils am :term:`Trichter` befestigt ist, und das Tretlager mit den Tretkurbeln und Pedalen trägt. Damit dieses verstellt werden kann, ist entweder der Mast an sich verstellbar (z.B. auszieh- und schwenkbar), oder das Tretlager ist verschiebbar auf den Mast geklemmt. Wenn der Mast vorne befestigt ist, wird meist auch die Frischluft durch den Mast nach hinten geleitet.

    Tretlagerüberhöhung
        Der Höhenunterschied zwischen Tretlager und tiefstem Punkt der Sitzschale. Für einen optimalen :term:`Körperöffnungswinkel` und eine möglichst kleine Querschnittsfläche muss die Überhöhung relativ groß sein; andererseits sind dann die Beine im Blickfeld – erst recht, wenn diese auch noch von einer Karosserie umgeben sind.

    Trichter
        Bauteil vorne an der Spitze, in das der :term:`Tretlagermast` geklebt wird. Da der Trichter auch am :term:`Staupunkt` sitzt, leitet er die Frischluft nach innen durch den Mast.

    Tiller
        Lenkung bei Dreirädern, bei der eine zentrale Lenkstange über ein Kreuzgelenk die :term:`Spurstange` bewegt. Der englische Name bedeutet ursprünglich *Ruderpinne*.

    Tubeless
        Normalerweise sorgt ein Reifen nur für die Haftung auf der Straße und nimmt mittels seiner :term:`Karkasse` die entstehenden Kräfte auf; die Luftdichtigkeit wird von einem darin befindlichen Schlauch gewährleistet. Bei einem Tubeless-Rad verzichtet man auf den Schlauch, indem der Reifen luftdicht gefertigt ist, und auch besonders fest auf der Felge sitzt. Diese hat ein flaches Felgenbett und oft auch :term:`Humps <Hump>`, um den Reifen besser zu fixieren; zudem dichtet ein spezielles Felgenband das Felgenbett luftdicht ab. Das Ventil wird direkt in die Felge geschraubt. Meist wird zusätzlich noch eine :term:`Dichtmilch` in den Reifen gegeben, die kleine Undichtigkeiten abdichtet. Durch den Verzicht auf den Schlauch verringert sich der Rollwiderstand und der Fahrkomfort erhöht sich; zudem sind niedrigere Drücke möglich, weil kein :term:`Snakebite` auftreten kann, und kleine Löcher werden von der Dichtmilch oft verschlossen. Dafür ist die Reparatur großer Löcher aufwändiger, weil der Reifen fester auf der Felge sitzt.

    Versatile-Dach
        Ein luftiges Dach, nicht so abgeschottet wie eine :term:`Haube`, aber auch nicht so aerodynamisch.

    Wasserrutsche
        Bauteil beim :index:`Alpha7`-Velomobil, das den Tretlagermast am hinteren Ende abstützt.

    Wiegetritt
        Tretweise, bei der man aus dem Sattel aufsteht und dabei den Körper relativ zum Fahrrad hin- und herbewegt. Hat den Vorteil, dass die Tretbewegung von der Kurbelumdrehung teilweise entkoppelt ist – mit den Beinen tritt man nicht nur das Pedal nach unten, sondern auch den Körper nach oben, und kann somit einen unpassenden Gang teilweise kompensieren sowie für mehr Abwechslung beim Treten sorgen. Leider ist das auf Liegerädern nicht möglich, da man rechtwinklig zur Schwerkraft tritt, und daher diese nicht zur Zwischenspeicherung von Tret-Energie verwenden kann.

    Zugtrum
        Der Teil der Kette, der vom Ritzel zum :term:`Kettenblatt` läuft und stark gespannt ist, weil er die Antriebskraft überträgt – im Unterschied zum :term:`Leertrum`.

.. vim: ft=rst wrap tabstop=4 :
