Velomobil-Grundwissen
=====================

Beschreibung
------------
Dies ist eine FAQ über Velomobile, abgeleitet aus den Diskussionen aus dem
[Velomobil-Forum](https://velomobilforum.de/). Mitarbeit erwünscht!

*English version: [see here](README_en.md)*

**Velomobil-Grundwissen lesen**:

* **HTML:** [deutsch](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.html), [englisch](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.html)
* **PDF:** [deutsch](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.pdf), [englisch](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.pdf)
* **PDF-Druckversion:** [deutsch](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen_print.pdf), [englisch](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge_print.pdf)
* **EPUB:** [deutsch](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.epub), [englisch](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.epub)
* **EPUB (Rasterbilder):** [deutsch](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen_raster.epub), [englisch](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge_raster.epub)
* **MOBI:** [deutsch](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.mobi), [englisch](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.mobi)
* **Klartext:** [deutsch](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.txt), [englisch](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.txt)

**Modellvergleich:** [VM-Slider](https://cmoder.gitlab.io/velomobil-grundwissen/vm-slider/vm-slider.html)

Changelog
---------
* Version 2.1: viele neue Diagramme, mehr Daten im VM-Slider
* Version 2.0: englischsprachige Version, VM-Slider, neueste Sphinx-Version
* Version 1.4: Diagramme mit Matplotlib
* Version 1.3: erste Grafiken, EPUB mit CSS, Kapitel umsortiert und unterteilt
* Version 1.2: EPUB repariert, und ebenfalls Stichwortverzeichnis hinzugefügt
* Version 1.1: Glossar und Stichwortverzeichnis hinzugefügt
* Version 1.0: Inhalt weitgehend vollständig
* Version 0.9: Versionierung funktioniert, PDF-Generierung funktioniert
* Version 0.5: erste Commits

Quellcode selber kompilieren
----------------------------
Dieses Projekt erfordert [Sphinx](http://www.sphinx-doc.org/); wenn dieses
installiert ist, sollte es ausreichen, mittels *make* die Ausgabeformate zu
erzeugen:

```sh
 $ make html
 $ make latexpdf
 $ make epub
 $ make text
```

=> Das Ergebnis ist dann im Unterverzeichnis *_build*.

Seit Version 1.3 sind mehrere Sprachversionen möglich; entsprechend muss die
Sprachversion in einer Umgebungsvariablen angegeben werden:

```sh
 $ make -e LANG=de latexpdf
```

Für die volle Funktionalität ist noch einige zusätzliche Software nötig,
beispielsweisweise *LaTex* (z.B. *TeX Live*), *Python 3*, *Matplotlib*,
*librsvg2* und die Sphinx-Erweiterungen *alabaster* und *svg2pdfconverter*.
Außerdem werden auf *Gitlab* nach der Erzeugung noch diverse Nachbearbeitungen
gemacht, siehe *.gitlab-ci.yml*.

Mitarbeit
---------
* per Pull-Request (erfordert Gitlab-Account)
* im Velomobil-Forum [in diesem Thread](https://www.velomobilforum.de/forum/index.php?threads/faq-velomobil-grundwissen.57837/) (erfordert dortigen Account)
* per Mail an die Autoren

Mitwirkende und Lizenz
----------------------
Siehe im Text, Abschnitt [*Mitwirkende und Quellen*](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.html#mitwirkende-und-quellen).

Soweit nicht anders angegeben, steht alles unter der Lizenz
[*Creative Commons CC-BY*](https://creativecommons.org/licenses/by/4.0/deed.de)
– kurz gesagt: Jeder darf den Text beliebig weiterverwenden, muss aber die Namen
der Autoren nennen.
