.. only:: latex

    Velomobile Knowledge Base
    =========================

.. raw:: html

    <h1>Velomobile Knowledge Base</h1>

.. only:: epub

    .. toctree::
        :hidden:

        Velomobile Knowledge Base

Work in progress

**Translation notes:**

* In the automobile world the steering rods are attached to the hub on the steering knuckle. I have used this term in preference to "steering plate" - often in a velobobile the steering knuckle is a flat plate of metal.
* Some of the terms are the ones used by Google Translate - they have either escaped my attention or I didn't have a better idea. Please feel free to suggest better translations on the forum thread.

General Notes
=============

What is a Velomobile?
---------------------

A fully covered recumbent bicycle, typically with three wheels. The fairing provides weather protection and improves aerodynamics significantly, but also provokes nicknames such as "egg on wheels", "torpeedo" or "pill".

How much does it cost?
----------------------

The :index:`prices <price>` for new velomobiles usually range between 5000 and 10000 EUR. Of course they are cheaper :index:`second hand <second hand price>` but because of, not only low volume production, but also increasing demand velomobiles are relatively stable in value - often up to very high mileage.

.. _fig_secondhand_prices:

.. figure:: images/GebrauchtPreise2019_optim.*
    :alt: Second-hand prices 2019
    :width: 100%

    Second-hand prices for velomobiles in 2019 (without consideration of original price, configuration and condition). As one can see, the Quest, one of the most produced velomobiles (see :numref:`fig_rijderslist_models`), has been offered accordingly frequently and at various prices. In contrast, newer models like DF or Quattrovelo are used still correspondingly expensive.


How much does one :index:`weigh <weight>`?
------------------------------------------

Most velomobiles weigh between 20 and 30 kg. A few older models or those with an :index:`electric assist motor <motor>` are often well above that weight. There are also special racing designs weighing from about 16 kg.

Who makes such a thing?
-----------------------

There are a handful of :index:`manufacturers <manufacturer>` that make velomobiles, most are built by hand - most of these companies are based in the Netherlands (three of them in Dronten), but there are also manufacturers in Australia, Germany, Denmark, France and Switzerland. The principle area of distribution for velomobiles is generally speaking the Netherlands, Benelux and Germany.

Who drives one?
---------------

Frequent drivers can be seen in the :ref:`Graph of monthly driving performance <fig_rijderslist_dist>`. The velomobile is often used for daily commuting.

The main areas of distribution of velomobiles are the Netherlands and Germany. Sales data of the Dutch manufacturers (*Velomobiel.nl*, *Intercitybike*) are publicly available; see :numref:`fig_rijderslist_countries`.

.. _fig_rijderslist_countries:

.. figure:: images/rijderslist_countries_en.*
    :alt: Countries to which the Dutch velomobiles were sold
    :width: 100%

    Countries to which Dutch velomobiles (i.e. Velomobiel.nl and Intercitybike) were sold

Age distribution, according to a survey in the `Velomobile Forum <https://www.velomobilforum.de/forum/index.php?threads/velomobil-alter-hase-oder-junger-spund.47682/>`_, see :numref:`fig_age`.

.. _fig_age:

.. figure:: images/age-histogram_en.*
    :alt: age distribution of velomobile riders from a survey in the Velomobil-Forum
    :width: 100%

    Age distribution of velomobile riders from a survey in the Velomobil-Forum

Where can you buy one?
----------------------

Apart from at the manufacturers, there are also a few :index:`dealers <dealers>` spread across Europe.  The best way to see who sells which models is checking dealers listed on the manufacturer's website.

Who are the most important manufacturers and what are their models called?
--------------------------------------------------------------------------

Manufacturers and models (alphabetical, incomplete):

* `Alligt <http://www.alligt.nl/>`_: :index:`Alleweder` (A1 to A6), :index:`Sunrider`
* `Beyss Leichtfahrzeuge <http://go-one.de/>`_: :index:`Go-One` Evo (K, Ks, R)
* `Cycles JV & Fenioux <https://cyclesjv.com/>`_: :index:`Le Mans`, :index:`Mulsanne`
* `Drymer (Sinner) <https://www.drymer.nl/>`_: :index:`Hilgo`, :index:`Mango` (Plus, Sport, Tour)
* `Flevobike <https://www.flevobike.nl/>`_: :index:`Orca`
* `InterCityBike <https://www.intercitybike.nl/>`_: :index:`DF`, DF XL
* `Katanga <http://www.katanga.eu/>`_: :index:`WAW`
* `Leiba <http://www.leiba.de/>`_: :index:`Leiba` (Classic, Hybrid, Record, X-Stream, XXL)
* `Leitra <http://leitra.dk/>`_: :index:`Leitra`
* `Räderwerk <https://velomobil.eu/>`_: :index:`Milan` (GT, SL)
* `Trisled <https://trisled.com.au/>`_: :index:`Aquila 2`, :index:`Kestrel`, :index:`Minihawk`, :index:`Rotovelo`, :index:`Tomahawk`
* `Velomobiel.nl <http://www.velomobiel.nl/>`_: :index:`Quattrovelo`, :index:`Quest`, Quest XS, :index:`Strada`
* `Velomobiel.ro <https://www.velomobiel.ro/>`_: :index:`Alpha7`
* Others: :index:`After7`, :index:`Velayo`

`Interactive model comparison <https://cmoder.gitlab.io/velomobil-grundwissen/vm-slider/vm-slider.html>`_

Photo galleries:

* https://cyclesjv.com/categorie-produit/velomobiles/
* http://velomobiles.de/html/modellubersicht.html

.. _fig_rijderslist_models:

.. figure:: images/rijderslist_models_en.*
    :alt: Relative market shares of Dutch velomobiles
    :width: 100%

    Relative market shares of Dutch velomobiles (Velomobiel.nl and Intercitybike)

Legal situation in the EU?
--------------------------

From a legal point of view, a velomobile is a bicycle - therefore it does not require registration, vehicle tax/duty or insurance. An exception is an S-Pedelec velomobile, this must be approved by the manufacturer and must have insurance and a license plate, they cannot be used on bike paths.

In practice, however, some regulations for velomobiles are not applicable. Firstly, this concerns the use of cycle paths, bike paths are rarely suitable for velomobile speeds, and often have bends with too small a radii, chicanes and other bars and obstacles. So there is on the one hand the right to use, but at the same time structural limits that make it impossible to use. Then there are things like blind spots, parked cars, inattentive pedestrians and cyclists, poor surface quality... Even if a cycle path is OK to use by a velomobile, it is often much safer to use the road - especially in inner cities where you drive as fast as the cars do.

Other laws concerning lighting and obligatory equipment are not adapted to velomobile use e.g. pedal reflectors, wearing of reflective vests at night (of course put one on before you get out onto the road).

French legal obligations: https://www.securite-routiere.gouv.fr/reglementation-liee-aux-modes-de-deplacements/velo

Other EU country laws:

Bike Europe has a very complete white paper on pedelec and s-pedelec laws in the EU:

http://bike-eu.com.s3-eu-central-1.amazonaws.com/app/uploads/2015/09/rules-regulation-on-electric-cycles-in-the-european-union-may-2017.pdf

The Netherlands, vehicules with more than two wheels and wider than 75 cm:

https://wetten.overheid.nl/BWBR0004825/2020-01-01/#HoofdstukII_Paragraaf1_Artikel5

Other countries:
--- to be written ---

Are there velomobiles for more than one person?
-----------------------------------------------

At least the :index:`Leiba Cargo` can carry a passenger, and a very small person or child fits in the :index:`Quattrovelo`. However, they cannot join in and pedal. Real :index:`tandems <tandem>` exist but only as prototypes, not as a series. But the point of having a tandem is also somewhat questionable: a velomobile tandem is much heavier than a solo, and if you drive it alone also much slower. In addition, it is not cheaper than two separate velomobiles and also not more efficient. (A correspondingly highly optimised tandem velomobile is conceivable, but has not yet been built, and would only have minor advantages and significant disadvantages with much larger external dimensions and a larger :index:`turning circle`).

Which model is the best?
------------------------

It depends on the requirements you have:

* First of all, it has to fit you and you have to feel comfortable in it. For an average sized velomobile driver (approx. 1.70–1.90 m) this is much easier than for very tall or short people.
* The :index:`seating position` also differs, in some one sits more upright and in others almost lying down. So it depends on your prefered riding style.
* Some models are particularly fast because they are as light and stiff as possible. But they also tend to be more uncomfortable (less space, firmer chassis).
* Some models offer a particularly large amount of storage space for luggage or for bulky items.
* Some models have a particularly large entry opening.
* Depending on the route, models with larger :index:`widths <width>` can be impractical.
* Depending on your garage space, particularly long models can be hard to park.

So there are many criteria, you can (and should) look at the technical data and tests and reviews, but whether it really fits you or not you will only notice during a test drive. Even if your :index:`body length` fits, it may be too tight at the shoulders, for example.

How complete should a :index:`test drive` be?
---------------------------------------------

Hard to say, you can see if you fit in, if you can see out and can pedal without problems. But whether a Velomobile is really the right thing for you, you won't know after a short test drive - your :index:`muscles` have to adapt over several 100 km, and an efficient and correct driving style takes weeks to months to master. And finally can a velomobile work for you on your :index:`everyday routes`? For example  in city traffic where it hardly shows its qualities. If you are unsure you should take into account that you may have to resell the velomobile - or it maybe best to buy a cheap used one to start off with to gain your first experiences.

Body and geometry
=================

Why do Velomobiles have three wheels?
-------------------------------------

A two-wheeler without fairing is significantly more compact than an unclad tricycle, and a whole lot lighter because a wheel and the steering and possibly suspension mechanisms are omitted. This is no longer so clear with bodywork, the vehicle is wide, regardless of whether it has two or three wheels, and together with the weight of the fairing, the relative weight difference is significantly smaller. Another reason is the :index:`wind sensitivity`. A fairing means a lot of exposed side area for cross winds, and a two-wheeler is more sensitive to this because it is narrower and taller, so it offers even more side surface. In addition, you can not easily put your feet on the ground to catch a fall on a faired two-wheeler. That is why tricycles ultimately prevailed because they are not that much heavier and slower, but are more relaxed to drive, especially at high speeds and on long journeys.

Why are there usually two wheels in front and one behind?
---------------------------------------------------------

There are two reasons for this:

* First, driving stability. In a curve, the force of inertia pulls the vehicle tangentially out of the curve. With two front wheels (so-called :index:`Tadpole` arrangement), the front wheel on the outside of the curve is exactly in the direction in which the inertia is acting - the vehicle cannot tip over so easily. If there are two rear wheels (so-called :index:`delta` arrangement), there is no wheel there, so it tilts more easily.
* Second, the aerodynamics. It is easier to build a fast vehicle with the front wheels forming the widest part and close to the front or in the middle. If you had the two wheels in the back, you would have a long, unused space behind the rear wheels - while the rear wheel fills this space in a tadpole configuration.

Why are the front wheels small and the rear wheel big?
------------------------------------------------------

The front wheels are small for space reasons - if you wanted big wheels in closed :index:`wheel wells` then the vehicle would have be very tall, and since you want to steer with the front wheels, the wheel arches would need to be deep enough, which in turn increases the overall width. So you make the front wheels no higher than the interior space that the recumbent cyclist needs for pedaling.

The rear wheel, on the other hand, has enough space because behind the head of the driver the vehicle is high anyway (exception: :ref:`four-wheeler <Why a velomobile with four wheels?>`). A large bike wheel has slightly lower rolling  resistance, and secondly, it transfers pedaling force of the driver to the road over a longer distance (larger circumference)- than with a small rear wheel, there the wheel needs to turn faster for the same speed, so the :index:`chainring` should have about 100 teeth or you will need an :index:`intermediate gear box`. Small rear wheels do provide more torque for climbing steep hills.

Why is the :index:`drive wheel` usually in the rear?
----------------------------------------------------

Because the steering is in front. It would be very cumbersome to drive a steered wheel because you would have to steer the chain somehow. Driving both front wheels would require, for example, a cardan shaft and a differential, and there is no space for this because the driver sits between the wheels. It is therefore the lesser evil to run a long chain to the back wheel, the two :index:`idler pulleys <idler pulley>` under the seat only take up a few centimeters extra height.

Why a velomobile with :index:`four wheels <four-wheeler>`?
----------------------------------------------------------

A four-wheeler is a velomobile that has two rear wheels instead of one. In order not to have to make :index:`the hood` extremely wide, small wheels are used as rear wheels so that you can place them closer to the center of gravity.

Four-wheelers have a few advantages:

* The tipping stability is higher.
* There is a larger luggage compartment between the rear wheels under :index:`the hood` which can accommodate bulky items.
* The rear breaks out less easily because, firstly, two wheels are hardly ever in the air at the same time, and, for other reasons they don't both lose :index:`grip` at the same time (see :ref:`Why is it dangerous if the rear wheel slides?`) And secondly the rear wheels are closer to the center of gravity so that they carry more weight.
* If both rear wheels are driven then :index:`traction` is better, also because they carry more load. For the same reason, a single driven small rear wheel is only slightly worse than a large driven rear wheel on a tricycle.
* You have four tires of the same size, so you only have to take one spare tube and tire with you.

But there are also disadvantages:

* An additional wheel with an additional or larger swing arm means more weight.
* Two driven rear wheels are problematic when cornering if you use a rigid axle, they will wash out in curves. You can solve this with a :index:`differential`, but this in turn means more weight and complexity and causes additional drive losses.
* If you only drive one rear wheel, the traction is less than with a tricycle.
* A small drive wheel needs a higher gear ratio, an intermediate gearbox may be necessary, which in turn means more weight.
* The chain runs in the middle, but the drive wheel is on the side so you need a heavy, torsionally stiff axle.
* Small wheels have higher :term:rolling resistance.


Open or closed :index:`wheel wells`?
------------------------------------

Both have advantages and disadvantages:

* Closed wheel wells are slightly better aerodynamically.
* With closed wheel wells it doesn't matter aerodynamically how far inside the wheel sits; with open wheel wells, it must be as flush as possible on the outside, i.e. the adjustment is more flexible.
* Closed wheel wells are often slightly larger, so wider tires can be used (these are also correspondingly taller). With open wheel wells, on the other hand, you try not to make them as large because the gap between the wheel and :index:`bodywork` disturbs :index:`airflow`.
* Velomobiles with closed wheel wells are not necessarily wider, which means that they have a smaller track width and are therefore somewhat more sensitive to tipping.
* And of course, with open wheel wells, removing and installing the wheels and adjusting the brakes is much easier. However, there are now velomobiles with closed wheel wells, where you can access the axles from outside via maintenance hatches; there at least the removal and installation of the wheels is similarly simple.


One-sided or two-sided :index:`swingarm`?
-----------------------------------------

* A double-sided swing arm is the classic design, normal rear hubs fit. Special :index:`hubs <hub>` are required for one-sided swingarms.
* Wheel :index:`hub motors <hub motor>` are only available with two-sided mounting (except for one direct drive model from Canada). With one-sided swingarms you can only easily install a :index:`bottom bracket motor <mid-drive>`.
* In the case of a double-sided swing arm, the forces act on the center, this makes :index:`torsion` significantly lower. A one-sided swingarm must be built much more solid to be similarly torsionally rigid.
* With a one-sided swingarm, you can change the tire without having to remove the wheel.
* With a one-sided swingarm there is more space for on the left side of the wheel arch for baggage. There is less space on the right-hand side because the swing arm is much more voluminous, however, the space for luggage would hardly be usable anyway, because the oily chain is there.
* In the case of a one-sided swingarm, there is only one hole in the wheel well on the right side through which dirt can penetrate into the interior. The left side remains completely closed and clean.
* With a one-sided swing arm it is possible to remove the wheel without touching the sprocket and chain. With a double-sided swing arm, it will be more difficult to disassemble the hub because there is not much space on the sides.


Why build velomobiles from :index:`carbon fibre` or :index:`fiberglass`?
------------------------------------------------------------------------

Fiber-reinforced plastic can withstand roughly the same amount of force as steel - but is significantly lighter. However, the fibers must always be layed in the pulling direction. So you always need parts with relatively large diameters and smooth transitions so that the forces that occur can always be absorbed somewhere by fibers under tension. This is not the case with metal; this can withstand the same amount of force in all directions and can also be subjected to pressure.

A thin diamond frame with highly stressed parts can therefore be built from steel, carbon offers few advantages, unless you build a much more voluminous frame. A velomobile, on the other hand, deals with large areas that are lightly loaded. That would be much too heavy to build from sheet steel - but with carbon you can build a thin, load-bearing shell, which is neverthelessis :index:`stiff` enough to transmit driving forces. So you can do without an additional supporting inner frame, the components combine both aerodynamic and load-bearing functions.

Why curved surfaces?
--------------------

Because of the :index:`rigidity`. This depends, among other things, on the thickness of the material. When you bend the material, it is stretched on the outside and compressed on the inside. The farther apart the inside and outside are, the greater the stretch/compression can be and the stiffer the material.

This is the reason why heavily loaded components need to be built quite voluminously - a tube twice as large is twice as thick, so it needs twice as much material and is twice as heavy. However, since the inside and outside of the bend are twice as far apart, it is four times as stiff, or can be constructed with correspondingly thin walls with the same stiffness and is lighter overall.

This does not only apply to closed forms such as tubrs, but also to open surfaces. Bending a material in one direction increases the thickness for a bend perpendicular to it. A 1 mm thick material that is curved down 1 cm is therefore as stiff in the other direction as a material that is 1 cm thick. This is exactly what makes corrugated iron or corrugated cardboard so rigid compared to the flat material. Or: If you fold a piece of pizza in the transverse direction, it no longer hangs down in the longitudinal direction, but becomes stiffer there.

And that's the reason why the curved surfaces of the :index:`body` of a velomobile can make a significant contribution to rigidity; for example, the chain channel is a tightly curved shape in the transverse direction, and therefore it is incredibly stiff in the longitudinal direction and can withstand the chain forces pulling in that direction.

Why do many velomobiles have no :index:`foot holes`?
----------------------------------------------------

* Because they are aerodynamically disadvantageous.
* Because they too impair :index:`torsional rigidity`, in such a velomobile either more material is needed to acheive the same rigidity, or power transmission is less efficient.
* Because dirt, water and winter snow can penetrate from below .


Glass dome or motorcycle :index:`visor`?
----------------------------------------

Admittedly, a rolling egg with a glass dome looks very chic, and from inside you have a much nicer panoramic view than from inside a dark, closed velomobile with small window openings in the :index:`hood`. But in the rain, cold and dark it has massive disadvantages, because the raindrops worsen the view, especially with oncoming headlights, and such a window also mists up. You have all that with a motorcycle visor too of course, but the area you have to keep clear is much smaller. You can also easily replace a visor if it is scratched after wiping it too much. A visor is also relatively vertical compared to looking through a glass dome at a much flatter angle. These problems can be solved in cars, namely with a powerful fan and thousands of watts of engine waste heat and large :index:`windshield wipers`. Ultimately, such a velomobile is more of a fair weather vehicle. But it shouldn't be too sunny either, because otherwise it heats up under the glass dome like in a greenhouse.

Do you see enough with a :index:`hood` and their tiny windows?
--------------------------------------------------------------

Actually you do.

Of course, you have a much smaller field of vision than if you were driving in convertible mode, overhead the view is of course completely restricted. But you can see enough to the front and to the sides - about as much as in a car. After all, you sit a lot closer to the windows so that they offer the same angle of view, even though they are smaller. And 95% of the time you just have to look through the front visor to see traffic, only at intersections do you need to look over your shoulders to the side. With rear view mirrors to look backwards you can see the traffic behind you.

Steering
========

:index:`Tank steering` or :index:`Tiller`?
------------------------------------------

This is largely a matter of taste; but both mechanisms have advantages and disadvantages:
**

* With the :term:`Tiller` , the position of the hands is flexible; you can steer no matter where you hold the handlebar.
* The position of the :term:`tank steering` lever is fixed, some prefer this because it allows them more precise and quicker control, which is particularly interesting in races.
* Since tank steering is often very sensitive, tiny finger movements are sufficient. However, the arms need to be supported for this. :index:`Armrests` must be fitted so that every bump does not lead to an involuntary steering movement.
* With a Tiller mechanically coupled :index:`brake levers` are easy to implement - the brake levers are next to each other anyway and only need to be connected. With tank steering you can only install hydraulic brakes and couple the hydraulic hoses, and then you would need a fallback so that a hydraulic leak does not lead to the failure of both brakes.
* For this reason, good-natured steering is necessary for tank steering, i.e. without or with negative scrub radius. With a Tiller, you either have coupled brake levers anyway, or at least can operate the individual brake levers together, so you have much more predictable behavior.
* With a Tiller the arms are higher up, which means that in hot weather you can hang your arms out of the cockpit in a :index:`double manta position` and still steer with them. This is completely impossible with tank steering, because at least one hand must be down on the steering lever and you would have to sit wrongly to bring the other hand up.
* With tank steering, the arms must be next to the body, this space cannot be used for luggage. With a tiller, the elbows are much higher.
* When it comes to tank steering, the braking and shift cables are shorter and thus there is less friction.
* With tank steering, the upper body is better ventilated because the hands are not in front of it.
* A Tiller is mechanically more complicated, with a universal joint on the steering bridge and a split tie rod.
* The tank steering tie rod is not for steering, the steering levers are directly connected to the steering knuckles so the tie rod only couples the two wheels.
* With tank steering, holes are required at the bottom of the steering lever through which water can penetrate when driving through a deep puddle.


Why is there no rear steering?
------------------------------

:index:`Rear wheel steering` is more difficult to control. When cornering the centrifugal force pulls the vehicle to the outside of the curve. Front wheel steering steers the front wheels towards the inside of the bend, i.e. both forces act in opposite directions, the vehicle must be actively steered into the bend. With rear wheel steering it is the other way around, to go into the curve, it has to swing the tail outwards, and as the centrifugal force increases and it is drawn outwards - it tends to oversteer. Countermeasures are therefore needed to retain control at high speeds. Example: The *kleine Schwarze* by Harald Winkler has a counterweight that is pulled outwards by centrifugal force in the curve and straightens the steering.

What about the :index:`scrub radius`?
-------------------------------------

In the case of a :index:`single-tracker`, the front wheel is located in the longitudinal axis of the bicycle and the steering bearing above the front wheel, i.e. the steering axis goes through the wheel. This is not necessarily the case with a velomobile: since the front wheels are suspended on one side, and it is not like a steering bearing at all, the steering axis does not necessarily go through the front wheel, but hits the ground next to it. So if you turn the steering, the front wheel does not turn on the spot, but rolls on a circular path around the intersection between the steering axis and the road. And the radius of this circular path is the scrub radius. Incidentally, this can be positive or negative:

* If the steering axis runs next to the front wheel, i.e. points to a point inside the front wheel, then one speaks of a positive scrub radius.
* If the steering axle is at an angle so that its extension goes through the front wheel and hits the ground on the outside, this is called a negative scrub radius.

The scrub radius is important when braking: when a front wheel brakes, it pulls the shock absorber backwards - not directly, but tangentially via a lever, the length of which is the scrub radius. This lever creates torque on the shock absorber. If the scrub radius is positive, the wheel pulls the shock absorber on the outside backwards - this steers the wheel outwards, i.e. the velomobile moves in the direction in which the brakes are applied. If the steering wheel radius is negative, it is the other way round, the front wheel steers inwards, the velomobile drives in the direction in which the brakes are not applied.

Now which is better? Ideally, the velomobile behaves neutrally and does not steer when braking. However, one-sided braking always ensures a change of direction even without a scrub radius - the braked wheel covers a shorter distance, so it is the inside of the curve. Therefore, one tends to have a slightly negative scrub radius, which has the opposite effect, and thus compensates for the behavior mentioned.

Where is the steering axis with a MacPherson strut?
---------------------------------------------------

It is not so easy to say. At first glance, the suspension strut rotates around itself when steering. But this only applies to the upper end: the suspension strut is attached to the wheel well, and rotates around this attachment point. On the other hand, there is no fixed pivot at the bottom.

The suspension strut is held at the bottom by trailing arms and wishbones (see: What are the names of all the parts on the chassis?), And this results in a virtual pivot point. However, this is not fixed, but changes somewhat with the steering angle; it is roughly where the extension of the trailing arms and wishbones would cross. And so it is also possible that the pivot point is somewhere between the spokes or even outside next to the wheel, although the shock absorber and the steering knuckle are completely inside next to it.

You can have a negative scrub radius by making the angle between the trailing arm and wishbone smaller. Here's an example:

* The trailing arm is not attached to the front of the wheel well so that it would be approximately parallel to the longitudinal axis of the vehicle, but inside the wheel well so that it runs obliquely outwards.
* The steering knuckle is extended to the front and the trailing arm shortened so that it hits the steering knuckle less parallel.


What is caster and what is its function?
----------------------------------------

With a conventional bicycle headset, bearings are usually not vertical. This so-called steering head angle is usually around 70 °, i.e. the head tube points 20 ° forward. The tracking point is accordingly in front of the front wheel. This results in self straightening torque - the front wheel is pulled behind the steering axle, which means that the steering adjusts itself automatically. The larger the caster, i.e. the distance between tracking point and wheel contact patch, the more pronounced is this behavior.

This behavior has an important function for a single-tracker: If the bicycle tilts to the side (i.e. the center of gravity moves to the side) while driving slowly, it automatically steers in this direction, i.e. the wheel moves under the center of gravity again and thus counteracts the tipping. Cornering and inclination are thus inseparable - with a lean you can trigger cornering (e.g. when driving or pushing hands-free), and due to the unstable position of the center of gravity (the steering head bearing is the highest above the ground), a steering lock also leads to opposite tilting.

With multiple track is it is fundamentally different, the steering and inclination are completely decoupled there - cornering does not push the center of gravity back into the center, but it always stays the same, so it does not trigger turning. The caster is therefore ineffective when driving slowly. When cornering quickly, there is at least the centrifugal force to which the caster reacts, the caster simply counteracts any fast cornering. A velomobile can lean very well - namely if it tips over when cornering at high speed. Then the wheel on the inside of the curve lifts and the velomobile becomes a single-tracker - but only when it is tilted so far that the center of gravity is beyond the wheel on the outside of the curve does the weight also contribute to counter-steering.

Thus caster in the velomobile could at least improve straight-ahead running at high speeds. But firstly, the space in the wheel well is limited, you can only slightly tilt the shock absorber. Secondly, the steering on a tadpole multi-track cannot turn as freely as on a single-track, because the entire steering linkage (i.e. steering lever or tiller) still has to be moved, which would require more power or would require more caster.

The caster is also relevant for external forces. For example, it is the reason why you can push a two-wheeler on the saddle and steer it by tipping it sideways. But wind is also a side force, especially with the velomobile with its large lateral contact surface. When the latter pushes the velomobile to the side, the caster ensures that the wheels turn in accordingly, that improves straight-line running but it increases wind sensitivity.

What about the Ackermann Condition?
-----------------------------------

When driving straight ahead, all wheels of a multi-lane vehicle are parallel. It's different in a curve, because not all wheels are the same distance from the center of the curve, they have to drive on different narrow circular arcs, the inside wheel must be turned more than the outside wheel. This is done by a so-called steering trapeze - the tie rod is shorter than the distance between the pivot points of the front wheels. Ultimately, the extensions of all axis must intersect at one point when cornering. If this is not the case, the wheels scrub on the road.

This is not a big problem with a velomobiles, as tight bends only make up a negligible part of the total distance.

.. _fig_ackermann-bedingung:

.. figure:: images/Ackermann_radius_M_nebeneinander.*
    :alt: Ackermann condition
    :width: 100%

    Ackermann condition: In a curve, all wheels steer around a common center. (`Quelle <https://commons.wikimedia.org/wiki/File:Ackermann_radius_M.svg>`_)

Chassis
=======

Why are the front wheels at an angle? Doesn't that slow you down?
-----------------------------------------------------------------

Of course it does. Imagine that a wheel is extremely sloped, almost horizontal - it would almost no longer roll, but would rub against the tire flanks tangentially to the rim.

However, smooth running is not the only goal. Another is to achieve the largest possible track width - this means that the velomobile does not tip over so easily and you can drive through curves faster. A wide velomobile would have a larger cross-sectional area. While the space in the middle is needed to accommodate the driver's legs as well as the shock absorber, drum brake and hub, significantly less space is needed below and above. If the wheels now provide negative camber, with the same hub spacing, you get a larger track width and a smaller width at the top, i.e. the Velomobile is narrower at the top (= less air resistance) and wider at the bottom (= higher cornering speed), and you can get this with a little more rolling resistance. For this reason, in most velomobiles, the axles of the front wheels are not perpendicular to the struts, but at an angle of 86°, that tilts the wheels closer to the strut at the top. However, the negative camber does not have to be 4°, and the struts themselves do not have to be vertical. Their angle depends on the setting of the tie rod, trailing arm and wishbone.

What are the names of the chassis parts?
----------------------------------------

How this looks in reality can be seen in the next picture.

.. figure:: images/Lenkplatte_Querlenker_Laengslenker_Spurstange.*
    :alt: lefdt front wheel, from above
    :width: 100%

    Sketch of the left front wheel (top view)

What it looks like in reality: ref: `can be seen on the next picture <fig_mango_steering>`.

* The velomobile is shown in gray on the sketch, the wheel in black. Direction of travel: to the right.
* The trailing arm is shown in green; it is usually screwed to the front of the wheel well. Together with the wishbone, it guides the steering knuckle. The dashed line shows how it can move (the steering knuckle and wishbone must follow the movement).
* The wishbone is shown in blue, it is typically attached in the middle of the vehicle under the steering bridge. The radius of movement also shown as a dashed line.
* The tie rod is shown in red, it connects the steering knuckles of both front wheels, and the tiller may be attached to it. The track is adjusted by changing the length of the tie rod, see How do you find out if the track is set correctly?
* The steering knuckle is shown in orange, it is located at the lower end of the shock absorber, and the three steering linkages above are attached to it on ball ends. Since their distance is constant, the rotation of the steering knuckle is determined by the fact that the attachment points of the trailing and transverse links can only move along the dashed lines. See: Where is the steering axis on a MacPherson strut?

With tank steering, the rods leading to the tank steering levers are attached to the rear of the steering knuckle.

.. _fig_mango_steering:

.. figure:: images/Lenkgestaenge-Mango.jpg
    :alt: Mango steering linkage
    :width: 100%

    Steering linkage on a Mango (view from the front on the underside, parts removed and placed on the outside of the body in the appropriate places). From front to rear: trailing arm, wishbone, tie rod (in two parts and connected to Tiller)

Toe-in, toe-out - which is correct?
-----------------------------------

Absolutely none. The wheels should be completely parallel. Anything else increases rolling resistance and above all tire wear.

Cars sometimes use a slight toe-in or toe-out; You can afford that because the tires are less sensitive and the engine has enough power. The advantages:

* Steering play: A worn steering with play can flutter. A slight toe-in or toe-out forces the steering into a fixed position.
* Turn in: In the curve, the weight shifts to the outer wheels. A toe-in on the front wheel supports the steering accordingly, because the loaded outer wheel then turns a little in the straight-ahead position, and the opposite influence of the inner wheel is reduced because it is relieved.
* The situation is reversed on the rear wheel, here a toe-in supports the steering. However, this then leads to oversteer - when cornering, weight is shifted to the outer wheel, this steers further outwards through the toe-in, which increases the steering movement and brings even more weight to the outer wheel.
* The driving force has an effect on the track, For example, a slight toe-in for driven rear wheels or a slight toe-in for driven front wheels can ensure that the wheels stay parallel under load.


How do you know if tracking is set correctly?
---------------------------------------------

If the front tires after a few 100 km are worn out, the track was set incorrectly.

But the track can be measured relatively easily. Two approaches have proven their worth:

* Measuring the position of the wheels: There are various methods, for example, place two slats on the inside of the wheels and measure their distance at the front and rear, or attach a laser pointer to the hubs. It is advisable to sit in the velomobile during the measurement and have it read by a second person so that the chassis is loaded just as it is while driving. The wheels must be as parallel as possible.
* Roll test: You roll down with the Velomobile and different settings of the tie rod and measure how far you can get. It is important here that the starting point is always identical and the surface is as smooth as possible (e.g. without potholes) and straight (without curves). The route does not have to be long, for example, you can roll off a ramp that is only a few centimeters high in an underground garage or cellar.

In a roll test, the distances of the best settings are quite close, the worse the track is set, the more the rolled distances deviate from each other.

What is “Bump Steer”?
---------------------

The suspension reacts to uneven surfaces, i.e. the front wheel moves up and down relative to the vehicle, and with it the wishbone. This is attached to the vehicle on the other side, i.e. the end of the wishbone attached to the front wheel scribes a circular path. If it is not nearly horizontal and/or is not very long, then this movement also has a noteworthy lateral component - i.e. the front wheel is pulled closer to the vehicle or pushed away. And there the control arm does not necessarily run in the direction of the virtual pivot point, which leads to a slight steering movement.

Ideally however, the wishbone is as long as possible and is as horizontal as possible in the loaded condition. In addition, the tie rod is ideally divided so that the parts are as long as the wishbones and are as horizontal as possible. A steering movement can thus be prevented during compression and rebound.

How should the suspension be adjusted?
--------------------------------------

* When the wheel wells are open, the shock absorber should be inclined so that the wheel is flush with the edge of the wheel arch.
* The scrub radius should be slightly negative (see: What is the scub radius?).
* The tracking should be neutral, i.e. the wheels are parallel (apart from the camber; see Toe-in, toe-out - which is correct?).
* The wishbones are as long as possible and as horizontal as possible when the vehicle is loaded (see: What is "Bump Steer"? ).
* The Ackermann condition is fulfilled (see: What is the Ackermann condition? ).
* The steering is ideally set in such a way that it is insensitive near the neutral position, because even small steering movements have a big impact at high speeds. In contrast, the steering can react more in the event of full steering locks, since these only occur when driving slowly.

Wheels and Tires
================

Which wheel size?
-----------------

The front wheels are simple: 406 mm (according to ETRTO corresponds to 20 inches). All other sizes of small wheels are much more unusual, and the choice of tires and rims is correspondingly small. Only 16 inches (349 mm and 305 mm) are still reasonably common, but would not bring any real advantages. A few hobbyists have built smaller wheels for their 20-inch velomobiles in order to fit wider tires (e.g. winter tires to accommodate spikes), but this is only necessary if the space in the wheel well is limited.

With the rear wheel you have more choice, with many velomobiles possible sizes are:

* 559 mm (26 inch, mountain bike)
* 571 mm (27 inches, triathlon)
* 584 mm (27.5 inches)
* 622 mm (28 inch, racing bike)

The influence on development is rather small and can be matched by using suitable chainrings. Therefore, the most important criterion is the size of the best tires. The fastest tires are basically for racing bikes (i.e. 622 mm), but they are quite narrow (and wide ones also often do not fit in the wheel well). For mountain bikes there are also a lot of tires, but almost only wide and with studs. Narrow, fast tires in 559 mm are less and less common, so the best compromise (fast, not too narrow tires) could be 584 mm.

Why use such wide rims in a velomobile?
---------------------------------------

The rim width depends on the tire width:

* Too narrow: the tire does not hold properly on the rim because it is not vertical to the rim flange (i.e. orthogonal to the pressure direction), but spreads outwards. In addition, the tread is far from the rim, i.e. side forces pull a larger lever force on the rim flange.
* Too wide: the tread is close to the rim, which means that the tire cannot deflect sideways very much and will run faster. If the tube is squeezed, it gets the classic snakebite holes.

Snakebites are not a big problem in the Velomobile because the weight is distributed over an additional wheel. In addition, you typically drive on smooth roads and not over curbs and stones. On the other hand, lateral forces in curves are a big problem - since you don't lean in the curve, the forces do not always only act perpendicular to the tread, but often also from the side. Therefore one tends to use wide rims. With open wheel wells, they also have the advantage that the tire does not protrude as far over the rim, i.e. the profile is smoother and therefore more aerodynamic.

What tire width?
----------------

The tire width is limited by the size of the wheel wells and even if a wide tire would basically fit in on the front wheels, that often reduces steering angle, i.e. the circumference becomes significantly larger.

Otherwise, the weight is distributed over at least three tires, i.e. a single wheel has to withstand less load than with a conventional bicycle. So there is nothing to be said against very narrow tires, which are more prone to snakebites. But since the tires run inside the wheel wells, a narrow tire is not necessary good for aerodynamic reasons.

Ultimately, you want tires that are as fast as possible; and that seems to be the best with a width of approx. 28 mm. But that also depends on the condition of the road - on bad roads, wide tires that have higher rolling resistance than the narrow racing tires, can still be faster because there are fewer vibrations.

What about Barlow's formula?
----------------------------

It's actually quite simple: pressure is force per surface, i.e. if the inner surface of the tire is larger, a higher force acts on the tire at the same pressure.

If a tire is twice as wide, then it is approximately twice as high, i.e. the circumference of the tire cross-section is also twice as large. Since the circumference of the wheel is almost the same, the inside of the tire has twice the area - so double the tensile force acts on the carcass of the tire.

That is why wide tires can withstand much less pressure than narrow tires. But wide tires also need less pressure because, firstly, the tire contact patch is wider - therefore, with the same load and pressure, the contact patch is shorter, i.e. you need less pressure so that the tire contact patch is as short as a narrow tire. Secondly, a wide tire is taller, i.e. it has a lot more travel until there is a puncture ("snakebite").

Of course, you could build a wide tire correspondingly more solid so that it can withstand more pressure. But this also makes it stiffer, i.e. the rolling resistance increases - which you actually want to reduce with higher pressure. But high pressure does not only make the tire, but also the rim load greater. Lightweight rims in particular can only withstand a certain pressure that is specified by the manufacturer, so if you fit a wider tire, you have to consider not only its maximum pressure, but also that of the rim (which is always related to a specific tire width) - corrected using the Barlow formula. If the tire is twice as wide, you can only load the rim with half of its maximum pressure.

Suspension
==========

Do you need suspension?
-----------------------

Normally yes. The main characteristic of a velomobile is its speed. And high speed means that driving over an obstacle and the resulting change in speed happens in a shorter time, i.e. the acceleration is higher. And acceleration times mass is power, the former is constant, i.e. the vehicle and driver have to endure stronger forces at higher acceleration. In addition, at high speeds there is less time to correct a change of direction due to an obstacle. Therefore, it makes sense to first reduce the force peaks that affect the vehicle and driver with the help of suspension, and also to make the vehicle more controllable.

Another effect is added to the front wheels: since the wheels are side by side, but rarely the bumps, the vehicle tilts around the longitudinal axis. The driver is not only accelerated up and down, but also thrown from side to side, which can be very uncomfortable.

For the rear wheel, suspension is important for another reason: if an undamped rear wheel encounters bumps with a suitable frequency, it can jump so much that the vehicle overturns sideways (see: Why is it dangerous if the rear wheel loses its lateral grip? ),

Does suspension not cost valuable energy?
-----------------------------------------

Yes of course. Or rather: not the suspension, but the associated damping, which is nothing more than energy destruction.

But what else would happen to the energy? The driver has laboriously built up momentum; and uneven surfaces ensure that a small amount of kinetic energy is directed in a different direction - for example from forward to upward, the vehicle is braked and raised. But only rarely does a suitable bump come afterwards, which accelerates the velomobile coming down again like on a small ramp. So most of this energy is lost anyway, and so it is preferable to destroy it in the damping element instead of stressing other parts of the vehicle. A suspension costs weight, but an unsprung, fast vehicle has to be built a lot more solid in order to withstand the higher force peaks. In addition, the driver (who ultimately makes up the largest part of the total mass) is also accelerated, and does not give the energy back 1:1 via the vehicle to the road, it is lost through muscles and tendons. And this is not only uncomfortable, but also puts a strain on the muscles - that is, it costs strength that should be invested in propulsion.

And then there is the fear that suspension reacts with pedaling frequency thus eating up part of the pedaling force. Of course, this can happen in principle, but is negligible in a well-tuned vehicle. Firstly, the drive can be designed so that the chain lift is almost perpendicular to the direction of suspension, and the chain almost goes through the pivot point of the swing arm joint, so that the force component acting on the suspension is very small. This cannot be completely prevented, because the chain line changes slightly due to the sprocket used or the load, and the leverage ratios are different in each gear. Secondly, the driver is very much fixed in a recumbent bike and the center of gravity does not change as much as when rocking on a tradtional saddle. Correspondingly fewer accelerations occur, which are then eaten up by the damping.

What does good suspension have to do?
-------------------------------------

Travel: You want to go fast with a velomobile and have little ground clearance. Accordingly, you drive mainly on the road, where there are no huge obstacles to overcome, but at worst potholes. Accordingly, you don't need a lot of travel. A large suspension travel would even be disadvantageous because, for example, the wheel wells would have to be correspondingly more voluminous, which ultimately worsens the external dimensions and the aerodynamics. In addition, the negative spring travel must be larger, i.e. the vehicle should be higher, which increases the tendency to tip over in bends.

Responsiveness: At high speeds, the accelerations are high, so good suspension must respond very quickly and even with small forces. This affects above all damping. In many damping mechanisms sliding friction occurs, which is unfavorable, because static friction must first be overcome before sliding friction. Therefore, friction dampers or poorly constructed piston mechanisms with oil dampers are unfavorable, while spring-damper systems that are based on bending (e.g. leaf springs), torsion (e.g. torsion element in the joint) or compression (e.g. elastomers) do not have this problem. (they do have other disadvantages, such as temperature dependency, or lack of adjustability of the suspension/damping characteristics.)

Why is it dangerous when the rear wheel loses lateral grip?
-----------------------------------------------------------

It's a combination of several things:

* The front wheels do not easily lose cornering grip, because they are rarely in the air at the same time and rarely is one suddenly in the air. It's different with the rear wheel, if it jumps over bumps undamped or if the tire bursts, the cornering is suddenly sharply reduced.
* Since the center of gravity is much closer to the front wheels than to the rear wheel, there is little weight on the rear wheel. Correspondingly, the cornering force is normally significantly lower than that of the front wheels, and it is therefore easily critical if it is reduced again.
* When turning, not only the load plays a role, but also the position relative to the center of gravity: the front wheels are almost next to the center of gravity - they roll tangentially to a rotation about the vertical axis and there is practically no resistance. The rear wheel, however, rolls radially, it opposes the tangential movement with  high resistance.
* With a velomobile it's steering angle is quite small, because it is limited by the width of the wheel wells. So if the rear wheel loses lateral grip and breaks out to the side, there are only limited countermeasures available.
* You often drive fast with a velomobile. The reaction time is correspondingly short in order to be able to react to a rear wheel breaking out.

If for some reason the rear wheel no longer has cornering power - regardless of whether it jumps or blocks, or a burst tire no longer sits on the rim - and there is also lateral force - because you are driving through a curve or  it gets a side blow from a pothole - then it breaks out. And if the velomobile turns faster than you can counteract it, it can turn sideways and roll over. This requires surprisingly little, one example is a velomobile with an unsprung rear wheel that hits the vibrating strip in a straight line with the rear wheel and is tipped over in just a few seconds at full speed.

What can you do about it?

* Luggage to the rear: firstly increase the load on the rear wheel and secondly moves the center of gravity further away from the front wheels.
* Check tires: tires rarely burst unannounced, there is usually visible pre-damage.
* Use reliable tires with a sticky rubber compound.
* Tubeless rim: Makes it more difficult for the tire to come off if it blows out.
* Wide rim: Makes the tire less tall, which means that as long as it doesn't bounce off, it bounces less.

.. _fig_schwerpunkt_spurfuehrung:

.. figure:: images/schwerpunkt_spurfuehrung.*
    :alt: Direction of the forces applied to the wheels
    :width: 100%

    Direction of the forces on the wheels as the velomobile tilts around its vertical axis, with different centers of gravity.
    Since the rear wheel is located behind the center of gravity, the forces are always applied perpendicularly to it when the velomobile tilts, tracking is thus good.
    Green: If the center of gravity is further back (here: 70% of the distance between the front and rear wheels), then there is a lot of load on the rear wheel (here: 70%), and in addition, the forces on the front wheels are almost perpendicular to them.
    Red: If the center of gravity is further forward (here: 10% of the distance), then the forces are almost parallel to the front wheels - so they can't provide much lateral support. The rear wheel can, but only 10% of the load is on it.
    In reality, the centre of gravity is usually located between these extremes, somewhere near the orange point; i.e. about one third of the distance between the front wheels and the rear wheel, and accordingly load distribution on the wheels is relatively balanced.

Gears
=====

What role do gears have to fulfill?
-----------------------------------

The challenge with velomobiles is that on the one hand you drive at a very constant speed - the gearing there must be very finely graded - but on the other hand you cover a much wider speed range than with all other bicycles, and you have a heavy vehicle which, uphill, must still be pedalled at a reasonable cadence.

A fine gradation could also be achieved with a half-step gradation of the chainrings. However, it is very impractical to need two gear changes for each gear at high speeds, instead of simply shifting on a pinion.

Is a hub gear possible?
-----------------------

Rather not - hardly any IGH has a sufficiently large range of at least 500%. Only the Rohloff Speedhub and the Pinion gearboxes are in that area. In addition, IGHs inherently have constant or symmetrical gear changes, a flexible gradation is only possible with derailleur gears.

But what is possible and popular: combinations of derailleur and IGH. First, there are combined derailleur and hub gears, here the hub gear extends the gear ratio range, and also enables gear changs to be made quickly at traffic lights, for example, and the combination of both gearshifts to mitigate rough gear changes through intermediate gears. Unfortunately, such a hub requires a two-sided swing arm. Second, there is the Schlumpf Mountain Drive, although these only offer two gears and are less quick to shift with the heel than with a gear lever, they offer an enormous gear jump, with which you can significantly expand the range of the derailleur and do without the second chainring. Third, you can with a velomobiles install an additional gearbox in the drive train, which can also be a hub gear. The disadvantage of all IGH systems is the high weight.

Mountain bike or road bike cassette?
------------------------------------

Neither nor. You cannot buy off-the-shelf an optimal cassette. Cassettes are no longer made from individual sprockets, but are riveted together in groups. And for racing cyclists there are finely graduated cassettes with unfortunately too little gear range and for mountain bikers there is enough range, but incorrectly spaced.

Therefore, some velomobile drivers facing this problem assemble the cassette themselves - either from a mountain bike cassette, which you supplement with individual sprockets or entire sprocket groups from other cassettes, or from special sets of individual sprockets. However, this rarely works without tinkering; for example, spacers have to be adjusted or protruding parts have to be ground off. For optimal gear range, it is often even necessary to drill out the rivets from riveted cassettes and rivet them back together with new sprockets. Since the pinion wear on a velomobile is low thanks to little dirt, such a cassette will last for many years.

Why can't you change through all the gears?
-------------------------------------------

Even a mountain bike derailleur with a long chain tensioner has a capacity of only 40 teeth, if the chainrings and sprockets have a larger tooth difference (i.e. teeth of the largest chainring + the largest sprocket minus the teeth of the smallest chainring + the smallest sprocket), then the chain can no longer be tensioned and sags. And an extended chain tensioner on the rear derailleur is rarely possible because the space within the bodywork is too small. In practice, this means that only the largest sprockets can be used on the small chainring. But you only lose "double" gears, and since these are mostly the small pinions, which are ideally spaced anyway, it doesn't matter.

Which chainring or cassette sizes work?
---------------------------------------

The smallest sprockets are freewheel body specific, smaller than 11 teeth does not fit on a normal Shimano freewheel body. If you have a large rear wheel, you need a chainring with approx. 60 teeth in order to be able to comfortably pedal up to approx. 60 km / h. And you want to be able to pedal uphill at 5 km / h; that calls for a gear ratio of approx. 1:1. Because with well spaced cassettes the sprocket sizes only go up to approx. 40 teeth, and with a bolt circle of 130 mm the smallest chainring must have 38 teeth, there is much to be said for compact cranksets, i.e. with 110 mm bolt circle, where you can also fit smaller chainrings. The huge jump that occurs between the small and large chainring is customary, front derailleurs are actually still changeable if they are carefully aligned (i.e. both the radial distance and the angle).

Cassette spacing?
-----------------

Most of the time you drive on the flat, at speeds between 40 and 50 km/h. Accordingly, the sprockets should be very finely spaced - a tooth difference is a much larger percentage difference with small sprocket sizes than with large sprockets, and at high speeds and thus pedaling power, this also corresponds to a big leap in performance that has to be mastered. You should ignore speeds higher than approx. 65 km / h. They occur relatively often, but only downhill. No normal person can pedal at this speed for a long time on the flat. And such speeds are usually over after a few seconds, you don't need an extra gear for that, you just let it roll a bit. Steep mountain climbs are not more common than steep descents - but it doesn't take seconds, but often many minutes to climb. A suitable gear is worth it here much sooner. So the bottom line for the cassette is: the small sprockets finely spaced, towards the large sprockets always wider spacing.

Big or small chainring/pinion?
------------------------------

At the same speed and cadence:

* With a small chainring and small sprocket, the chain speed is low, but the chain tension high. This means: high losses on the traction run (with chain deflection), and a strong tensile load/deformation of the drive.
* With a large chainring and large sprocket, the chain tension is low and the chain speed is high. This means: higher losses where there is little traction, for example on the empty run .

In total, it is therefore advisable to drive on large chainrings - even if it feels as if the idle is less with smaller chainrings. That's true, but it's the other way around under load.

Small chainring or large largest sprocket?
-------------------------------------------------

This is actually a matter of taste, the latter, however, has the attraction that you can then drive a lot with the large chainring and rarely need the front derailleur. The efficiency is also somewhat higher there. However, getting a well spaced cassette is more difficult if the largest sprocket is to be particularly large (over 34 teeth).

Drivetrain
==========

How big are the losses from chain idlers?
-----------------------------------------

The following things happen on an idler:

* The chain links are angled. The smaller the idler pulley, the stronger it needs to be - because the chain links are distributed over fewer teeth.
* The idler loads the bearing. If, for example, the chain is deflected by 90 °, it presses against the idler pulley at a 45 ° angle.

The forces that occur are not negligible: with a deflection of, for example, 90 °, a force of sine(45°) = approx. 70% of the chain pull acting on the idler. Crank and chainring size can easily double the pedal force. That can then correspond to a weight of 100 kg.

This is not so bad when the bearing is loaded: a ball bearing has very little friction, i.e. the losses are low despite the high force. It is different with the force on the chain links. The deflection per chain link is much less, the 90 ° deflection is distributed, for example, to 10 links on a large idler, and the force is correspondingly low (here, for example, sine(9°) = 16% of the chain pull). But chain links are not ball bearings, but plain bearings that have much higher friction - especially if they are also dirty.

Therefore, the most important measure is to keep the losses from idlers small: choose large idler pulleys!

One or two idlers?
------------------

Good question. Basically you can get the chain to the rear with an idler under the seat. But if it is also supposed to be large so that the losses are low, it protrudes further down, and the chain runs obliquely at the front and back. With two idlers you can lower the seat (i.e. lower center of gravity ), but still have more ground clearance, and the chain can come off the chainring more steeply so that you do not touch it with your calves. In total, two idlers are slightly less efficient, but offer these other advantages.

Are chain tubes inefficient?
----------------------------

A chain that grinds in a pipe naturally causes friction. And if the chain is oily and there is a lot of dirt stuck to it, it also settles in the chain tube; the inside diameter becomes smaller and the chain grinds more often. Wet dirt cannot dry and fall off as easily, but the wet dirt stays in the tubes forever.

But: in a velomobile, the chain is fairly clean because it has no direct contact with street dirt. Only the dirt that the driver brings in, for example through dirty shoes, can land on the chain. And there a chain tube is rather positive because it protects the chain from dirt. It is always damp in a velomobile and dirt collects at the lowest point; i.e. happily below in the chain channel. It would not be a good thing if the chain were permanently pulled through this damp dirt, but at least there tubes can help against the pollution.

The laying of the tubes is also important. Frictional force is the product of contact pressure and sliding friction coefficient, where the chain is under tension, it does not follow the course of the tube, but is pressed against the tube, the friction is correspondingly high. It is therefore important in the drive section that the chain does not follow the tube, but vice versa the tube follows the chain. If the chain does not touch the tube under tension, there is no friction.

It is different in the empty run. The chain sags there - in order to be frictionless, the chain tube would have to follow the course of the sagging chain. But the contact pressure there is also low (namely only approximately the weight of the chain), i.e. the friction is low. And in the Velomobile there is no space anyway for the chain to sag freely - whether it is dragging in the chain tube or on the floor doesn't make much difference. In the first case, it is protected from dirt.

Why a stiff drivetrain?
-----------------------

Whenever you pedal hard, a stiff velomobile is important, because then the pedaling force ends up on the road instead of in the deformation of the vehicle. This deformation is of course elastic, i.e. as if you were tensioning a spring. However, such a deformation naturally has hysteresis, that means you don't get back all the energy you put into it.

But that's not even the main problem: when you pedal on a stiff bike, the pedaling energy is immediately converted into speed - if you stop pedaling afterwards, the bike is faster and the pressure on the pedals is in the correct path. If, on the other hand, the frame is deformed by the pedaling energy, it is then under tension - i.e. it pulls on the chain and accelerates the bike, but it also presses on the pedals and thus on the muscles. But they don't like that at all. For example, when you lift a weight, the muscles do lifting work; but if you keep a weight at the same height, physically no work is done, but it still feels tiring, and the muscles are consuming nutrients. Muscles therefore consume energy when they only have to exert a static counterforce - a screw in the ceiling, on the other hand, can carry the weight of a lamp for centuries without tiring. And that is why this counteraction against an elastically deformed drive train costs force that is then missing for acceleration..

What has to be stiff?
---------------------

The forces that occur are primarily the chain pull and the pressure of the driver's back in the seat shell. Everything that goes with it has to be rigid:

* The bottom bracket is attached to the bottom bracket mast.
* This is above the steering bridge with the front wheel connected to the wheel wells. There are many curved surfaces and relatively thick material here, which means that there is enough rigidity.
* The chain movement pulls the idlers up, the front one is therefore usually connected to the stiff steering bridge.
* The chain idler also wants to pull the vehicle floor between the two idlers together like a bowstring. That is why in many velomobiles the chain tunnel also has a static function, because it serves as a U-profile that stiffens the vehicle floor in the longitudinal direction.
* The chain pull goes on to the rear wheel, whose swing arm must be correspondingly rigid and also must be well supported by the bodywork.
* Also the seat must be securely attached. For this reason, for example, some seats are bolted to the front of the front wheel arches and are supported at the rear against the rear wheel arch, which often holds the swing arm bearing in the front and is accordingly strong.
* The body between the rear swing arm bearing and front wheel housings must also be sufficiently rigid. But this is basically the case because it is a tube with a large diameter. However, openings that are too large are disadvantageous, the velomobiles, which have huge entry openings, are correspondingly massive and heavy. (There were also convertible cars, for example, which had to be reinforced in comparison to their closed coupé counterparts, because otherwise they would not have been stiff enough.)

The rest is not so important and can be made thin-walled accordingly.

Brakes
======

What are the requirements for a velomobile brake?
-------------------------------------------------

In any case, not particularly high braking forces. Firstly, the Velomobile has two front wheels that can be easily braked, secondly, the front wheels are small, i.e .the lever between the radius of the brakes and the radius of the wheels is smaller than with large wheels, and thirdly, you drive more smoothly, not downhill like on a mountain bike, on a surface which has surprises in store. In practice, the limiting factor is the tires, a well adjusted brake can usually be blocked.

To do this, the brake must be able to withstand high heat output. Firstly, there is little drag on the Velomobile that slows it, while a mountain bike hardly ever sees speeds faster than 60 km/h, a velomobile easily reaches over 100 km/h. Second, with the Velomobile, the usual speeds are higher, with the same gradient, a higher speed means that more altitude energy has to be braked away per time. And thirdly, a velomobile is heavier and the brakes in the wheel wells are less well cooled.

So the problem is not short braking maneuvers, but long descents. Larger drum brakes help here, simply because they are larger their heat disapation capacity is higher - there is more material that can be heated, and the surface area of the hub shell is also slightly larger.

And what happens if the brakes are too hot? First, they fade i.e. the braking effect decreases noticeably. Second, the hub shell can expand so far that the spokes lose their tension and get loose when the wheel rolls, and soon break through material fatigue due to the alternating load.

.. _fig_speed_vs_brakepower_downhill:

.. figure:: images/speed_vs_brakepower_downhill_en.*
    :alt: Braking power required to maintain speed
    :width: 100%

    Braking power needed to maintain speed on various gradients: the solid curves show a velomobile, the dashed ones a racing bike. While on the latter, drag increases so much that, at higher speeds, the necessary braking power decreases again, and on a gradient below 5%, you don't have to brake at all to keep from going over 50 km/h, on the velomobile, this maximum power required is much higher and at much higher speeds. While a road bike brake does not have to withstand more than 500 W on a 10% incline, with a velomobile it is almost twice as much at the same speed and at its highest at three times the speed, almost four times as much is needed.

How can you cool the brakes or make them less hot?
--------------------------------------------------

One way is to direct more of the airstream onto the brakes. This was done, for example, with the Velomobile that Marcel Graber used to do the Trans America Bike Race 2018 - he used wheel well covers ("pants") and, correspondingly, front wheels without a wheel disc, and air channels were cut into the wheel well linings pointing towards the hub.

Another option is water cooling, which was implemented by Patrick Flé (Velomobilize): with a plastic bottle and a hose, water is sprayed into the brake drum, the water evaporates immediately because of the heat and the vaporization means a very high amount of energy can be dissipated. The brakes can withstand this temperature shock mechanically.

Then there were also experiments with continuous braking. Firstly, with an additional rear brake, owever, this has the disadvantage that the rear wheel locks slightly and then tends to break out. Another method is to use braking parachutes, it has proven useful to use two parachutes, each with a diameter of approx. 40 cm. However, this is only of limited practical use because firstly the parachutes have to be ejected manually at the start of the descent, secondly you have to stop at the end of the descent to repack them, and thirdly the parachutes dancing around are probably not recommended in heavy traffic.

The most practical methods are, however cooling fins and cooling towers of brakes sold by Ginkgo bike parts. These are milled into the brake drums or glued to them. This increases the surface area of the brake drums and air can cool them more effectively.

And last but not least, you can make the brakes heat less with your driving style: the worst thing is to drive downhill at high speed and brake constantly. Intermittent braking means that higher top speeds are achieved so braking drag is higher when the brakes are applied, and then higher temperatures are reached when braking, which makes heat conduction to the air and heat radiation more effective. During the following pause, the brakes can cool down more than if the brakes were applied permanently.

Why does a Velomobile have drum brakes?
---------------------------------------

Because no other brakes are suitable.

It has no disc brakes because they are attached next to the wheel. Exactly where there is the least space, because the wheel well is insufficient, and a brake would also have to be housed next to the driver's thighs. That would only be possible if the velomobile were made wider. A brake disc itself is not particularly wide, but additional space is required when turning, which must be available in the wheel well. There are velomobiles with disc brakes, they do not have normal spoked wheels, but disc wheels - these are concave in shape so that the brake disc can be accommodated further inside the wheel. However, these wheels have other disadvantages, such as higher weight, and cannot be centered. Another argument against disc brakes is the poorer cooling in the wheel well, the brake disc and brake pad are relatively small and therefore heat up quickly. Since the wheels are in wheel wells, the rainwater does not simply run off, but is repeatedly thrown by the wheel into the wheel arch and drips down in there, onto the brakes together with street dirt. An unprotected disc brake would therefore wear out quickly in a wheel well.

There are no rim brakes because that would be difficult to achieve with front wheels suspended on one side, cantilever bosses on both sides are fundamentally not possible, and even a side-pull brake does not fit easily into the wheel well if the wheel must also be able to turn. In addition, a rim brake protrudes outwards, which would be aerodynamically very unfavorable. A rim brake also heats the rim, this would quickly overheat the rim when going downhill due to the high driving speeds and small wheel sizes.

So there are only drum brakes.

Why no brake on the rear wheel?
-------------------------------

For two reasons:

* The rear wheel is relatively far away from the center of gravity - while the driver sits almost between the front wheels, he sits quite a way from the rear wheel. Therefore, there is little weight on the rear wheel - you can tell by how easily it spins on slippery roads. In addition, the braking deceleration ensures dynamic shifting of the wheel load to the front, which further relieves the load on the rear wheel. You could only transfer small braking forces with the rear wheel before it slips.
* A slipping rear wheel is dangerous because it slides equally well in all directions - while a rolling wheel moves much easier in the direction of roll than across it. That means a slipping wheel doesn't have opposing lateral grip anymore and lateral forces accordingly, which can be very dangerous (see: Why is it dangerous if the rear wheel loses lateral grip? ).

What is this about shims for drum brakes?
-----------------------------------------

The Sturmey Archer drum brakes consist of two brake pads, which are attached to a common bearing on one side and are pressed apart by a rotating cam on the other side. With new brake pads, this cam only has to rotate a little until the brake pads touch the brake drum. Because of the small actuation angle, the rotating cam mostly pushes the pads apart and hardly ever moves sideways.

It is different with worn brake pads. The braking force remains approximately the same since the brake cable has been adjusted accordingly. However, the cam now has to rotate much more and also moves sideways quite a bit. When the brake lever is released, it is reset by means of a spring that pulls the brake pads back together, this spring no longer manages to press the cam hard enough so that it also slides along the side of the brake pad holder - the brake no longer resets to off and locks up.

As a remedy, you can increase the distance between the brake pads and cams so that the worn thickness of the brake pad is compensated for. This is achieved by using a thin metal shim under the sliding shoes on which the brake pad holders slide on the cam (shim thickness e.g. 0.8 mm). Then the cam no longer has to turn so far and accordingly moves less sideways. This is not critical for the brake pads, their thickness is ample even if the cam causes problems. So you can realign the brakes several times, and even then the problem is that the shoe is not holding, but there is still plenty enough brake pad.

On this occasion you should also lubricate the cam and the sliding shoes, it is best to use copper paste because it is heat-resistant.


Electrical
==========

Headlights: which ones? Where?
------------------------------

TThere is never enough light on a velomobile. First, you are often traveling at high speed - possibly up cycle paths that are not made for this and have no painted lines or reflectors down the sides - secondly, you sit low and therefore have a poor view of the street and are often dazzled by car headlights (especially when the left-hand cycle path is lower than the street), and thirdly, the headlights are often mounted quite low - and bicycle headlights are usually designed for a significantly higher mounting position.

While bright lighting is sufficient on a dry road, you will come up against limits on wet roads and in dazzling oncoming traffic with normal bicycle lights. Here at least it helps to mount the headlight as high as possible. But being on the hood is often counterproductive because you blind yourself. Therefore, an internal headlight is recommended, but as high as possible, without ruining the aerodynamics.

Turn signals: which ones? Where?
--------------------------------

Since you can't practically make hand signals, turn signals are highly recommended. These should be far enough apart that it is clearly visible in the dark which side is flashing. Since turn signals are only used very briefly, the power consumption is irrelevant - LEDs, as bright as possible (e.g. 3 W, 700 mA, with largest possible beam angle), and in the front for aerodynamic reasons inserted into the bodywork. Suitable flashing relays are available for motorcycles.

Electronics inside are weatherproof and protected right?
--------------------------------------------------------

Yes and no. Of course, wind and weather do not hit the inside of the velomobile. However, you sweat a lot more, and in cold weather the air you breathe condenses on the cold bodywork surfaces. The velomobile has a climate like that of a tropical stalactite cave, and the electronics have to be able to withstand this. Anything that is not properly sealed or protected corrodes quickly.

Dynamo yes or no?
-----------------

A bicycle which is more than a fair weather bike should have a good lighting system powered by a hub dynamo. This is a bit more difficult with velomobiles: on the one hand these are everyday bikes with high mileage - on the other hand there is no good dynamo solution yet. There are high quality hub dynamos for front wheel hubs suspended on one side but these are for trikes that have disc brakes. Velomobiles usually have drum brakes, i.e. the space in the hub is used for those.

It also is difficult on the rear wheel, there are dynamos for a cassette rear hub, however, they have rather poor performance. And they are only available as double-sided hubs. Velomobile drivers with one-sided swing arms abstain!

Then there would also be cross-country dynamos. However, you can only run these on the rim, since the thin flanks of faster tires would not be able to withstand the stress of a dynamo roller. In addition, you would have to attach the dynamo somewhere in the wheel arch, where there is a lot of water and dirt when it rains - so the dynamo doesn't have an easy life and would probably slip easily.

And last but not least there is a combination of hub dynamo and drum brake from Sturmey Archer. However, this is not really convincing, because firstly, the efficiency is not great, secondly, the permanent magnets will not tolerate the heat of the brakes for a long time (Curie temperature!), and thirdly, the dismantling of the wheel is nowhere near as easy, because not only one screw has to be loosened, but also the wires have to be removed.

Finally, there is also the problem that a velomobile uses a relatively large amount of electricity. For a headlight, the typical 3 W of a dynamo is absolutely the bottom limit, it can sometimes be triple that. At least one buffer battery is required. And that's why most velomobile drivers stick to pure battery lighting.

Which voltage for the vehicle electrical system?
------------------------------------------------

It used to be easy; since the dynamo always delivered 6 V at moderate driving speed, with which the incandescent light bulb shone passably, and at high speeds the dynamo made sure that not too much current flowed despite possibly higher voltages. With a light bulb you only have to apply a voltage, the resistance of the filament increases with the temperature, so that the current can only increase until it glows brightly.

It is different with today's on-board electrical systems. A battery can deliver almost any current, and even a LED no longer has this negative feedback that limits the current, rather the opposite. This is why electronics are needed to regulate current and voltage. Many devices have such a thing built-in and tolerate a wide voltage range, for example from 6 to 16 V. And naked LEDs do not really care about the voltage anyway, it has to reach a certain minimum value, but otherwise the current has to be limited, which is so-called constant current source (CCS).

Accordingly, some people prefer 12v battery systems because they can easily install motorcycle parts. Others use 6v to install bicycle components. As long as all devices can cope with the voltage, and all LEDs are connected to constant current sources and, for example, a USB cable is connected to a 5 V voltage converter, the voltage of the vehicle electrical system does not matter much.

How to treat a lithium-ion battery?
-----------------------------------

* Do not overcharge: When the final charge voltage is reached, the charging process must be stopped.
* Do not undercharge: When the minimum voltage is reached, no more current may be drawn.
* The drawn current must not be too high (rarely a problem in the velomobile).
* The charging current must not be too high.
* The temperature must not be too high.

These things are checked by any standard charger when charging, the charging current is kept constant and, with increasing battery voltage, the charging voltage is increased until the final charging voltage is reached. Small BMS boards are usually integrated in the removable battery (Battery Mnagement System), which switches off the battery if the current is exceeded or the voltage limits are exceeded or undershot.

In addition, batteries with cells connected in series also need balancing, in which all cells are fully charged and evenly if they have previously discharged unevenly. However, this is more relevant for high currents, for example for electric drive motors, with usual use as light batteries, one can usually do without balancing.

To be sure that the battery lasts as long as possible, firstly the charging current should be kept low, and secondly the battery should not be completely drained or fully charged, ideally it is not discharged below 30% and not above 80% to 90% (depending on the source).

Where do I attach the speedometer / GPS / Navi?
-----------------------------------------------

Where you can see it well. If the display is large, this can be on the wheel arch or directly in front of the eyes on the front edge of the cockpit. With a tiller handlebar, you can also attach the navigation system there.

GPS reception under carbon?
---------------------------

It is well known, carbon fibers are electrically conductive - and will  shield from electromagnetic waves accordingly. Experience has shown that the GPS reception inside a velomobile works quite well, at least with modern GPS receivers. But the signal strength decreases noticeably when the hood is put on, and if there is also rainy weather with thick layers of cloud, the devices find it difficult to find the exact position. However, signal is usually sufficient for navigation.

.. _fig_GPS_tracks_elevation:

.. figure:: images/GPS_100tracks_2maps_42elevation_optim.*
    :alt: GPS tracks and altitude profiles superimposed
    :width: 100%

    GPS recordings made from inside the wheel arch (device: Wahoo Elemnt). On both maps you can see 10 rides each, which were made directly one after the other – on the left without a hood, on the right with a hood. On the right of the plot you can see 42 altitude profiles; while the shapes of the altitude profiles are very similar there are big differences in the absolute heights.

Which navigation system?
------------------------

In a velomobile, a navigation device or GPS with a map display is very useful, because it allows you to drive with much more foresight and thus save energy in unknown areas, after getting lost it takes a lot of time and effort to regain speed after braking, especially in a velomobile.

However, there are hardly any independent navigation devices that are suitable for bicycles and especially for velomobiles. Car sat navs are completely unsuitable because they prefer highways and main roads - where bicycles are either prohibited or one feels very uncomfortable due to large speed differences. Bicycle sat navs often prefer cycle paths and dirt roads, on which efficient travel is impossible and where confusion or getting lost at high speeds is dangerous.

Real-time navigation therefore only works effectively on a smartphone, where the route calculation is carried out by BRouter and any supported app can be used to display it (e.g. OsmAnd, Locus Map).

Otherwise you can create a track before the trip and either follow the line on the map, or, if the track is provided with navigation instructions, follow these via audio announcements.

In addition to this functionality, there are the following things to consider with the hardware:

* Power consumption (higher with a large display)
* Protection (against splashing water): a velomobile is like a stalactite cave, electronics have to withstand humidity
* Usability, even when damp, beware of bad touchscreens


Motor yes or no?
----------------

A velomobile drives fairly quickly on the flat, but is slow uphill and also accelerates slowly. The average speed is the average over time, i.e. slow sections are more important - it is more useful to be faster in slow sections than to increase top speed. Therefore, an engine is useful for travel time if there are many slow sections that can be shortened with it. Because you only need the engine on these few sections and not otherwise, the battery can be comparatively small.

However, an engine does not help much in stop-and-go traffic; there are many exhausting acceleration processes, but because of the braking in between, you will not be able to reach the speed of the open road, even with an engine. Here, an engine relieves the driver but does not make her much faster.

Also the amount of elevation gain is not as meaningful. On small climbs you can get some momentum beforehand and still have enough remaining speed at the top to be faster overall with a much lighter bike, but this only works up to about 10 meters of altitude at a time. You only get slow on long climbs, and only here would an engine reduce the travel time noticeably.

And then of course there are the legal provisions. In the EU you are allowed motor support up to a maximum of 25 km/h, it would only be used in a velomobile on a straight flat road for a few seconds and is therefore simply not worth it. Uphill is of course different. Then there's the S-Pedelec, where the motor support reaches up to 45 km/h. However, there is compulsory insurance, an approval procedure (very few velomobiles are approved for this), and you are also severely restricted in the choice of bicycle components.

Which engine?
-------------

There are basically two types here bottom bracket motor and hub motor, direct drive or geared.

A direct drive hub motor has the advantage that it can also brake, you can feed the braking power back into the battery (recuperation), and thus recover up to 2/3 of the braking energy. This is particularly interesting in very hilly terrain, where you have to brake a lot, so you can protect the brakes and then use the energy for short accelerations. The problem is that these direct drive wheel hub motors have no gears, So they can either drive fast or be powerful, but not both. And the steeper the slope, the more power the engine would have to deliver, but its power increases with engine speed. So the slower it goes uphill, the lower the efficiency - the engine overheats quickly. Direct drive wheel hub motors are therefore unsuitable for long gradients. They are also quite heavy.

Geared hub motors are lighter (2.3 to 3 kg) and more adapted to a velonobile, being geared they develop more torque and climb quite well with relatively little power. In the correct gear - the driver still has to participate - long gradients up to 13-14% are climbed with ease. Most popular geared hub motors fit standard dropout width and accept 10 speed cassettes. Some common motors (Mxus) are freewheel motors and limited to 8 speed freewheels to stay within 138 mm dropout width.

Bottom bracket motors, on the other hand, are mounted beyond freewheels and gears. Therefore, they can be small and light, because they can work at high speed and use the bicycles gears, i.e. always work in the optimal speed range regardless of the gradient without overheating. Therefore, no recuperation is possible. If you have long or very steep inclines, a bottom bracket motor is the better choice.

How should the gearing of a bottom bracket motor be chosen?
-----------------------------------------------------------

Electric motors are most efficient at a certain speed. The gearing should be chosen so that it is at comfortable pedaling cadence in the assistance range (i.e. pedelec up to 25 km / h). If the motor then also ensures that the driver accelerates as quickly as possible beyond the support area, the power consumption is the lowest - either the driver (mostly) pedals himself or the engine works in its most efficient speed range.

Aerodynamics
============

.. index:: air resistance, cross-sectional area, upright bike, road bike, recumbent, car

Why are velomobiles fast?
-------------------------

Because of their low drag. As can be seen in Figure 11, bicycles usually have rather poor aerodynamics, their drag coefficient (cW) is much higher than that of a car. The only reason why drag is not higher is because the cross-sectional area (A) is much smaller than that of a car. In contrast, the cross-sectional area of a velomobile is not necessarily smaller than that of a racing or recumbent bike, but the aerodynamics are much better than even an average car, so that the effective cross-sectional area (:math:`c_\text{W} \times A`) is much smaller - for a very good velomobile only about 0.03 m², which corresponds to the area of a DIN-A5 sheet of paper.

.. _fig_comparison_cW-A-cWA:
.. figure:: images/comparison_cW-A-cWA_en.*
    :alt: Vergleich von cW-Wert, Querschnittsfläche und effektiver Querschnittsfläche zwischen verschiedenen Fahrrädern, Velomobil und Auto
    :width: 100%

    Comparison of the drag coefficient (:math:`c_\text{W}`), the cross-sectional area (:math:`A`) and the effective cross-sectional area (:math:`c_\text{W} \times A`) between different bicycles, velomobiles and cars As you can see, racing bikes and recumbents are faster than Dutch bikes mainly because their cross-sectional area is smaller; the aerodynamics are not very good either. On the other hand, the cross-sectional area of a velomobile is similar, but the aerodynamics are much better.

How to achieve low air resistance?
----------------------------------

Ultimately, by releasing as little kinetic energy into the air as possible. I.e. the air should be slowly pushed aside and slowly return to its original position. Any acceleration energy that is released into the air is lost for the drivetrain.

A single air molecule cannot be accelerated linearly very much because the neighboring air molecules are in the way, they should also be accelerated. You can squeeze them a bit, but that creates back pressure. Linear acceleration therefore quickly reaches its limits. The situation is completely different with rotations: an air molecule can rotate almost anywhere on the spot without being obstructed by the neighboring air. You can lose a lot of energy by rotating air - so it is essential to avoid turbulence.

Why is a teardrop shape usually quite efficient?
------------------------------------------------

In short: because it is easier to push air part without a vortex than to let it flow back together again without a vortex. The former can therefore happen quickly, the latter takes more time and therefore a longer journey.

But of course there are exceptions. For example, the Kamm tail, which corresponds to a sharply cut teardrop shape. This will interupt the flow suddenly, instead of it being sucked back in. Aerodynamically, this is not quite as good, but it saves you a long stern, with its extra weight and its larger wind attack surface.


Why no wing profile?
--------------------

The profile of an airfoil is considered the perfect example of an aerodynamically good shape. But why does it have this shape? First, a wing must accelerate air downwards, and for this it must have an angle of attack relative to the flow angle. Second, on the other hand, where there is negative pressure, they do not break the current, that is why this side is not straight, but curved so that the air is slowly drawn around the curve instead of abruptly tearing off at the front edge. A velomobile, on the other hand, has no angle of attack (if the wind results from the front), it shouldn't accelerate the air if possible. The same thing, however, is that a stall should be prevented - hence the even, flowing shape without sharp kinks.

Why is the front blunt instead of being pointed?
------------------------------------------------

Usually one associates little flow resistance with slim, pointed shapes. But the front of most velomobiles is round. It is true that the incoming air has to be slowly moved to the side instead of suddenly hitting a blunt surface. But this is not the case: the air builds up in front of the velomobile during the journey, and this “mound of air” acts like an elongated snout that deflects the incoming air to the side beforehand. Such a mound of air would not build up on a point, i.e. the air would suddenly hit the body, nothing would be gained.

Why are open wheel wells sharp at the front and round at the back?
------------------------------------------------------------------

At the front of the wheel well, the edge is as sharp as possible so that the air flow does not follow the body, but tears as much as possible and flows back past the wheel. Of course, this is only possible to a certain extent. At the rear, on the other hand, the part of the flow that has penetrated further into the wheel well should not hit and swirl on the rear of the wheel well, but should be gently pressed outwards so that the air flow is again parallel to the body.

What are pants?
---------------

These are fairings that are used on velomobiles with open wheel wells and are attached outside over the wheels to improve aerodynamics. As a result, you have the good aerodynamics of a velomobile with closed wheel wells - but a slightly wider track (i.e. better cornering stability) and the flexibility to convert to open wheel wells again. Pants can be removed if something has to be done on the tires or wheels, but they also make overall width increase significantly. The latter can be mitigated with a special wheelset with the rim spoked further inwards.

Pants were first made for the DF design, but fit most velomobiles with open wheel arches. They are quite light and flexible and are simply stuck on with adhesive tape.

Why is the underbody next to the foot bumps not flat, but curved?
-----------------------------------------------------------------

Foot bumps protrude outwards and displace the incoming air. However, this cannot escape arbitrarily, because the road is a few centimeters below. So it can only escape to the side. If the vehicle floor next to the foot bumps were flat, the flow cross-section for the air would be reduced at the level of the foot bumps, the air would be braked and pushed out to the side where it would disrupt flow. If the floor next to the foot bumps is arched inwards, the cross-sectional area remains the same and the current can flow around the foot bumps more easily.

Why are no wind tunnel tests done?
----------------------------------

Because they are complex and expensive.

Also, it's not that easy to generate realistic flow conditions. While an aeroplane is smooth on the outside except for the engines and is only surrounded by air, a velomobile has rotating wheels in wheel wells as well as the road, which is a few centimeters under the velomobile. For correct simulation, it is therefore not sufficient to place the velomobile in the wind tunnel or to model it on the computer (so-called CFD simulations), the wheels have to turn and the road has to move underneath it.

And last but not least: to get new insights, it is not enough to simply place a vehicle in the wind tunnel and observe it. You have to set up and test hypotheses in a targeted manner, i.e. have a clear idea of what you could change and how. And precisely because velomobiles are already pretty good aerodynamically, further improvements can only be achieved if you know exactly what detail you are looking for.

Why is the large air intake at the front of the body?
-----------------------------------------------------

Because he stagnation point is there.

If the air intake were further back on the fuselage, then you would have to somehow channel the air inwards without the outside flow detaching from the fuselage - that would happen if an air inlet was in the wind. On the other hand, as the air builds up in the front, you don't need a device that directs the air inside, just a simple hole is enough. In addition, the flow is always there due to the dynamic pressure, and an air inlet there does not damage aerodynamics.

What is a NACA duct?
--------------------

This is a triangular air inlet, which is located on the side of bodywork. The inventor and namesake is NACA, a US aeronautical research institute and forerunner of NASA.

If you want to bring air from the side into the interior of a vehicle, a simple hole is not enough - the air flows across the hole and therefore not through the opening. One would have to redirect the air, however, a corresponding “scoop” would increase the cross-sectional area and thus the air resistance, and possibly create braking vortices behind it. The NACA duct takes a different approach: the opening is in the direction of travel (i.e. in the direction of flow), but offset inwards - this does not increase the cross-sectional area. In order for the air to come in, there are two sharp edges in the direction of travel. It tears at this edge, and vortices are formed that rotate inwards and come into the inlet opening. The NACA-Duct brings the air inwards by creating inward vortices and then sucking these vortices away - so that the outside flow is nice and smooth.

Does ventilation slow you down?
-------------------------------

Basically yes. The air hits the velomobile at driving speed and is braked inside - at the latest when it hits the driver. The energy for the deceleration of the air (from the perspective of the velomobile) or the acceleration (from the perspective of the environment) must indeed come from somewhere, namely from the kinetic energy of the vehicle. On the outside, on the other hand, the air would be deflected but hardly slowed down, meaning that less energy would be lost there.

On the other hand, ventilation would have a positive effect if it were possible to divert an already turbulent boundary layer to the inside, so that the outside flow is smooth again. However, this happens primarily in the rear, behind the driver.

Are freestanding wheels a lot worse than wheel wells?
-----------------------------------------------------

At first glance, a velomobile with free-standing wheels like the Le Mans by Cycles JV & Fenioux looks very attractive: the body is narrower yet the track is still wide, the steering linkage can be covered aerodynamically, larger wheels are possible, and steering lock is not limited by wheel wells. And if the wheels are covered with wheel disks, you would think that the aerodynamics should not be that bad either.

The problem, however, is that the top of the wheels are in the wind. Even a smooth tire draws a lot of air with it (with tread it is much worse), and with free-standing wheels, the top moves against the air at twice the speed of the rotation. When the wheel wells are closed, the airstream is not attacked, the air in the wheel wells is stationary relative to the vehicle, so that the wheels only work against the air at that speed. Aerodynamic drag increases quadratically with speed, performance even cubic - i.e. a free-standing wheel needs eight times the power at the top because of its double speed. Worse: the air transported there by the tires not only moves parallel to the tires, but also to the side, where it hits the wind - i.e. the aerodynamically effective width of such a wonderfully narrow tire is much larger. And even worse: since the wheels are not extremely far from the bodywork, these air turbulences also destroy the flow along the bodywork. Wheels are relatively far forward, thus most of the velomobiles laminar flow is destroyed. This is the reason why free-standing wheels are extremely poor aerodynamically.

It would be different if you provided the wheels with wheel covers - one could make them very narrow (because they steer in curves), and it is only important that the upper half or the upper 2/3 of the wheel are covered.

How does wind make itself felt?
-------------------------------

Headwind or tail wind is felt much less than on normal bicycles. You can see on the speedometer that the speed differs slightly in headwinds or tailwinds, you don't have the feeling of "pushing against a wall" in headwinds.

Cross winds are noticeable mainly due to the influence on the steering, if you have a suitably sensitive velomobile, you should drive carefully in strong winds. The absolute force is not the problem, but its sudden change - if you come out of the slipstream at the end of a hedge, for example, when driving downhill quickly and a gust of wind suddenly pushes on the velomobile, you need a space of one meter to the side to be able to compensate. So this is of course extremely unfavorable if it is a narrow street with oncoming traffic. The actual offset by the wind is much smaller, but without warning you need very good reflexes to keep the steering reasonably calm.

If the cross wind is very strong, it can also knock over a velomobile. This is not surprising, since the side of a velomobile is not much smaller than a car (approx. One third), but weighs much less (approx. one tenth with the driver), the ratio of the height (or height of the wind attack point) to the track width is also less favorable than in most cars. And so it can happen in a strong storm (approx. Wind force 9/10) that the wind tips you in a straight line.

How can the crosswind sensitivity be reduced?
---------------------------------------------

One way is reduced caster. So-called stormstrips :index: `<Stormstrip>` have also proven their worth - these are thin, angular profiles that point in the longitudinal direction glued to the body. Thus, they do not create additional air resistance in the direction of travel, but wind flow across the vehicle breaks off and swirls there instead of generating lateral lift.

What is the sailing effect?
---------------------------

Cross winds can provide propulsion, i.e. the velomobile “sails” forward. This is actually not surprising, because it has a relatively large side surface that the wind can blow along, it rolls easily, and it is very stable. So like a sailboat - with a very small sail, but very little drag and an excellent keel without any drift. The only problem is that a velomobile is usually much faster than the wind. That means: no matter from which direction the wind blows, the apparent wind always comes more or less from the front - and for it to come from the side at full speed, it has to blow quite hard.

The sailing effect varies depending on the velomobile model. A more detailed investigation has so far only been made for the Milan GT 2011 in the Volkswagen wind tunnel; this has shown that the sailing effect exists and is strongest when the apparent wind direction is 75 °.

Under what conditions is drag the least?
----------------------------------------

The shape and cross-sectional area of a vehicle are unchangeable, and you don't want to reduce the speed, you want to increase it - this leaves only the air density with which the air resistance can be changed. There is the lowest possible air density:

* At high altitude. However, physical performance also decreases with altitude, since oxygen supply to the body becomes more difficult. Since oxygen is relatively heavy compared to the other air components, the amount of oxygen available decreases disproportionately with altitude. Therefore, attempting records at 1000 m is a good idea, but not at 3000 m.
* At high temperature. Then the rubber of the tires is not as rigid and the rolling resistance is lower. However, the body must not overheat, because otherwise the performance drops sharply.
* In humid air. However, this makes cooling more difficult because sweat does not evaporate as well and the body overheats more easily. In addition, raindrops on the surface interfere with aerodynamics, and water on the road increases rolling resistance.

As you can see Battle Mountain in the Nevada desert at an altitude of 1375 m with a warm desert climate is quite well suited for record attempts. But there is another reason why the records are set there of all places: there are hardly any roads with perfect asphalt that are absolutely level over several kilometers. Accordingly, the IHPVA allows in their rules, a maximum gradient of 2/3%. The Battle Mountain course is very close, with a gradient of 0.64%. This is very little and is roughly in the range of the breakaway resistance, this means that a bicycle does not roll off on this slope. But if a record driver is traveling at 113 km / h, that's a good 31 meters per second, with a gradient of 0.64%, that's 20 cm difference in height per second, which means almost 200 W additional power for a weight of 100 kg - almost as much as a legal German Pedelec motor gives at continuous performance.


Basic Physics
=============

What is rolling resitance?
--------------------------

Rolling resistance is fundamentally complicated because it is a collective term that includes many things:

* Deformation of the tire
* Roughness of the road
* Rolling and sliding friction in the bearings

In particular, the deformation of the tire is not only geometrically complex, it's hysteresis losses also depend on the material properties of the rubber. And this is itself a complex combination of many substances and a company secret of the manufacturer. What exactly happens there cannot be calculated - at least not as an outsider.

Nevertheless, it shows that the rolling resistance is primarily dependent on the load. Accordingly, one might summarize all influences on the rolling resistance value, which can be regarded as a constant, and thus very well describes the rolling resistance in the relevant speed range. This constant has little to do with the physical processes behind it, but it works well enough.

This is not enough for bean counters like velomobile drivers, here it has been shown that there is still a small speed-dependent share in rolling resistance - and this is the so-called dynamic rolling resistance. There are hardly any studies of the causes, only speculations. It is also an empirical factor that works well enough, and with the help of which the rolling resistance can be adequately described within the scope of measurement accuracy.

How do you reduce rolling resistance?
-------------------------------------

* Tire pressure: High air pressure = lower friction. But this only works to a certain degree; if the tire is already hard, it hardly deforms, and therefore, no matter how high the pressure, it can hardly be made to deform even less. This increases the risk of tire damage. And on rough ground, the tire does not spring, but jumps, which also costs performance. Therefore, a good recommendation is to use the maximum pressure specified by the manufacturer and somewhat less on rough roads.
* Fast tires: It largely depends on the rubber compound, which cannot be assessed from the outside. Here you have to rely on the information provided by the manufacturers, together with practical reports.
* Thin-walled tires: All fast tires have in common that they are relatively thin-walled - tire flanks especially often have very little rubber. The less rubber there is, the less it has to be deformed, the losses are correspondingly lower and the comfort is also higher because the tire adapts better to the road.
* Latex tubes or tubeless: The tube also contributes to the thickness and therefore rigidity of the tire. That is why a thin tube is better than a thick one, so use latex tubes or go tubeless (where the tires are a bit thicker).
* Wide tires: Geometrically, wide tires are faster because the tire contact patches  are shorter and are not deformed as much in the direction of travel. However, a wide tire can withstand less pressure, and is usually strongly built. Therefore, one cannot simply conclude the rolling resistance from the tire width, but must look at the entire tire.

Why are good tires so important on a velomobile ?
-------------------------------------------------

On a straight line at constant speed, the drive energy is primarily spent in air resistance and rolling resistance. While the rolling resistance force is constant, the air resistance force increases quadratically with speed, that's why air resistance dominates on normal bicycles as soon as you roll more than just comfortably (about 25 km/h).

It's different with velomobiles. There the air resistance is much lower, so even at about 50 km/h air and rolling resistance are approximately the same value. But hardly any velomobile driver constantly drives more than 50 km/h where air resistance dominates. Because the air resistance is so small, the share of rolling resistance is much larger, so that it dominates during most of the journey. (On fast downhill sections, the air resistance clearly dominates, but that only makes up a small part of the driving time.) That is why good tires are important for the velomobile, because it can reduce the dominant rolling resistance. This goes so far that even the material of the tubes (Butyl or Latex) and the ambient temperature noticeably affect the attainable speed.

Why are velomobile drivers so set on low weight?
------------------------------------------------

Many people say that low weight is not so important to them:

* You won't be racing, so gaining a second or two is not important, reliability is more important.
* The weight saving is negligible in relation to the total weight (driver, vehicle and luggage).

Both are basically correct, but apart from the fact that reliability has little to do with weight (a light vehicle is not necessarily more fragile, but only more elaborately manufactured), you have to look at the energy balance. And that's where air resistance comes in again - or its absence. While with a normal bike at high speed a large part of the energy is used to beat air resistance, it is usually less than half of that in the velomobile, despite significantly higher speeds. And the mass does not go into overcoming air resistance, but into the following resistances:

* rolling resistance: this is usually the dominant resistance, and it increases linearly with weight.
* Motor power: as a velomobile can achieve significantly higher speeds, acceleration phases are also much longer - the kinetic energy increases with the square of speed, i.e. put in four times the energy for twice the speed. Accordingly, acceleration phases make up a much larger share in the energy balance.
* Uphill: Here you can clearly notice the weight, and because the Velomobile has no aerodynamic advantage uphill but a disadvantage due to its higher weight, the difference is felt even more.

Weight therefore plays a greater role in a velomobile, which at the same time is significantly heavier - which is why weight loss pays off more. And not only for sporty drivers: the weaker a driver (i.e. the lower the speed), the lower the air resistance - and the rolling resistance plays a greater role. In addition, strong drivers in undulating terrain can often take hills with momentum, while weak drivers have to lug their weight up without momentum.

And very pragmatic: it is very pleasant when a velomobile is so light that you can carry it alone over a few steps or lift it onto a train instead of always needing a second person to help.

Why is it said that the wheels count twice in weight?
-----------------------------------------------------

The following energy is required to bring a wheel up to a certain speed:

* :index:`Kinetic energy`: :math:`E_\text{kin} = 1/2 \times m \times v^2`
* :index:`Rotational energy`: :math:`E_\text{rot} = 1/2 \times J \times \omega^2`

As you can see, both formula are quite similar:

* :math:`J` is the :index:`Moment of inertia`, it depends on the mass and its distance from the fulcrum. If you assume that the mass is on the outside of the wheel, the following applies: :math:`J = m \times r^2`.

* :math:`\omega` is the :index:`Angular velocity`, i.e. the number of revolutions over time (:math:`2 \pi / T`). Since the tread of the wheel must move at driving speed, i.e. the circumference over time must be equal to the speed ( :math:`2 \times r \times \pi / T = v`), we get: :math:`\omega =v / r`

If you use this formula, the following results as rotational energy: :math:`E_\text{rot} = 1/2 \times m \times r^2 \times (v / r )^2 = 1/2 \times m x v^2`. This means that if the entire mass of the wheel sits on the outside of the tread, the rotational energy is equal to the kinetic energy - hence the statement that the weight of the wheels would count twice, because rotational energy and kinetic energy taken together are twice as large as the kinetic energy of one rotating component.

In reality, the rim and tire make up the largest part of the wheel, but the mass is not completely on the outside, so the contribution of the rotational energy is smaller - so it is not quite twice as much. And as you can see, the size of the wheel is irrelevant: a large wheel has a higher moment of inertia (into which the radius is squared), but rotates correspondingly slower (the angular velocity is also squared) so that the rotational energy is the same.

Why is it so difficult to to drive faster?
------------------------------------------

Top speed is primarily determined by air resistance - because it increases faster than other driving resistances. Air resistance increases quadratically with speed, so to drive a route twice as fast requires four times as much energy to overcome air resistance. But since you then cover the distance in half the time, you have to provide the aforementioned four times the energy in half the time - work per time is performance, so you have to provide eight times the performance for twice the speed.

Another problem is time: if you need 20 minutes for a certain distance at a certain speed, you can save 10 minutes at double speed - but (if you only consider the air resistance) you have to provide eight times as much effort. A further doubling of the speed in turn leads to eight times the power requirement (i.e. a total of 64 times the power), but only saves another 5 minutes (i.e. only 1/4 of the original travel time).

How fast can a velomobile go?
-----------------------------

With a velomobile air resistance is significantly lower and the theoretically achievable top speed is significantly higher than that of other bicycles. However, kinetic energy is not less - due to the higher mass of a velomobile, it is even slightly higher. The speed is quadratic to the kinetic energy, in order to drive twice as fast, you not only have to achieve eight times the power to overcome the air resistance, but also four times the kinetic energy during acceleration.

If you can achieve 300 watts, for example, it takes about two minutes and 1.5 km to reach 60 km / h (system weight: 100 kg). If you drive downhill, you get quite a few watts from the slope, however, as the speed increases, the necessary downhill section becomes longer and longer: if you go downhill at 5%, for example, you can reach around 125 km / h with 300 watts of pedaling power in two minutes - but the downhill section must also be more than 3 km long and have 160 m difference in altitude - and be significantly longer for the braking distance without curves or obstacles. Therefore, a trained driver rarely ever reaches 100 km / h in ideal conditions, although much more should be possible on an ideal gradient.

Are Velomobiles faster because of their high top speed?
-------------------------------------------------------

Not really. Since people are quite underperforming, the maximum speed that can be achieved depends more on the gradient than on the pedaling power, and since high top speeds require very long acceleration distances, in practice the maximum speeds of velomobiles and other fast bicycles are not all that different.

But what is significantly different with a velomobile is that you can keep a high continuous speed much longer than with another fast bike - if you do not have to brake.

How to get the highest possible Average speed?
----------------------------------------------

Since the air resistance increases a lot with speed, it is energetically best to drive at constant speed.

The body, wants the power output to be as constant as possible - high power peaks require anaerobic metabolism and tire the muscles.

Especially on gradients, however, a multiple of the power is required at the same speed. So you have to find a compromise: on the one hand, don't let the speed drop too much, on the other hand, keep the average power constant. Uphill that means sprinting off short ramps and relaxing on the plain. This is the reason why dolphin climbing on small hills is so efficient: you don't spend too much energy, because the average performance is not excessively high, but the speed does not drop too low.

On the plain, the motto is accordingly: get up to cruising speed quickly and then drive comfortably there. However, while you can easily sprint to cruising speed in a few seconds with a racing bike, the velomobile has a higher top speed and therefore much higher kinetic energy so the acceleration processes is significantly longer, you have to take that into account and should not sprint off at full power, but conserve your strength. Otherwise, routes with frequent braking and acceleration are very strenuous. However, this does not mean that a velomobile is basically slow on short distances and in city traffic - it takes about the same time to accelerate to the same speed as a racing bike, only higher speeds are difficult to reach. So you're not slower in city traffic, just less agile than with a traditional bike.

Does tilting technology for velomobiles make sense?
---------------------------------------------------

With tilting technology you can drive faster through bends they say. But that's only partially true, whether the vehicle is traveling up or down through a curve usually does not change the possible speed at all (if the position of the center of gravity is the same).

The first limiting factor in cornering speed is losing grip. In addition to the surface quality of the road, this mainly depends on the tire material - a good tire should roll easily, but have a lot of grip in the corners. There there are actually models that use different rubber compound in the middle of the tread than on the edge, so that you drive straight on the smooth-running rubber, but when you corner, you are on the better-adhering rubber. Of course, this effect cannot be exploited in a multi-lane vehicle without tilting technology.

The second limiting factor is tipping over. And that mainly depends on the height of the center of gravity (as low as possible) and its distance from the outside wheel (as large as possible). With tilting technology you can of course move the center of gravity towards the inside of the curve, thus increasing its distance from the wheel on the outside of the curve. But you can also do this by simply leaning towards the inside of the curve - provided you have a seating position with sufficient lateral support where possible.

So tilting technology would actually have certain advantages, but also higher weight, greater technical complexity, and that also needs space. You can shift the center of gravity laterally with tilting technology, but you will probably not get it as low as without tilting technology. Even if you don't lean the whole vehicle, but only the heaviest part - the driver. And then you also need an energy source that performs the inclination in the opposite direction to the centrifugal force - if you do it by hand, you can immediately do without the mechanics and simply lean your body inwards. There is something like a passive tilting technique, but in principle the vehicle is suspended on the chassis like a swing - yes, it tilts in the curve of its own accord, but that only prevents you from slipping on the seat, you can't drive faster through the curve, but on the contrary, only slower (because the center of gravity swings outwards instead of inwards).

And last but not least there are very few tight bends in practice that could be driven at high speed, real curves are usually wide enough, and with narrow junctions you rarely have the overview to drive through without slowing down. For these few special cases and a few seconds saved, complex mechanics are not worthwhile.

Is there a calculator for air and rolling resistance?
-----------------------------------------------------

Yes, even several:

* Adder calculator: calculates the final speed or pedaling power from numerous parameters, and contains default settings for various upright bikes, recumbent bikes and velomobiles. This variant of the calculator even allows the roll and air resistance coefficient to be freely entered.
* HPV Speed Simulator: This calculator is similar to Adder, but also graphically shows the increase in speed; you can also see how long it takes to reach top speed.
* Performance analysis diagram: Here you can upload a file with recorded performance data and get a diagram with the various driving resistances at all times.

There are also example values for air and rolling resistance coefficients on the respective pages. These were mostly obtained from performance measurement data, so the calculations from them are reasonably realistic.

Ergonomics
==========

What are the limiting factors in size? What do you have to watch out for?
-------------------------------------------------------------------------

* Leg length: Since the velomobile tapers at the front, the feet rub against the bodywork if the legs are too long. The knees can also be a problem.
* Upper body length: small people can not look out over the nose, and tall people do not fit under the :index: hood.
* Shoulder width: in rare cases, a velomobile is too narrow at the shoulders.
* Thigh width: with large thighs you no longer fit between the wheel arches. Since you have to move your legs, absolutely nothing should rub on the bodywork here.
* Foot length: people with large feet usually have long legs and may also want to use longer cranks, overall, this ensures that the feet rub against the side of the bodywork and/or below and above.
* Preferences: e.g. flat or upright sitting position


What is the ideal sitting position?
-----------------------------------

Basically, it has to feel pleasant - some people prefer a steep sitting position, others prefer a flat one. Upper body opening angle (i.e. the angle between the upper body and legs) must be correct, many people can exert more pedaling power when sitting upright.

Compared to an unclad recumbent bike the bottom bracket height in a velomobile is rather low. While with the recumbent bike the legs are in front of the body, reducing cross-sectional area would not work in a velomobile - you still have to look over the hood, so the legs are lower. In addition, the low aerodynamic drag of a velomobile does not primarily result from a small cross-sectional area, but from a low drag coefficient.

What is the impact of a short crank?
------------------------------------

A short crank means that the lever that turns the chainring is shorter - at the same pedal frequency thus requires more power and less distance (= less circumference of the pedal circuit), the pedal speed drops. Conversely: To pedal with the same force and pedal speed, you have to drive in a lower gear than with a long crank. Since the pedal circumference is smaller, the same pedal speed means that the cadence is higher.

You also have to push the bottom bracket forward so that the leg is stretched equally when the crank is in the front. The leg is then angled significantly less in the opposite crank position.

Why do recumbent riders often use short Cranks?
-----------------------------------------------

The crank length of course depends heavily on personal preference and habits, and of course also on anatomy - longer legs need larger cranks. Apart from that, shorter cranks have proven themselves because the sitting position allows stronger strokes than on an upright bike (i.e. there is no rocking step) and at the same time significantly higher pedaling forces are possible (i.e. you can not only push the pedal with your own weight, but against the back of the seat). This leads to high forces with strongly bent legs which can cause knee problems. On an upright bike, on the other hand, it is rather unusual to remain completely in the saddle when pedaling hard - but when you stand up, the leg is hardly angled at top dead center. With short cranks, the knee is less bent on the recumbent bike, i.e. it experiences the highest load in a position in which less shear forces occur.

With velomobiles, there is also a purely geometric reason for short cranks: with long legs, and the bottom bracket far ahead - there the bodywork is narrower and lower than further back. At the same time, tall drivers usually have big feet. In order to be able to pedal without pushing your feet up and down, the cranks have to be shorter.

With an open recumbent bike, on the other hand, there are aerodynamic reasons: if the legs move less far thanks to short cranks, less air is disturbed.

How do I shorten cranks?
------------------------

Most pedal cranks have a length of approx. 170 mm; shorter than 165 mm is usually difficult to obtain. That's why you have to shorten yourself or have the cranks shortened.

In itself, it's pretty easy if you have the right tool: drill a hole and cut a thread. Since pedal thread has a diameter of a good 14 mm, you cannot simply place a second hole next to the existing one at a closer distance - because the new hole must be surrounded by sufficient material, it is advisable to use a crank that is 20 mm longer than the desired length.

Solid aluminum cranks can of course be shortened without any problems. But also hollow-forged cranks are usually not a problem - you only drill the edge of the cavity, where there is still enough wall thickness for the thread. An exception are rotor cranks, for example, because they contain several cavities next to each other - among other things, where the thread would be on the side. Also carbon cranks cannot be shortened eaasily, since the thread cannot be cut directly into the carbon (which would mean point loads much too high), but is done via metal inlays.

What is the Q factor and why is it best to make it small on a velomobile?
-------------------------------------------------------------------------

The Q factor is the width between the cranks, i.e. the lateral distance between the pedals. It is determined by the width of the bottom bracket, the width of the crank arms, and their offset.

In the case of an upright bicycle with wide tires, the chainstays must offer enough space, therefore the Q factor must not be too small. Accordingly, the cranks are offset to get past the wide chain stays at the rear. But the chainrings can also come into contact with the chainstay if they sit too far inside, so the bottom bracket should not be too narrow there either.

With a velomobile, on the other hand, there are no chainstays at the bottom bracket, even the widest bottom bracket mast is even narrower than a bottom bracket. There is therefore no reason for a large Q factor. On the contrary, with a tall rider (= long legs, big feet) the feet can easily touch the bodywork if they are too far apart. For this reason, attempts are being made to keep the Q factor small - for example, by shortening the bottom bracket axis a little and using cranks that don't have much offset.

And last but not least, it is also more pleasant for many drivers to be able to put their feet closer and keep their legs parallel.

High or low cadence?
--------------------

Depending on personal preference and anatomy, but also habit. Both have advantages and disadvantages, since low cadence means high strength and vice versa.

It is often said that too high a pedaling force would break the knee joint. This is only partially true, because a trained knee can easily withstand great forces. But you shouldn't go untrained on long tours using high strength, but with a slower approach. The direction of movement also plays a role: a joint can withstand very high forces if it is only subjected to pressure. If it still has to move, complications are much more likely - therefore a pushing  movement with short-term high strength should be much less critical than a grinding movement with constantly high strength.

But even a high cadence is not necessarily a problem. In order for a knee to be free of symptoms,  well-trained muscles that hold the joint in position - and muscles only develop under load, and not if you only ever crank there without force. It also needs a certain alternating load because articular cartilage has fluid and therefore nutrients that pump into the interior.

The cadence also has an effect on technology: a bike with a very responsive suspension bounces more when the cadence is low and uneven - because there the mass of the legs is accelerated and braked more. Accordingly, more energy is lost into the suspension damping. But you can change the pedaling force faster than the cadence, those who pedal at a high frequency need a better-tuned gearshift because they cannot jump gears when accelerating as easily as a powerful driver.

Uphill the cadence gets lower for most people. This also makes sense because the pedal resistance is higher there, i.e. in the dead center speed drops more - and you need more strength behind the dead center. In contrast, the load is more even in the plain, it would be counterproductive to exert a high shear load on the knees with a very slow and grinding movement over the entire pedal rotation.

Everyday use
============

How much luggage can you accommodate?
-------------------------------------

That depends very much on the respective velomobile. Basically bulky items are often problematic. But otherwise the luggage volume is surprisingly large, even in the smallest velomobiles you can accommodate at least as much as a touring bike can fit in two large saddlebags plus on the luggage rack. So touring is not a problem at all. Larger velomobiles even have the same luggage volume as a smaller bicycle trailer, and thus space for large purchases.

Can a trailer be used?
----------------------

Basically yes, if it has a low drawbar. If you look at the back of the bodywork and it can be strengthened, you can bolt on a standard trailer hitch. But this only makes sense in exceptional cases, because in the velomobile you can accommodate quite a lot of luggage, and a trailer is heavy, bulky, and significantly increases air resistance. So you can't drive fast, and uphill you not only have to pull the weight of the trailer and the load, but also the heavy velomobile. Therefore, it usually makes more sense to pull the trailer with a normal bike.

What clothes?
-------------

Similar to upright bikes but less. Even in deepest winter you can still drive with a T-shirt, then you have to have warm clothes with you, otherwise a breakdown will quickly make you very cold. Overall, a thin shirt, short or long trousers, and possibly a scarf or a neck warmer are usually sufficient. You can definitely leave windbreakers, sleeves and gaiters at home.

Do you need click shoes?
------------------------

Actually yes. Firstly, because on a recumbent bike the foot pushes on the pedal from behind instead of from above i.e. it is not already pressed on the pedal by gravity. Second, because otherwise you can slip off the pedal. While that on an upright bike often has only minor consequences, in the velomobile there is almost no space next to the feet - if the foot is not sitting correctly on the pedal, you bump into something, and if you slip off while pedaling hard, you can even damage the bodywork on thin-walled velomobiles. And thirdly, efficiency is also somewhat higher if you simply don't have to worry about slipping, but can always rely on a firm hold on the pedal.

How big is the turning circle?
------------------------------

About 10 meters, about the same as a car.

That sounds like a lot for a bike (it is too), but since you mainly drive on roads for cars, you almost never have a problem in practice. On known routes you can often adjust to problem areas and make sufficient swings in time, only on unknown routes and then typically on bike paths, it can happen that you occasionally have to maneuver to get out of a trouble spot. But that happens too rarely to be a real problem.

The exact turning circle depends on several factors:

* Velomobil type: the narrower the wheel well, the larger the turning circle.
* Velomobiles with closed wheel wells usually have a slightly larger turning circle.
* Tire width: the wider the tire, the smaller the possible steering angle. First, the tire hits the side of the wheel well faster, second, the tire is also taller and protrudes further forward / backward.
* Wheel lacing: in velomobiles with closed wheel wells, the turning circle is smallest when the wheel is in the center of the wheel well. You can also dress up open wheel wells with so-called pants, this increases the turning circle significantly and you have to countersteer on many curves beforehand. However, if you re-spoke the wheels so that they no longer sit flush with the open wheel well, but in the middle of the well, the turning circle is similar to that with open wheel wells.


How do you go backwards?
------------------------

Bicycles usually do not have a reverse gear. You don't normally need it either, because the turning circle is small and otherwise you get off and push very quickly. Neither is the case with the velomobile, getting out is more cumbersome, especially if you have to remove the hood or foam cover, and the turning circle is similar to that of a car.

In a car, you need to go into reverse to park or to turn it several times. You don't need the former with the Velomobile: you just push it out of the parking space. And you rarely have to turn, especially not on familiar routes. It may only be necessary on cycle paths with hairpin bends on a regular basis. But even then you can often do without a reverse gear:

* Hardly any street is really flat, and certainly not adjacent entrances, you can often drive uphill there, let yourself roll back, and then get around the curve. A barely perceptible slope is sufficient for this.
* With velomobiles with open wheel wells, you can at least do without getting out and turn the front wheels with your hands like a wheelchair. But you get your hands dirty, and it only works on a really flat roads, not uphill.
* And then of course there are velomobiles with two foot holes where you can push back with the Fred Flintstone technique.


What about ventilation?
-----------------------

In the velomobile you are shielded from the wind; i.e. if not driving during winter, the body needs cooling - especially on the thorax and head. Another point is humidity, on the body this does not necessarily matter, but if it is too moist, you can irritate yourself in the crotch, for example. With the hood on it mists up the windows at cool temperatures. With a fresh air supply you can blow away the humid air.

Most velomobiles have an air inlet at the front - this is the stagnation point where there is increased pressure, so you can achieve a large air throughput with a small opening. In addition, a hole there does not disturb the air flow along the body.

On some models there is also the headlight cut-out - this has the advantage that no further holes have to be drilled in the bodywork, and the LEDs are well cooled by the wind, which increases efficiency. In return, the headlamp sits quite low, and lighting of the road is correspondingly poor (only a small proportion is scattered back).

In some Velomobile models, the bottom bracket mast is attached to the front of the air intake. This has the advantage that the bottom bracket mast can also be supported from the front, and the air flows through the mast backwards and the feet are not cooled by the cold fresh air.

If this cooling is not sufficient, you can also install a NACA duct usually in the front and in the top of the bodyork.

Anyone who drives with a hood can also drive with a raised visor, a narrow opening is usually sufficient there for sufficient air flow.

In addition, some drivers have drilled holes in the seat so that the back is better ventilated.

Can I drive in the rain?
------------------------

Rain is not as annoying in a velomobile as on an open bike, but you don't stay dry either. You are protected from street dirt and splash water thanks to wheel wells and you are not soaked to the bone by a cloudburst, but due to the high humidity inside, no sweat is removed, and a partly open visor or without a hood the head and upper body get wet.

In addition, raindrops obstruct the view - regardless of whether you are driving with a hood or in convertible mode with glasses. In the former case, it helps to have windshield wipers with which at least the large drops can be distributed so that the view is tolerable.

While there are no real windshield wipers, at least the following solutions have proven their worth:

* Magnetic wiper: Two elongated neodymium magnets, wrapped with absorbent material, stuck together on both sides of the pane. A viewing window can be wiped freely from the inside - that's enough because you sit close to the window and therefore don't have to keep large areas clear. In addition, the magnets provide contact pressure so that even large snowflakes can be wiped away.
* Thread wiper: an elastic cord is stretched lengthways or across the visor and can be pulled back and forth perpendicular to it with a second cord. This means that larger areas can be wiped than with a magnetic wiper, but the contact pressure is lower.

Driving at night and in rain is still not fun, because first of all the oncoming drivers headlights reflect in the raindrops on the windshield or your glasses, and secondly, the often low to the ground attached velomobile headlights only dimly illuminate a wet road.

What to do against fogged up windows?
-------------------------------------

What is the cause of fogged windows? The driver sweats and the warm, moist air condenses on cold surfaces, such as the windscreen.

The remedy is accordingly:

* Insulate the pane so that it is not cold on the inside. That works for example with the Pinlock visor, with which you can almost achieve double glazing.
* Keep the moisture away from the window. You can do this by ventilating the velomobile well, if fresh air flows in continuously from the front, the moist air is pushed out to the rear.
* You can also open the visor a little, and place a small barrier in front of the lower edge of the visor, which deflects the incoming air upwards, over the inside of the visor. This air film prevents the moist indoor air from coming into contact with the visor.
* And if none of this helps - for example because you are not moving and therefore have no air flow - you have to wipe the inside of the window.

Then why is that not a problem in cars?

* You sit passively in the car, so you don't sweat. The humidity is correspondingly lower.
* In the car you sit much further away from the windscreen. So you don't breathe directly against the window.
* The interior of the car has a much larger volume. Even if you were sweating, it would take much longer for all of the air to become damp.
* The car has a heater. An internal combustion engine is inefficient, only about 20% of the energy goes into the drive, the rest is heat - and with these thousands of Watts, the interior can be heated without any problems, and fresh air can also be heated and directed to the window.


Is a velomobile suitable as a car replacement?
----------------------------------------------

Depends. It is and remains a bike that only offers a slightly higher speed and range and a little better weather protection. A car, on the other hand, is a "one size fits all", you can take several people or large amounts of luggage with you, drive hundreds of kilometers through very mountainous terrain, but also only drive a short distance alone into town for shopping. A bicycle is of course significantly more restricted. But that also means that a car is completely oversized for most everyday tasks, this can be done excellently with the Velomobile. If this is the case on most days, you can do without a car - and still rent a car that fits your needs for the few remaining transport tasks.

You can drive without being sweaty on arrival?
----------------------------------------------

Difficult. Because inside the airflow is missing and you have to drive very slowly to avoid sweating down your back. If you are not going uphill, a pedelec engine is not much help, because you practically always drive faster than the maximum speed of the engine.

How does a velomobile drive in winter?
--------------------------------------

Not much different from summer, you may need long trousers instead of shorts and a scarf against the cold wind, and with a hood one more visor against fogging - but otherwise everything is like summer, you can usually drive in a T-shirt (but you should have a warm jacket with you if you have to get out in the event of a breakdown at freezing temperatures and tinker for longer).

In addition, good lighting is even more important in winter than in summer - because drivers expect fewer cyclists in winter. Semi-puncture-proof tires are also recommended, first, it's not fun to have to patch a flat in the dark and cold, and secondly, the cold also makes the rubber stiffer, so just changing the tires becomes very tedious. However, the rolling resistance also increases noticeably in the cold - that's why easy-running tires are not unimportant, especially in winter.

Do you need winter tires or Spikes?
-----------------------------------

Not really, at least not on a flat route. Spikes on the front wheels prevent from slipping and shorten the braking distance, and tread on the rear wheel ensures that the rear wheel is less likely to spin in snow. The latter is actually only a problem when starting off and with steep climbs, so only in a few places. And since black ice does not form all over the place, it is not worth installing spikes due to a few smooth bends - spikes, like tread tires, significantly increase rolling resistance so that high speeds can no longer be reached even on a straight road. It is better to drive carefully in curves and make slow steering movements, and calculate that the velomobile can slip a bit - which is normally completely harmless. Only steep, winding descents are critical on black ice, because then you may not be able to steer or brake. But as long as you travel on cleared and clean streets, there are generally few problems - and if it is very slippery, then the car traffic also slides.

How do people behave?
---------------------

Basically, velomobiles are received very positively, there is almost exclusively positive interest or comments - but you always draw everyone's attention, more than with the most expensive automobile. It's rarely a problem on the go, children point at the velomobile, and drivers often drive slowly alongside, marvel at the vehicle, crank down the window, make a comment and/or film or take pictures with their smartphone.

Interest, however, can become a real problem when parking. People don't just have to look at everything up close or touch it carefully, damage is not uncommon. Children seem magical mirrors and other exposed parts, it is not uncommon for the mirror to be twisted, maybe even broken off. If the velomobile was open, footprints can be found inside because, for example, parents have put their children in without having asked.

But it gets annoying when something breaks. Even if the Velomobile foam lid or the hood is closed, it can be partially torn open violently, so that a Velcro strip on the foam lid was torn off or a crack appeared in the carbon on the hood. It is simply incomprehensible how naturally some people tear off the foam lid as they walk past so that they can take a look inside. But also the bodywork is at risk. Again and again there is a crack in the carbon at the front because someone has obviously sat on it. While none of this affects functionality and security, it is very annoying and also very expensive. So it's best not to park the velomobile where there are too many people with too much time and very bad manners.

How to lock and secure?
-----------------------

Like every bike: connect to a fixed object. Due to the lack of triangles, this is only possible on the rear wheel - or on the stern handle or carrying eye, if present. While that's not as good as a normal bike where you can put a lock around the stable main frame - however, a velomobile is neither handy to steal nor inconspicuous, i.e. not for casual thieves. In addition, the removal of the rear wheel on some models requires special knowledge, and sometimes also special tools.

There is a bigger problem - vandalism. The best remedy here is a tarpaulin in the most inconspicuous color possible - while an open velomobile attracts everyone's attention, a covered vehicle is largely ignored.

Route selection
===============

Can you drive in the mountains?
-------------------------------

If you have a suitable gear ratio, this is no problem. But you need a different technique than on the upright bike: do not pedal hard but crank uphill in a low gear - slowly but steadily.

Nevertheless, a velomobile shows its strengths especially in the lowlands:

* Uphill aerodynamics have no advantages.
* Downhill, the air resistance brakes much less, so the brakes overheat much faster.
* The higher weight costs more power uphill and puts more strain on the brakes downhill.
* Uphill there is no cooling without a headwind, that makes mountain trips in the summer heat very tedious.

Can I ride a bike path?
-----------------------

Good question - the quality of bike paths is far too different to make a general statement about it, but the following generally applies:

* Cycle paths are not designed for velomobile speeds, even with a racing bike you reach your limits.
* Cycle paths often have tight curves and are very confusing, even if you could sometimes drive fast, you would often have to brake.
* Junctions are often confusing and blocked by stopped cars and cyclists are not often given the right of way.
* Cycle paths are often narrow, so that other cyclists can hardly be overtaken.
* Cycle paths often have obstacles such as jostle bars or hairpin bends that cannot be passed with a velomobile.
* Roots and other bumps are particularly uncomfortable with a multi-track with a tight chassis.

That is why it is usually not only more comfortable, but above all much safer to drive on the road. But there are also good, wide and clear inter-country cycle paths that can be used perfectly by a velomobile. However, you cannot rely on a minimum standard.

Can you drive on unpaved roads?
-------------------------------

Walking speed does it, but it's not fun. As mentioned, the rolling resistance plays a major role in the velomobile, this is much higher on unpaved roads, and accordingly you are much slower. In addition, a velomobile typically has narrow, hard-inflated tires and a tight chassis, which makes driving less comfortable. A multi-track not only hops up and down, but also tilts around the longitudinal axis. And since a velomobile is a wonderful sound box, it rattles and booms quite well.

How about driving in city traffic?
----------------------------------
Basically you can also drive well in the city, however, a velomobile has hardly any advantages there. Since the acceleration distance is long and you often have to stop in the city, you can hardly reach high speeds. Since velomobiles are not so agile, many bike paths are unsuitable, either not passable due to obstacles, or too narrow to overtake other cyclists, or too confusing for higher speed. And heavy traffic is a problem on the road, there you are stuck with the cars. On larger thoroughfares, however, you can definitely benefit from a "green wave" for which you would be too slow with a normal bicycle.

How does it work on short distances?
------------------------------------

A velomobile is rather unsuitable for short distances. The higher speed does not save any significant time, the weather protection is less important (you can hardly cool down in a few minutes, you can better close a gap between rain fronts), getting in and out takes longer, and you cannot carry a backpack, you have to Stow luggage and take it out again.

How does it go on long distances?
---------------------------------

Outstanding. This is where the strengths of a velomobile come into play: great time savings thanks to high average speeds, good weather protection, large luggage capacity, no seat problems, no numb hands, no neck pain.

Is a velomobile suitable for commuting?
---------------------------------------

Depends on, for example, if you want to travel to work in less than an hour, you can travel up to 30 km in the lowlands - correspondingly less with mountains. Depending on the route, the velomobile is a few minutes faster than an upright bike, but above all much more comfortable (since it is less worth driving at full throttle everywhere), you can take more luggage with you and is also less dependent on the weather - the clothes are almost the same summer or winter, and even a cloudburst doesn't get you completely soaked, you can continue driving.

You should always have a change clothes with you - unless the route is very short. Ideally, you can then take a shower at the workplace, otherwise you can get by with a reduced “cat wash”.


Which distance can be covered daily?
------------------------------------
This depends a lot on the particular route; but the lists at `Velomobiel.nl <http://www.velomobiel.nl/rijderslijst/index.php?amount=0>`_ and `Intercitybike <https://www.intercitybike.nl/rijderslijst/index.php?amount=0>`_ can give a rough idea. Entering the kilometers is voluntary and not always up-to-date, but one can assume at least for larger mileages that it was not a short vacation trip, but that the kilometers have accumulated over several months. This gives :numref:`fig_rijderslist_dist`; here one can see that faster velomobiles tend to be used for slightly larger distances.

.. _fig_rijderslist_dist:

.. figure:: images/rijderslist_dist-histogram_de.*
    :alt: Monthly travel distances of velomobiles from  Velomobiel.nl and Intercitybike
    :width: 100%

    Monthly travel distances of velomobiles from  the manufacturers Velomobiel.nl and Intercitybike. Data entry is voluntary and thus not representative. Only velomobiles with a total mileage of at least 7500 km have been evaluated.

How to plan a route for the velomobile?
---------------------------------------

If you want to travel as quickly and relaxed as possible, you have to be able to drive at a constant speed. That means:

* clear thoroughfares instead of winding alleys or residential streets
* Priority roads or roundabouts instead of stop signs
* as few gradients as possible
* Descents should be as flat as possible so that you don't have to brake, but can always pedal along

You can tend to look at online route planners for cars instead of bicycles, the latter mostly lead over low-traffic routes, but are often very winding. In contrast you don't need a high maximum speed, but as few braking operations as possible - each acceleration not only costs a lot of power, but also several 100 meters of distance, so that with frequent traffic lights you never get to high speed, but still exhaust yourself. It is better to choose a route where you can not drive quite as fast, but instead smoothly - instead of a single braking process that ruins the speed over several hundred meters.

BRouter is recommended as a route planner, this has, among other things, a routing profile for velomobiles and is completely configurable. The data comes from Openstreetmap, incorrect routing is therefore usually caused by incorrectly entered or incomplete Open Street map data. Fortunately, everyone can correct this themselves, until the changed data arrives in BRouter, however, it will take some time. The assumptions made by BRouter have generally proven themselves, however, they can be inappropriate in certain areas or in other countries.

If you want to plan the route by hand, you can still use the BRouter velomobile profile as a guide. This converts all obstacles into detours; For example, the specification uphillcost = 80 from the profile file means that one vertical meter uphill corresponds to a detour of 80 meters, and turncost = 150 means that a right-angled branch corresponds to a 150m detour.

Repair and maintenance
======================

Do you need a special workshop?
-------------------------------

Not really. Velomobile technology is not rocket science, basically most things are easy to master with a little basic knowledge of bicycle technology. And since the vast majority of components come from the normal bicycle sector, a bicycle workshop should have no problem with most things. You don't have to be a hobby mechanic to drive a velomobile. The fact that there are many velomobile drivers who are is probably due to their interest in bicycles and bicycle technology, and secondly, they often do high mileages and therefore save a lot of time/money if they can do routine work themselves.

Manufacturer support is only required for comparatively few defects, but even special parts (e.g. struts ) are usually sent quickly and are easy to install. And if you have bought a somewhat older velomobile model whose teething troubles have now been eliminated, you should be able to do many tens of thousands of kilometers without any velomobile-specific spare parts.

However, the Velomobile is definitely more difficult for troubleshooting because many parts are difficult to see and you cannot turn the crank with one hand and watch the rear derailleur. It is therefore a thankless task to find the cause of some symptoms. A velomobile driver should therefore keep an eye and both ears open when something feels or sounds different and get to the bottom of things as soon as possible.

Wear parts, in contrast to normal bikes?
----------------------------------------

Basically, there is less wear and tear on the velomobile, the drive train in particular is very durable because it is protected from dirt. The chain lasts a very long time because the wear is also distributed over a much greater length. Also drum brake pads are very durable - not because they are not used very much, but because they are quite oversized. The short-lived parts are usually the tires.

What are common maintenance tasks?
----------------------------------

Most likely: check air pressure, inflate tires, and examine the tires (especially on the rear wheel) for damage (stones, cuts, damage to the carcass ). And of course charge the light battery.

Occasionally the chain must be wiped and oiled and the brakes adjusted. If the latter no longer resets properly, you can  place shims on the brakes (see What is it about placing shims on drum brakes?), Or replace the brake pads completely. And if necessary, the braking and Shift cables can be changed. And if the chain tubes are dirty, you put a thin piece of fabric around the chain and pull it gently through the tubes.

How long does carbon fiber last? How can you protect it?
--------------------------------------------------------

Carbon fibers do not age, just the epoxy resin under the effects of UV light. But that's just a cosmetic problem, if the top layer of resin changes color or decomposes, you can grind it down and apply a new thin layer of resin. A UV resistant lacquer, Gelcoat or Film can protect the epoxy resin.

Why shouldn't aluminum parts be glued directly on carbon?
---------------------------------------------------------

Because of the electrochemical voltage effect. Both carbon and aluminum conduct electricity and there is also sufficient moisture in the velomobile, Carbon has an electrochemical potential of +0.75 V (compared to the normal hydrogen electrode), aluminum of -1.66 V - making aluminum the less noble material. If a current can flow between the two materials, it beccomes contact corrosion, Carbon serves as the cathode and aluminum as the anode, it is oxidized to aluminum oxide - it slowly dissolves.

Epoxy resin has an insulating effect, but, for example, at holes there is a direct connection between carbon and aluminum. It is therefore advisable to place a thin layer of glass fiber fabric around the metal that isolates it from the carbon.

How can the Velomobile body be decorated?
-----------------------------------------

Three methods have been established:

* Gelcoat: This is a colored synthetic resin, which is incorporated into the mold during construction. Only a few colors are possible here, and only single-color components.
* Painting: the finished velomobile is painted, this means more colors are possible and, by masking, the combination of several colors. In addition, the layer tends to be thinner (approx. 0.08 mm) and therefore lighter (approx. 200 g / m²).
* Filming: vynil film offers the most design options and requires the least equipment to achieve a good result. The thickness (approx. 0.09 mm) is comparable to lacquer, but tends to be lighter. Compared to a car, however, velomobile filming is relatively expensive because there are many curved surfaces.


Safety
======

How is a velomobile perceived by drivers?
-----------------------------------------

Basically, you are treated more like a car than a cyclist. This also means that you usually have more side passing distance, and are rarely referred to take the bike path.

However, speed is often underestimated, it often happens that you are slowed down by drivers who want to pass the bike too quickly, or that overtaking drivers need a lot more distance and are close to oncoming traffic or another constrictions.

In addition, overtaking is often prohibited in oncoming traffic, Here you have to react appropriately and just close the lane in dangerous places.

Visibility?
-----------

A velomobile in itself is definitely not inconspicuous or invisible; and if a  still don't see it, you should consider whether he is fit for public road traffic.

Nevertheless, there are weak points, for example, the narrow and low silhouette is difficult to see, especially in difficult lighting conditions. Good lighting helps here especially daytime running lights. The second main problem is likely to be selective perception - cyclists are often not expected, least of all to be fast, and also not in bad weather. To change that, more people would have to cycle.

How to behave in traffic?
-------------------------

As usual: foresighted, confident, calm.

However, a particular danger is waiting behind stopped cars without being visible in their mirrors. Many SUV-like cars only have very high and small rear windows, so you should keep a good distance to be visible from the inside mirror. In addition, a velomobile is too narrow to be seen from the side mirrors if you are not exactly offset to the side of the car. Many cars have reversing sensors, but these are often mounted quite high, so that a velomobile may not be reliably recognized.

What is the risk of injury when an accident?
--------------------------------------------

Alone accidents are generally less common in multi-lane vehicles because it is not so easy to tip over, when it's slick you slip a bit, but usually nothing else happens.

In a collision a velomobile has neither the stability nor the mass of a car in order to counter it with something worth mentioning. Nevertheless, many accidents have gone surprisingly lightly so far for the following reasons:

* Protection by the bodywork: the velomobile slides on its body over the asphalt instead of the driver with his bare skin - even if the bodywork is damaged by a collision.
* Lying position: in the event of a frontal impact, the legs are affected first, however, these can withstand high forces quite well, in contrast to the head or arms that often contact first on an upright bike.
* Shape: the convex shape of the body prevents the bike from sliding under the car and being overrun.

This is why injuries often arise primarily from sharp objects in the interior, such as screws, handlebars, brake and shift levers, or from a collision of the head and upper body with the coaming or the inside of the hood.

However, there have also been some serious accidents, in these, the driver has either been thrown out of the velomobile or has collided with an object (e.g. stone, post). For this reason, some velomobiles have a cockpit hatch that is as small as possible, which is not only aerodynamically favorable, but (especially if it tapers to the rear) fixes the body at the shoulders. Since the rear hood is usually higher than the head, it acts as a roll bar - if the driver sits far enough back. A shoulder strap would also help here.

In contrast, a hood is usually not so stiff and firm that it can withstand high forces. But above all, it is only fastened with rubber tensioners, so that it could fly away under greater force. At least it should direct objects past the head and prevent skin contact with the asphalt.

Fear is relatively widespread that a sharp-edged carbon splinter will result in a crash. But this is largely unfounded, the carbon shell is so thin that it tears quickly and hardly produces splinters. In addition, modern velomobiles have a nylon liner that largely prevents the cover from tearing, some carbon fibers break, but remain in place. A danger would only arise if very massive and stiff carbon parts were pulverized by the collision, but in such a violent accident, you have completely different problems than a few splinters of carbon in your skin.

Because of the roll bar function of the scoop and the lower risk of self inflicted accident, there are significantly fewer reasons for one to wear a helmet, in addition, many do not fit under the hood or are too long at the back. In addition, the face would still be unprotected.


Contributors and Sources
========================

`Version <https://gitlab.com/cmoder/velomobil-grundwissen>`_ |version| (Git commit: |commit|, date: |commit_date|)

Contributors:

* Text and diagrams: `Christoph Moder <https://christoph-moder.de/>`_
* English translation: `Tony Grant <https://www.velomobilforum.de/forum/index.php?members/anotherkiwi.21983/>`_
* VM slider: `Jockel Hofmann <https://www.velomobilforum.de/forum/index.php?members/24kmh_sammler.17233/>`_

.. raw:: latex

    \begin{samepage}

Sources:

* :numref:`fig_secondhand_prices`: `LazyBiker <https://www.velomobilforum.de/forum/index.php?members/lazybiker.21824/>`_
* :numref:`fig_rijderslist_countries`, :numref:`%s <fig_rijderslist_models>`, :numref:`%s <fig_rijderslist_dist>`: data `Intercitybike <https://www.intercitybike.nl/rijderslijst/index.php?amount=0>`_, `Velomobiel <http://www.velomobiel.nl/rijderslijst/index.php?amount=0>`_ (as of: 2020-02-29)
* :numref:`fig_age`: `data <https://www.velomobilforum.de/forum/index.php?threads/velomobil-alter-hase-oder-junger-spund.47682/>`_ (as of: 2020-03-04)
* :numref:`fig_ackermann-bedingung`: Is derived from `an image from Wikipedia <https://commons.wikimedia.org/wiki/File:Ackermann_radius_M.svg>`_ which has originally been created by user *Bromskloss* and is licensed under the `GNU Free Documentation License 1.2 <https://commons.wikimedia.org/wiki/Commons:GNU_Free_Documentation_License,_version_1.2>`_ and `Creative Commons CC-BY-SA <https://creativecommons.org/licenses/by-sa/3.0/deed.en>`_.
* :numref:`fig_mango_steering`: `WeLi-Arno <https://www.velomobilforum.de/forum/index.php?members/weli-arno.8903/>`_

.. raw:: latex

    \end{samepage}


Glossary
========
.. glossary::

    Kinetic energy
        must be be used to set a mass in motion. The formula is :math:`E_\text{kin} = 1/2 \times m \times v^2`, i.e. the mass m is linear and the speed v is squared.

    Bigfoot
        component of the DF velomobile, where the bottom bracket mast supports the rear end.

    Boruttisieren (need English equivalent)
        theft protection by making the bike look ugly and neglected makes it look unattractive for Thieves.

    Bump Steer
        Suspension-induced steering influence, see what is "Bump Steer"?,

    CFD Computational Fluid Dynamics
        i.e. simulation of the flow of fluids (liquids, but also gases such as Air) by computer.

    Cleat
        metal plate on the sole of click shoes with which this engages in click pedals.

    Delta
        tricycle with one wheel in front and two in the back. Named after the Greek letter Delta, which like looks like a triangle.

    Dolphin
        in hilly terrain you can go quickly and be energy efficient when the hills are not very steep or high - then you can rest on the flats between. Take up momentum to overcome the slope, and downhill gain speed for the next hill. Like dolphins swimming, jumping out of the water and not slowing down when they dive back in.

    Sealing milk
        A latex-containing liquid with which a tubeless tire is sealed. This is put in before assembly of the tire, or can, with the MilkIt systems special valves, be put in after with a syringe. Because the sealing milk dries out, it needs to be refilled every few months.

    Double manta
        Driving style, in which both arms are hanging out of the cockpit. This only works well with tiller handlebars, because tank steering levers are too low to to operate them with arms hanging out. The name comes from the Opel Manta, whose drivers were known for liking to keep the left elbow out of the window. Since the velomobile is narrower, it can be used as the poor mans air conditioning - especially in summer.

    Single-tracker
        A bike in which the wheels are one behind the other and which can therefore lean into the curve. This can be a recumbent bike, but also an upright bike. However, velomobiles are practically always multiple tracks.

    Development
        Distance traveled with one crank revolution; and thus a measurement of the of the drivetrain. The development depends on the chainwheel, the selected pinion and the size of the drive wheel and tire; in the lowest gear, velomobiles have a development of about 2 m, in the highest gear about 11 m.

    ETRTO (European Tyre and Rim Technical Organisation)
        standard for designating wheel sizes. The diameter is measured up to the height of the rim flange; i.e. that is neither the inner diameter of the rim, nor the outside diameter of the rim. The specification indicates not the size of the rim, but the total size with tire  - therefore a mountain bike bike wheel with large tires can actually be larger than a theoretically larger road bike wheel.

    Strut
        Part of an independent suspension, on which the wheel hub carrier with suspension and damping Is provided. Also called MacPherson strut.

    Gelcoat
        Colored synthetic resin, which, as an outer layer, contains fiber composite material, and among other things protects from UV radiation. Is mainly used in boat building, but also in some velomobiles. When laminating, the gel coat is first painted into the negative form; then the fiber fabric and epoxy resin is laid up over it. If no gel coat is used, the finished velomobile must be painted or filmed.

    Straight running
        Straight running is positively influenced by a large wheelbase, neutral steering, low sensitivity to wind, and (especially with a Einspurer) from a large wake.

    Boundary layer
        area of a flowing fluid near a surface at which its speed is relative to the surface growing from zero to the flow rate of the remaining fluid. This can be laminar (i.e. there is a speed within the current gradient) or turbulent (i.e. there are small vortices along the surface that are seperate from the laminar flow).

    Hood
        The cover of the entrance hatch, usually with side windows and at the front a hinged motorcycle visor, is called hood.

    Pants
        An aerodynamic cover for open wheel wells, See What are pants?,

    HPV
        Human Powered Vehicle, so accordingly muscle powered vehicle. Also the name of the German association, HPV Germany eV; the international umbrella organization is called IHPV.

    Hump
        Rise in the rim bed of some tubeless rims, which ensures that the tire remains close to the outside rim flange and does not slip inside. To do this, you usually have to over-inflate the tire a bit until the tire slides over the hump. You can retrofit a hump yourself by putting a cord on the rim bed parallel to the rim flange and covering it with tubeless rim tape.

    Scoop
        The rear upper part of a velomobile that starts behind the head and runs in an arc to the tail. It serves as an aerodynamic continuation of the head and prevents turbulence there, similar to an oversized aero helmet. At the same time it is the highest point of the velomobile and because of the strong curvature it is very stiff, so it also serves as a roll bar in an accident.

    IGUS
        A company that produces plastic plain bearings. Since several ball heads are installed in the steering linkage of a velomobile, you can save a lot of weight with plastic versions.

    Ingmarize chain
        Chain care by not caring for the chain. Since wear is mainly caused by dirt and this adheres particularly well to oil, an oil-free chain has a certain justification if the chain is permanently exposed to water and dirt, i.e. you would have to re-oil and the chain is effectively water-lubricated - and the bike too is used frequently so that the chain does not rust. This is not a good idea in a velomobile because the chain is exposed to almost no dirt and water, and a clean chain has the least friction if it is sufficiently oiled.

    Kamm tail
        A vehicle rear that is not completely tapered, but is cut off sharply. The sharp edge ensures that the flow is not diverted inwards at the back and swirled there, but abruptly breaks off. Although it is slightly more inefficient than a long tail, the losses due to the greater turbulence are partially compensated for by the smaller surface area and the lower weight. Named after Wunibald Kamm, hence the Kamm-tail.

    Capacity
        Length that a chain tensioner can shorten the chain. Chain tensioners with long cages, as are common in the mountain bike sector, have a capacity of around 40 chain links. In order to be able to shift all gears, the capacity must be equal to the difference between the sum of the number of teeth of the largest chainring and the largest sprocket and the sum of the number of teeth of the smallest chainring and the smallest sprocket. If the capacity is not sufficient, you should still be able to switch from large to large, as otherwise a chain that is too tight can cause damage. With small - small, the chain then sags; but this is less problematic.

    Carcass
        A fabric that is embedded in the tire and absorbs the tensile forces due to the tire pressure and  deformation. The rubber only ensures the airtightness and grip of the tire on the road; therefore, damage to the carcass (e.g. due to a deep cut) often leads to a dent in the tire or causes the tire to burst immediately. To temporarily repair such a flat, patching only is not enough, but you need a piece of solid material that distributes the pressure of the tube to the surrounding enveloppe for example tape.

    Chainring
        Chainrings are the gears driven by the crank. In order for them to fit a certain crankset, the bolt circle must match.

    Chain line
        A bicycle chain has a certain amount of lateral mobility, but should preferably not run permanently at an angle, as this increases the pedal resistance and wear. However, the deflection rollers on velomobiles are far enough away from the chainring or sprocket that the skew is limited in all gears. In addition, the deflection rollers on some velomobiles can be moved laterally.

    Clothes hanger
        A component in Velomobiel.nl velomobiles, eg the Quest or Strada.

    Clipless pedal
       A pedal on which special shoes (click shoes) snap into place with a metal plate (cleat) and thus attaches the foot solidly. This allows more efficient power transmission because you cannot slide off the pedal. There are several different systems; those in the racing bike area usually have large triangular cleats that are attached with three screws. In the mountain bike area, on the other hand, the SPD system dominates, the smaller cleats of which are fastened with two screws and sunk into the profile of the sole, so that you can walk quite well with them. In addition, Speedplay is also worth mentioning, for example, which offers a particularly large turning range.

    Click shoes
        Shoes for click pedals, See also: Do you need click shoes?

    Compact crankset
       A crankset with two chainrings. Since the bolt circle is only 110 mm, a fairly small chainring can be installed - you can achieve a similarly large transmission range with just two chainrings as with three chainrings.

    konschen
        Hang upside down in the velomobile (so that only the legs are sticking out) to adjust something at the front, for example on the gear shift. However, many velomobiles now have a maintenance hatch or a removable front, so that should be largely unnecessary.

    Trailing arm
       A rod that holds the lower end of the strut and typically extends from the front end of the wheel well to the steering knuckle. The farther inside the wheel well and the further to the front of the trailing arm is attached to the steering knuckle, the further out the directions of trailing arms and wishbones intersect, ie the more negative the steering scrub becomes . See: What are the names of all the parts on the chassis?

    Empty strand
        The part of the chain that runs from the sprocket back to the chainring and does not transmit any tractive force ( i.e. is not tensioned) - in contrast to the traction strand.

    Steering bridge
        Cover between the front wheel arches under which the steering linkage is located (i.e. tie rod and wishbone). Since the bridge is quite voluminous and therefore stiff, the bottom bracket mast or the front pulley is often attached there.

    Steering knuckle
        A fastening at the lower end of the shock absorber, to which the trailing arm, wishbone arm, tie rod and, in the case of tank steering, the steering levers are attached. See: What are the names of all the parts on the chassis?

    Scrub Radius
        The radius by which the wheel turns around the pivot point of the steering when steering. See What is the scrub radius? and where is the steering axis on a MacPherson strut? ,

    PCD Bolt circle
       Diameter at which the fixing screws of the chainrings are located. Common standards are 130 mm, 110 mm (compact crank) and 104 mm (Mountain bike).

    Air resistance (drag)
        Force that has to be used to displace the air. The formula is :math:`F_\text{air} = 1/2 \times c_W \times  A \times \rho \times v^2` that is, the cross-sectional area A is linear and the speed v is squared. The factor :math:`1/2 \times \rho \times v^2` corresponds to the dynamic pressure, which depends on the speed :math:`v` (square) and air density :math:`\rho` (linear).

    Multi-track
        A vehicle with multiple wheels that do not run in a row. Such a vehicle cannot lean into the curve (except for special leaning vehicles), nor does it fall over when it is stationary or a wheel slips.

    Meufl
        An abbreviation for Central European Bicycle Laboratory ; this is how Harald Winkler describes his bicycle projects. But he became known in the 1990s for his bicycle covers made of closed-cell foam, which were aerodynamic and very light. Correspondingly, devilish mostly means that a component is made of foam.

    NACA Duct
        See What is a NACA Duct?

    Caster
        The distance between the track point and the contact point of a steered wheel. If this is positive, i.e. the wheel contact point is behind the track point, the wheel follows the direction of the frame - or from the point of view of the frame, the steered wheel automatically returns to the straight-ahead position. You can achieve a positive caster by placing the steering bearing in front of the wheel, or if this is not the case, tilt the steering axle so that its extension points in front of the wheel. See: What is caster and what is its function?

    Tank steering
        Steering on tricycles, where two steering levers steer the wheels on the left and right. The steering levers are connected to the steering knuckles, the tie rod only serves to couple the two wheels and thus the steering levers.

    Pinlock
        Pane that is attached to the inside of a motorcycle visor and ensures airtight double glazing. This serves as thermal insulation and prevents fogging. See: What do you do against fogged windows? and How does a velomobile drive in winter?

    Wishbone
        A rod that holds the lower end of the strut and is typically attached in the middle of the velomobile, below the steering bridge . The longer the wishbone is, the less its angle changes when the shock absorber is deflected, and the effect of bump steer is correspondingly less . See: What are the names of all the parts on the chassis?

    Wheel well
        Housing in which the wheels are located. These serve as aerodynamic fairing, but also to protect against dirt (like fenders); and since the wheel wells are often an integral part of the body, they also have a supporting function; For example, the struts are screwed to the wheel wells at the top of the front wheels .

    Wheel disks,
        Discs that are attached to spoke wheels to ensure a smooth surface and thus reduced air resistance. The discs are usually glued to the rims; if they are made of fabric, also clipped onto the spokes.

    Tire contact patch
        The area of the tire that is flattened by the load on the road. The size of the patch depends on the load and the air pressure; since pressure times area equals pressure force, the area is so large that the pressure force equals the load - with higher pressure the tire patch can be reduced accordingly. The shape depends on the tire width; with a wide tire the tire contact is short and wide, with a narrow tire narrow and long.

    Crack damper
        A rear wheel damper for certain velomobiles. Offers better damping behavior, but has a reputation for being more sensitive.

    Sprocket
        The sprocket is the chain-driven gears which are on the freewheel of the drive wheel. It is usually a Shimano freewheel body with grooves, on which the sprockets slide either individually or riveted together on a common sprocket carrier ("spider"). The group of sprockets is called a cassette.

    Rolling resistance
        force that must be applied to a Roll the wheel over a rough surface. (insert formula)

    The icing on the cake
        is what a hood is called for the Quest velomobile.

    Rear derailleur
        The rear derailleur is the derailleur system for the pinions, typically on the rear wheel. It is usually shifted by moving the derailleur cage with the pulley wheels; this also serves as a chain tensioner - the longer the cage, the greater the capacity of the rear derailleur. In some velomobiles, however, these tasks are separate; there the rear derailleur pushes the chain to the side using two plates, and the chain tensioner is located separately at another point.

    Foam lid
        With some velomobiles (especially from the Dutch manufacturers ), the entry opening can not only be closed with a hood, but alternatively with a flexible lid made of a foam. This is attached from the inside and offers an opening so that the head can protrude outside, but the rest of the body is largely protected from the wind and rain. The remaining opening can also be closed for parking.

    Apparent wind
        Vector sum of the wind ("true wind") and the head wind - if, for example, the true wind and head wind are of the same size and the true wind comes from the side at right angles, then the apparent wind comes at an angle of 45 ° from the front and is according to the theorem of Pythagoras 41% stronger than true wind and head wind.

    Center of gravity 
        Average location of the mass of a body. For many calculations, it can be assumed that the entire mass of a body is gathered in the center of gravity and all forces attack there.

    Snakebite
        Loss of air in the tire through two holes in the tube that looks like a snake's bite. The cause is a puncture; The tire is pressed in so much that the tube is crushed between the tire and the rim flange, which leads to the two holes. With higher air pressure and / or wider tires, the susceptibility to snakebites can be reduced.

    Track point
        Intersection of the steering axis extended to the street

    Tie-rod
       Crossbar of the vehicle that connects the two front wheels and transmits the steering movements. With tiller steering, the steering movement is transmitted to the front wheels via the tie rod, while in the case of tank steering, the steering levers act directly on the steering knuckles and the tie rod merely couples the steering movements of both wheels. A tie rod can be continuous or split; in the latter case, it moves similar to the wishbone when deflected, which reduces the bump steer. See: What are the names of all the parts on the chassis?

    Stagnation point
        The point in the front of a vehicle where the air does not come in at an angle and is deflected sideways, but hits the surface perpendicularly and builds up. The kinetic energy of the air results in an increase in pressure. Here, a ventilation opening is a good idea because, due to the increased pressure, a small opening is sufficient for a large air throughput. In addition, there is no laminar flow here that could be disturbed by the opening.

    Stormstrip
        A tape in the longitudinal direction of the vehicle which hinders the overflow in the transverse direction and thus reduces the susceptibility to cross winds. See: How can I reduce cross wind sensitivity? ,

    Stall
        Air flow is either laminar to a surface and follows its course, or detaches and swirls. The transition is relatively abrupt, hence the name.

    Camber
        Inclination of the wheel so that it is not perpendicular to the road, but tipped outwards ("positive camber") or inwards ("negative camber"). See: Why are the front wheels at an angle? Doesn't that slow you down?

    Tadpole
        tricycle with one wheel at the back and two at the front. Named after the tadpole, since that Tricycle is large at the front and thin at the back.

    Bottom bracket mast
        Mast that runs from the steering bridge to the front and is often attached to the front of the velomobile at the funnel , and carries the bottom bracket with the cranks and pedals. So that this can be adjusted, either the mast itself is adjustable (e.g. extendable and swiveling), or the bottom bracket is slidably clamped on the mast. When the mast is attached at the front, the fresh air is usually also directed through the mast to the rear.

    Funnel
        Component at the front at the top into which the bottom bracket mast is glued. Since the funnel is also at the stagnation point , it directs the fresh air inside through the mast.

    Tiller steering
       Steering on tricycles, in which a central steering rod moves the tie rod via a universal joint. The English name originally means boat tiller .

    Tubeless
        Normally, a tire only ensures grip on the road and absorbs the forces created through its carcass ; the airtightness is guaranteed by a tube inside. In the case of a tubeless wheel, the tube is dispensed with because the tire is made airtight and also sits particularly firmly on the rim. This has a flat rim base and often also humps to fix the tire better; In addition, a special rim tape seals the rim bed airtight. The valve is screwed directly into the rim. Usually, there is also a sealing milk placed in the tire that seals small leaks. Eliminating the tube reduces rolling resistance and increases driving comfort; also lower pressures are possible because no Snakebite may occur and small holes are often sealed by the sealing milk. But repairing large holes is more complex because the tire sits more firmly on the rim.

    Versatile roof
        An airy roof, not as closed as a hood , but not as aerodynamic either.

    Water slide
        component of the Alpha7 Velomobile that supports the bottom bracket at the rear end.

    Saddle
        Step by step, when you get out of the saddle and move your body back and forth relative to the bike. Has the advantage that the pedaling movement is partially decoupled from the crank rotation - with the legs you not only press the pedal down, but also the body upwards, and can thus partially compensate for an inappropriate gear and provide more variety when pedaling. Unfortunately, this is not possible on recumbent bikes because you step at right angles to gravity and therefore cannot use weight to temporarily store pedaling energy.

    Tension strand
        The part of the chain that runs from the chainring to the sprocket and is tensioned because it transmits the driving force - in contrast to the empty strand.


.. vim: ft=rst wrap tabstop=4 :
